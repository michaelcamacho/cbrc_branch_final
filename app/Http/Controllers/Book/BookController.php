<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Branch;
use App\Program;
use App\Model\DataModel\BooksInventory;
use App\Model\DataModel\BooksStockMovementRecord;
use App\Model\DataModel\BooksOrder;
use App\Model\DataModel\BooksOrderList;


class BookController extends Controller
{
    public function books(){
        $Branches = Branch::all(); 
        $program =  program::all();
        $books = BooksInventory::all();

        return view('admin.books')
        ->with('books' , $books)
        ->with('program' , $program)
        ->with('Branches' , $Branches);
    }


    public function add_book(Request $request){
        $date=date('M-d-Y');
        $input = $request->except(['_token']);
    
    

        BooksInventory::insert([
            "branch_id" => request("branch"),
            "program_id" => request("program"), 
            "book_id" =>  request("book_title"),
            "price" => request("remarks"),
        ]);



        return redirect("/admin/books");
    }
}
