<?php

namespace App\Http\Controllers\Discount;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Category;
use App\Branch;
use App\CbrcStudent;
use App\StudentEnrollmentCard;
use App\Model\AdminSettings;
use App\PaymentInfo;
use App\Program;
use App\Reservationfee;
use App\Model\DataModel\Tuition;
use App\Model\DataModel\Discount;


class DiscountDataFetchController extends Controller
{
   
  public function fetch_discount_data($discount_id){
                
    
    $discount_data = Discount::leftJoin('categories','discounts.category_id','=','categories.id')
    ->leftJoin('programs','discounts.program_id','=','programs.id')
    ->leftJoin('branches','discounts.branch_id','=','branches.id')
    ->select('discounts.*','categories.category_name As category_name' ,'branches.branch_name As branch_name' , 'programs.program_name As program_name' )
    ->where('discounts.id','=',$discount_id)->first();

    
     return response()->json($discount_data);
  }



   
    public function fetch_discount( $program , $branch){
        $branch_id = Branch::where('branch_name', '=' , $branch)->first();
    
        $fetch_discount = Discount::join('categories','discounts.category_id','=','categories.id')
        ->join('programs','discounts.program_id','=','programs.id')
        ->select('discounts.*','categories.category_name As category_name' , 'programs.program_name As program_name' )
        ->where('discounts.branch_id','=',$branch_id->id)
        ->where('discounts.program_id','=',$program)
        ->get();

        
         return response()->json($fetch_discount);
      }



    
}
