<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Alert;
use Auth;
use App\Branch;
use App\Model\ExpenseSetup;
use App\Model\DataModel\Remit;

class RemitController extends Controller
{
    public function proceed_insert_remit(Request $request){
        $input = $request->except(['_token']);

        $settings = ExpenseSetup::where('branch','=', $input['branch'] )->first();
        $branch_id = Branch::where('branch_name', '=' , $input['branch'] )->first();
        
      

        Remit::insert([
            "branch_id" => $branch_id->id,
            "date" => $request['date'],
            "category" => $input['category'],
            "season" => $input['season'],
            "year" => $input['year'],
            "amount" => $input['amount'],
            "remarks" => $input['remarks'],
            "author" => Auth::user()->name
        ]);
       
        Alert::success('Success!', 'Cash Remit Saved.');
       
        return redirect('/new_cash_remit/'.$input['branch']);

    }
}
