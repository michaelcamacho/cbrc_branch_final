@extends('main')
@section('title')
NEW PAYMENT
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
<link href="{{ asset('../../css/select.css') }}" rel="stylesheet" />
<link href="{{ asset('../../css/print-payment.css') }}" rel="stylesheet" media="print" type="text/css">
@stop
@section('main-content')


@if ($flash = session('log'))
<input type="hidden" value="{{$flash}}" id="logstat" />
<input type="hidden" value="" id="ip" />
<input type="hidden" value="" id="branch" />
<input type="hidden" value="" id="user" />
<input type="hidden" value="" id="user_id" />
@endif

<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Enrollee's Payment</h6>
                        
                    </div>
                  
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <div class="row pull-right">
                                        <h5> <span class="card_status"></span></h5>
                                    </div>    

                                   <br>
                                   <br>
                                   <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form">
                                        @csrf
                                        
                                   <input type="hidden" name="hidden_branch" id="hidden_branch" value="{{$branch}}">            

                                        <div class="row">

                                            <div class="col-lg-4">
                                                        <div class="form-group">
                                                            <select class="form-control" name="program" id="program">
                                                                <option value="0" disable="true" selected="true" class="selector3">--- Select Program ---</option>
                                                                @if(isset($program))
                                                                @foreach($program as $programs)
                                                                <option value="{{$programs->id}}">{{$programs->program_name}}</option>
                                                                @endforeach
                                                                @else
                                                                <option value="0" disable="true" selected="true" class="selector3">No Programs</option>
                                                                @endif
                                                            </select>
                                                            <label>Program</label>
                                                        </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" name="date" id="date" readonly>
                                                    <label>Date Today</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <label >Balance</label>
                                                <div class="form-group balance">
                                                    <input class="form-control balance" type="number" value="" id="balance" name="balance" readonly> 
                                                </div>
                                            </div>

                                            <div class="col-lg-2">
                                                <label>Reservation Fee</label>
                                                <div class="form-group reservation_fee">
                                                    <input class="form-control balance" type="text" id="reservation_fee" value="" name="reservation_fee" readonly>
                                                </div>
                                            </div>
                                                    

                                            <div class="col-lg-2">
                                                <label>Date Registered</label>
                                                <div class="form-group date_registered">
                                                        <input class="form-control " type="text" id="date_registered" value="" readonly>
                                                </div>
                                            </div>
                                                
                                           
                                        </div>
                                        <div class="row">
                                            
                                                <div class="col-lg-6">
                                                        <div class="form-group input-has-value">
                                                           <select class="form-control js-example-basic-single" name="name" id="student" required>
                                                                <option value="" disable="true" selected="true" class='selector'>Select program first.</option>
                                                            </select>
                                                            <label>Student Name</label>
                                                        </div>
                                                    </div>

                                            


                                                <div class="col-lg-2">
                                                    <label>Tuition Fee</label> 
                                                    <div class="form-group val">
                                                        <input class="form-control" name="tuition_fee" type="number" id="tuition_fee" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label>Facilitation Fee</label>
                                                    <div class="form-group val">
                                                        <input class="form-control" name="facilitation" type="number" id="facilitation" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label>Discount</label>
                                                    <div class="form-group val">
                                                        <input class="form-control" value='0' name="discount_amount" type="number" id="discount_amount" readonly>
                                                    </div>
                                                </div>
                                                                
                                            </div>
                                               


                                        
                                         <div class="row">
                                                <div class="col-lg-6">
                                                        <div class="form-group">
                                                             <select class="form-control" name="student_category" type="text" id="student_category" >
                                                                <option value="0" disable="true"  class="cat" selected>--- Select Category ---</option>
                                                               
                                                                    @foreach($categories as $category)
                                                                        <option value="{{$category->id}}" class="{{$category->tuition_fee}}">{{$category->category_name}}</option>
                                                                    @endforeach
                                                            </select>
                                                            <label>Student Category</label>
                                                        </div>
                                                    </div>
                                                   
                                                
                                                    <div class="col-lg-4">
                                                        <label>Total Amount</label>
                                                            <div class="form-group total">
                                                                <input class="form-control readonly" name="total_amount" id="total_amount"  type="text" readonly required>
                                                            </div>
                                                        </div>
            
                                                        <div class="col-lg-2">
                                                            <div class="form-group val">
                                                                <button class="btn btn-primary" type="button" id="compute">Compute</button>
                                                            </div>
                                                        </div>
                                        </div>
                                        
                                         <div class="row">
                                                <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <select class="form-control" name="discount" id="discount" >
                                                                <option value="0" disable="true" selected="true">Select category first.</option>
                                                            </select>
                                                            <label>Discount</label>
                                                        </div>
                                                    </div>
                                          
                                           
                                            <div class="col-lg-4">
                                                <label>Amount Paid</label>
                                                <div class="form-group dis">
                                                    <input class="form-control decimal" name="amount_paid" id="amount_paid"  type="text" maxlength="12" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                    <div class="form-actions btn-list">
                                                            <input type="submit" name="submit" onclick="proceedpayment()" value="proceed" id="submit" class="btn btn-primary print main">
                                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                                        </div>
                                            </div>
                                           
                                        </div>
                                       
                                       
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>

            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>
<script src="{{asset('/js/newpayment.js')}}"></script>

<script>

   
        
      
@stop