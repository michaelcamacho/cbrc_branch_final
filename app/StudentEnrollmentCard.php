<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentEnrollmentCard extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'student_enrollment_card';
    protected $fillable = [

        'id',
        'date_registered',
        'student_id',
        'category_id',
        'program_id',
        'major_id',
        'tuition_fee_price',
        'discount_category_id',
        'discount',
        'facilitation_price',
        'balance',
        'season',
        'year',
        'status',
        'remark',
        'created_by_id',
        'deleted',
    ];

}
