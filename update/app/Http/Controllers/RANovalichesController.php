<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Model\Novaliches\NovalichesAgri;
use App\Model\Novaliches\NovalichesBookCash;
use App\Model\Novaliches\NovalichesBooksInventorie;
use App\Model\Novaliches\NovalichesBooksSale;
use App\Model\Novaliches\NovalichesBudget;
use App\Model\Novaliches\NovalichesCivil;
use App\Model\Novaliches\NovalichesCrim;
use App\Model\Novaliches\NovalichesDiscount;
use App\Model\Novaliches\NovalichesDropped;
use App\Model\Novaliches\NovalichesExpense;
use App\Model\Novaliches\NovalichesIelt;
use App\Model\Novaliches\NovalichesLet;
use App\Model\Novaliches\NovalichesMid;
use App\Model\Novaliches\NovalichesNclex;
use App\Model\Novaliches\NovalichesNle;
use App\Model\Novaliches\NovalichesOnline;
use App\Model\Novaliches\NovalichesPsyc;
use App\Model\Novaliches\NovalichesReceivable;
use App\Model\Novaliches\NovalichesS1Sale;
use App\Model\Novaliches\NovalichesS2Sale;
use App\Model\Novaliches\NovalichesS1Cash;
use App\Model\Novaliches\NovalichesS2Cash;
use App\Model\Novaliches\NovalichesScholar;
use App\Model\Novaliches\NovalichesSocial;
use App\Model\Novaliches\NovalichesTuition;
use App\Model\Novaliches\NovalichesPettyCash;
use App\Model\Novaliches\NovalichesRemit;
use App\Model\Novaliches\NovalichesReservation;

use App\Model\Novaliches\NovalichesLecturerAEvaluation;
use App\Model\Novaliches\NovalichesLecturerBEvaluation;
use App\Model\Novaliches\NovalichesComment;

use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use Auth;
use User;
use DB;

class RANovalichesController extends Controller
{
   private $branch = "Novaliches";

   private $sbranch = "novaliches";

    public function __construct()
    {
         $this->middleware('role:ra|admin'); 
    }

    public function index(){

    	
    }
}
