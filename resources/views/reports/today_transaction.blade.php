@extends('main')
@section('title')
Today Transaction Report
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;    
}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')

<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} today transaction reports</h6>
                    
                </div>
            </div>
        </div>

        <div class="widget-holder">    

            {{-- transaction reports --}}
            <div class="col-md-12 widget-holder">
                    <div class="widget-bg">

                            <ul class="nav nav-tabs">
                                  @include('reports.nav_transaction')
                                   
                            </ul>
                                 
                                
                            
                </div>
            </div>
            {{-- transaction reports --}}
    
        </div>

       
                     
    </main>
    <!-- end of main -->
@stop

@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
  $('.reports').addClass('active');
  $('.reports').addClass('collapse in');
});


</script>



@stop