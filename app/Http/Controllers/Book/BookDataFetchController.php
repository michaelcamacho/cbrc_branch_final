<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Branch;
use App\Program;
use App\Model\DataModel\BooksInventory;
use App\Model\DataModel\BooksStockMovementRecord;
use App\Model\DataModel\BooksOrder;
use App\Model\DataModel\BooksOrderList;


use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class BookDataFetchController extends Controller
{
    public function fetch_book_data($book_id){
        $chosen_one=''; 



        //login to api
       $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
       $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
           'form_params' => [
               "email"=>"admin@main.cbrc.solutions",
               "password"=>"main@dmin"
           ]
       ]);
       //login to api


       if ($res->getStatusCode() == 200) { // 200 OK
            $response_data = json_decode($res->getBody()->getContents());    
                $book_data = $client->request('GET', 'https://cbrc.solutions/api/main/books?token='.$response_data->access_token);
                    $book_data = json_decode($book_data->getBody()->getContents());
        //extract the book title
        foreach($book_data as $items){
            foreach($items as $item){

                if($item->Book_ID == $book_id){
                    $chosen_one = $item->Title;
                }
            }
        }
    }//end of api


        $book_data = BooksInventory::leftJoin('programs','books_inventories.program_id','=','programs.id')
        ->leftJoin('branches','books_inventories.book_id','=','branches.id')
        ->select('books_inventories.*','branches.branch_name  as branches_name','programs.program_name  as program_name' )
        ->where('books_inventories.id','=', $book_id)->first();
    
        
         return response()->json(
         [
            "book_title," => $chosen_one,
            "book_data" => $book_data
         ]);
      }
    
}
