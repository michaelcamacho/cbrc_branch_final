

$('#program').on('change', function(e){
            
  $('#balance').val(0);
  $('#amount_paid').val();
  $('#discount_amount').val(0);
  $('#tuition_fee').val(0);
  $('#facilitation').val(0);
  $('#reservation_fee').val(0);
  $('.card_status').text('');      



 var program = e.target.value;
 var branch = $('#hidden_branch').val();
     console.log(program);


     //fetch student in program
 $.get('/fetch-student/' + program + '/' + branch,function(data){
     console.log(data);

      $('#student').empty();
      $('#student').append('<option value="" disable="true" selected="true">--- Select Student ---</option>');

     $('#amount').val('');

       $.each(data, function(index, studentObj){
         $('#student').append('<option value="'+ studentObj.id +'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
     });

 });

 
       $.get('/data/fetch-discount/'+ program +'/' + branch,function(data){
           console.log(data);

           $('#discount').empty();
           $('#discount').append('<option value="" disable="true" selected="true">--- Select Discount ---</option>');

           $('#amount').val('');

             $.each(data, function(index, discount){
               $('#discount').append('<option value="'+ discount.id +'">'+ discount.discount_category+'</option>');
           });

       });

       
 
});

 











///studemt

$('#student').on('change', function(e){

 var branch = $('#hidden_branch').val();
 var student_id = e.target.value;


 console.log(student_id);
 let program = $('#program').val();
  $('#balance').val(0);
  $('#amount_paid').val();
  $('#discount_amount').val(0);
  $('#discount').val(0);
  $('#student_category').val(0);
  $('#tuition_fee').val(0);
  $('#facilitation').val(0);
  $('#reservation_fee').val(0);
  $('#compute').prop('disabled' , false);
  $('#submit').prop('disabled' , false);

  


 $.get('/fetch_check_student_status/' + student_id + '/' + branch,function(data){
 
        //option 1 means for enrollment 
          if(data.option == 1){
            console.log("1");
            console.log(data.student.major);
              $('#form').prop('action', '/new_payment/proceed/make_transact_enroll');
              $('#student_category').prop('disabled' , false);
              $('#discount').prop('disabled' , false);      
              $('#hidden_student_major').val();      
              $('.card_status').text('Status:Student Not Enrolled');      
          }
                    
         

              //option 2 means for new payment student status is enrolled
              if(data.option == 2){

                console.log("2");
                  $('#form').prop('action', '/new_payment/proceed/make_transact_add_payment');
                  $('#student_category').prop('disabled' , 'disabled');
                  $('#discount').prop('disabled' , 'disabled');
                  $('#balance').val(data.latest_card.balance);    
                  $('.card_status').text('Status:Student Enrolled');      

               }   


              //option 3 means for enrollment student status is reserved
              if(data.option == 3){
                console.log("3");
                
            console.log(data);
                $('#balance').val(data.latest_card.balance);
                $('#date_registered').val(data.latest_card.date_registered);
                // $('#hidden_student_major').val(data.student.major);      
                
                if(data.latest_card.program_id === 0){
                  
                  console.log("payment no 1");
                  $('#form').prop('action', '/new_payment/proceed/make_transact_enroll_reserve');
                  $('#reservation_fee').val(data.firstPayment.amount_paid);
                  $('#student_category').prop('disabled' , false);
                  $('#discount').prop('disabled' , false);
                  $('.card_status').text('Status:Student is Reserved');      
                  
                }else{
                  console.log("payment no 0");
                    $('#form').prop('action', '/new_payment/proceed/make_transact_add_payment');
                    $('#reservation_fee').val(0);
                    $('#student_category').prop('disabled' , 'disabled');
                    $('#discount').prop('disabled' , 'disabled');
                    $('.card_status').text('Status:Student is Reserved');      

                  }
              } 

              //option 4 means student fully paid
              if(data.option == 4){
                console.log("4");   
                $('#student_category').prop('disabled' , 'disabled');
                $('#discount').prop('disabled' , 'disabled');
                $('#compute').prop('disabled' , 'disabled');
                $('#submit').prop('disabled' , 'disabled');
                $('.card_status').text('Status:Student Fully Paid for this season');      

              }

});
});





//tuition fee
$('#student_category').on('change', function(e){
 var program = $('#program').val();
 var branch = $('#hidden_branch').val();
 var major_id = $('#hidden_major_id').val();
 var category_id = e.target.value;
if(program == 1){
  $.get('/fetch_tuition_by_category_let/' + category_id +'/'+ program +'/'+major_id +'/' + branch,function(data){
    console.log(data);
    
  $('#tuition_fee').val(data.tuition_fee);
  
  $('#facilitation').val(data.facilitation_fee);
  });
  

}else{
 $.get('/fetch_tuition_by_category/' + category_id +'/'+ program + '/' + branch,function(data){
   console.log(data);
$('#tuition_fee').val(data.tuition_fee);
$('#facilitation').val(data.facilitation_fee);
 });
}


});





$('#discount').on('change', function(e){

 var branch = $('#hidden_branch').val();
 var discount_id = e.target.value;

if(discount_id == 0){
  $('#discount_amount').val(0);
 
}else{
  $.get('/fetch_discount_by_discount/' + discount_id + '/' + branch ,function(data){
    console.log(data);
 $('#discount_amount').val(data.discount_amount);
 
 });


}


$('#total_amount').val(0  );

});



$('#program').on('change', function(e){
           
 var branch = $('#hidden_branch').val();
 var program = e.target.value;

     //fetch student in program
 $.get('/data/fetch-discount/'+ program +'/'+ branch,function(data){
     console.log(data);

      $('#discount').empty();
      $('#discount').append('<option value="0" disable="true" selected="true">--- Select Discount ---</option>');

     $('#amount').val('');

       $.each(data, function(index, discount){
         $('#discount').append('<option value="'+ discount.id +'">'+ discount.discount_category+'</option>');
     });

 });


});


//compute 

$('#compute').on('click',function(){

 let balance = $('#balance').val();
 let amount_paid = $('#amount_paid').val();
 let discount_amount = $('#discount_amount').val();
 let tuition_fee = $('#tuition_fee').val();
 let facilitation = $('#facilitation').val();
 let reservation_fee = $('#reservation_fee').val();
 let result = 0;
 let bill = 0;


 if (Number(discount_amount) == NaN || Number(discount_amount) == null ) {
   $('#discount_amount').val('0');
 }

 if (Number(balance) == 0 || balance == NaN || balance == null) {
   bill = (Number(facilitation) + Number(tuition_fee)) - Number(discount_amount);
 }
 if(Number(reservation_fee) > 0 || Number(reservation_fee) != NaN) {

   bill = (Number(facilitation) + Number(tuition_fee)) - Number(discount_amount) - Number(reservation_fee);
 }
 if (Number(balance) != 0) {
   bill = balance;
 }
 else{

 }

 // if (balance < amount_paid) {
   
   
 //   alert('enter valid amount');  
 //   $('#total_amount').val('0');

 //   return;
 // }

 // result = balance - amount_paid
$('#total_amount').val(bill);

});

const proceedpayment = ()=>{

 let total_amount = $('#total_amount').val();
 let amount_paid = $('#amount_paid').val();

 if (Number(total_amount) < Number(amount_paid)) {
   alert('Please Enter valid Amount Equal or Less than the total Amount');
   return;
 }else{

   $('#payment_form').submit();

 }
}