<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Category;
use App\Branch;
use App\CbrcStudent;
use App\StudentEnrollmentCard;
use App\Model\AdminSettings;
use App\PaymentInfo;
use App\Program;
use App\Major;
use App\Reservationfee;
use App\Model\DataModel\Tuition;
use App\Model\DataModel\Discount;

class StudentViewController extends Controller
{
    

    private $year;
    private $Branches;

    public function __construct()
    {
            $this->middleware('auth');

            $settings = AdminSettings::findOrFail(1);
            $this->year = $settings->year;
     
            $main_id = Branch::select('main_branchid')->get();
            $this->Branches = Branch::whereNotIn('id', $main_id)->get();
    }

    
    
        public function enrolled_list($branch){
            $row_counter=1;   
            
            $season_year = AdminSettings::findorfail(1);


            $enrollmentCard = DB::connection('satelite_branch_db_connection')->table('student_enrollment_card')
            ->leftJoin('cbrc_students','cbrc_students.id','=','student_enrollment_card.student_id')
            ->leftJoin('majors','majors.id','=','student_enrollment_card.major_id')
            ->leftJoin('programs','programs.id','=','student_enrollment_card.program_id')
            ->leftJoin('card_status','card_status.id','=','student_enrollment_card.status')
            ->leftJoin('categories','categories.id','=','student_enrollment_card.category_id')
            ->leftJoin('discounts','discounts.id','=','student_enrollment_card.discount_category_id')
            ->select('student_enrollment_card.*','cbrc_students.*','discounts.discount_category AS discount_category','programs.program_name AS program_name','majors.major AS major_name','card_status.status As status_name','categories.category_name As category_name' )
            ->where('student_enrollment_card.status' , '=' , 2)
            ->where('student_enrollment_card.season' , '=' , $season_year->season)
            ->where('student_enrollment_card.year' , '=' , $season_year->year)
            ->orderBy('student_enrollment_card.id' , 'desc')
            ->get();
 
         
        return view ('student.enrolled_list')
        ->with('row_counter',$row_counter)
        
        ->with('Branches',$this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('enrollmentCard',$enrollmentCard)
        ->with('branch',$branch);
        }


        public function scholar_list($branch){
            $row_counter=1;   
            
            
            $season_year = AdminSettings::where('branch', '=' , Auth::user()->branch)->first();


            $enrollmentCard = DB::connection('satelite_branch_db_connection')->table('student_enrollment_card')
            ->leftJoin('cbrc_students','cbrc_students.id','=','student_enrollment_card.student_id')
            ->leftJoin('majors','majors.id','=','student_enrollment_card.major_id')
            ->leftJoin('programs','programs.id','=','student_enrollment_card.program_id')
            ->leftJoin('card_status','card_status.id','=','student_enrollment_card.status')
            ->leftJoin('categories','categories.id','=','student_enrollment_card.category_id')
            ->leftJoin('discounts','discounts.id','=','student_enrollment_card.discount_category_id')
            ->select('student_enrollment_card.*','cbrc_students.*','discounts.discount_category AS discount_category','programs.program_name AS program_name','majors.major AS major_name','card_status.status As status_name','categories.category_name As category_name' )
            ->where('student_enrollment_card.category_id' , '=' , 3)
            ->where('student_enrollment_card.category_id' , '!=' , 3)
            ->where('student_enrollment_card.season' , '=' , $season_year->season)
            ->where('student_enrollment_card.year' , '=' , $season_year->year)
            ->orderBy('student_enrollment_card.id' , 'desc')
            ->get(); 
 
        return view ('student.scholar_list')
        ->with('row_counter',$row_counter)
        
        ->with('Branches',$this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('enrollmentCard',$enrollmentCard)
        ->with('branch',$branch);
        }


        public function reserved_list($branch){
            $row_counter=1;   
            
            $enrollmentCard = DB::connection('satelite_branch_db_connection')->table('student_enrollment_card')
            ->leftJoin('cbrc_students','cbrc_students.id','=','student_enrollment_card.student_id')
            ->leftJoin('majors','majors.id','=','student_enrollment_card.major_id')
            ->leftJoin('programs','programs.id','=','student_enrollment_card.program_id')
            ->leftJoin('card_status','card_status.id','=','student_enrollment_card.status')
            ->leftJoin('categories','categories.id','=','student_enrollment_card.category_id')
            ->leftJoin('discounts','discounts.id','=','student_enrollment_card.discount_category_id')
            ->select('student_enrollment_card.*','cbrc_students.*','discounts.discount_category AS discount_category','programs.program_name AS program_name','majors.major AS major_name','card_status.status As status_name','categories.category_name As category_name' )
            ->where('student_enrollment_card.status' , '=' , 2)
            ->orderBy('student_enrollment_card.id' , 'desc')
            ->get();
 
        return view ('student.reserved_list')
        
        ->with('Branches',$this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('row_counter',$row_counter)
        ->with('enrollmentCard',$enrollmentCard)
        ->with('branch',$branch);
        }



        public function student_summary($id){
            
            $balance=0;
            $show_enroll_reserve_button = "show";

            $enrollmentCard = DB::connection('satelite_branch_db_connection')->table('student_enrollment_card')
            ->leftJoin('majors','majors.id','=','student_enrollment_card.major_id')
            ->leftJoin('programs','programs.id','=','student_enrollment_card.program_id')
            ->leftJoin('card_status','card_status.id','=','student_enrollment_card.status')
            ->leftJoin('discounts','discounts.id','=','student_enrollment_card.discount_category_id')
            ->select('student_enrollment_card.*','discounts.discount_category AS discount_category','programs.program_name AS program_name','majors.major AS major_name','card_status.status As status_name' )
            ->where('student_enrollment_card.student_id' , '=' , $id)
            ->orderBy('student_enrollment_card.id' , 'desc')
            ->get();
            
           
           
            $student = CbrcStudent::leftJoin('programs','programs.id','=','cbrc_students.program')
            ->leftJoin('majors','majors.id','=','cbrc_students.major')
            ->select('cbrc_students.*','programs.program_name','majors.major')
            ->where('cbrc_students.id' , '=' , $id)
            ->get();


            $latest_card = StudentEnrollmentCard::latest('date_registered')
            ->where('student_id','=',$id)->first();
           
            //over all total balance
            foreach($enrollmentCard as $data){
                
                $balance = $balance +  $data->balance;
            }
            //over all total balance

            return view('student.student-summary')
            ->with('latest_card',$latest_card)
            ->with('student_id',$id)
            ->with('Branches',$this->Branches)
            ->with('main_branch',$this->main_branch_id())
            ->with('student',$student)
            ->with('balance',$balance)
            ->with('enrollmentCard',$enrollmentCard);
        }

    

        public function add_payment ($card_id){

            $card_info = DB::connection('satelite_branch_db_connection')->table('student_enrollment_card')
            ->join('cbrc_students','cbrc_students.id','=','student_enrollment_card.student_id')->select('cbrc_students.first_name','cbrc_students.middle_name','cbrc_students.last_name')
            ->join('majors','majors.id','=','student_enrollment_card.major_id')->select('majors.major')
            ->join('card_status','card_status.id','=','student_enrollment_card.status')->select('card_status.status')
            ->join('programs','programs.id','=','student_enrollment_card.program_id')->select('programs.program_name')
            ->join('discounts','discounts.id','=','student_enrollment_card.discount_category_id')->select('discounts.discount_category')
            ->select('student_enrollment_card.*','card_status.status As status_name','cbrc_students.first_name As first_name','cbrc_students.middle_name As middle_name','cbrc_students.last_name As last_name','majors.major AS major','discounts.discount_category AS discount_category','programs.program_name AS program_name')
            ->where('student_enrollment_card.id' , '=' , $card_id)
            ->get();

            return view('student.add-payment')
            ->with('main_branch',$this->main_branch_id())
            ->with('Branches' , $this->Branches)
            ->with('card_info',$card_info);
        }



        public function enroll_reserved_student($card_id , $student_id){
    
            $student = CbrcStudent::join('programs','cbrc_students.program','=','programs.id')
            ->select('cbrc_students.*','programs.program_name')
            ->where('cbrc_students.id' , '=' , $student_id)
            ->get();

            $card = StudentEnrollmentCard::join('categories','student_enrollment_card.category_id','=','categories.id')
            ->select('student_enrollment_card.*','categories.category_name As category_name' )
            ->where('student_enrollment_card.id','=',$card_id)->first();

            $student_category = Category::all();

            $reservation_payment = PaymentInfo::where('card_id','=',$card_id)->first();
            
            return view('student.enroll-reserved-student')
            ->with('reservation_payment' , $reservation_payment)
            ->with('student' , $student)
            ->with('card' , $card)
            ->with('main_branch',$this->main_branch_id())
            ->with('Branches' , $this->Branches)
            ->with('student_category' , $student_category)
            ;
        }




        public function reserve_student ($student_id){

            $student_category = Category::all();

            $student = CbrcStudent::leftJoin('programs','programs.id','=','cbrc_students.program')
            ->leftJoin('majors','majors.id','=','cbrc_students.major')
            ->select('cbrc_students.*','programs.program_name', 'majors.major')
            ->where('cbrc_students.id' , '=' , $student_id)
            ->get();
            
            
            return view('student.reserve-student')
            ->with('student',$student)
            ->with('main_branch',$this->main_branch_id())
            ->with('Branches' , $this->Branches)
            ->with('student_category',$student_category);
        }


        public function enroll_student ($student_id){

            $student_category = Category::all();

            $student = CbrcStudent::leftJoin('programs','programs.id','=','cbrc_students.program')
            ->leftJoin('majors','majors.id','=','cbrc_students.major')
            ->select('cbrc_students.*','programs.program_name', 'majors.major')
            ->where('cbrc_students.id' , '=' , $student_id)
            ->get();
            
            
            return view('student.enroll-student')
            ->with('student',$student)
            ->with('main_branch',$this->main_branch_id())
            ->with('Branches' , $this->Branches)
            ->with('student_category',$student_category);
        }







}
