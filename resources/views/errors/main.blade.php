<!DOCTYPE html>
<html lang="en">

<head>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('../../assets/img/favicon.png') }}">

    <title>@yield('title') | CBRC - {{$_COOKIE['system_title'] }} </title>
    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('../../assets/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('../../assets/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('../../assets/vendors/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('../../cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('../../assets/css/style.css') }}" rel="stylesheet" type="text/css">

    <style type="text/css">
         .color-white{
            color: #000 !important;
         }

         .btn-outline-white{

            color: #000 !important;
            border-color: #000;
         }
    </style>
    <!-- Head Libs -->
    <script src="../../cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>

<body class="body-bg-full profile-page" style="background-image: url(/img/site-bg.png);">
    <!--#0e8dba-->
    @yield('main-content')

    <script src="{{ asset('../../cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js') }}"></script>
    <script src="{{ asset('../../cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('../../assets/js/material-design.js') }}"></script>

    @yield('js')
</body>

</html>