@extends('main')
@section('title')
Master List 
@stop
@section('css')
<style type="text/css">
.form-control:disabled, .form-control[readonly] {
color: #000;
}
@media print {
.content-header, .left-side, .main-header, .main-sidebar, .no-print {
display: none!important;
}
.table {
border-collapse: collapse !important;
}
td{
color: #000;
}

}


</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
<!-- Page Title Area -->
<div class="container-fluid">
<div class="row page-title clearfix">
<div class="page-title-left">
<h6 class="page-title-heading mr-0 mr-r-5">Enrolled Students</h6>

</div>





</div>
<!-- /.page-title -->
</div>
<!-- /.container-fluid -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="container-fluid">
<div class="widget-list">
<div class="row">
<div class="col-md-12 widget-holder">
<div class="widget-bg">



    <div class="widget-body clearfix">
        @include('student.student_page_nav')
    </div>



<div class="widget-body clearfix">
<table class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
<thead>
            <tr>
                <th>no.</th>
                <th>CBRC ID</th>
                <th>Name</th>
                <th>Program</th>
                <th>Option</th>
            </tr>
</thead>
<tbody>
        @if(isset($enrollmentCard))
            @foreach($enrollmentCard as $students)
            <tr>
                <td>{{ $row_counter++ }}</td>
                <td>{{$students->cbrc_id}}</td>
                <td>{{$students->last_name}}, {{$students->first_name}} {{$students->middle_name}}</td>
                <td>{{$students->program_name}}</td>
                <td>
                    <a href="#" type="button" id="{{$students->id}}" class="btn btn-info card_info" data-toggle="modal" data-target="#exampleModalCenter">
                            View Card
                    </a>    
                </td>
            </tr>
            @endforeach
        @endif
</tbody>
</table>
</div>



                    
            <!-- Enrollment Card Modal -->
                  <!-- Modal -->
                  <div class="modal fade"style="width: 100%" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"  aria-hidden="true" >
                        <div class="modal-dialog modal-lg modal-dialog-centered"    role="document">
                          <div class="modal-content" >
                            <div class="modal-header ">
                                        <a type="button" class="btn btn-danger" style="color:white;float:right" data-dismiss="modal">close</a>
                            </div>
                            <div class="modal-body">
                            <fieldset class="info_separator">
                                <legend class="info-border">Card Information</legend>
                             
                                    <h6 class="pull-right">Status: <span id="card_status"></span></h6>
                                    
                                    
                                    <b>Date Registered:<span id="card_date_registered"></span></b>
                                    <br>
                                    <b>Name:<span id="card_name"></span></b>
                                    <br>
                                    <b>Category:<span id="card_category"></span></b>
                                    <br>
                                    <b>Program:<span id="card_program"></span></b>
                                    <br>
                                    <b>Major:<span id="card_major"></span></b>
                                    <br>
                                    <b>Remaining Balance: <span id="card_balance"></span></b>
                                
                                    {{-- <div class="row  pull-right justify-content-between" style="padding:0px 50px 0px 40px; ">
                                            <div id="add_payment_button"></div>
                                    </div> --}}
        
                               
                            </fieldset>
                            <fieldset class="info_separator">
                                <legend class="info-border">Payment History</legend>
                            
                                    <hr>
                                    <table class="payment_history_table" style="width: 100% !important">
                                        <thead>
                                            <tr>
                                                <th>Payment no.</th>
                                                <th>Date</th>
                                                <th>Amount</th>
                                                <th>Balance Before</th>
                                                <th>Balance After</th>
                                                <th>View</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                    
                            </fieldset>                 
                                    <div class="modal-footer">
                                    </div>
                            </div>
                            
                          </div>
                        </div>
                      </div>
            <!-- Enrollment Card Modal -->
                
    

        
        
        <!-- payment information Modal -->
        <div class="modal fade  " id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                  <div class="modal-content">
                        <div class="modal-header">
                        
                                <h5 class="modal-title" id="paymentModal">Payment Information</h5>
                         
                        <a type="button" class="btn btn-danger" style="color:white" data-dismiss="modal">close</a>
                        </div>
                    <div class="modal-body">
                    
                            <div>
                                    <h6>Transaction no: 000<span id="payment_id"></span></h6>
                                    <b>Transaction Date:<span id="payment_transaction_date"></span></b>
                                    <br>
                                    <b>Amount Paid:<span id="payment_amount_paid"></span></b>
                                    <br>
                                    <b>Balance Before:<span id="payment_balance_before"></span></b>
                                    <br>
                                    <b>Balance After:<span id="payment_balance_after"></span></b>
                                    <br>
                                    <b>Transact By: <span id="payment_created_by"></span></b>
                                    <br>
                                    <b>Remarks <span id="payment_remarks"></span></b>
                            
                                </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              </div>
<!-- payment information Modal -->





                             

            
            


<!-- /.widget-bg -->
</div>
<!-- /.widget-holder -->
</div>
<!-- /.row -->
</div>
<!-- /.widget-list -->
</div>
<!-- /.container-fluid -->
</main>

@stop
@section('js')
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/student-summary.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
$('table.display').DataTable();
});

$(document).ready(function() {
$('.record').addClass('active');
$('.record').addClass('collapse in');
});



</script>
@stop