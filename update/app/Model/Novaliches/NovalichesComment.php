<?php

namespace App\Model\Novaliches;

use Illuminate\Database\Eloquent\Model;

class NovalichesComment extends Model
{
    protected $fillable = [
    	'name',
		'lecturer',
		'like',
		'dislike',
		'comment',
		'branch',
		'program',
		'section',
		'class',
		'subject',
		'date',
    ];
}
