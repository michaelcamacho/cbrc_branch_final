<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use App\Model\ExpenseSetup;
use App\Model\AdminSettings;

use App\Branch;
use App\Model\Novaliches\NovalichesAgri;
use App\Model\Novaliches\NovalichesBookCash;
use App\Model\Novaliches\NovalichesBooksInventorie;
use App\Model\Novaliches\NovalichesBooksSale;
use App\Model\Novaliches\NovalichesBudget;
use App\Model\Novaliches\NovalichesCivil;
use App\Model\Novaliches\NovalichesCrim;
use App\Model\Novaliches\NovalichesDiscount;
use App\Model\Novaliches\NovalichesDropped;
use App\Model\Novaliches\NovalichesExpense;
use App\Model\Novaliches\NovalichesIelt;
use App\Model\Novaliches\NovalichesLet;
use App\Model\Novaliches\NovalichesMid;
use App\Model\Novaliches\NovalichesNclex;
use App\Model\Novaliches\NovalichesNle;
use App\Model\Novaliches\NovalichesOnline;
use App\Model\Novaliches\NovalichesPsyc;
use App\Model\Novaliches\NovalichesReceivable;
use App\Model\Novaliches\NovalichesS1Sale;
use App\Model\Novaliches\NovalichesS2Sale;
use App\Model\Novaliches\NovalichesS1Cash;
use App\Model\Novaliches\NovalichesS2Cash;
use App\Model\Novaliches\NovalichesScholar;
use App\Model\Novaliches\NovalichesSocial;
use App\Model\Novaliches\NovalichesTuition;
use App\Model\Novaliches\NovalichesPettyCash;
use App\Model\Novaliches\NovalichesRemit;
use App\Model\Novaliches\NovalichesReservation;
use App\Model\Novaliches\NovalichesEmployee;
use App\Model\Novaliches\NovalichesScoreCards;
use App\Model\Novaliches\NovalichesBookTransfer;

use App\Model\Novaliches\NovalichesLecturerAEvaluation;
use App\Model\Novaliches\NovalichesLecturerBEvaluation;
use App\Model\Novaliches\NovalichesComment;

use App\facilitation;
use App\bookTranferTrans;
use App\Expense;
use App\Program;
use App\Subject;
use App\Section;
use Alert;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Auth;
use User;
use DB;
use File;




class NovalichesController extends Controller
{
        private $branch = "Novaliches";

        private $sbranch = "novaliches";

public function __construct()
    {

         $this->middleware('role:Member|admin');
        
    }

public function index(){

        $sid = AdminSettings::where('Account','=','admin')->value('id');
        $setting = AdminSettings::findorfail($sid);

        $facilitation = facilitation::where('Year','=',$setting->Year)->sum('facilitation');

        $pettycash = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

        $s1_sale = NovalichesS1Sale::where('Year','=',$setting->Year)->sum('amount_paid');

        $s2_sale = NovalichesS2Sale::where('Year','=',$setting->Year)->sum('amount_paid');

        $s1_cash = NovalichesS1Cash::where('id','=','1')->value('cash');

        $s2_cash = NovalichesS2Cash::where('id','=','1')->value('cash');

        $receivable = NovalichesReceivable::sum('balance');

        $expense = NovalichesExpense::where('Season','=',$setting->Season)
        ->where('Year','=',$setting->Year)->sum('amount');


        $unpaid = NovalichesReceivable::count();
        
        $s1_dis = NovalichesS1Sale::sum('discount');

        $s2_dis = NovalichesS2Sale::sum('discount');

        $discount = $s1_dis + $s2_dis;

        $book = NovalichesBooksSale::sum('amount');

        $book_cash = NovalichesBookCash::where('id','=','1')->value('cash');

        return view ('member.member-dashboard')
        ->with('pettycash',$pettycash)
        ->with('s1_sale',$s1_sale)
        ->with('s2_sale',$s2_sale)
        ->with('s1_cash',$s1_cash)
        ->with('s2_cash',$s2_cash)
        ->with('receivable', $receivable)
        ->with('expense',$expense)
        ->with('enrollee',$enrollee)
        ->with('unpaid',$unpaid)
        ->with('discount',$discount)
        ->with('book',$book)
        ->with('book_cash',$book_cash)
        ->with('facilitation',$facilitation);
    }

// Register New Enrollee

public function add_enrollee($branch){

     
        $program = Program::all();
        $Branches = $this->list_branches();

        return view('member.add-enrollee')
        ->with('Branches',$Branches)->with('branch',$branch)->with('program',$program);
}


public function insert_enrollee(Request $request){

        $input = $request->except(['_token']);
        $branch = $this->branch;
        $course = $input['program'];

        if($course == 'lets'){
            $course = 'LET';
        }
        if($course == 'nles'){
            $course = 'NLE';
        }
        if($course == 'crims'){
            $course = 'Criminology';
        }
        if($course == 'civils'){
            $course = 'Civil Service';
        }

        if($course == 'psycs'){
            $course = 'Psychometrician';
        }
        if($course == 'nclexes'){
            $course = 'NCLEX';
        }
        if($course == 'ielts'){
            $course = 'IELTS';
        }
        if($course == 'socials'){
            $course = 'Social Work';
        }
        if($course == 'agris'){
            $course = 'Agriculture';
        }
        if($course == 'mids'){
            $course = 'Midwifery';
        }
        if($course == 'onlines'){
            $course = 'Online Only';
        }

        $program = $this->sbranch.'_'.$input['program'];

        $lastname = strtoupper($input['last_name']);
        $firstname = strtoupper($input['first_name']);
        $middlename = strtoupper($input['middle_name']);

        $existent = DB::table($program)->where('last_name','=',$lastname)->where('first_name','=',$firstname)->where('middle_name','=',$middlename)->first();

        if($existent != null){
            Alert::error('Failed!', 'This name is already registered.');
            return redirect ($this->sbranch.'/add-enrollee');
        }

        if($existent == null){
        DB::table($program)->insert([

            'cbrc_id'       => $input['cbrc_id'],
            'section'       => $input['section'],
            'last_name'     => strtoupper($input['last_name']),
            'first_name'    => strtoupper($input['first_name']),
            'middle_name'   => strtoupper($input['middle_name']),
            'username'      => $input['username'],
            'password'      => $input['password'],
            'course'        => $course,
            'major'         => $input['major'],
            'program'       => $input['program'],
            'school'        => $input['school'],
            'noa_no'        => $input['noa_no'],
            'take'          => $input['take'],
            'branch'        => $branch,
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'registration'  => 'Walk-in',
            'created_at'    => date('Y-m-d'),
        ]);
        Alert::success('Success!', 'New student has been registered.');
        return redirect ($this->sbranch.'/add-enrollee');

    }
    }

public function update_enrollee(Request $request){
    $input = $request->except(['_token']);
        $branch = $this->branch;
        $program = $input['program'];
        $id = $input['id'];

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psychometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexes';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $program = $this->sbranch.'_'.$program;

        DB::table($program)->where('id','=',$id)->update([

            'cbrc_id'       => $input['cbrc_id'],
            'last_name'     => $input['last_name'],
            'first_name'    => $input['first_name'],
            'middle_name'   => $input['middle_name'],
            'username'      => $input['username'],
            'password'      => $input['password'],
            'major'         => $input['major'],
            'school'        => $input['school'],
            'noa_no'        => $input['noa_no'],
            'take'          => $input['take'],
            'birthdate'     => $input['birthdate'],
            'contact_no'    => $input['contact_no'],
            'email'         => $input['email'],
            'address'       => $input['address'],
            'contact_person'=> $input['contact_person'],
            'contact_details'=> $input['contact_details'],
            'section'        => $input['section'],
        ]);
       
       //login to api
       $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
       $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
           'form_params' => [
               "email"=>"admin@main.cbrc.solutions",
               "password"=>"main@dmin"
           ]
       ]);
   //insert data to api
  
       if ($res->getStatusCode() == 200) { // 200 OK
           $response_data = json_decode($res->getBody()->getContents());
        
           
               //save student info to api
               $studentForApi =  DB::table($program)->where('id','=' , $id)->where('status','=','Enrolled')->first();
               $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
               ,[
               'form_params' => [
                   "_method" => "PUT",
                   "BranchStdID" => $studentForApi->id,
                   "Branch_Name" =>lcfirst($this->sbranch),
                   "cbrc_id" =>     $input['cbrc_id'],
                   "Lastname" => $input['last_name'],
                   "Firstname" =>$input['first_name'],
                   "Middlename" =>$input['middle_name'],
                   "Birthday" =>$input['birthdate'],
                   "Contact_Number" =>$input['contact_no'],
                   "Address" =>$input['address'],
                   "Email" =>$input['email'],
                   "Username" =>$input['username'],
                   "Password" =>$input['password'],
                   "School" => $input['school'],
                   "Program" =>$studentForApi->program,
                   "Section" =>$input['section'],
                   "Major" =>$input['major'],
                   "Take" =>$input['take'],
                   "Noa" =>$input['noa_no'],
                   "Category" =>$studentForApi->category,
                   "Status" =>$studentForApi->status,
                   "Contact_Person" =>$input['contact_person'],
                   "Contact_Details" =>$input['contact_details'],
                   "Facilitation" =>$studentForApi->facilitation,
                   "Season" =>$studentForApi->season,
                   "Year" =>$studentForApi->year
           
               ]
               ]);
               // return dd($studentForApi);


       }//end of 200 ok

        return response()->json([
            'success' => true,
            'message' => 'Enrollee has been updated',
            // 'response_data1 '=>   $response_data1, 
            'input '=>   $input, 
            // 'responsed '=>   $responsed,
            // 'studentForApi '=>   $studentForApi,
        ]);

    }

    public function delete_enrollee(Request $request){
    $input = $request->except(['_token']);
        $branch = $this->branch;
        $program = $input['program'];
        $id = $input['id'];

        if($program == 'LET'){
            $program = 'lets';
        }
        if($program == 'NLE'){
            $program = 'nles';
        }
        if($program == 'Criminology'){
            $program = 'crims';
        }
        if($program == 'Civil Service'){
            $program = 'civils';
        }

        if($program == 'Psychometrician'){
            $program = 'psycs';
        }
        if($program == 'NCLEX'){
            $program = 'nclexes';
        }
        if($program == 'IELTS'){
            $program = 'ielts';
        }
        if($program == 'Social Work'){
            $program = 'socials';
        }
        if($program == 'Agriculture'){
            $program = 'agris';
        }
        if($program == 'Midwifery'){
            $program = 'mids';
        }
        if($program == 'Online Only'){
            $program = 'onlines';
        }

        $program = $this->sbranch.'_'.$program;

        DB::table($program)->where('id','=',$id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Enrollee has been deleted',
            
        ]);

    }

// Add New Payment



public function fetch_student(){

        $program = Input::get('program');

        if ($program == 'lets' ){

        $student = NovalichesLet::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = NovalichesNle::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = NovalichesCrim::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = NovalichesCivil::orderBy('last_name')->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = NovalichesPsyc::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = NovalichesNclex::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = NovalichesIelt::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = NovalichesSocial::orderBy('last_name')->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = NovalichesAgri::orderBy('last_name')->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = NovalichesMid::orderBy('last_name')->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = NovalichesOnline::orderBy('last_name')->get();
            return response()->json($student);
        }       
    }

public function fetch_tuition(){

        $category = Input::get('category');
        $program = Input::get('program');


        if ($program == 'lets' ){

        $tuition = NovalichesTuition::where('program','=', 'LET')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'nles' ){
            $tuition = NovalichesTuition::where('program','=', 'NLE')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'crims' ){
            $tuition = NovalichesTuition::where('program','=', 'Criminology')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'civils' ){
            $tuition = NovalichesTuition::where('program','=', 'Civil Service')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

         if ($program == 'psycs' ){
            $tuition = NovalichesTuition::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'nclexes' ){
            $tuition = NovalichesTuition::where('program','=', 'NCLEX')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'ielts' ){
            $tuition = NovalichesTuition::where('program','=', 'IELTS')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'socials' ){
            $tuition = NovalichesTuition::where('program','=', 'Social Work')->where('category','=', $category)->get();
        return response()->json($tuition);
        }

        if ($program == 'agris' ){
            $tuition = NovalichesTuition::where('program','=', 'Agriculture')->where('category','=', $category)->get();
            return response()->json($tuition);
        }

        if ($program == 'mids' ){
            $tuition = NovalichesTuition::where('program','=', 'Midwifery')->where('category','=', $category)->get();
            return response()->json($tuition);
        }  

        if ($program == 'onlines' ){
            $tuition = NovalichesTuition::where('program','=', 'Online Only')->where('category','=', $category)->get();
            return response()->json($tuition);
        }          
    }

public function fetch_discount(){

        $program = Input::get('program');
        $category = Input::get('category');


        if ($program == 'lets' ){

        $discount = NovalichesDiscount::where('program','=', 'LET')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'nles' ){
            $discount = NovalichesDiscount::where('program','=', 'NLE')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'crims' ){
            $discount = NovalichesDiscount::where('program','=', 'Criminology')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'civils' ){
            $discount = NovalichesDiscount::where('program','=', 'Civil Service')->where('category','=', $category)->get();
        return response()->json($discount);
        }

         if ($program == 'psycs' ){
            $discount = NovalichesDiscount::where('program','=', 'Psychometrician')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'nclexes' ){
            $discount = NovalichesDiscount::where('program','=', 'NCLEX')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'ielts' ){
            $discount = NovalichesDiscount::where('program','=', 'IELTS')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'socials' ){
            $discount = NovalichesDiscount::where('program','=', 'Social Work')->where('category','=', $category)->get();
        return response()->json($discount);
        }

        if ($program == 'agris' ){
            $discount = NovalichesDiscount::where('program','=', 'Agriculture')->where('category','=', $category)->get();
            return response()->json($discount);
        }  

        if ($program == 'mids' ){
            $discount = NovalichesDiscount::where('program','=', 'Midwifery')->where('category','=', $category)->get();
            return response()->json($discount);
        } 

        if ($program == 'onlines' ){
            $discount = NovalichesDiscount::where('program','=', 'Online Only')->where('category','=', $category)->get();
            return response()->json($discount);
        }       


    }
    public function add_new_payment(Request $request){
    
            $input = $request->except(['_token']);
            $season = $input['season'];
            $total_amount = $input['total_amount'];
            $amount_paid = $input['amount_paid'];
            $student = explode('*',$input['name']);
            $balance = $total_amount - $amount_paid;
            $firstPayment =0;       
            
            //-----------------for scorecard payment starts here 
            $curr_date = date('M-d-Y');
            
            $program = $this->sbranch.'_'.$input['program'];
            
            $statusforapi = DB::table($program)->where('id','=',$student[1])->value('status');
            $dis_category = explode(',',$input['discount']);
          
          
            
            //reffer to views/js/payment.blade.php for "student[5]" 
            //skip this if already made any payment   
            if($input['tuition_fee'] == null){                                                                                                                                            //--add facilitation amount to facilitation table
            facilitation::create([
                'year' => date('Y',strtotime($curr_date)),
                'season' => $season,
                'branch' => "Novaliches",
                'facilitation' => $input['facilitation'],
            ]);
//--end of add facilitation amount to facilitation table

    
            } //end of skip this if already made any payment  

//-----------end of score card update in payment
//-----------end of score card update in payment
//-----------end of score card update in payment
    
    


            $reserve = $input['reserve'];
    
            if($balance < 0 ){
                $balance = 0;
            }
            $discount = explode(',',$input['discount']);
            $prog = $input['program'];
            if ($prog == 'lets' ){
            $prog = 'LET';
            }
            if ($prog == 'nles' ){
                $prog = 'NLE';
            }
            if ($prog == 'crims' ){
                $prog = 'Criminology';
            }
            if ($prog == 'civils' ){
                $prog = 'Civil Service';
            }
             if ($prog == 'psycs' ){
                $prog = 'Psychometrician';
            }
            if ($prog == 'nclexes' ){
                $prog = 'NCLEX';
            }
            if ($prog == 'ielts' ){
                $prog = 'IELTS';
            }
            if ($prog == 'socials' ){
                $prog = 'Social Work';
            }
            if ($prog == 'agris' ){
                $prog = 'Agriculture';
            }
            if ($prog == 'mids' ){
                $prog = 'Midwifery';
            }
    
            if ($prog == 'onlines' ){
                $prog = 'Online Only';
            }
            $discount = explode(',',$input['discount']);
    
            if($discount[0] == 0) {
                $discount_amount = null;
                $discount_category = null;
            } 
            else {
                
                $discount_amount = $discount[0];
                $discount_category = $discount[1];
            }
    
            if($input['tuition_fee'] != null){
            if ($season == 'Season 1'){
            DB::table($this->sbranch.'_s1_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'season'  => $season,
                'year'  => $input['year'],
                'created_at'    => date('Y-m-d'),
            ]);
            $ini = NovalichesS1Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            NovalichesS1Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
    
            $program = $this->sbranch.'_'.$input['program'];
    
            DB::table($program)->where('id','=',$student[1])->update([
    
                'category' => $input['category'],
                'status'   => 'Enrolled',
                'facilitation' => $input['facilitation'],
                'year'  => $input['year'],
                'season' => $season
            ]);
            }
            if ($season == 'Season 2'){
            DB::table($this->sbranch.'_s2_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'season' => $season,
                'year'  => $input['year'],
                 'created_at'    => date('Y-m-d'),
            ]);
    
            $ini = NovalichesS2Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            NovalichesS2Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
    
            $program = $this->sbranch.'_'.$input['program'];
    
            DB::table($program)->where('id','=',$student[1])->update([
    
                'category' => $input['category'],
                'status'   => 'Enrolled',
                'facilitation' => $input['facilitation'],
                'year'  => $input['year'],
                'season' => $season
            ]);
            }
    
            Alert::success('Success!', 'Payment has been submitted.');
        }
            if($input['tuition_fee'] == null){
                
                //get year
                $ryear = DB::table($program)->where('id','=',$student[1])->value('year');
                
                if ($input['rseason'] == 'Season 1'){
                    
                

            DB::table($this->sbranch.'_s1_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'year'  => $ryear,
                 'created_at'    => date('Y-m-d'),
            ]);
    
            $ini = NovalichesS1Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            NovalichesS1Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
    
            }
            if ($input['rseason'] == 'Season 2'){
            DB::table($this->sbranch.'_s2_sales')->insert([
                'date' => $input['date'],
                'student_id' => $student[1],
                'name' => $student[0],
                'program'=> $prog,
                'category' => $input['category'],
                'discount_category' => $discount_category,
                'tuition_fee' =>$input['tuition_fee'],
                'facilitation_fee' =>$input['facilitation'],
                'discount' => $discount_amount,
                'amount_paid' =>$input['amount_paid'],
                'balance' => $balance,
                'year'  => $ryear,
                'season'  => "Season 2",
                 'created_at'    => date('Y-m-d'),
            ]);
    
            $ini = NovalichesS2Cash::where('id','=','1')->value('cash');
    
            $total = $ini + $input['amount_paid'];
    
            NovalichesS2Cash::where('id','=','1')->update([
                'cash' => $total,
            ]);
            }
            Alert::success('Success!', 'Payment has been submitted.');
            }
    
           
    
            if($balance > 0){
    
                if($input['balance'] == null){
                    NovalichesReceivable::insert([
    
                        'enrollee_id' => $student[1],
                        'name'        => $student[0],
                        'program'     => $prog,
                        'contact_no'  => $student[2],
                        'season'      => $season,
                        'balance'     => $balance,
                    ]);
                }
                }
                if($input['balance'] != null && $input['tuition_fee'] == null){
    
                    if($amount_paid >= $total_amount){
                        NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                        }
                    if($amount_paid < $total_amount){
                    $remaining = $input['balance'];
                    $present_balance = $remaining - $amount_paid;
                    NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                        'balance' => $present_balance,
                    ]);
                    }
                }
                if($input['balance'] != null && $input['tuition_fee'] != null){
    
                    if($amount_paid >= $total_amount){
                        NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->delete();
                        }
                    else{
                    NovalichesReceivable::where('enrollee_id','=',$student[1])->where('program','=',$prog)->update([
                        'balance' => $balance,
                    ]);
                    }
                }
    
               if($input['reserve'] != null){
    
                NovalichesReservation::where('enrollee_id','=',$student[1])->where('program','=',$input['program'])->delete();
               }

               //check if not enrolled
    if($statusforapi  != "Enrolled" ){
        
            if($firstPayment == 1 ){
               //get last inserted payment
               //check what season
              
            if($season === "Season 1"){
                $lastRec = DB::table($this->sbranch.'_s1_sales')->where('program',$prog)->where('student_id',$student[1])->orderby('id','desc')->get();
               
            }
            if($season === "Season 2"){
                $lastRec = DB::table($this->sbranch.'_s2_sales')->where('program',$prog)->where('student_id',$student[1])->orderby('id','desc')->get();
               
            }//end check what season
            

            foreach($lastRec as $data){
                $lastRecDate  = $data->date;
                $lastRecName  = $data->name;
                $lastRecstudent_id  = $data->student_id;
                $lastRecdiscout_category  = $data->discount_category;
                $lastRectuition_fee  = $data->tuition_fee;
                $lastRecfacilitation_fee  = $data->facilitation_fee;
                $lastRecyear  = $data->year;
            }
           
        //login to api
        $client  = new \GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
        $res = $client->request('POST', 'https://cbrc.solutions/api/auth/login', [
            'form_params' => [
                "email"=>"admin@main.cbrc.solutions",
                "password"=>"main@dmin"
            ]
        ]);
    //insert data to api
   
        if ($res->getStatusCode() == 200) { // 200 OK
            $response_data = json_decode($res->getBody()->getContents());
         //save first payment to api   
            $sendPayment = $client->request('POST', 'https://cbrc.solutions/api/main/payment?token='.$response_data->access_token
            ,[
            'form_params' => [
                
                        "Branch" => ucwords($this->sbranch),
                        "Season" => $season,
                        "Date" => $lastRecDate,
                        "Name" => $lastRecName,
                        "Stdid" => $lastRecstudent_id,
                        "Program" => $input['program'],
                        "Category" => $input['category'],
                        "Discount_category" => $lastRecdiscout_category,
                        "Tuition_fee" =>  $lastRectuition_fee,
                        "Facilitation_fee" => $lastRecfacilitation_fee,
                        "year" => $lastRecyear
                        ]
            ]);
            
                //save student info to api
                $studentForApi =  DB::table($program)->where('id','=',$student[1])->where('status','=','Enrolled')->first();
                $sendStudentInfo = $client->request('POST', 'https://cbrc.solutions/api/main/student?token='.$response_data->access_token
                ,[
                'form_params' => [
                    "BranchStdID" => $studentForApi->id,
                    "Branch_Name" =>lcfirst($this->sbranch),
                    "cbrc_id" =>$studentForApi->cbrc_id,
                    "Lastname" =>$studentForApi->last_name,
                    "Firstname" =>$studentForApi->first_name,
                    "Middlename" =>$studentForApi->middle_name,
                    "Birthday" =>$studentForApi->birthdate,
                    "Contact_Number" =>$studentForApi->contact_no,
                    "Address" =>$studentForApi->address,
                    "Email" =>$studentForApi->email,
                    "Username" =>$studentForApi->username,
                    "Password" =>$studentForApi->password,
                    "School" =>$studentForApi->school,
                    "Program" =>$studentForApi->program,
                    "Section" =>$studentForApi->section,
                    "Major" =>$studentForApi->major,
                    "Take" =>$studentForApi->take,
                    "Noa" =>$studentForApi->noa_no,
                    "Category" =>$studentForApi->category,
                    "Status" =>$studentForApi->status,
                    "Contact_Person" =>$studentForApi->contact_person,
                    "Contact_Details" =>$studentForApi->contact_details,
                    "Facilitation" =>$studentForApi->facilitation,
                    "Season" =>$studentForApi->season,
                    "Year" =>$studentForApi->year
            
                ]
                ]);
                // return dd($studentForApi);


        }//end of 200 ok
    }//end of firstPayment save to api

    }
               return redirect ($this->sbranch.'/new-payment');
        }

public function add_expense(){
        $branch=$this->branch; 
        $program = Program::all();
        $date = date('M-d-Y');
        return view('member.add-expense')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }

public function fetch_expense(){
        $category = Input::get('category');
        $sub_category = Expense::where('category','=', $category)->get();
        return response()->json($sub_category);
    }

public function fetch_id(){

    $id = Input::get('id');
    $program = Input::get('program');

    if ($program == 'lets' ){

        $student = NovalichesLet::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nles' ){
            $student = NovalichesNle::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'crims' ){
            $student = NovalichesCrim::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'civils' ){
            $student = NovalichesCivil::where('id','=',$id)->get();
        return response()->json($student);
        }

         if ($program == 'psycs' ){
            $student = NovalichesPsyc::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'nclexes' ){
            $student = NovalichesNclex::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'ielts' ){
            $student = NovalichesIelt::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'socials' ){
            $student = NovalichesSocial::where('id','=',$id)->get();
        return response()->json($student);
        }

        if ($program == 'agris' ){
            $student = NovalichesAgri::where('id','=',$id)->get();
            return response()->json($student);
        }
        if ($program == 'mids' ){
            $student = NovalichesMid::where('id','=',$id)->get();
            return response()->json($student);
        }

        if ($program == 'onlines' ){
            $student = NovalichesOnline::where('id','=',$id)->get();
            return response()->json($student);
        }  
}

public function fetch_balance(){

        $id = Input::get('id');
        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 

        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 

        if ($program == 'onlines' ){
            $program = 'Online Only';
        }

        

        $balance = NovalichesReceivable::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        return response()->json($balance);
    }

public function fetch_book(){

        $program = Input::get('program');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $book = NovalichesBooksInventorie::where('program','=', $program)->get();
        return response()->json($book);
    }

public function fetch_book_price(){

    $program = Input::get('program');
    $book_title = Input::get('book_title');

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        }
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 

        $avai = NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->value('available');

        if($avai == 0){
            $price[] = array('price' => '0' , );
        return response()->json($price);

        }
        else{
            $price = NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$book_title)->get();
                return response()->json($price);
        }


}
public function insert_book_payment(Request $request){

 $input = $request->except(['_token']);


 
$validatedData = $request->validate([
    'name' => 'required'
]);

 $program = $input['program'];
 $price = $input['price'];
 $book_title = $input['book_title'];

        if ($program == 'lets' ){
            $program = 'LET';
        }

        if ($program == 'nles' ){
            $program = 'NLE';
        }

        if ($program == 'crims' ){
           $program = 'Criminology';
        }

        if ($program == 'civils' ){
            $program = 'Civil Service';
        }

         if ($program == 'psycs' ){
            $program = 'Psychometrician';
        }

        if ($program == 'nclexes' ){
            $program = 'NCLEX';
        }

        if ($program == 'ielts' ){
            $program = 'IELTS';
        }

        if ($program == 'socials' ){
            $program = 'Social Work';
        }

        if ($program == 'agris' ){
            $program = 'Agriculture';
        } 
        if ($program == 'mids' ){
            $program = 'Midwifery';
        } 
        if ($program == 'onlines' ){
            $program = 'Online Only';
        } 
        if($input['total_amount'] <= $input['amount_paid'] || $input['amount_paid'] == 0 || $input['amount_paid'] == 0.00 )
        {

        if(isset($input['book_title'])){
            foreach ($input['book_title'] as $book => $value ) {

             NovalichesBooksSale::create([
                'date'      => $input['date'],
                'branch'    => $this->branch,
                'name'      => $input['name'],
                'program'   => $program,
                'book_title'=> $value,
                'amount'    => $price[$book],
             ]);
        
        $avai = NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->value('available');

        $new = $avai - 1;

        NovalichesBooksInventorie::where('program','=', $program)->where('book_title','=',$value)->update([

            'available' => $new,
        ]);     
        }
        $book_cash = NovalichesBookCash::where('id','=','1')->value('cash');
        $new_book_cash = $book_cash + $input['amount_paid'];

        NovalichesBookCash::where('id','=','1')->update([
            'cash'  => $new_book_cash,
        ]);
            }

            Alert::success('Success!', 'New book payment has been submitted.');
         return redirect ($this->sbranch.'/book-payment');
                }

        else{
            Alert::error('Failed!', 'Payment is insufficient, please try again');
            return redirect ($this->sbranch.'/book-payment');
        }
}
public function add_new_expense(Request $request){

        $input = $request->except(['_token']);
        $author = Auth::user()->name;
        $ubranch = "Novaliches";
        $expid = ExpenseSetup::where('branch','=',$ubranch)->value('id');
        $expset = ExpenseSetup::findorfail($expid);
       

        NovalichesExpense::insert([
            'date'=>$input['date'],
            'branch'=>$this->branch,
            'program'=>$input['program'],
            'category'=>$input['category'],
            'sub_category'=>$input['sub_category'],
            'amount'=>$input['amount'],
            'remarks'=>$input['remarks'],
            'author' => $author,
            'Season' => $expset->Season,
            'Year' => $expset->Year
            ]);
        $pettycash = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

        $updated_pettycash = $pettycash - $input['amount'];

        NovalichesPettyCash::where('id','=','1')->update([
            'petty_cash' => $updated_pettycash,
        ]);

        $chk = $input['category'];
        if ($chk == 'Facilitation') {
            # code...
            $amt = $input['amount'];

            $facilitation = facilitation::where('id','=','1')->value('facilitation');
            $new_faci = $facilitation - $amt;
            facilitation::where('id','=','1')->update(['facilitation' => $new_faci]);
            //d2ako
        }
        
        Alert::success('Success!', 'New expense has been added.');
        return redirect ($this->sbranch.'/add-expense');
    }

public function add_budget(){
    $branch=$this->branch;
    $date = date('M-d-Y');
    return view('member.add-budget')->with('branch',$branch)->with('date',$date);
}

public function insert_new_budget(Request $request){
    $input = $request->except(['_token']);
    NovalichesBudget::insert($input);

    $initial = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

    $amount = $input['amount'];

    $total = $initial + $amount;

    NovalichesPettyCash::where('id','=','1')->update([
        'petty_cash' => $total,
    ]);
    Alert::success('Success!', 'New budget has been added.');
    return redirect ($this->sbranch.'/add-budget');
}

public function book_payment(){

    $branch=$this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view('member.book-payment')->with('branch',$branch)->with('date',$date)->with('program',$program);
}

public function let_table(){
    $prog = 'LET';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = NovalichesLet::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nle_table(){
    $prog = 'NLE';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesNle::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function crim_table(){
    $prog = 'Criminology';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesCrim::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function civil_table(){
    $prog = 'Civil Service';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesCivil::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function psyc_table(){
    $prog = 'Psychometrician';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesPsyc::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function nclex_table(){
    $prog = 'NCLEX';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesNclex::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function ielts_table(){
    $prog = 'IELTS';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesIelt::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function social_table(){
    $prog = 'Social Work';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = NovalichesSocial::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function agri_table(){
    $prog = 'Agriculture';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = NovalichesAgri::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function mid_table(){
    $prog = 'Midwifery';
    $branch=$this->branch; 
    $sbranch=$this->sbranch; 
    $enrollee = NovalichesMid::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}

public function online_table(){
    $prog = 'Online Only';
    $sbranch=$this->sbranch; 
    $branch=$this->branch; 
    $enrollee = NovalichesOnline::all();

    return view ('member.registered-enrollees')->with('enrollee',$enrollee)->with('branch',$branch)->with('sbranch',$sbranch)->with('prog',$prog);
}
public function scholar_table(){

    $let = NovalichesLet::where('category','=','Scholar')->get();
    $nle = NovalichesNle::where('category','=','Scholar')->get();
    $crim = NovalichesCrim::where('category','=','Scholar')->get();
    $civil = NovalichesCivil::where('category','=','Scholar')->get();
    $psyc = NovalichesPsyc::where('category','=','Scholar')->get();
    $nclex = NovalichesNclex::where('category','=','Scholar')->get();
    $ielt = NovalichesIelt::where('category','=','Scholar')->get();
    $social = NovalichesSocial::where('category','=','Scholar')->get();
    $agri = NovalichesAgri::where('category','=','Scholar')->get();
    $mid = NovalichesMid::where('category','=','Scholar')->get();
    $online = NovalichesOnline::where('category','=','Scholar')->get();

    return view ('member.scholar')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}
public function enrolled_table(){

    $let = NovalichesLet::where('status','=','Enrolled')->get();
    $nle = NovalichesNle::where('status','=','Enrolled')->get();
    $crim = NovalichesCrim::where('status','=','Enrolled')->get();
    $civil = NovalichesCivil::where('status','=','Enrolled')->get();
    $psyc = NovalichesPsyc::where('status','=','Enrolled')->get();
    $nclex = NovalichesNclex::where('status','=','Enrolled')->get();
    $ielt = NovalichesIelt::where('status','=','Enrolled')->get();
    $social = NovalichesSocial::where('status','=','Enrolled')->get();
    $agri = NovalichesAgri::where('status','=','Enrolled')->get();
    $mid = NovalichesMid::where('category','=','Enrolled')->get();
    $online = NovalichesOnline::where('category','=','Enrolled')->get();

    $sale1 = NovalichesS1Sale::all();

    $sale2 = NovalichesS2Sale::all();
   

    $s1sum_lets =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_lets as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','LET')
    ->get();

    $s1sum_nles =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_nles as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','NLE')
    ->get();
    
    $s1sum_crims=  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_crims as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Criminology')
    ->get();

    $s1sum_civils =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_civils as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Civil Service')
    ->get();

    $s1sum_psycs =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_psycs as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Pyschometrician')
    ->get();

    $s1sum_nclexes =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_nclexes as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','NCLEX')
    ->get();
    
    $s1sum_ielts =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_ielts as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','IELTS')
    ->get();

    $s1sum_socials =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_socials as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Social Service')
    ->get();

    $s1sum_agris =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_agris as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Agriculture')
    ->get();

    $s1sum_mids =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_mids as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Midwifery')
    ->get();

    $s1sum_onlines =  DB::table("novaliches_s1_sales AS f")
    ->join('novaliches_onlines as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Online Only')
    ->get();

    $s2sum_lets =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_lets as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','LET')
    ->get();

    $s2sum_nles =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_nles as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','NLE')
    ->get();
    
    $s2sum_crims=  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_crims as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Criminology')
    ->get();

    $s2sum_civils =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_civils as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Civil Service')
    ->get();

    $s2sum_psycs =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_psycs as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Pyschometrician')
    ->get();

    $s2sum_nclexes =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_nclexes as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','NCLEX')
    ->get();
    
    $s2sum_ielts =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_ielts as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','IELTS')
    ->get();

    $s2sum_socials =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_socials as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Social Service')
    ->get();

    $s2sum_agris =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_agris as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Agriculture')
    ->get();

    $s2sum_mids =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_mids as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Midwifery')
    ->get();

    $s2sum_onlines =  DB::table("novaliches_s2_sales AS f")
    ->join('novaliches_onlines as prog','prog.id','=','f.student_id')
    ->select([DB::raw('min(f.balance) AS total_balance'),DB::raw('sum(f.amount_paid) AS amount_paid'),DB::raw('max(f.discount) AS discount'),DB::raw('max(f.tuition_fee) AS tuition_fee'),DB::raw('max(f.facilitation_fee) AS facilitation_fee'),DB::raw('max(f.student_id) AS student_id'),DB::raw('count(f.student_id) AS no_of_transaction'),'f.season','f.program'])
    ->groupBy('student_id')
    ->where('student_id','!=',NULL)
    ->where('f.program','=','Online Only')
    ->get();


    return view ('member.enrolled')
                ->with('let',$let)
                ->with('nle',$nle)
                ->with('crim',$crim)
                ->with('civil',$civil)
                ->with('psyc',$psyc)
                ->with('nclex',$nclex)
                ->with('ielt',$ielt)
                ->with('social',$social)
                ->with('agri',$agri)
                ->with('mid',$mid)
                ->with('online',$online)
                ->with('s2sum_lets',$s2sum_lets)
                ->with('s2sum_nles',$s2sum_nles)
                ->with('s2sum_crims',$s2sum_crims)
                ->with('s2sum_civils',$s2sum_civils)
                ->with('s2sum_psycs',$s2sum_psycs)
                ->with('s2sum_nclexes',$s2sum_nclexes)
                ->with('s2sum_ielts',$s2sum_ielts)
                ->with('s2sum_socials',$s2sum_socials)
                ->with('s2sum_agris',$s2sum_agris)
                ->with('s2sum_mids',$s2sum_mids)
                ->with('s2sum_onlines',$s2sum_onlines)

                ->with('s1sum_lets',$s1sum_lets)
                ->with('s1sum_nles',$s1sum_nles)
                ->with('s1sum_crims',$s1sum_crims)
                ->with('s1sum_civils',$s1sum_civils)
                ->with('s1sum_psycs',$s1sum_psycs)
                ->with('s1sum_nclexes',$s1sum_nclexes)
                ->with('s1sum_ielts',$s1sum_ielts)
                ->with('s1sum_socials',$s1sum_socials)
                ->with('s1sum_agris',$s1sum_agris)
                ->with('s1sum_mids',$s1sum_mids)
                ->with('s1sum_onlines',$s1sum_onlines)

                ->with('sale1',$sale1)
                ->with('sale2',$sale2);
}

public function dropped_table(){

    $let = NovalichesLet::where('status','=','Dropped')->get();
    $nle = NovalichesNle::where('status','=','Dropped')->get();
    $crim = NovalichesCrim::where('status','=','Dropped')->get();
    $civil = NovalichesCivil::where('status','=','Dropped')->get();
    $psyc = NovalichesPsyc::where('status','=','Dropped')->get();
    $nclex = NovalichesNclex::where('status','=','Dropped')->get();
    $ielt = NovalichesIelt::where('status','=','Dropped')->get();
    $social = NovalichesSocial::where('status','=','Dropped')->get();
    $agri = NovalichesAgri::where('status','=','Dropped')->get();
    $mid = NovalichesMid::where('category','=','Dropped')->get();
    $online = NovalichesOnline::where('category','=','Dropped')->get();

    return view ('member.dropped')->with('let',$let)->with('nle',$nle)->with('crim',$crim)->with('civil',$civil)->with('psyc',$psyc)->with('nclex',$nclex)->with('ielt',$ielt)->with('social',$social)->with('agri',$agri)->with('mid',$mid)->with('online',$online);

}

public function drop_student(Request $request){
    $input = $request->except(['_token']);

    $id = $input['id'];
    $program = $input['program'];

    if($program == 'lets'){

        NovalichesLet::where('id','=',$id)->update([
            'status' => 'Dropped',
        ]);

        return response()->json([
            'success' => true,
            'message' => '1 Student has beed dropped.',
        ]);
    }


}
public function tuition_table(){

    $tuition = NovalichesTuition::where('branch','=',$this->branch)->get();

    $discount = NovalichesDiscount::where('branch','=',$this->branch)->get();

    return view ('member.tuition')->with('tuition',$tuition)->with('discount',$discount);

}

public function expense_table(){

    $expense = NovalichesExpense::all();

    return view ('member.expense')->with('expense',$expense);

}
public function sales_enrollee_table(){

    $sale = NovalichesS1Sale::all();

    $sale2 = NovalichesS2Sale::all();

    return view ('member.sales-enrollee')->with('sale',$sale)->with('sale2',$sale2);

}

public function sales_program_table(){

    $let_1_sale=NovalichesS1Sale::where('program','=','LET')->sum('amount_paid');
    $nle_1_sale=NovalichesS1Sale::where('program','=','NLE')->sum('amount_paid');
    $crim_1_sale=NovalichesS1Sale::where('program','=','Criminology')->sum('amount_paid');
    $civil_1_sale=NovalichesS1Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psyc_1_sale=NovalichesS1Sale::where('program','=','Psychometrician')->sum('amount_paid');
    $nclex_1_sale=NovalichesS1Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ielts_1_sale=NovalichesS1Sale::where('program','=','IELTS')->sum('amount_paid');
    $social_1_sale=NovalichesS1Sale::where('program','=','Social')->sum('amount_paid');
    $agri_1_sale=NovalichesS1Sale::where('program','=','Agriculture')->sum('amount_paid');
    $mid_1_sale=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $online_1_sale=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $let_2_sale=NovalichesS2Sale::where('program','=','LET')->sum('amount_paid');
    $nle_2_sale=NovalichesS2Sale::where('program','=','NLE')->sum('amount_paid');
    $crim_2_sale=NovalichesS2Sale::where('program','=','Criminology')->sum('amount_paid');
    $civil_2_sale=NovalichesS2Sale::where('program','=','Civil Service')->sum('amount_paid');
    $psyc_2_sale=NovalichesS2Sale::where('program','=','Psychometrician')->sum('amount_paid');
    $nclex_2_sale=NovalichesS2Sale::where('program','=','NCLEX')->sum('amount_paid');
    $ielts_2_sale=NovalichesS2Sale::where('program','=','IELTS')->sum('amount_paid');
    $social_2_sale=NovalichesS2Sale::where('program','=','Social')->sum('amount_paid');
    $agri_2_sale=NovalichesS2Sale::where('program','=','Agriculture')->sum('amount_paid');
    $mid_2_sale=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');
    $online_2_sale=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

    $let =  NovalichesLet::where('status','=','Enrolled')->count();
    $nle =  NovalichesNle::where('status','=','Enrolled')->count();
    $crim = NovalichesCrim::where('status','=','Enrolled')->count();
    $civil= NovalichesCivil::where('status','=','Enrolled')->count();
    $psyc = NovalichesPsyc::where('status','=','Enrolled')->count();
    $nclex = NovalichesNclex::where('status','=','Enrolled')->count();
    $ielts = NovalichesIelt::where('status','=','Enrolled')->count();
    $social = NovalichesSocial::where('status','=','Enrolled')->count();
    $agri = NovalichesAgri::where('status','=','Enrolled')->count();
    $mid = NovalichesMid::where('status','=','Enrolled')->count();
    $online = NovalichesOnline::where('status','=','Enrolled')->count();

   

    return view ('member.sales-program')
    ->with('let_1_sale',$let_1_sale)
    ->with('nle_1_sale',$nle_1_sale)
    ->with('crim_1_sale',$crim_1_sale)
    ->with('civil_1_sale',$civil_1_sale)
    ->with('psyc_1_sale',$psyc_1_sale)
    ->with('nclex_1_sale',$nclex_1_sale)
    ->with('ielts_1_sale',$ielts_1_sale)
    ->with('social_1_sale',$social_1_sale)
    ->with('agri_1_sale',$agri_1_sale)
    ->with('mid_1_sale',$mid_1_sale)
    ->with('online_1_sale',$online_1_sale)
    ->with('let_2_sale',$let_2_sale)
    ->with('nle_2_sale',$nle_2_sale)
    ->with('crim_2_sale',$crim_2_sale)
    ->with('civil_2_sale',$civil_2_sale)
    ->with('psyc_2_sale',$psyc_2_sale)
    ->with('nclex_2_sale',$nclex_2_sale)
    ->with('ielts_2_sale',$ielts_2_sale)
    ->with('social_2_sale',$social_2_sale)
    ->with('agri_2_sale',$agri_2_sale)
    ->with('mid_2_sale',$mid_2_sale)
    ->with('online_2_sale',$online_2_sale)
    ->with('let',$let)
    ->with('nle',$nle)
    ->with('crim',$crim)
    ->with('civil',$civil)
    ->with('psyc',$psyc)
    ->with('nclex',$nclex)
    ->with('ielts',$ielts)
    ->with('social',$social)
    ->with('agri',$agri)
    ->with('mid',$mid)
    ->with('online',$online);

}

public function receivable_enrollee_table(){

    $receivable = NovalichesReceivable::all();
    return view ('member.receivable-enrollee')->with('receivable',$receivable);

}
public function receivable_program_table(){

    $let_receivable=NovalichesReceivable::where('program','=','LET')->sum('balance');
    $nle_receivable=NovalichesReceivable::where('program','=','NLE')->sum('balance');
    $crim_receivable=NovalichesReceivable::where('program','=','Criminology')->sum('balance');
    $civil_receivable=NovalichesReceivable::where('program','=','Civil Service')->sum('balance');
    $psyc_receivable=NovalichesReceivable::where('program','=','Psychometrician')->sum('balance');
    $nclex_receivable=NovalichesReceivable::where('program','=','NCLEX')->sum('balance');
    $ielts_receivable=NovalichesReceivable::where('program','=','IELTS')->sum('balance');
    $social_receivable=NovalichesReceivable::where('program','=','Social')->sum('balance');
    $agri_receivable=NovalichesReceivable::where('program','=','Agriculture')->sum('balance');
    $mid_receivable=NovalichesReceivable::where('program','=','Midwifery')->sum('balance');
    $online_receivable=NovalichesReceivable::where('program','=','Online Only')->sum('balance');

    $let=NovalichesReceivable::where('program','=','LET')->count();
    $nle=NovalichesReceivable::where('program','=','NLE')->count();
    $crim=NovalichesReceivable::where('program','=','Criminology')->count();
    $civil=NovalichesReceivable::where('program','=','Civil Service')->count();
    $psyc=NovalichesReceivable::where('program','=','Psychometrician')->count();
    $nclex=NovalichesReceivable::where('program','=','NCLEX')->count();
    $ielts=NovalichesReceivable::where('program','=','IELTS')->count();
    $social=NovalichesReceivable::where('program','=','Social')->count();
    $agri=NovalichesReceivable::where('program','=','Agriculture')->count();
    $mid=NovalichesReceivable::where('program','=','Midwifery')->count();
    $online=NovalichesReceivable::where('program','=','Online Only')->count();

    return view ('member.receivable-program')
    ->with('let_receivable',$let_receivable)
    ->with('nle_receivable',$nle_receivable)
    ->with('crim_receivable',$crim_receivable)
    ->with('civil_receivable',$civil_receivable)
    ->with('psyc_receivable',$psyc_receivable)
    ->with('nclex_receivable',$nclex_receivable)
    ->with('ielts_receivable',$ielts_receivable)
    ->with('social_receivable',$social_receivable)
    ->with('agri_receivable',$agri_receivable)
    ->with('mid_receivable',$mid_receivable)
    ->with('online_receivable',$online_receivable)
    ->with('let',$let)
    ->with('nle',$nle)
    ->with('crim',$crim)
    ->with('civil',$civil)
    ->with('psyc',$psyc)
    ->with('nclex',$nclex)
    ->with('ielts',$ielts)
    ->with('social',$social)
    ->with('agri',$agri)
    ->with('mid',$mid)
    ->with('online',$online);
}

public function books_table(){

$book = NovalichesBooksInventorie::all();
$sale = NovalichesBooksSale::all();

return view ('member.books')->with('book',$book)->with('sale',$sale);

}
public function new_remit(){
    $date = date('M-d-Y');
    $branch = $this->branch;
    return view ('member.new-remit')->with('branch',$branch)->with('date',$date);
}

public function insert_remit(Request $request){

    $input = $request->except(['_token']);
    $author = Auth::user()->name;
    

    if($input['category'] == 'Sales' && $input['season'] == 'Season 1'){
        
        NovalichesRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'season'    => $input['season'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);


        $available = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }
        else{
            NovalichesS1Cash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from season 1 has been remitted.');
    }

    if($input['category'] == 'Sales' && $input['season'] == 'Season 2'){

        NovalichesRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'season'    => $input['season'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);

        $available = NovalichesS2Cash::where('id','=','1')->value('cash');
        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            NovalichesS2Cash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from season 2 has been remitted.');
    }

    if($input['category'] == 'Books'){

        NovalichesRemit::create([
            'date'      => $input['date'],
            'category'  => $input['category'],
            'amount'    => $input['amount'],
            'remarks'   => $input['remarks'],
            'author'    => $author,
        ]);

        $available = NovalichesBookCash::where('id','=','1')->value('cash');
        $total_available = $available - $input['amount'];

        if($total_available < 0 ){

            $total_available = 0;
        }

        else{
            NovalichesBookCash::where('id','=','1')->update([
                'cash'  =>$total_available,
            ]);
        }
        Alert::success('Success!', 'Cash from books has been remitted.');
    }

    return redirect ($this->sbranch.'/new-remit');

}


public function remit(){

    $remit = NovalichesRemit::all();

    return view ('member.remit')->with('remit',$remit);
}

public function clear_enrollee(Request $request){

    $input = $request->except(['_token']);
    $program = $input['program'];

    DB::table($this->sbranch.'_'.$program)->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}
public function clear_sale_season1(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_s1_sales')->truncate();

    NovalichesS1Cash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_sale_season2(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_s2_sales')->truncate();

    NovalichesS2Cash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_receivable(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_receivables')->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_expense(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_expenses')->truncate();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function clear_book(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_books_sales')->truncate();

    NovalichesBookCash::where('id','=','1')->update([
        'cash'  =>  '0.00',
    ]);

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function new_reservation(){
    $branch= $this->branch; 
    $date = date('M-d-Y');
    $program = Program::all();
    return view ('member.new-reservation')->with('branch',$branch)->with('date',$date)->with('program',$program);
}

public function insert_reservation(Request $request){

    $input = $request->except(['_token']);
    $details = explode('*',$input['name']);

    $name = $details[0];
    $school = $details[1];
    $email = $details[2];
    $contact_no = $details[3];
    $id = $details[4];

    $prog = $input['program'];
        if ($prog == 'lets' ){
        $prog = 'LET';
        }
        if ($prog == 'nles' ){
            $prog = 'NLE';
        }
        if ($prog == 'crims' ){
            $prog = 'Criminology';
        }
        if ($prog == 'civils' ){
            $prog = 'Civil Service';
        }
         if ($prog == 'psycs' ){
            $prog = 'Psychometrician';
        }
        if ($prog == 'nclexes' ){
            $prog = 'NCLEX';
        }
        if ($prog == 'ielts' ){
            $prog = 'IELTS';
        }
        if ($prog == 'socials' ){
            $prog = 'Social Work';
        }
        if ($prog == 'agris' ){
            $prog = 'Agriculture';
        }
        if ($prog == 'mids' ){
            $prog = 'Midwifery';
        }

        if ($prog == 'onlines' ){
            $prog = 'Online Only';
        }

    $existent = NovalichesReservation::where('enrollee_id','=',$id)->first();

        if($existent != null){
           
        $old = NovalichesReservation::where('enrollee_id','=',$id)->value('reservation_fee');
        
        $new = $old + $input['amount_paid'];

        NovalichesReservation::where('enrollee_id','=',$id)->update([
            'reservation_fee' => $new,
        ]);

         $season = $input['season'];
        if ($season == 'Season 1'){

        NovalichesS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        NovalichesS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', '1 student has been updated.');
    return redirect ($this->sbranch.'/new-reservation');
        }

    if($existent == null)
    {

    NovalichesReservation::create([
        'enrollee_id'    =>     $id,
        'name'           =>     $name,
        'branch'         =>     $this->branch,
        'program'        =>     $input['program'],
        'prog'           =>     $prog,
        'school'         =>     $school,
        'email'          =>     $email,
        'contact_no'     =>     $contact_no,
        'reservation_fee'=>     $input['amount_paid'],
    ]);

    $season = $input['season'];
    if ($season == 'Season 1'){

        NovalichesS1Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS1Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }


    if ($season == 'Season 2'){

        NovalichesS2Sale::create([
            'date' => $input['date'],
            'name' => $name,
            'program'=> $prog,
            'amount_paid' =>$input['amount_paid'],
            'year'  =>$input['year'],
        ]);

        $ini = NovalichesS2Cash::where('id','=','1')->value('cash');

        $total = $ini + $input['amount_paid'];

        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

    }
    Alert::success('Success!', 'New student has been reserved.');
    return redirect ($this->sbranch.'/new-reservation');
}
}

public function reservation_table(){

    $reserve = NovalichesReservation::all();

    return view ('member.reservation')->with('reserve',$reserve);
}

public function fetch_reserved(){

    $id = Input::get('id');
    $program = Input::get('program');
    

    $fee = NovalichesReservation::where('enrollee_id','=', $id)->where('program','=',$program)->get();
        
        return response()->json($fee);

}

public function clear_reservation(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_reservations')->truncate();


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}


/* Lecturer Evaluation */

public function add_lec(){

    $program = Program::all();

    return view ('member.add-lecturer')->with('program',$program);
}

public function fetch_class(){
    $program = Input::get('program');

        $class = DB::table('classes')->where('program','=',$program)->get();

        return response()->json($class);
        
}

public function fetch_subject(){
    $program = Input::get('program');
    $class = Input::get('class');

        $subject = DB::table('subjects')->where('program','=',$program)->where('class','=',$class)->get();

        return response()->json($subject);
        
}

public function fetch_section(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');

        $section = Section::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('branch','=',$this->branch)->get();

        return response()->json($section);
        
}

public function fetch_lecturer(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');
    $section = Input::get('section');

        $lecturer = NovalichesLecturerAEvaluation::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('section','=',$section)->get();

        return response()->json($lecturer);
        
}

public function fetch_lecturerb(){
    $program = Input::get('program');
    $class = Input::get('class');
    $subject = Input::get('subject');
    $section = Input::get('section');

        $lecturer = NovalichesLecturerBEvaluation::where('program','=',$program)->where('aka_class','=',$class)->where('aka_subject','=',$subject)->where('section','=',$section)->get();

        return response()->json($lecturer);
        
}

public function insert_lecturer(Request $request){

    $input = $request->except(['_token']);

    $class   = explode('*',$input['class']);
    $subject = explode('*',$input['subject']);

    $existent1 = NovalichesLecturerAEvaluation::where('lecturer','=',strtoupper($input['lecturer']))->where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->first();

    $existent2 = NovalichesLecturerBEvaluation::where('lecturer','=',strtoupper($input['lecturer']))
        ->where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->first();

    if($existent1 != null && $existent2 != null){

        Alert::error('Failed!', 'This lecturer was already assigned.');
        return redirect ($this->sbranch.'/add-lecturer');

    }
    if($existent1 == null && $existent2 == null){
    NovalichesLecturerAEvaluation::create([
        'date'      => $input['date'],
        'lecturer'  => strtoupper($input['lecturer']),
        'branch'    => $this->branch,
        'program'   => $input['program'],
        'section'   => $input['section'],
        'class'     => $class[1],
        'aka_class' => $class[0],
        'subject'   => $subject[1],
        'aka_subject'=>$subject[0],
        'review_ambassador'=>strtoupper($input['ambassador']),
        'excellentA'    => 0,
        'goodA'         => 0,
        'fairA'         => 0,
        'poorA'         => 0,
        'verypoorA'     => 0,
        'excellentB'    => 0,
        'goodB'         => 0,
        'fairB'         => 0,
        'poorB'         => 0,
        'verypoorB'     => 0,
        'excellentC'    => 0,
        'goodC'         => 0,
        'fairC'         => 0,
        'poorC'         => 0,
        'verypoorC'     => 0,
        'excellentD'    => 0,
        'goodD'         => 0,
        'fairD'         => 0,
        'poorD'         => 0,
        'verypoorD'     => 0,
        'excellentE'    => 0,
        'goodE'         => 0,
        'fairE'         => 0,
        'poorE'         => 0,
        'verypoorE'     => 0,
        'excellentF'    => 0,
        'goodF'         => 0,
        'fairF'         => 0,
        'poorF'         => 0,
        'verypoorF'     => 0,
        'excellentG'    => 0,
        'goodG'         => 0,
        'fairG'         => 0,
        'poorG'         => 0,
        'verypoorG'     => 0,
    ]);

    NovalichesLecturerBEvaluation::create([
        'date'      => $input['date'],
        'lecturer'  => strtoupper($input['lecturer']),
        'branch'    => $this->branch,
        'program'   => $input['program'],
        'section'   => $input['section'],
        'class'     => $class[1],
        'aka_class' => $class[0],
        'subject'   => $subject[1],
        'aka_subject'=>$subject[0],
        'review_ambassador'=>strtoupper($input['ambassador']),
        'excellentH'    => 0,
        'goodH'         => 0,
        'fairH'         => 0,
        'poorH'         => 0,
        'verypoorH'     => 0,
        'excellentI'    => 0,
        'goodI'         => 0,
        'fairI'         => 0,
        'poorI'         => 0,
        'verypoorI'     => 0,
        'excellentJ'    => 0,
        'goodJ'         => 0,
        'fairJ'         => 0,
        'poorJ'         => 0,
        'verypoorJ'     => 0,
        'excellentK'    => 0,
        'goodK'         => 0,
        'fairK'         => 0,
        'poorK'         => 0,
        'verypoorK'     => 0,
        'excellentL'    => 0,
        'goodL'         => 0,
        'fairL'         => 0,
        'poorL'         => 0,
        'verypoorL'     => 0,
        'excellentM'    => 0,
        'goodM'         => 0,
        'fairM'         => 0,
        'poorM'         => 0,
        'verypoorM'     => 0,
        'excellentN'    => 0,
        'goodN'         => 0,
        'fairN'         => 0,
        'poorN'         => 0,
        'verypoorN'     => 0,
        'excellentO'    => 0,
        'goodO'         => 0,
        'fairO'         => 0,
        'poorO'         => 0,
        'verypoorO'     => 0,
        'excellentP'    => 0,
        'goodP'         => 0,
        'fairP'         => 0,
        'poorP'         => 0,
        'verypoorP'     => 0,
        'excellentQ'    => 0,
        'goodQ'         => 0,
        'fairQ'         => 0,
        'poorQ'         => 0,
        'verypoorQ'     => 0,
    ]);

    $validate = Section::where('program','=',$input['program'])
        ->where('section','=',$input['section'])
        ->where('aka_class','=',$class[0])
        ->where('aka_subject','=',$subject[0])
        ->where('branch','=',$this->branch)
        ->first();
    if($validate == null){
        Section::create([
            'branch'        =>  $this->branch,
            'program'       =>  $input['program'],
            'aka_class'     =>  $class[0],
            'aka_subject'   =>  $subject[0],
            'section'       =>  $input['section'],
        ]);
    }
    Alert::success('Success!', 'New lecturer has been added.');
    return redirect ($this->sbranch.'/add-lecturer');
}
}

public function insert_eval(Request $request){

    $input = $request->except(['_token']); 
    
    $id = $input['id'];
    $qA = $input['qA'];
    $qB = $input['qB'];
    $qC = $input['qC'];
    $qD = $input['qD'];
    $qE = $input['qE'];
    $qF = $input['qF'];
    $qG = $input['qG'];
    $qH = $input['qH'];
    $qI = $input['qI'];
    $qJ = $input['qJ'];
    $qK = $input['qK'];
    $qL = $input['qL'];
    $qM = $input['qM'];
    $qN = $input['qN'];
    $qO = $input['qO'];
    $qP = $input['qP'];
    $qQ = $input['qQ'];

    $evalA = NovalichesLecturerAEvaluation::find($id);
    $evalA->$qA += 1;
    $evalA->$qB += 1;
    $evalA->$qC += 1;
    $evalA->$qD += 1;
    $evalA->$qE += 1;
    $evalA->$qF += 1;
    $evalA->$qG += 1;
    $evalA->save();

    $evalB = NovalichesLecturerBEvaluation::find($id);
    $evalB->$qH += 1;
    $evalB->$qI += 1;
    $evalB->$qJ += 1;
    $evalB->$qK += 1;
    $evalB->$qL += 1;
    $evalB->$qM += 1;
    $evalB->$qN += 1;
    $evalB->$qO += 1;
    $evalB->$qP += 1;
    $evalB->$qQ += 1;
    $evalB->save();

    NovalichesComment::create([
        'name'      =>  $input['student'],
        'lecturer'  =>  $input['lecturer'],
        'like'      =>  $input['like'],
        'dislike'   =>  $input['dislike'],
        'comment'   =>  $input['comment'],
        'branch'    =>  $this->branch,
        'program'   =>  $input['program'],
        'section'   =>  $input['section'],
        'class'     =>  $input['class'],
        'subject'   =>  $input['subject'],
        'date'      =>  $input['date'],
    ]);

    return response()->json([
            'success' => true,
            'message' => '1 has been lecturer has been evaluated',
        ]);

}

public function eval_lec(){

    $program = Program::all();

    return view ('member.evaluate-lecturer')->with('program',$program);;
}

public function lec_eval(){

    $program = Program::all();
    $comment = NovalichesComment::all();
    return view ('member.lecturer-evaluation')->with('program',$program)->with('comment',$comment);
}

public function clear_lecturers(Request $request){

    $input = $request->except(['_token']);

    NovalichesLecturerAEvaluation::truncate();
    NovalichesLecturerBEvaluation::truncate();
    NovalichesComment::truncate();
    Section::where('branch','=','Novaliches')->delete();

    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}





public function today(){

    $branch= $this->branch; 
    $date = date('M-d-Y');

    $let_sale = NovalichesS1Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid');

    $let_book = NovalichesBooksSale::where('program','=','LET')->where('date','=',$date)->sum('amount');

    $nle_sale = NovalichesS1Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid');

    $nle_book = NovalichesBooksSale::where('program','=','NLE')->where('date','=',$date)->sum('amount');

    $crim_sale = NovalichesS1Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid');

    $crim_book = NovalichesBooksSale::where('program','=','Criminology')->where('date','=',$date)->sum('amount');

    $civil_sale = NovalichesS1Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid');

    $civil_book = NovalichesBooksSale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount');

    $psyc_sale = NovalichesS1Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid');

    $psyc_book = NovalichesBooksSale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount');

    $nclex_sale = NovalichesS1Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid');

    $nclex_book = NovalichesBooksSale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount');

    $ielts_sale = NovalichesS1Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid');

    $ielts_book = NovalichesBooksSale::where('program','=','IELTS')->where('date','=',$date)->sum('amount');

    $social_sale = NovalichesS1Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid');

    $social_book = NovalichesBooksSale::where('program','=','Social Work')->where('date','=',$date)->sum('amount');

    $agri_sale = NovalichesS1Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid');

    $agri_book = NovalichesBooksSale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount');

    $mid_sale = NovalichesS1Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid');

    $mid_book = NovalichesBooksSale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount');

    $online_sale = NovalichesS1Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid');

    $online_book = NovalichesBooksSale::where('program','=','Online Only')->where('date','=',$date)->sum('amount');

    return view ('member.today-report')->with('branch',$branch)->with('date',$date)
    ->with('let_sale',$let_sale)->with('let_book',$let_book)
    ->with('nle_sale',$nle_sale)->with('nle_book',$nle_book)
    ->with('crim_sale',$crim_sale)->with('crim_book',$crim_book)
    ->with('civil_sale',$civil_sale)->with('civil_book',$civil_book)
    ->with('psyc_sale',$psyc_sale)->with('psyc_book',$psyc_book)
    ->with('nclex_sale',$nclex_sale)->with('nclex_book',$nclex_book)
    ->with('ielts_sale',$ielts_sale)->with('ielts_book',$ielts_book)
    ->with('social_sale',$social_sale)->with('social_book',$social_book)
    ->with('agri_sale',$agri_sale)->with('agri_book',$agri_book)
    ->with('mid_sale',$mid_sale)->with('mid_book',$mid_book)
    ->with('online_sale',$online_sale)->with('online_book',$online_book);

}

public function yesterday(){
 
 $branch= $this->branch; 
    $date = date('M-d-Y',strtotime("-1 days"));


    $let_sale = NovalichesS1Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','LET')->where('date','=',$date)->sum('amount_paid');

    $let_book = NovalichesBooksSale::where('program','=','LET')->where('date','=',$date)->sum('amount');

    $nle_sale = NovalichesS1Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NLE')->where('date','=',$date)->sum('amount_paid');

    $nle_book = NovalichesBooksSale::where('program','=','NLE')->where('date','=',$date)->sum('amount');

    $crim_sale = NovalichesS1Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Criminology')->where('date','=',$date)->sum('amount_paid');

    $crim_book = NovalichesBooksSale::where('program','=','Criminology')->where('date','=',$date)->sum('amount');

    $civil_sale = NovalichesS1Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount_paid');

    $civil_book = NovalichesBooksSale::where('program','=','Civil Service')->where('date','=',$date)->sum('amount');

    $psyc_sale = NovalichesS1Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount_paid');

    $psyc_book = NovalichesBooksSale::where('program','=','Psychometrician')->where('date','=',$date)->sum('amount');

    $nclex_sale = NovalichesS1Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount_paid');

    $nclex_book = NovalichesBooksSale::where('program','=','NCLEX')->where('date','=',$date)->sum('amount');

    $ielts_sale = NovalichesS1Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','IELTS')->where('date','=',$date)->sum('amount_paid');

    $ielts_book = NovalichesBooksSale::where('program','=','IELTS')->where('date','=',$date)->sum('amount');

    $social_sale = NovalichesS1Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Social Work')->where('date','=',$date)->sum('amount_paid');

    $social_book = NovalichesBooksSale::where('program','=','Social Work')->where('date','=',$date)->sum('amount');

    $agri_sale = NovalichesS1Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount_paid');

    $agri_book = NovalichesBooksSale::where('program','=','Agriculture')->where('date','=',$date)->sum('amount');

    $mid_sale = NovalichesS1Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount_paid');

    $mid_book = NovalichesBooksSale::where('program','=','Midwifery')->where('date','=',$date)->sum('amount');

    $online_sale = NovalichesS1Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid') + NovalichesS2Sale::where('program','=','Online Only')->where('date','=',$date)->sum('amount_paid');

    $online_book = NovalichesBooksSale::where('program','=','Online Only')->where('date','=',$date)->sum('amount');

    return view ('member.yesterday-report')->with('branch',$branch)->with('date',$date)
    ->with('let_sale',$let_sale)->with('let_book',$let_book)
    ->with('nle_sale',$nle_sale)->with('nle_book',$nle_book)
    ->with('crim_sale',$crim_sale)->with('crim_book',$crim_book)
    ->with('civil_sale',$civil_sale)->with('civil_book',$civil_book)
    ->with('psyc_sale',$psyc_sale)->with('psyc_book',$psyc_book)
    ->with('nclex_sale',$nclex_sale)->with('nclex_book',$nclex_book)
    ->with('ielts_sale',$ielts_sale)->with('ielts_book',$ielts_book)
    ->with('social_sale',$social_sale)->with('social_book',$social_book)
    ->with('agri_sale',$agri_sale)->with('agri_book',$agri_book)
    ->with('mid_sale',$mid_sale)->with('mid_book',$mid_book)
    ->with('online_sale',$online_sale)->with('online_book',$online_book);

}

public function budget_record(){

    $branch= $this->branch; 
    $budget = NovalichesBudget::all();

    return view ('member.budget')->with('budget',$budget)->with('branch',$branch);
}

public function clear_budget(Request $request){

$input = $request->except(['_token']);

    DB::table($this->sbranch.'_budgets')->truncate();


    return response()->json([
            'success' => true,
            'message' => 'All data cleared.',
        ]);
}

public function delete_sale1($id){

    $val = NovalichesS1Sale::where('id','=',$id)->value('amount_paid');
    $ini = NovalichesS1Cash::where('id','=','1')->value('cash');
    $bal = NovalichesS1Sale::where('id','=',$id)->value('balance');
    $name = NovalichesS1Sale::where('id','=',$id)->value('name');
    $id1 = NovalichesS1Sale::where('id','=', $id)->value('student_id');
    //program
    $program1 = NovalichesS1Sale::where('id','=',$id)->value('program');
    $program = Program::where('program_name','=',$program1)->value('aka');
    $studentIds1 = DB::table($this->branch.'_'. $program)->where('id','=', $id1)->value('id');

          //save student info to api
          $studentForApi =  DB::table($this->branch."_".$program)->where('id','=',$studentIds1)->where('status','=','Enrolled')->first();
        

    //date for scorecard table
    $transDate = NovalichesS1Sale::where('id','=',$id)->value('date');
    
    //if retake or 1st timer
    $dis_category = NovalichesS1Sale::where('id','=',$id)->value('discount_category');
   
    

    if ($bal != null){

        $receivable = NovalichesReceivable::where('name','=',$name)->where('season','=','Season 1')->value('balance');
        
        $new_bal = $receivable + $val;

        NovalichesReceivable::where('name','=',$name)->where('season','=','Season 1')->update([
        'balance' => $new_bal,
        ]);
         
    }
    else{
        $reservation = NovalichesReservation::where('name','=',$name)->value('reservation_fee');

        $new_bal = $reservation - $val;

        $reservation = NovalichesReservation::where('name','=',$name)->update([
            'reservation_fee' => $new_bal,
        ]);///end for reservation
    }//end of else 

   
    if($val <= $ini){

        $sale = NovalichesS1Sale::findorfail($id);
        $sale->delete();
    
        $total = $ini - $val;
    
        NovalichesS1Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

     



    //update score card
    //check student overall transaction in novalichesS1sale
    $student_allBal = NovalichesS1Sale::where('student_id','=',$studentIds1)->count();

    
  
    //major
    $studentInfo = DB::table($this->branch.'_'. $program)->where('id','=', $studentIds1)->value('major');

    //end of check if retaker or first timer
    $remark = "1st timer";
        if (strpos(strtolower($dis_category), 'bounce') !== false || strpos(strtolower($dis_category), 'retaker') !== false|| strpos(strtolower($dis_category), 'retake') !== false) {
            $remark = 'retake';
    }//end of check if retaker or first timer


    //if  stud_allball == 0
    if($student_allBal === 0){

    
        NovalichesReceivable::where('name','=',$name)->where('season','=','Season 1')->update([
            'balance' => 0,
            ]);
    //major
    DB::table($this->branch.'_'. $program)->where('id','=', $studentIds1)->update([
        'status' => null,
    ]);
    
   
// return $program1 . " " . $program . " " . $studentInfo;
        if($program1 == "LET"){
            $prev =  NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
            $score = $prev[strtolower($studentInfo)] - 1;
            
                NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                    strtolower($studentInfo)  => $score,
            ]);
        }
        else{

            if($remark == "retake") {
                $prev =  NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_retakers'] - 1;
                    NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_retakers' => $score,
                ]);
            }else{
                $prev =  NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_1stTimers'] - 1;
                    NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_1stTimers' => $score,
                ]);
            }
        }
   

        }//end of if  student balance == 0
//end of update scorecard
  



    Alert::success('Success!', '1 sales record has been deleted.');
    }//end of if $val <= $ini
    else{
    Alert::error('Failed!', 'Insufficient Cash.');

    }

    

    return redirect ($this->sbranch.'/sales-enrollee');
}

public function delete_sale2($id){
    $val = NovalichesS2Sale::where('id','=',$id)->value('amount_paid');
    $ini = NovalichesS2Cash::where('id','=','1')->value('cash');
    $bal = NovalichesS2Sale::where('id','=',$id)->value('balance');
    $name = NovalichesS2Sale::where('id','=',$id)->value('name');
    $id1 = NovalichesS2Sale::where('id','=', $id)->value('student_id');
    //program
    $program1 = NovalichesS2Sale::where('id','=',$id)->value('program');
    $program = Program::where('program_name','=',$program1)->value('aka');
    $studentIdS2 = DB::table($this->branch.'_'. $program)->where('id','=', $id1)->value('id');

          //save student info to api
          $studentForApi =  DB::table($this->branch."_".$program)->where('id','=',$studentIdS2)->where('status','=','Enrolled')->first();
        

    //date for scorecard table
    $transDate = NovalichesS2Sale::where('id','=',$id)->value('date');
    
    //if retake or 1st timer
    $dis_category = NovalichesS2Sale::where('id','=',$id)->value('discount_category');
   
    

    if ($bal != null){

        $receivable = NovalichesReceivable::where('name','=',$name)->where('season','=','Season 2')->value('balance');
        
        $new_bal = $receivable + $val;

        NovalichesReceivable::where('name','=',$name)->where('season','=','Season 2')->update([
        'balance' => $new_bal,
        ]);
         
    }
    else{
        $reservation = NovalichesReservation::where('name','=',$name)->value('reservation_fee');

        $new_bal = $reservation - $val;

        $reservation = NovalichesReservation::where('name','=',$name)->update([
            'reservation_fee' => $new_bal,
        ]);///end for reservation
    }//end of else 

   
    if($val <= $ini){

        $sale = NovalichesS2Sale::findorfail($id);
        $sale->delete();
    
        $total = $ini - $val;
    
        NovalichesS2Cash::where('id','=','1')->update([
            'cash' => $total,
        ]);

     



    //update score card
    //check student overall transaction in novalichesS2sale
    $student_allBal = NovalichesS2Sale::where('student_id','=',$studentIdS2)->count();

    
  
    //major
    $studentInfo = DB::table($this->branch.'_'. $program)->where('id','=', $studentIdS2)->value('major');

    //end of check if retaker or first timer
    $remark = "1st timer";
        if (strpos(strtolower($dis_category), 'bounce') !== false || strpos(strtolower($dis_category), 'retaker') !== false|| strpos(strtolower($dis_category), 'retake') !== false) {
            $remark = 'retake';
    }//end of check if retaker or first timer


    //if  stud_allball == 0
    if($student_allBal === 0){

    
        NovalichesReceivable::where('name','=',$name)->where('season','=','Season 2')->update([
            'balance' => 0,
            ]);
    //major
    DB::table($this->branch.'_'. $program)->where('id','=', $studentIdS2)->update([
        'status' => null,
    ]);
    
   
// return $program1 . " " . $program . " " . $studentInfo;
        if($program1 == "LET"){
            $prev =  NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
            $score = $prev[strtolower($studentInfo)] - 1;
            
                NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                    strtolower($studentInfo)  => $score,
            ]);
        }
        else{

            if($remark == "retake") {
                $prev =  NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_retakers'] - 1;
                    NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_retakers' => $score,
                ]);
            }else{
                $prev =  NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->first();
                $score = $prev[$program.'_1stTimers'] - 1;
                    NovalichesScoreCards::where('date','=',date("M-d-Y",strtotime($transDate)))->update([
                        $program.'_1stTimers' => $score,
                ]);
            }
        }
   



    }
    Alert::success('Success!', '1 sales record has been deleted.');
    }//end of if $val <= $ini
    else{
    Alert::error('Failed!', 'Insufficient Cash.');

    }

    

    return redirect ($this->sbranch.'/sales-enrollee');
    
}
public function csv_enrollee(Request $request){

            $input = $request->except(['_token']);
            $program = $input['program'];
            $prog = "";
            if($program == 'LET'){
            $prog = 'let';
            $program = 'lets';
            $db = 'NovalichesLet';
            }
            if($program == 'NLE'){
                $prog = 'nle';
                $program = 'nles';
                $db = 'NovalichesNle';
            }
            if($program == 'Criminology'){
                $prog = 'crim';
                $program = 'crims';
                $db = 'NovalichesCrim';
            }
            if($program == 'Civil Service'){
                $prog = 'civil';
                $program = 'civils';
                $db = 'NovalichesCivil';
            }

            if($program == 'Psychometrician'){
                $prog = 'psyc';
                $program = 'psycs';
                $db = 'NovalichesPsyc';
            }
            if($program == 'NCLEX'){
                $prog = 'nclex';
                $program = 'nclexes';
                $db = 'NovalichesPsyc';
            }
            if($program == 'IELTS'){
                $prog = 'ielt';
                $program = 'ielts';
                $db = 'NovalichesIelt';
            }
            if($program == 'Social Work'){
                $prog = 'social';
                $program = 'socials';
                $db = 'NovalichesSocial';
            }
            if($program == 'Agriculture'){
                $prog = 'agri';
                $program = 'agris';
                $db = 'NovalichesAgri';
            }
            if($program == 'Midwifery'){
                $prog = 'mid';
                $program = 'mids';
                $db = 'NovalichesMid';
            }
            if($program == 'Online Only'){
                $prog = 'online';
                $program = 'onlines';
                $db = 'NovalichesOnline';
            }



            $input['csv_enrollee'] = null;
            $csv="";
            $file = "";
            if($request->hasFile('csv'))
            {
            $extension = $request->csv;
            $extension = $request->csv->getClientOriginalExtension(); // getting excel extension
            $uploader = Auth::user()->branch;
            $input['csv'] = 'uploads/csv/'.''.$uploader;
            $csv = uniqid().'_'.time().'_'.date('Ymd').'.'.$extension;
            $request->csv->move($input['csv'], $csv); 

            $file = $input['csv']."/".$csv;
             }
            else{
                $file = Auth::user()->csv;
            }

            if ($input['program'] == 'LET'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesLet ();
                    $cbrc_id = NovalichesLet::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){
                     
                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                    else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'NLE'){
            if (($handle = fopen (public_path () .'/uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesNle ();
                    $cbrc_id = NovalichesNle::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }

                fclose ( $handle );
            }
        }


        if ($input['program'] == 'Criminology'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesCrim ();
                      $cbrc_id = NovalichesCrim::where('cbrc_id','=',$data[0])->first();
                        if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }

                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Civil Service'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesCivil ();
                     $cbrc_id = NovalichesCivil::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Psychometrician'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesPsyc ();
                    $cbrc_id = NovalichesPsyc::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'NCLEX'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesNclex ();
                     $cbrc_id = NovalichesNclex::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'IELTS'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesIelt ();
                     $cbrc_id = NovalichesIelt::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Social Work'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesSocial ();
                     $cbrc_id = NovalichesSocial::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Agriculture'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesAgri ();
                       $cbrc_id = NovalichesAgri::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Midwifery'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesMid ();
                      $cbrc_id = NovalichesMid::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                 else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

        if ($input['program'] == 'Online Only'){
            if (($handle = fopen ('uploads/csv/'.$uploader.'/'.$csv, 'r' )) !== FALSE) {

                //fetching data from csv file
                while ( ($data = fgetcsv ( $handle, 10000, ',' )) !== FALSE ) {
                    $csv_data = new NovalichesOnline ();
                     $cbrc_id = NovalichesOnline::where('cbrc_id','=',$data[0])->first();
                    if($cbrc_id == null){

                    $csv_data->cbrc_id = $data [0];
                    $csv_data->last_name = $data [1];
                    $csv_data->first_name = $data [2];
                    $csv_data->middle_name = $data [3];
                    $csv_data->birthdate=$data[4];
                    $csv_data->contact_no=$data[5];
                    $csv_data->email = $data[6];
                    $csv_data->address = $data[7];
                    $csv_data->school = $data[8];
                    $csv_data->course = $data[9];
                    $csv_data->major = $data[10];
                    $csv_data->program = $program;
                    $csv_data->take = $data[11];
                    $csv_data->noa_no = $data[12];
                    $csv_data->section = $data[13];
                    $csv_data->branch = Auth::user()->branch;
                    $csv_data->contact_person=$data[14];
                    $csv_data->contact_details=$data[15];
                    $csv_data->registration='Walk-in';
                    $csv_data->save();
                }
                else{
                        Alert::error('Failed!', 'Some enrtries has been existed.');
                        }
                    }
                fclose ( $handle );
            }
        }

            
            Alert::success('Success!', 'Your file has been successfully imported.');

            return redirect ()->back(); 
    }




// =================== Register New Employee =============================

public function add_employee(){
    $branch= $this->branch; 
    $program = Program::all();
    $branch_name = Branch::all();
    return view('member.add-employee')->with('branch',$branch)->with('program',$program)->with('branch_name',$branch_name);
}

//================ edit information of employee  ===================================

public function update_employee(Request $request){ 
    $input = $request->except(['_token']);    

     if ($request->hasfile('cover_image') == true) {
    // # code...

     $raw = NovalichesEmployee::find($input['update_id']);
     $oldpath = $raw->cover_image;

   
    File::Delete('cover_images',$oldpath);
    //new image upload
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $filename. '_'.time().'.webp';
            $path = $request->file('cover_image')->move('cover_images',$fileNameToStore);


            DB::table($this->sbranch.'_employees')->where('id','=',$input['update_id'])->update([

            'last_name'             => $input['last_name'],
            'first_name'            => $input['first_name'],
            'middle_name'           => $input['middle_name'],
            'birthdate'             =>  $input['birthdate'],
            'gender'                => $input['gender'],
            'status'                => $input['status'],
            'address'               => $input['address'],
            'email'                 => $input['email'],
            'contact_no'            => $input['contact_no'],
            'contact_person'        => $input['contact_person'],
            'contact_details'       => $input['contact_details'],
            'position'              =>$input['position'],
            'employment_status'     =>$input['employment_status'],
            'rate'                  => $input['rate'],
            'sss'                   => $input['sss'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'],
            'branch_name'           => $input['branch_name'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'],
            'cover_image'           => $fileNameToStore

            ]);

            Alert::success('Success!', 'Updated employee Record');

    }

    else {
            DB::table($this->sbranch.'_employees')->where('id','=',$input['update_id'])->update([

            'last_name'             => $input['last_name'],
            'first_name'            => $input['first_name'],
            'middle_name'           => $input['middle_name'],
            'birthdate'             =>  $input['birthdate'],
            'gender'                => $input['gender'],
            'status'                => $input['status'],
            'address'               => $input['address'],
            'email'                 => $input['email'],
            'contact_no'            => $input['contact_no'],
            'contact_person'        => $input['contact_person'],
            'contact_details'       => $input['contact_details'],
            'employment_status'     =>$input['employment_status'],
            'position'              =>$input['position'],
            'rate'                  => $input['rate'],
            'sss'                   => $input['sss'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'],
            'branch_name'           => $input['branch_name'],
            'phil_health'           => $input['phil_health'],
            'pag_ibig'              => $input['pag_ibig'],
            'tin'                   => $input['tin'] 

        ]); 

        Alert::success('Success!', 'Updated employee Record');
     
    }
}

// =================== View Employeee Record =================================
public function employee_record(){
        $employee = NovalichesEmployee::all();
        $branch_name = Branch::all();
        return view('member.employee-record')->with('employee',$employee)->with('branch_name',$branch_name);
}


// ================ insert data of employee ======================================  

public function insert_employee(Request $request) {
    $input = $request->except(['_token']); 
        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just ext
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $fileNameToStore = $filename. '_'.time().'.webp';
            $path = $request->file('cover_image')->move('cover_images',$fileNameToStore);
        }
        else{
            $path = 'NO IMAGE';
        }
            $data = new NovalichesEmployee;
            $data->employee_no = $input['employee_no'];
            $data->last_name = strtoupper($input['last_name']);
            $data->first_name = strtoupper($input['first_name']);
            $data->middle_name = strtoupper($input['middle_name']);
            $data->birthdate = $input['birthdate'];
            $data->gender = strtoupper($input['gender']);
            $data->status = strtoupper($input['status']);
            $data->address = strtoupper($input['address']);
            $data->email = strtoupper($input['email']);
            $data->contact_no = $input['contact_no'];
            $data->contact_person = strtoupper($input['contact_person']);
            $data->contact_details = strtoupper($input['contact_details']);
            $data->position = strtoupper($input['position']);
            $data->employment_status = strtoupper($input['employmentStatus']);
            $data->rate = $input['rate'];
            $data->date_hired = $input['date_hired'];
            $data->sss = $input['sss'];
            $data->phil_health = $input['phil_health'];
            $data->pag_ibig = $input['pag_ibig'];
            $data->tin = $input['tin']; 
            $data->branch_name = $input['branch_name']; 
            $data->cover_image = $fileNameToStore;
            $data->save();
            if ($data->save()){
               $test =  NovalichesEmployee::latest()->first();
               return response()->json($test);
                Alert::success('Success!', 'Added employee Record');

    }
}

// ============================= delete Employee Record  ==================================

    public function delete_employee($id) {
    $data = NovalichesEmployee::findorfail($id);

    $img = $data->cover_image;
    File::delete('cover_images/'.$img);
    $data->delete();
    Alert::success('Success!', ' Successfully record deleted');
    return redirect()->back();
    }

    //======================= View employee Member Dashboard  =====================

    public function view_employee(){
         $employee = NovalichesEmployee::all();
        return view('member.view-employee-record')->with('employee',$employee);
    
    } 
    //====================== Student Generate Id's ===================================
    public function student_id(){
         $branch= $this->branch; 
        $program = Program::all();


        return view('admin.student-id')->with('branch',$branch)->with('program',$program);
    }


    // =================== Bulletin Board ===============================================
        public function Bulletin(){  
            
            return view('admin.bulletin');

        }
    // Newsfeed
         public function newsfeed()
         {
             return view('admin.newsfeed');
         }    

// =================== taskHistory ====================================
         public function taskHistory(){
               return view ('admin.taskHistory'); 
        }

    //====================== Book transfer and purchase  ===============================
        public function fetch_branches($bn){
            $branch = Branch::where('branch_name','!=',$bn)->get();
            return response()->json($branch);
        }

    //======== mike ScoreCard and Finacial Report Controller  ==============================



public function scorecard($season){
    
    $scorecard = NovalichesScoreCards::all()->where('season',$season);
    $scorecard_last = NovalichesScoreCards::where('season',$season)->orderby('id','desc')->first();
    //this is sunday variable
    $Sun = date("D" , strtotime('2/17/2019'));

    $weekly_beed = 0;
    $weekly_bsed = 0;
    $weekly_let = 0;
    $weekly_math = 0;
    $weekly_tle = 0;
    $weekly_english = 0;
    $weekly_filipino = 0;
    $weekly_biosci = 0;
    $weekly_physci = 0;
    $weekly_socsci = 0;
    $weekly_mapeh = 0;
    $weekly_values = 0;
    $weekly_afa = 0;
    $weekly_ufo = 0;

    $weekly_tot_nle = 0;
    $weekly_tot_nle_1stTimers = 0;
    $weekly_tot_nle_retakers = 0;
    
    $weekly_tot_crim = 0;
    $weekly_tot_crim_1stTimers = 0;
    $weekly_tot_crim_retakers = 0;
    
    $weekly_tot_civil = 0;
    $weekly_tot_civil_1stTimers = 0;
    $weekly_tot_civil_retakers = 0;
    
    $weekly_tot_psyc = 0;
    $weekly_tot_psyc_1stTimers = 0;
    $weekly_tot_psyc_retakers = 0;
    
    $weekly_tot_nclex = 0;
    $weekly_tot_nclex_1stTimers = 0;
    $weekly_tot_nclex_retakers = 0;
    
    $weekly_tot_ielts = 0;
    $weekly_tot_ielts_1stTimers = 0;
    $weekly_tot_ielts_retakers = 0;
    
    $weekly_tot_social = 0;
    $weekly_tot_social_1stTimers = 0;
    $weekly_tot_social_retakers = 0;
    
    $weekly_tot_agri = 0;
    $weekly_tot_agri_1stTimers = 0;
    $weekly_tot_agri_retakers = 0;
    
    $weekly_tot_mid = 0;
    $weekly_tot_mid_1stTimers = 0;
    $weekly_tot_mid_retakers = 0;
    
    $weekly_tot_online = 0;
    $weekly_tot_online_1stTimers = 0;
    $weekly_tot_online_retakers = 0;

    $tot_beed = 0;
    $fin_tot_let = 0;
    $tot_bsed = 0;
    $fin_tot_bsed = 0;
    $tot_math = 0;
    $tot_tle = 0;
    $tot_english = 0;
    $tot_filipino= 0;
    $tot_biosci = 0;
    $tot_physci = 0;
    $tot_mapeh = 0;
    $tot_socsci = 0;
    $tot_values= 0;
    $tot_afa = 0;
    $tot_ufo = 0;
    $tot_let = 0;

    $tot_nle_1stTimers = 0;
    $tot_nle_retakers = 0;
    $tot_nle = 0;

    $tot_crim_1stTimers = 0;
    $tot_crim_retakers = 0;
    $tot_crim = 0;

    $tot_civil_1stTimers = 0;
    $tot_civil_retakers = 0;
    $tot_civil = 0;

    $tot_psyc_1stTimers = 0;
    $tot_psyc_retakers = 0;
    $tot_psyc = 0;

    $tot_nclex_1stTimers = 0;
    $tot_nclex_retakers = 0;
    $tot_nclex = 0;

    $tot_ielts_1stTimers = 0;
    $tot_ielts_retakers = 0;
    $tot_ielts = 0;

    $tot_social_1stTimers = 0;
    $tot_social_retakers = 0;
    $tot_social = 0;    

    $tot_agri_1stTimers = 0;
    $tot_agri_retakers = 0;
    $tot_agri = 0;
    
    $tot_mid_1stTimers = 0;
    $tot_mid_retakers = 0;
    $tot_mid = 0;
    
    $tot_online_1stTimers = 0;
    $tot_online_retakers = 0;
    $tot_online = 0;

  foreach($scorecard as $sub_score){

        
        $tot_beed = $tot_beed + $sub_score['beed'];
        $tot_math = $tot_math + $sub_score['math'];
        $tot_tle = $tot_tle + $sub_score['tle'];
        $tot_english = $tot_english + $sub_score['english'];
        $tot_filipino = $tot_filipino + $sub_score['filipino'];
        $tot_biosci = $tot_biosci + $sub_score['biosci'];
        $tot_physci = $tot_physci + $sub_score['physci'];
        $tot_mapeh = $tot_mapeh + $sub_score['mapeh'];
        $tot_socsci = $tot_socsci + $sub_score['socsci'];
        $tot_values = $tot_values + $sub_score['values'];
        $tot_afa = $tot_afa + $sub_score['afa'];
        $tot_ufo = $tot_ufo + $sub_score['ufo'];
        
        $tot_bsed =     $sub_score['math']    +
                        $sub_score['tle']     +
                        $sub_score['english'] +
                        $sub_score['filipino']+
                        $sub_score['biosci']  +
                        $sub_score['physci']  +
                        $sub_score['socsci']  +
                        $sub_score['mapeh']   +
                        $sub_score['values']  +
                        $sub_score['afa']     +
                        $sub_score['ufo'];


        $tot_let =  $sub_score['beed'] + $tot_bsed;

        $fin_tot_bsed = $fin_tot_bsed + $tot_bsed;
        $fin_tot_let = $fin_tot_let +  $tot_let;

        $tot_nle_1stTimers += $sub_score['nles_1stTimers'];
        $tot_nle_retakers += $sub_score['nles_retakers'];
        $tot_nle = $tot_nle_1stTimers + $tot_nle_retakers;

        $tot_crim_1stTimers += $sub_score['crims_1stTimers'];
        $tot_crim_retakers += $sub_score['crims_retakers'];
        $tot_crim = $tot_crim_1stTimers + $tot_crim_retakers;

        $tot_civil_1stTimers += $sub_score['civils_1stTimers'];
        $tot_civil_retakers += $sub_score['civils_retakers'];
        $tot_civil = $tot_civil_1stTimers + $tot_civil_retakers;

        $tot_psyc_1stTimers += $sub_score['psycs_1stTimers'];
        $tot_psyc_retakers += $sub_score['psycs_retakers'];
        $tot_psyc = $tot_psyc_1stTimers + $tot_psyc_retakers;

        $tot_nclex_1stTimers += $sub_score['nclexes_1stTimers'];
        $tot_nclex_retakers += $sub_score['nclexes_retakers'];
        $tot_nclex = $tot_nclex_1stTimers + $tot_nclex_retakers;

        $tot_ielts_1stTimers += $sub_score['ielts_1stTimers'];
        $tot_ielts_retakers += $sub_score['ielts_retakers'];
        $tot_ielts = $tot_ielts_1stTimers + $tot_ielts_retakers;

        $tot_social_1stTimers += $sub_score['socials_1stTimers'];
        $tot_social_retakers += $sub_score['socials_retakers'];
        $tot_social = $tot_social_1stTimers + $tot_social_retakers;

        $tot_agri_1stTimers += $sub_score['agris_1stTimers'];
        $tot_agri_retakers += $sub_score['agris_retakers'];
        $tot_agri = $tot_agri_1stTimers + $tot_agri_retakers;

        $tot_mid_1stTimers += $sub_score['mids_1stTimers'];
        $tot_mid_retakers += $sub_score['mids_retakers'];
        $tot_mid = $tot_mid_1stTimers + $tot_mid_retakers;

        $tot_online_1stTimers += $sub_score['onlines_1stTimers'];
        $tot_online_retakers += $sub_score['onlines_retakers'];
        $tot_online = $tot_online_1stTimers + $tot_online_retakers;
    }

    $branch= $this->branch; 
    $date = date('M-d-Y');

    //return $scorecard;

    return view('member.scorecard-season1')
                ->with('scorecard',$scorecard)
                ->with('scorecard_last',$scorecard_last)
                ->with('branch',$branch)
                ->with('date',$date)
                ->with('Sun',$Sun)
                ->with('season',$season)
                

                ->with('weekly_beed',$weekly_beed)
                ->with('weekly_bsed',$weekly_bsed)
                ->with('weekly_let',$weekly_let)
                ->with('weekly_math',$weekly_math)
                ->with('weekly_tle',$weekly_tle)
                ->with('weekly_english',$weekly_english)
                ->with('weekly_filipino',$weekly_filipino)
                ->with('weekly_biosci',$weekly_biosci)
                ->with('weekly_physci',$weekly_physci)
                ->with('weekly_socsci',$weekly_socsci)
                ->with('weekly_mapeh',$weekly_mapeh)
                ->with('weekly_values',$weekly_values)
                ->with('weekly_afa',$weekly_afa)
                ->with('weekly_ufo',$weekly_ufo)

                ->with('weekly_tot_nle',$weekly_tot_nle)
                ->with('weekly_tot_nle_1stTimers',$weekly_tot_nle_1stTimers)
                ->with('weekly_tot_nle_retakers',$weekly_tot_nle_retakers)

                ->with('weekly_tot_crim',$weekly_tot_crim)
                ->with('weekly_tot_crim_1stTimers',$weekly_tot_crim_1stTimers)
                ->with('weekly_tot_crim_retakers',$weekly_tot_crim_retakers)

                ->with('weekly_ufo',$weekly_tot_civil)
                ->with('weekly_tot_civil_1stTimers',$weekly_tot_civil_1stTimers)
                ->with('weekly_tot_civil_retakers',$weekly_tot_civil_retakers)

                ->with('weekly_tot_psyc',$weekly_tot_psyc)
                ->with('weekly_tot_psyc_1stTimers',$weekly_tot_psyc_1stTimers)
                ->with('weekly_tot_psyc_retakers',$weekly_tot_psyc_retakers)

                ->with('weekly_tot_nclex',$weekly_tot_nclex)
                ->with('weekly_tot_nclex_1stTimers',$weekly_tot_nclex_1stTimers)
                ->with('weekly_tot_nclex_retakers',$weekly_tot_nclex_retakers)

                ->with('weekly_tot_ielts',$weekly_tot_ielts)
                ->with('weekly_tot_ielts_1stTimers',$weekly_tot_ielts_1stTimers)
                ->with('weekly_tot_ielts_retakers',$weekly_tot_ielts_retakers)
                
                ->with('weekly_tot_social',$weekly_tot_social)
                ->with('weekly_tot_social_1stTimers',$weekly_tot_social_1stTimers)
                ->with('weekly_tot_social_retakers',$weekly_tot_social_retakers)
                
                ->with('weekly_tot_agri',$weekly_tot_agri)
                ->with('weekly_tot_agri_1stTimers',$weekly_tot_agri_1stTimers)
                ->with('weekly_tot_agri_retakers',$weekly_tot_agri_retakers)
                
                ->with('weekly_tot_mid',$weekly_tot_mid)
                ->with('weekly_tot_mid_1stTimers',$weekly_tot_mid_1stTimers)
                ->with('weekly_tot_mid_retakers',$weekly_tot_mid_retakers)
                
                ->with('weekly_tot_online',$weekly_tot_online)
                ->with('weekly_tot_online_1stTimers',$weekly_tot_online_1stTimers)
                ->with('weekly_tot_online_retakers',$weekly_tot_online_retakers)
                
                ->with('tot_beed',$tot_beed)
                ->with('fin_tot_let',$fin_tot_let)
                ->with('tot_bsed',$tot_bsed)
                ->with('fin_tot_bsed',$fin_tot_bsed)
                ->with('tot_math',$tot_math)
                ->with('tot_tle',$tot_tle)
                ->with('tot_english',$tot_english)
                ->with('tot_filipino',$tot_filipino)
                ->with('tot_biosci',$tot_biosci)
                ->with('tot_physci',$tot_physci)
                ->with('tot_mapeh',$tot_mapeh)
                ->with('tot_socsci',$tot_socsci)
                ->with('tot_values',$tot_values)
                ->with('tot_afa',$tot_afa)
                ->with('tot_ufo',$tot_ufo)
                ->with('tot_let',$tot_let)
                
                ->with('tot_nle_1stTimers',$tot_nle_1stTimers)
                ->with('tot_nle_retakers',$tot_nle_retakers)
                ->with('tot_nle',$tot_nle)

                ->with('tot_crim_1stTimers',$tot_crim_1stTimers)
                ->with('tot_crim_retakers',$tot_crim_retakers)
                ->with('tot_crim',$tot_crim)
                
                ->with('tot_civil_1stTimers',$tot_civil_1stTimers)
                ->with('tot_civil_retakers',$tot_civil_retakers)
                ->with('tot_civil',$tot_civil)
                
                ->with('tot_psyc_1stTimers',$tot_psyc_1stTimers)
                ->with('tot_psyc_retakers',$tot_psyc_retakers)
                ->with('tot_psyc',$tot_psyc)
                
                ->with('tot_nclex_1stTimers',$tot_nclex_1stTimers)
                ->with('tot_nclex_retakers',$tot_nclex_retakers)
                ->with('tot_nclex',$tot_nclex)
                
                ->with('tot_ielts_1stTimers',$tot_ielts_1stTimers)
                ->with('tot_ielts_retakers',$tot_ielts_retakers)
                ->with('tot_ielts',$tot_ielts)
                
                ->with('tot_social_1stTimers',$tot_social_1stTimers)
                ->with('tot_social_retakers',$tot_social_retakers)
                ->with('tot_social',$tot_social)
                
                ->with('tot_agri_1stTimers',$tot_agri_1stTimers)
                ->with('tot_agri_retakers',$tot_agri_retakers)
                ->with('tot_agri',$tot_agri)
                
                ->with('tot_mid_1stTimers',$tot_mid_1stTimers)
                ->with('tot_mid_retakers',$tot_mid_retakers)
                ->with('tot_mid',$tot_mid)

                ->with('tot_online_1stTimers',$tot_online_1stTimers)
                ->with('tot_online_retakers',$tot_online_retakers)
                ->with('tot_online',$tot_online);
}

public function financialreport($year){
 //branch name
    $branch= $this->branch;
    $year = $year;
//over all total    
    $revenue = 0;


    
    $letS1=NovalichesS1Sale::where('program','=','LET')->where('year','=',$year)->sum('amount_paid');
    $nleS1=NovalichesS1Sale::where('program','=','NLE')->where('year','=',$year)->sum('amount_paid');
    $crimS1=NovalichesS1Sale::where('program','=','Criminology')->where('year','=',$year)->sum('amount_paid');
    $civilS1=NovalichesS1Sale::where('program','=','Civil Service')->where('year','=',$year)->sum('amount_paid');
    $psychoS1=NovalichesS1Sale::where('program','=','Psychometrician')->where('year','=',$year)->sum('amount_paid');
    $nclexS1=NovalichesS1Sale::where('program','=','NCLEX')->where('year','=',$year)->sum('amount_paid');
    $ieltsS1=NovalichesS1Sale::where('program','=','IELTS')->where('year','=',$year)->sum('amount_paid');
    $socialsS1=NovalichesS1Sale::where('program','=','Social')->where('year','=',$year)->sum('amount_paid');
    $agriS1=NovalichesS1Sale::where('program','=','Agriculture')->where('year','=',$year)->sum('amount_paid');
    $midwiferyS1=NovalichesS1Sale::where('program','=','Midwifery')->where('year','=',$year)->sum('amount_paid');
    $onlineS1=NovalichesS1Sale::where('program','=','Online Only')->where('year','=',$year)->sum('amount_paid');

    $letS2=NovalichesS2Sale::where('program','=','LET')->where('year','=',$year)->sum('amount_paid');
    $nleS2=NovalichesS2Sale::where('program','=','NLE')->where('year','=',$year)->sum('amount_paid');
    $crimS2=NovalichesS2Sale::where('program','=','Criminology')->where('year','=',$year)->sum('amount_paid');
    $civilS2=NovalichesS2Sale::where('program','=','Civil Service')->where('year','=',$year)->sum('amount_paid');
    $psychoS2=NovalichesS2Sale::where('program','=','Psychometrician')->where('year','=',$year)->sum('amount_paid');
    $nclexS2=NovalichesS2Sale::where('program','=','NCLEX')->where('year','=',$year)->sum('amount_paid');
    $ieltsS2=NovalichesS2Sale::where('program','=','IELTS')->where('year','=',$year)->sum('amount_paid');
    $socialsS2=NovalichesS2Sale::where('program','=','Social')->where('year','=',$year)->sum('amount_paid');
    $agriS2=NovalichesS2Sale::where('program','=','Agriculture')->where('year','=',$year)->sum('amount_paid');
    $midwiferyS2=NovalichesS2Sale::where('program','=','Midwifery')->where('year','=',$year)->sum('amount_paid');
    $onlineS2=NovalichesS2Sale::where('program','=','Online Only')->where('year','=',$year)->sum('amount_paid');
    
    
    $finalCoachingS1= NovalichesS1Sale::where('category','=','Final Coaching')->where('Year','=',$year)->sum('tuition_fee');
    $finalCoachingS2= NovalichesS2Sale::where('category','=','Final Coaching')->where('Year','=',$year)->sum('tuition_fee');
    
    $faciFeeS1 = NovalichesS1Sale::sum('facilitation_fee');
    $faciFeeS2 = NovalichesS2Sale::sum('facilitation_fee'); 

    //start of Distribution
    $totLoanDisbursmentS1= NovalichesExpense::where('category','=', 'Loans')->where('Season','=','Season 1')->where('Year','=',$year)->sum('amount'); 
    $totBookDisbursementS1= NovalichesExpense::where('category','=', 'Books')->where('Season','=','Season 1')->where('Year','=',$year)->sum('amount'); 
    $insuranceS1= NovalichesExpense::where('category' ,'=', "Insurance")->where('Season','=','Season 1')->where('Year','=',$year)->sum('amount'); 
    $taxAndLicenseS1 = NovalichesExpense::where('category','=','Tax')->where('Season','=','Season 1')->where('Year','=',$year)->sum('amount'); 
    $lecProfFeeS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Professional Fee")->sum('amount'); 
    $lecTaxS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Tax")->sum('amount'); 
    $lecTranspoS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Transportation")->sum('amount'); 
    $lecReimburseS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Reimbursement")->sum('amount'); 
    $lecAllowS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Allowance")->sum('amount'); 
    $lecAccommodationS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Accommodation")->sum('amount'); 
    $utilVenueS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Venue")->sum('amount'); 
    $mktingAllowanceS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Allowance")->sum('amount'); 
    $mktingGasolineS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Gasoline")->sum('amount'); 
    $mktingMealsS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Meals")->sum('amount'); 
    $mktingProfFeeS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Professional Fee")->sum('amount'); 
    $mktingTranspoS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Transportation")->sum('amount'); 
    $mktingPostSignageFlyersS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Posters | Signage | Flyers")->sum('amount'); 
    $mktingGiftsS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Gifts")->sum('amount'); 
    $mktingDailyExpWaybillS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Waybill")->sum('amount'); 
    $dailyexploadS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Load")->sum('amount'); 
    $dailyexpmaintenanceS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Maintenance")->sum('amount'); 
    $dailyexptranspoS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Transportation")->sum('amount'); 
    $dailyexpgasolineS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Gasoline")->sum('amount'); 
    $dailyexpofficeSupplyS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Office Supply")->sum('amount'); 
    $dailyexpgroceryS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Grocery")->sum('amount'); 
    $dailyexpAllowS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Allowance")->sum('amount'); 
    $dailyexpOthersS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Others")->sum('amount'); 
    $admnsalaryS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Salary")->sum('amount');  
    $contributionsssS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Contribution")->where('sub_category','=',"SSS")->sum('amount'); 
    $contributionpagibigS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Contribution")->where('sub_category','=',"Pag-ibig")->sum('amount'); 
    $contributionphilhealthS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Contribution")->where('sub_category','=',"PhilHealth")->sum('amount'); 
    $utilityrentS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Rent")->sum('amount'); 
    $utilitywaterS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Water")->sum('amount'); 
    $utilityelectricityS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Electricity")->sum('amount'); 
    $utilitysoundS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Sound System")->sum('amount'); 
    $utilityinternetS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Internet")->sum('amount'); 
    $utilitytelephoneS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Telephone")->sum('amount'); 
    $investOfficeEquipS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Investment")->where('sub_category','=',"Office Equipment")->sum('amount'); 
    $investFurnitureAndfixtureS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Investment")->where('sub_category','=',"Furniture & Fixture")->sum('amount'); 
    $dailyexpMealsS1= NovalichesExpense::where('Season','=','Season 1')->where('Year','=',$year)->where('category','=',"Investment")->where('sub_category','=',"Meals")->sum('amount'); 
  
    
    
    
    
    $totLoanDisbursmentS2= NovalichesExpense::where('category','=', 'Loans')->where('Season','=','Season 2')->where('Year','=',$year)->sum('amount'); 
    $totBookDisbursementS2= NovalichesExpense::where('category','=', 'Books')->where('Season','=','Season 2')->where('Year','=',$year)->sum('amount'); 
    $insuranceS2= NovalichesExpense::where('category' ,'=', "Insurance")->where('Season','=','Season 2')->where('Year','=',$year)->sum('amount'); 
    $taxAndLicenseS2= NovalichesExpense::where('category','=','Tax')->where('Season','=','Season 2')->where('Year','=',$year)->sum('amount'); 
    $lecProfFeeS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Professional Fee")->sum('amount'); 
    $lecTaxS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Tax")->sum('amount'); 
    $lecTranspoS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Transportation")->sum('amount'); 
    $lecReimburseS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Reimbursement")->sum('amount'); 
    $lecAllowS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Allowance")->sum('amount'); 
    $lecAccommodationS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Lecturer")->where('sub_category','=',"Accommodation")->sum('amount'); 
    $utilVenueS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Venue")->sum('amount'); 
    $mktingAllowanceS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Allowance")->sum('amount'); 
    $mktingGasolineS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Gasoline")->sum('amount'); 
    $mktingMealsS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Meals")->sum('amount'); 
    $mktingProfFeeS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Professional Fee")->sum('amount'); 
    $mktingTranspoS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Transportation")->sum('amount'); 
    $mktingPostSignageFlyersS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Posters | Signage | Flyers")->sum('amount'); 
    $mktingGiftsS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Marketing")->where('sub_category','=',"Gifts")->sum('amount'); 
    $mktingDailyExpWaybillS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Waybill")->sum('amount'); 
    $dailyexploadS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Load")->sum('amount'); 
    $dailyexpmaintenanceS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Maintenance")->sum('amount'); 
    $dailyexptranspoS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Transportation")->sum('amount'); 
    $dailyexpgasolineS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Gasoline")->sum('amount'); 
    $dailyexpofficeSupplyS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Office Supply")->sum('amount'); 
    $dailyexpgroceryS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Grocery")->sum('amount'); 
    $dailyexpAllowS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Allowance")->sum('amount'); 
    $dailyexpOthersS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Daily Expense")->where('sub_category','=',"Others")->sum('amount'); 
    $admnsalaryS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Salary")->sum('amount'); 
    $contributionsssS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Contribution")->where('sub_category','=',"SSS")->sum('amount'); 
    $contributionpagibigS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Contribution")->where('sub_category','=',"Pag-ibig")->sum('amount'); 
    $contributionphilhealthS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Contribution")->where('sub_category','=',"PhilHealth")->sum('amount'); 
    $utilityrentS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Rent")->sum('amount'); 
    $utilitywaterS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Water")->sum('amount'); 
    $utilityelectricityS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Electricity")->sum('amount'); 
    $utilitysoundS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Sound System")->sum('amount'); 
    $utilityinternetS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Internet")->sum('amount'); 
    $utilitytelephoneS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Utilities")->where('sub_category','=',"Telephone")->sum('amount'); 
    $investOfficeEquipS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Investment")->where('sub_category','=',"Office Equipment")->sum('amount'); 
    $investFurnitureAndfixtureS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Investment")->where('sub_category','=',"Furniture & Fixture")->sum('amount'); 
    $dailyexpMealsS2= NovalichesExpense::where('Season','=','Season 2')->where('Year','=',$year)->where('category','=',"Investment")->where('sub_category','=',"Meals")->sum('amount'); 
  
    //books
    $bookNCLEX = NovalichesBooksSale::where('program','=',"NCLEX")->sum('amount');
    $bookLET = NovalichesBooksSale::where('program','=',"LET")->sum('amount');
    $bookNLE = NovalichesBooksSale::where('program','=',"NLE")->sum('amount');
    $bookCrim = NovalichesBooksSale::where('program','=',"Criminology")->sum('amount');
    $bookCS = NovalichesBooksSale::where('program','=',"Civil Service")->sum('amount');
    $bookIELTS = NovalichesBooksSale::where('program','=',"IELTS")->sum('amount');
    $bookSW = NovalichesBooksSale::where('program','=',"Social Work")->sum('amount');
    $bookAgri = NovalichesBooksSale::where('program','=',"Agriculture")->sum('amount');
    $bookMid = NovalichesBooksSale::where('program','=',"Midwifery")->sum('amount');
    $bookOnline = NovalichesBooksSale::where('program','=',"Online Only")->sum('amount');
    $bookPsycho = NovalichesBooksSale::where('program','=',"Psychometrician")->sum('amount');

    $booksSale= NovalichesBooksSale::sum('amount'); 
    //end of books

 
    // compute  total disbursement   
    $totDisbursementS1 = $lecProfFeeS1 + $lecTaxS1 + $lecAllowS1 + $lecTranspoS1 + $faciFeeS1
                     + $lecReimburseS1 + $lecAccommodationS1 + $utilVenueS1 + $mktingAllowanceS1
                     + $mktingGasolineS1 + $mktingMealsS1 + $mktingProfFeeS1 + $mktingTranspoS1
                     + $mktingDailyExpWaybillS1 + $mktingPostSignageFlyersS1 + $mktingGiftsS1 + $admnsalaryS1
                     + $contributionsssS1 + $contributionpagibigS1 + $contributionphilhealthS1 + $utilityrentS1
                     + $utilitywaterS1 + $utilityelectricityS1 + $utilitysoundS1 + $utilityinternetS1
                     + $utilitytelephoneS1 + $dailyexploadS1 + $dailyexpmaintenanceS1 + $dailyexptranspoS1
                     + $dailyexpgasolineS1 + $dailyexpofficeSupplyS1 + $dailyexpgroceryS1 + $taxAndLicenseS1
                     + $insuranceS1 + $investOfficeEquipS1 + $investFurnitureAndfixtureS1 + $dailyexpOthersS1
                     + $dailyexpMealsS1 + $dailyexpAllowS1 + $totBookDisbursementS1 + $totLoanDisbursmentS1;

    // compute  total disbursement   
    $totDisbursementS2 = $lecProfFeeS2 + $lecTaxS2 + $lecAllowS2 + $lecTranspoS2 + $faciFeeS2 
                     + $lecReimburseS2 + $lecAccommodationS2 + $utilVenueS2 + $mktingAllowanceS2
                     + $mktingGasolineS2 + $mktingMealsS2 + $mktingProfFeeS2 + $mktingTranspoS2
                     + $mktingDailyExpWaybillS2 + $mktingPostSignageFlyersS2 + $mktingGiftsS2 + $admnsalaryS2
                     + $contributionsssS2 + $contributionpagibigS2 + $contributionphilhealthS2 + $utilityrentS2
                     + $utilitywaterS2 + $utilityelectricityS2 + $utilitysoundS2 + $utilityinternetS2
                     + $utilitytelephoneS2 + $dailyexploadS2 + $dailyexpmaintenanceS2 + $dailyexptranspoS2
                     + $dailyexpgasolineS2 + $dailyexpofficeSupplyS2 + $dailyexpgroceryS2 + $taxAndLicenseS2
                     + $insuranceS2 + $investOfficeEquipS2 + $investFurnitureAndfixtureS2 + $dailyexpOthersS2
                     + $dailyexpMealsS2 + $dailyexpAllowS2 + $totBookDisbursementS2 + $totLoanDisbursmentS2;

// total season 1
$totSeason1 =  $letS1 + $nleS1 + $crimS1 + $civilS1 + $psychoS1 + $nclexS1 + $ieltsS1 + $socialsS1 + $agriS1 + $midwiferyS1 + $onlineS1;

//total season 2
$totSeason2 =  $letS2 + $nleS2 + $crimS2 + $civilS2 + $psychoS2 + $nclexS2 + $ieltsS2 + $socialsS2 + $agriS2 + $midwiferyS2 + $onlineS2;
   
//total revenue    
$revenue = $totSeason1 + $totSeason2 + $booksSale;


$totDisbursement =  $totDisbursementS1 + $totDisbursementS2;
//netexcess netincome
$netExcessIncome =  $revenue -  $totDisbursement ;

    return view('member.financial-report')
    ->with('branch', $branch)
    
    ->with('totSeason1', $totSeason1)
    ->with('totSeason2', $totSeason2)
    ->with('booksSale', $booksSale)
 
    ->with('bookNCLEX', $bookNCLEX)
    ->with('bookPsycho', $bookPsycho)
    ->with('bookLET', $bookLET)
    ->with('bookNLE', $bookNLE)
    ->with('bookCrim', $bookCrim)
    ->with('bookCS', $bookCS)
    ->with('bookIELTS', $bookIELTS)
    ->with('bookSW', $bookSW)
    ->with('bookAgri', $bookAgri)
    ->with('bookMid', $bookMid)
    ->with('bookOnline', $bookOnline)
    
    ->with('revenue', $revenue)
    ->with('netExcessIncome', $netExcessIncome)
    ->with('totLoanDisbursmentS1', $totLoanDisbursmentS1)
    ->with('totBookDisbursementS1', $totBookDisbursementS1)
    
    ->with('totLoanDisbursmentS2', $totLoanDisbursmentS2)
    ->with('totBookDisbursementS2', $totBookDisbursementS2)
    
    ->with('finalCoachingS1', $finalCoachingS1)
    ->with('finalCoachingS2', $finalCoachingS2)

    
    ->with('lecTaxS1', $lecTaxS1)
    ->with('lecProfFeeS1', $lecProfFeeS1)
    ->with('lecTranspoS1', $lecTranspoS1)
    ->with('lecReimburseS1', $lecReimburseS1)
    ->with('lecAllowS1', $lecAllowS1)
    ->with('lecAccommodationS1', $lecAccommodationS1)
    ->with('utilVenueS1', $utilVenueS1)
    ->with('mktingAllowanceS1', $mktingAllowanceS1)
    ->with('mktingGasolineS1', $mktingGasolineS1)
    ->with('mktingMealsS1', $mktingMealsS1)
    ->with('mktingProfFeeS1', $mktingProfFeeS1)
    ->with('mktingTranspoS1', $mktingTranspoS1)
    ->with('mktingDailyExpWaybillS1', $mktingDailyExpWaybillS1)
    ->with('mktingPostSignageFlyersS1', $mktingPostSignageFlyersS1)
    ->with('mktingGiftsS1', $mktingGiftsS1)
    ->with('admnsalaryS1', $admnsalaryS1)
    ->with('contributionsssS1', $contributionsssS1)
    ->with('contributionpagibigS1', $contributionpagibigS1)
    ->with('contributionphilhealthS1', $contributionphilhealthS1)
    ->with('utilityrentS1', $utilityrentS1)
    ->with('utilitywaterS1', $utilitywaterS1)
    ->with('utilityelectricityS1', $utilityelectricityS1)
    ->with('utilitysoundS1', $utilitysoundS1)
    ->with('utilityinternetS1', $utilityinternetS1)
    ->with('utilitytelephoneS1', $utilitytelephoneS1)
    ->with('dailyexploadS1', $dailyexploadS1)
    ->with('dailyexpmaintenanceS1', $dailyexpmaintenanceS1)
    ->with('dailyexptranspoS1', $dailyexptranspoS1)
    ->with('dailyexpgasolineS1', $dailyexpgasolineS1)
    ->with('dailyexpofficeSupplyS1', $dailyexpofficeSupplyS1)
    ->with('dailyexpgroceryS1', $dailyexpgroceryS1)
    ->with('taxAndLicenseS1', $taxAndLicenseS1)
    ->with('insuranceS1', $insuranceS1)
    ->with('investOfficeEquipS1', $investOfficeEquipS1)
    ->with('investFurnitureAndfixtureS1', $investFurnitureAndfixtureS1)
    ->with('dailyexpOthersS1', $dailyexpOthersS1)
    ->with('dailyexpMealsS1', $dailyexpMealsS1)
    ->with('dailyexpAllowS1', $dailyexpAllowS1)

    
    
    ->with('lecTaxS2', $lecTaxS2)
    ->with('lecProfFeeS2', $lecProfFeeS2)
    ->with('lecTranspoS2', $lecTranspoS2)
    ->with('lecReimburseS2', $lecReimburseS2)
    ->with('lecAllowS2', $lecAllowS2)
    ->with('lecAccommodationS2', $lecAccommodationS2)
    ->with('utilVenueS2', $utilVenueS2)
    ->with('mktingAllowanceS2', $mktingAllowanceS2)
    ->with('mktingGasolineS2', $mktingGasolineS2)
    ->with('mktingMealsS2', $mktingMealsS2)
    ->with('mktingProfFeeS2', $mktingProfFeeS2)
    ->with('mktingTranspoS2', $mktingTranspoS2)
    ->with('mktingDailyExpWaybillS2', $mktingDailyExpWaybillS2)
    ->with('mktingPostSignageFlyersS2', $mktingPostSignageFlyersS2)
    ->with('mktingGiftsS2', $mktingGiftsS2)
    ->with('admnsalaryS2', $admnsalaryS2)
    ->with('contributionsssS2', $contributionsssS2)
    ->with('contributionpagibigS2', $contributionpagibigS2)
    ->with('contributionphilhealthS2', $contributionphilhealthS2)
    ->with('utilityrentS2', $utilityrentS2)
    ->with('utilitywaterS2', $utilitywaterS2)
    ->with('utilityelectricityS2', $utilityelectricityS2)
    ->with('utilitysoundS2', $utilitysoundS2)
    ->with('utilityinternetS2', $utilityinternetS2)
    ->with('utilitytelephoneS2', $utilitytelephoneS2)
    ->with('dailyexploadS2', $dailyexploadS2)
    ->with('dailyexpmaintenanceS2', $dailyexpmaintenanceS2)
    ->with('dailyexptranspoS2', $dailyexptranspoS2)
    ->with('dailyexpgasolineS2', $dailyexpgasolineS2)
    ->with('dailyexpofficeSupplyS2', $dailyexpofficeSupplyS2)
    ->with('dailyexpgroceryS2', $dailyexpgroceryS2)
    ->with('taxAndLicenseS2', $taxAndLicenseS2)
    ->with('insuranceS2', $insuranceS2)
    ->with('investOfficeEquipS2', $investOfficeEquipS2)
    ->with('investFurnitureAndfixtureS2', $investFurnitureAndfixtureS2)
    ->with('dailyexpOthersS2', $dailyexpOthersS2)
    ->with('dailyexpMealsS2', $dailyexpMealsS2)
    ->with('dailyexpAllowS2', $dailyexpAllowS2)
    //season 1 sale
    ->with('letS1', $letS1)
    ->with('nleS1', $nleS1)
    ->with('crimS1', $crimS1)
    ->with('civilS1', $civilS1)
    ->with('psychoS1', $psychoS1)
    ->with('nclexS1', $nclexS1)
    ->with('ieltsS1', $ieltsS1)
    ->with('socialsS1', $socialsS1)
    ->with('agriS1', $agriS1)
    ->with('midwiferyS1', $midwiferyS1)
    ->with('onlineS1', $onlineS1)

    //season 2 sale
    ->with('letS2', $letS2)
    ->with('nleS2', $nleS2)
    ->with('crimS2', $crimS2)
    ->with('civilS2', $civilS2)
    ->with('psychoS2', $psychoS2)
    ->with('nclexS2', $nclexS2)
    ->with('ieltsS2', $ieltsS2)
    ->with('socialsS2', $socialsS2)
    ->with('agriS2', $agriS2)
    ->with('midwiferyS2', $midwiferyS2)
    ->with('onlineS2', $onlineS2)
    ->with('totDisbursement', $totDisbursement)

    ->with('faciFeeS1', $faciFeeS1)
    ->with('faciFeeS2', $faciFeeS2)
    ->with('year', $year)
    ;
}//end of financial report




public function insert_book_transfer(Request $request){

    $input = $request->except(['_token']);
   
   
    
   $validatedData = $request->validate([
       'remark' => 'required'
   ]);
   
           $quantities = $input['quantities'];
           $available = $input['available'];
           $program = $input['book_program'];
           $author = $input['book_author'];
           $final = $input['final'];
          bookTranferTrans::create([

              'book_date'              => $input['date'],
              'book_remarks'           => $input['remark'],
              'book_branchSender'      => $this->branch,
              'book_branchReciever'    => $input['transfer_to'],
          ]);
          $last_id = bookTranferTrans::orderby('id','desc')->first();
           if(isset($input['book_title_item'])){
               foreach ($input['book_title_item'] as $book => $value ) {

                NovalichesBookTransfer::create([
                   'book_transId'             => $last_id['id'],
                   'book_program'           => $program[$book],
                   'book_major'             => null,
                   'book_author'             => $author[$book],
                   'book_title'             => $value,
                   'book_quantity'          => $quantities[$book],
                   'book_stockInitial'      => $available[$book],
                   'book_stockFinal'        => $final[$book]
                ]);
   
   
              
          
   
           NovalichesBooksInventorie::where('book_title','=',$value)->update([
   
               'available' => $final[$book],
           ]);   
        }      
   
               Alert::success('Success!', 'New book tranfer has been submitted.');
            return redirect ($this->sbranch.'/book-transfer');
                   }
   
           else{
               Alert::error('Failed!', 'Oops Something went wrong!, please try again');
               return redirect ($this->sbranch.'/book-transfer');
           }
       
   
           
   }//end of book transfer



   public function book_transfer(){
    
        $branch=$this->branch; 
        $date = date('M-d-Y');
        $program = Program::all();
        return view('member.book-transfer')->with('branch',$branch)->with('date',$date)->with('program',$program);
    }
    
    
    public function bookTransfer_table(){
    $book = BookTranferTrans::orderby('id','desc')->get();
    return view ('member.book-transferRecord')->with('book',$book);
    
}
public function bookTransfer_table_fetch($id){
    $data = NovalichesBookTransfer::where('book_transId','=',$id)->get();
   return response()->json($data);
    }
    
    public function filter_lecturer(){
        $programs = Program::all();
        $branch = $this->sbranch;
        $sy = date("Y")+1;
        $years = range($sy,2010);


        return view('radashboard.lecturer')
        ->with('programs',$programs)
        ->with('years',$years)
        ->with('branch',$branch);
    }


    public function onlineCompletion(){
        $programs = Program::all();
        $sy = date("Y")+1;
        $years = range($sy,2010);
        $branch = $this->sbranch;
    
        return view('radashboard.online_completion')
        ->with('years',$years)
        ->with('programs',$programs)
        ->with('branch',$branch);
    }



    public function delete_expense($id){
        
        
        $expense_amount = NovalichesExpense::where('id','=',$id)->value('amount');
        $pettycash = NovalichesPettyCash::where('id','=','1')->value('petty_cash');

        NovalichesExpense::where('id','=',$id)->delete();
            

        $updated_pettycash = $pettycash + $expense_amount;

        NovalichesPettyCash::where('id','=','1')->update([
            'petty_cash' => $updated_pettycash,
        ]);

       
        
        Alert::success('Success!', 'expense deleted.');
        return redirect ($this->sbranch.'/expense');

    }


}
