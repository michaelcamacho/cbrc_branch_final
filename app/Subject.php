<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $connection = 'satelite_branch_db_connection';
    protected $fillable = [
    	'program',
		'class',
		'subject',
		'aka',
    ];
}
