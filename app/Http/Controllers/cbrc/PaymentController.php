<?php

namespace App\Http\Controllers\cbrc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\StudentEnrollmentCard;
use App\CbrcStudent;
use App\PaymentInfo;
use App\Model\AdminSettings;

class PaymentController extends Controller
{
    
    public function proceed_enroll_reserved_student(Request $request){
            

        $settings = AdminSettings::findorfail(1)->first();

        $cardid = StudentEnrollmentCard::where('student_id','=',$request->input('name'))
                                        ->where('season','=',$settings->season)
                                        ->where('year','=',$settings->year)
                                        ->first();

        // return $cardid;

        
                                               
        $registration_date=date('M-d-Y');
        $input = $request->except(['_token']);
        $status = 1;
        // return  $input;
 
        $card_balance = $input['total_amount'] - $input['amount_paid'] ;
       
        StudentEnrollmentCard::where('id','=',$cardid->id)->update([
            'date_registered' => $registration_date ,
            'facilitation_price' => $input['facilitation'] , 
            'tuition_fee_price' => $input['tuition_fee'] , 
            'discount_category_id' => $input['discount'] , 
            'balance' => $card_balance, 
            'discount' => $input['discount_amount'] , 
            'category_id' => $input['student_category'] ,  
            'status' => $status,
        ]);

        $n_card = StudentEnrollmentCard::findorfail($cardid->id);

        
        PaymentInfo::insert([
            'payment_date' => $registration_date ,
            'payment_no' => 2,
            'card_id' => $cardid->id,
            'amount_paid' => $input['amount_paid'] ,
            'balance_before' => $request->input('balance') ,
            'balance_after' => $n_card->balance,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 ,
        ]);


        return redirect('/new_payment/'. request('hidden_branch'));
    }
    
    public function proceed_enroll_student(Request $request){
        
        $settings = AdminSettings::findorfail(1)->first();
        
        $cardid = StudentEnrollmentCard::where('student_id','=',$request->input('name'))
                                        ->where('season','=',$settings->season)
                                        ->where('year','=',$settings->year)
                                        ->first();

        $registration_date=date('M-d-Y');
        $input = $request->except(['_token']);
        $major = 0;

        if(isset($input['student_major']) ){
            $major = 1;
        }
        $status = 1;
        $season_year = AdminSettings::findorfail(1)->first();
        $card_balance =   $input['total_amount'] - $input['amount_paid'];

        if(request('amount_paid') <= 2999){
            $status = 2;
        }

         StudentEnrollmentCard::insert([
            'date_registered' => $registration_date ,
            'student_id' => $input['name'],
            'program_id' => $input['program'],
            'major_id' => $major,
            'facilitation_price' => $input['facilitation'] , 
            'tuition_fee_price' => $input['tuition_fee'] , 
            'discount_category_id' => $input['discount'] , 
            'balance' => $card_balance, 
            'discount' => $input['discount_amount'] , 
            'category_id' => $input['student_category'], 
            'season' => $season_year->season,
            'year' => $season_year->year,
            'status' => $status,
        ]);
        $latest_card = StudentEnrollmentCard::latest('id')
        ->where('student_id','=',$input['name'])->first();  
        PaymentInfo::insert([
            'payment_date' => $registration_date ,
            'payment_no' => 1,
            'card_id' => $latest_card->id,
            'amount_paid' => $input['amount_paid'] ,
            'balance_before' => $input['total_amount'],
            'balance_after' => $card_balance ,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 ,
        ]);



        return redirect('/new_payment/'.request('hidden_branch'));
    }



    
    public function proceed_add_payment(Request $request){
        
        $status = 2;    
        $payment_date=date('M-d-Y');
        $input = $request->except(['_token']);
    

        $settings = AdminSettings::findorfail(1)->first();
        
        
        $cardid = StudentEnrollmentCard::where('student_id','=',$request->input('name'))
                                        ->where('season','=',$settings->season)
                                        ->where('year','=',$settings->year)
                                        ->first();

        
        $total_amount_paid = PaymentInfo::where('card_id' , '=' , $cardid->id)->sum('amount_paid');
 
        $total_amount_paid = $total_amount_paid + $input['amount_paid'];

            if($total_amount_paid >= 3000){
            $status = 1; 
            }       


            $card_balance = request('balance') -  $input['amount_paid'] ;
           
            StudentEnrollmentCard::where('id',$cardid->id)->update([
                'status' => $status,
                'balance' => $card_balance, 
            ]);
                

            $last_payment = PaymentInfo::latest('id')
            ->where('card_id','=',$cardid->id)->first();


            PaymentInfo::insert([
            'payment_date' => request('date') ,
            'payment_no' => $last_payment->payment_no + 1,
            'card_id' => $cardid->id,
            'amount_paid' => $input['amount_paid'] ,
            'balance_before' => request('balance') ,
            'balance_after' => $card_balance ,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 ,
            ]);
           
        return redirect('/new_payment/'.request('hidden_branch')); 
    }
}
