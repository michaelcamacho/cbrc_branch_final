<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from kineticpro.dharansh.in/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Aug 2018 15:06:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pace.css') }}">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title') | CBRC {{ isset($_COOKIE['system_title']) ? $_COOKIE['system_title'] : '' }}</title>
    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/select2/4.0.5/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/jqvmap/1.5.1/jqvmap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/public.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/owl.theme.default.css') }}" rel="stylesheet" type="text/css">
     
     <!-- SweetAlert2  -->
      <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
      <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">

      <link href="{{ asset('css/styles.css')}} " rel="stylesheet" />
      <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css"> 
      {{-- <link rel="stylesheet" type="text/css" href="assets/dropzone/dist/dropzone.css"> --}}

<!-- FontAwesome-->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

<!-- End FontAwesome -->


      <meta name="author" content="cipherfusionphilippines.com" />
        <link rel="canonical" href="https://www.cipherfusionphilippines.com/" />
        <meta property="fb:admins" content="100001809531980">
        <meta property="fb:app_id" content="206704316521457">
        <meta property="og:image" content="https://cbrc-novaliches.com/img/fblogo.png"/>
        <meta property="og:locale" content="en_PH" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="CBRC Novaliches" />
        <meta property="og:description" content="The BIGGEST and LEADING Brand in TEST PREPARATION" />
        <meta property="og:url" content="https://cbrc-novaliches.com" />
        <meta property="og:site_name" content="CBRC Novaliches" />
        <!-- Mobile Specific Metas
                ================================================== -->
                
                <meta name="viewport" content="user-scalable=yes, initial-scale=1, maximum-scale=1, width=device-width">

                <meta name="application-name" content="CBRC Novaliches"/>
                        <meta name="msapplication-square70x70logo" content="/img/small.jpg"/>
                        <meta name="msapplication-square150x150logo" content="/img/medium.jpg"/>
                        <meta name="msapplication-wide310x150logo" content="/img/wide.jpg"/>
                        <meta name="msapplication-square310x310logo" content="/img/large.jpg"/>
                        <meta name="msapplication-TileColor" content="#000000"/>
                <script type="text/javascript">
                    cdn_url = 'https://cbrc-novaliches.com/';
                </script>


        <script src="{{ asset('js/select2.min.js') }}"></script>
    @yield('css')
    <!-- Head Libs -->
    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
    <script data-pace-options='{ "ajax": false, "selectors": [ "img" ]}' src="{{ asset('assets/js/pace.min.js') }}"></script>

</head>

<body class="sidebar-light sidebar-expand navbar-brand-dark" id="data">

        <a id="fbmessenger" href="https://www.facebook.com/messages/t/cipherfusion" target="_blank"><i class="fab fa-facebook-square"></i> Chat Now</a>

        <a href="fb-messenger://user/1060720417298686" id="fbmessenger-mobile"><i class="fab fa-facebook-messenger"></i> Chat Now</a>
    
<div id="wrapper" class="wrapper">

@include('template.header')

<div class="content-wrapper">


@role('Admin')

@if($main_branch == "")
@include('template.nav')
@else
@include('template.member-nav')
@endif

@endrole

@role('Member')
@include('template.member-nav')
@endrole


@include('sweet::alert')

@yield('main-content')
</div>

@include('template.footer')

</div>
    <script src="{{ asset('assets/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>       
    <script src="{{ asset('assets/ajax/libs/popper.js/1.14.3/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/metisMenu/2.7.9/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.js"></script>
    <script src="{{asset('assets/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js')}}"></script>
    <script src="{{ asset('assets/ajax/libs/countup.js/1.9.2/countUp.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/Chart.js/2.7.2/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/select2/4.0.5/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jqvmap/1.5.1/jquery.vmap.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jqvmap/1.5.1/maps/jquery.vmap.usa.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/template.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/owl.carousel.js') }}"></script>

    @yield('js')
    <script type="text/javascript">
        $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    </script>

    
</body>

</html>