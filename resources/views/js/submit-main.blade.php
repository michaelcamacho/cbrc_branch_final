{{-- 
<script>
const AddPayment = async (details) => {
  const settings = {
    method: 'POST',
    body: JSON.stringify(details),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    }
  }

  const response = await fetch('/payment/insert-studentPayment', settings);
  // if (!response.ok) throw Error(response.message);
  try {
    const data = await response.json();
    console.log(data.status);
    console.log('ok');
    return data;
  } catch(err) {
    console.log(err.response);
    //throw err;
  }
};

$('#button-prevent').on('click',()=>{

    const details = {
      "date":$('#date').val(),
      "program":$('#program').val(),
      "student":$('#student').val(),
      "category":$('#category').val(),
      "discount":$('#discount').val(),
    }


    AddPayment(details);
    
    
  
});

  

</script> --}}


{{-- 
  
  important note for debugger!

  once the payment has been submitted 
  the controller must count the number of payment made by the selected student
  and  if the its a first payment the data of student must be send at cbrc.solutions
  student api and payment api for data verification and to compute the total facilitation to be collected 
  at the cbrc branches..
  --}}



{{-- <script type="text/javascript">
$('#main').submit(function(event){
    
  event.preventDefault();
  event.stopPropagation();
   
               var formData = new FormData();
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('branch', $('#hd_branch').val());
                formData.append('id', $('#hd_id').val());
                formData.append('cbrc_id', $('#hd_cbrc_id').val());
                formData.append('last_name', $('#hd_last_name').val());
                formData.append('first_name', $('#hd_first_name').val());
                formData.append('middle_name', $('#hd_middle_name').val());
                formData.append('birthdate', $('#bhd_irthdate').val());
                formData.append('contact_no', $('#hd_contact_no').val());
                formData.append('address', $('#hd_address').val());
                formData.append('email', $('#hd_email').val());
                formData.append('username', $('#hd_username').val());
                formData.append('password', $('#hd_password').val());
                formData.append('school', $('#hd_school').val());
                formData.append('program', $('#hd_program').val());
                formData.append('major', $('#hd_major').val());
                formData.append('take', $('#hd_take').val());
                formData.append('noa_no', $('#hd_noa_no').val());
                formData.append('category', $('#category').val());
                formData.append('status', 'Enrolled');
                formData.append('contact_person', $('#hd_contact_person').val());
                formData.append('contact_details', $('#hd_contact_details').val());
                formData.append('facilitation', $('#facilitation').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                  url: "",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                           
                 
                   
        });
});//
</script> --}}