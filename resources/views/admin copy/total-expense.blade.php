@extends('main')
@section('title')
Total Expenses
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Total Expenses Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Total Expenses Records</h5>
                                   <br/>
                                   
                                </div>
            <div class="widget-body clearfix">
                <div id="sale-table">
                        <h4>Season 1 Expense</h4>
                    <table id="sale" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                        <tr>
                            <th>Date</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                            <th>Year</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                        @if(isset($a_sale))
                            @foreach($a_sale as $a_sales)
                                @if($a_sales->Season == "Season 1")
                                    <tr id="row_{{$a_sales->id}}">
                                        <td>{{$a_sales->date}}</td>
                                        <td>{{$a_sales->branch}}</td>
                                        <td>{{$a_sales->program}}</td>
                                        <td>{{$a_sales->category}}</td>
                                        <td>{{$a_sales->sub_category}}</td>
                                        <td>{{$a_sales->amount}}</td>
                                        <td>{{$a_sales->remarks}}</td>
                                        <td>{{$a_sales->Year}}</td>  
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    </tbody>           
                    
                </table>
            </div>
        </div>
        <div class="widget-body clearfix">
            <div id="sale2-table">
                <h4>Season 2 Expense</h4>
                <table id="sale2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                 <thead>
                    <tr>
                        <th>Date</th>
                        <th>Branch</th>
                        <th>Program</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Amount</th>
                        <th>Remarks</th>
                        <th>Year</th>
                    </tr>
                   
                </thead>
                <tbody>
                    @if(isset($a_sale))
                        @foreach($a_sale as $a_sales)
                            @if($a_sales->Season == "Season 2")
                                <tr id="row_{{$a_sales->id}}">
                                    <td>{{$a_sales->date}}</td>
                                    <td>{{$a_sales->branch}}</td>
                                    <td>{{$a_sales->program}}</td>
                                    <td>{{$a_sales->category}}</td>
                                    <td>{{$a_sales->sub_category}}</td>
                                    <td>{{$a_sales->amount}}</td>
                                    <td>{{$a_sales->remarks}}</td>
                                    <td>{{$a_sales->Year}}</td>  
                                </tr>
                            @endif
                        @endforeach
                    @endif
                </tbody>           
                
            </table>
        </div>
    </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/book.js') }}"></script>
<script type="text/javascript">

@stop