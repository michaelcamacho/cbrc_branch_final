<?php


Route::get('/', function () {
    return view('auth.login');
});

/* START DAET */
/* App */
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/user/activation/{token}', 'Auth\RegisterController@userActivation');

Route::get('cbrc/online-registration','RegistrationController@index');

Route::post('new-register','RegistrationController@register');

Route::get('thank-you','RegistrationController@thank_you');

Route::get('/json-major','MainController@fetch_major');


/* Admin */
Route::get('/total-program','AdminController@total_program')->middleware('auth');
Route::get('/total-agri','AdminController@total_agri')->middleware('auth');
Route::get('/total-civil','AdminController@total_civil')->middleware('auth');
Route::get('/total-crim','AdminController@total_crim')->middleware('auth');
Route::get('/total-ielts','AdminController@total_ielts')->middleware('auth');
Route::get('/total-let','AdminController@total_let')->middleware('auth');
Route::get('/total-nclex','AdminController@total_nclex')->middleware('auth');
Route::get('/total-nle','AdminController@total_nle')->middleware('auth');
Route::get('/total-psyc','AdminController@total_psyc')->middleware('auth');
Route::get('/total-mid','AdminController@total_mid')->middleware('auth');
Route::get('/total-online','AdminController@total_online')->middleware('auth');
Route::get('/total-social','AdminController@total_social')->middleware('auth');
Route::get('/total-dropped','AdminController@total_dropped')->middleware('auth');
Route::get('/total-scholars','AdminController@total_scholars')->middleware('auth');
Route::get('/tuition-fees','AdminController@tuition_fees')->middleware('auth');
Route::get('/discounts','AdminController@discounts')->middleware('auth');
Route::get('/total-books','AdminController@total_books')->middleware('auth');
Route::get('/total-expense','AdminController@total_expense')->middleware('auth');
Route::get('/novaliches/settings/{param}','AdminController@dash_settings')->middleware('auth');
Route::get('/expense-settings','AdminController@expense_settings')->middleware('auth');
Route::post('/setup-expense','AdminController@expense_setup')->middleware('auth');




/* Add Data */
Route::post('/insert-tuition','AdminController@insert_tuition')->middleware('auth');
Route::post('/insert-discount','AdminController@insert_discount')->middleware('auth');
Route::post('/insert-book','AdminController@insert_book')->middleware('auth');

/* Update Data */
Route::post('/update-tuition','AdminController@update_tuition')->middleware('auth');
Route::post('/update-discount','AdminController@update_discount')->middleware('auth');
Route::post('/update-book','AdminController@update_book')->middleware('auth');

/* Delete Data */
Route::post('/delete-tuition','AdminController@delete_tuition')->middleware('auth');
Route::post('/delete-discount','AdminController@delete_discount')->middleware('auth');
Route::post('/delete-book','AdminController@delete_book')->middleware('auth');

/* Member */
/* novaliches */

Route::get('/new-reservation/{branch}','NovalichesController@new_reservation')->middleware('auth');
Route::get('/add-expense/{branch}','NovalichesController@add_expense')->middleware('auth');
Route::get('/add-budget/{branch}','NovalichesController@add_budget')->middleware('auth');
Route::get('/book-payment/{branch}','NovalichesController@book_payment')->middleware('auth');
Route::get('/new-remit/{branch}', 'NovalichesController@new_remit')->middleware('auth');



/* Data fetch */
// Route::get('/novaliches/json-student','NovalichesController@fetch_student')->middleware('auth');


Route::get('/novaliches/json-tuition','NovalichesController@fetch_tuition')->middleware('auth');
Route::get('/novaliches/json-discount','NovalichesController@fetch_discount')->middleware('auth');
Route::get('/novaliches/json-balance','NovalichesController@fetch_balance')->middleware('auth');
Route::get('/novaliches/json-expense','NovalichesController@fetch_expense')->middleware('auth');
Route::get('/novaliches/json-book','NovalichesController@fetch_book')->middleware('auth');
Route::get('/novaliches/json-price','NovalichesController@fetch_book_price')->middleware('auth');
Route::get('/novaliches/json-reserved','NovalichesController@fetch_reserved')->middleware('auth');
Route::get('/novaliches/json-id','NovalichesController@fetch_id')->middleware('auth');

/* Add Data */

// Route::post('/novaliches/insert-studentPayment','PaymentController@student_payment')->middleware('auth');



Route::post('/novaliches/insert-enrollee','NovalichesController@insert_enrollee')->middleware('auth');
Route::post('/novaliches/insert-new-expense','NovalichesController@add_new_expense')->middleware('auth');
Route::post('/novaliches/insert-new-budget','NovalichesController@insert_new_budget')->middleware('auth');
Route::post('/novaliches/insert-book-payment', 'NovalichesController@insert_book_payment')->middleware('auth');
Route::post('/novaliches/insert-book-transfer', 'NovalichesController@insert_book_transfer')->middleware('auth');
Route::post('/novaliches/drop-student', 'NovalichesController@drop_student')->middleware('auth');
Route::post('/novaliches/insert-remit', 'NovalichesController@insert_remit')->middleware('auth');
Route::post('/novaliches/insert-reservation','NovalichesController@insert_reservation')->middleware('auth');


/* Update Data */
Route::post('/novaliches/update-enrollee','NovalichesController@update_enrollee')->middleware('auth');


/* Delete Data */
Route::post('/novaliches/delete-enrollee','NovalichesController@delete_enrollee')->middleware('auth');
/* Tables */



	/* Tuition and Discounts table */
Route::get('/novaliches/tuition','NovalichesController@tuition_table')->middleware('auth');
	/* Scholar table */
Route::get('/novaliches/scholar','NovalichesController@scholar_table')->middleware('auth');
	/* Enrolled table */
Route::get('/novaliches/enrolled','NovalichesController@enrolled_table')->middleware('auth');
	/* Dropped table */
Route::get('/novaliches/dropped','NovalichesController@dropped_table')->middleware('auth');
	/* Sales table */
Route::get('/novaliches/sales-enrollee','NovalichesController@sales_enrollee_table')->middleware('auth');
Route::get('/novaliches/sales-program','NovalichesController@sales_program_table')->middleware('auth');
	/* Receivable table */
Route::get('/novaliches/receivable-enrollee','NovalichesController@receivable_enrollee_table')->middleware('auth');
Route::get('/novaliches/receivable-program','NovalichesController@receivable_program_table')->middleware('auth');
	/* Expense table */
Route::get('/novaliches/expense','NovalichesController@expense_table')->middleware('auth');
	/* Books table */
Route::get('/novaliches/books','NovalichesController@books_table')->middleware('auth');
Route::get('/novaliches/remit', 'NovalichesController@remit')->middleware('auth');
Route::get('/novaliches/reservation', 'NovalichesController@reservation_table')->middleware('auth');

Route::post('novaliches/clear-enrollee','NovalichesController@clear_enrollee')->middleware('auth');
Route::post('novaliches/clear-sale-season1','NovalichesController@clear_sale_season1')->middleware('auth');
Route::post('novaliches/clear-sale-season2','NovalichesController@clear_sale_season2')->middleware('auth');
Route::post('novaliches/clear-receivable','NovalichesController@clear_receivable')->middleware('auth');
Route::post('novaliches/clear-expense','NovalichesController@clear_expense')->middleware('auth');
Route::post('novaliches/clear-book','NovalichesController@clear_book')->middleware('auth');
Route::post('novaliches/clear-reservation','NovalichesController@clear_reservation')->middleware('auth');

/* Daet Lecturer */
Route::get('novaliches/lecturer-evaluation','NovalichesController@lec_eval')->middleware('auth');
Route::get('novaliches/add-lecturer','NovalichesController@add_lec')->middleware('auth');
Route::get('novaliches/evaluate-lecturer','NovalichesController@eval_lec')->middleware('auth');
Route::get('novaliches/json-class','NovalichesController@fetch_class')->middleware('auth');
Route::get('novaliches/json-subject','NovalichesController@fetch_subject')->middleware('auth');
Route::get('novaliches/json-section','NovalichesController@fetch_section')->middleware('auth');
Route::get('novaliches/json-lecturer','NovalichesController@fetch_lecturer')->middleware('auth');
Route::get('novaliches/json-lecturerb','NovalichesController@fetch_lecturerb')->middleware('auth');

Route::post('novaliches/insert-lecturer','NovalichesController@insert_lecturer')->middleware('auth');
Route::post('novaliches/insert-evaluation','NovalichesController@insert_eval')->middleware('auth');

Route::post('novaliches/clear-lecturers','NovalichesController@clear_lecturers')->middleware('auth');


/* Reports Novaliches */

Route::get('novaliches/today','NovalichesController@today')->middleware('auth');
Route::get('novaliches/yesterday','NovalichesController@yesterday')->middleware('auth');

/* End Reports */
/* Budget Record Novaliches */

Route::get('novaliches/budget','NovalichesController@budget_record')->middleware('auth');
Route::post('novaliches/clear-budget','NovalichesController@clear_budget')->middleware('auth');
/* End Budger Record */


/* Delete Sales */
Route::get('novaliches/delete-sale/s1/{id}','NovalichesController@delete_sale1');
Route::get('novaliches/delete-sale/s2/{id}','NovalichesController@delete_sale2');
/* End Delete */

/** csv */
Route::post('novaliches/csv-enrollee','NovalichesController@csv_enrollee')->middleware('auth');


// student id
Route::get('/novaliches/student-id','NovalichesController@student_id')->middleware('auth');



// bulletin Board
Route::get('bulletin', '\App\Http\Controllers\NovalichesController@bulletin');
Route::get('/fetch-branches/{bn}','NovalichesController@fetch_branches')->middleware('auth');

// newsfeed
Route::get('novaliches/newsfeed', 'AdminController@newsfeed');



// financialreport 
Route::get('novaliches/financial-report','NovalichesController@financialreport')->middleware('auth');

/** scorecard */
Route::get('novaliches/scorecard-season/{season}','NovalichesController@scorecard')->middleware('auth');


Route::get('/novaliches/bookTransferRecord','NovalichesController@bookTransfer_table')->middleware('auth');
Route::get('/novaliches/book-transfer','NovalichesController@book_transfer')->middleware('auth');



// ======================= RA dashbaord Routes ===========================

Route::get('/radashboard/attendance', 'AdminController@attendance');
Route::get('/radashboard/evaluation', 'AdminController@evaluation');
Route::get('/radashboard/lecturer', 'AdminController@lecturer');
Route::get('/radashboard/result-analysis', 'AdminController@result_analysis');



Route::get('/radashboard/viewLecturerReports', 'NovalichesController@filter_lecturer');
Route::get('/radashboard/onlineCompletion', 'NovalichesController@onlineCompletion');


Route::get('/radashboard/filter-exam', 'AdminController@filter_exam');
Route::post('/radashboard/exam-monitoring ', 'AdminController@exam_monitoring');
Route::get('/fetch-book-list/{id}','NovalichesController@bookTransfer_table_fetch')->middleware('auth');
Route::get('/novaliches/delete-expense/{id}','NovalichesController@delete_expense')->middleware('auth');


// ====================== Task to do list =======================================
Route::get('/todolist','AdminController@task_todolist')->middleware('auth');
Route::post('/task_image','AdminController@task_image')->middleware('auth');


// ====================== New Routes =======================================


//payment

Route::get('/new-payment/{branch}','Payment\PaymentController@new_payment')->middleware('auth');
Route::post('/payment/insert-studentPayment','Payment\PaymentController@store_payment')->middleware('auth');
Route::post('/requesting-studentdatainPayment','Payment\PaymentController@studentDataInPayment')->middleware('auth');
Route::post('/requesting-programdiscount','Payment\PaymentController@programDiscount')->middleware('auth');
Route::post('/payment/getBalance','Payment\PaymentController@getBalance')->middleware('auth');
//per branch

//add-enrollee
Route::post('/getseasonyearandstatus','cbrc\AddEnrolleeController@getSeasonYearAndStatus')->middleware('auth');


//Reservation




//current fixing

// Route::post('/novaliches/insert-employee','NovalichesController@insert_employee')->middleware('auth');

// Route::get('/HRIS/add_employee_view','HRIS\EmployeeController@add_employee')->middleware('auth');
// Route::post('/proceed/add_employee','HRIS\EmployeeController@proceed_add_employee')->middleware('auth');


// Route::get('/novaliches/add-employee','NovalichesController@add_employee')->middleware('auth');
// Route::get('/novaliches/employee-record','NovalichesController@employee_record')->middleware('auth');
// Route::post('/novaliches/update-employee','NovalichesController@update_employee')->middleware('auth');
// Route::get('/novaliches/delete-employee/{id}','NovalichesController@delete_employee')->middleware('auth');

// // Member
// Route::get('/novaliches/view-employee','NovalichesController@view_employee')->middleware('auth');

// Route::post('/novaliches/update-employee','NovalichesController@update_employee')->middleware('auth');
// Route::get('/novaliches/delete-employee/{id}','NovalichesController@delete_employee')->middleware('auth');

// Route::get('/radashboard/ra-useraccount', 'AdminController@create_user');
// Route::delete('/delete-user','AdminController@delete_user')->middleware('auth');
// Route::post('/insert-user','AdminController@insert_user')->middleware('auth');

// Route::post('/update-user','AdminController@update_user')->middleware('auth');
// Route::get('/users','AdminController@user')->middleware('auth');

// /* check username and email if exist*/
// Route::post('/email_available/checkemail', 'AdminController@checkemail')->name('email_available.checkemail');
// Route::post('/username_available/checkusername', 'AdminController@checkusername')->name('username_available.checkusername');

// Route::get('/fetch-userinfo/{uid}','MainController@get_userinfo')->middleware('auth');


// Route::get('/finance/{branch}','cbrc\MakeTransactionController@finance')->middleware('auth');
// Route::get('/finance/expenses/{branch}','cbrc\MakeTransactionController@expenses')->middleware('auth');
// Route::get('/finance/sales/{branch}','cbrc\MakeTransactionController@sales')->middleware('auth');


//current fixing





//added by mike
//page route
// dashboard
Route::get('/dashboard','Dashboard\DashboardController@admin_dashboard')->middleware('auth');
Route::get('/dashboard/{branch}','Dashboard\DashboardController@branch_dashboard')->middleware('auth');
// dashboard

//transactions
Route::get('/new_enrollee/{branch}','Page\PageController@new_enrollee')->middleware('auth');
Route::get('/new_payment/{branch}','Page\PageController@new_payment')->middleware('auth');
Route::get('/new_reservation/{branch}','Page\PageController@new_reservation')->middleware('auth');
Route::get('/new_expense/{branch}','Page\PageController@new_expense')->middleware('auth');
Route::get('/new_budget/{branch}','Page\PageController@new_budget')->middleware('auth');
Route::get('/new_book_payment/{branch}','Page\PageController@new_book_payment')->middleware('auth');
Route::get('/new_cash_remit/{branch}','Page\PageController@new_cash_remit')->middleware('auth');
//transactions

//reports
Route::get('/today_transactions/{branch}','Page\PageController@today_transactions')->middleware('auth');
Route::get('/yesterday_transactions/{branch}','Page\PageController@yesterday_transactions')->middleware('auth');
Route::get('/score_card/{branch}','Page\PageController@score_card')->middleware('auth');
Route::get('/financial_report/{branch}','Page\PageController@financial_report')->middleware('auth');
//reports


//records
Route::get('/master_list/{branch}','Page\PageController@master_list')->middleware('auth');



Route::get('/admin/books/{branch}','Page\PageController@books')->middleware('auth');
Route::get('/admin/finance/{branch}','Page\PageController@finance')->middleware('auth');
Route::get('/admin/sales/{branch}','Page\PageController@sales')->middleware('auth');
Route::get('/admin/recievables/{branch}','Page\PageController@recievables')->middleware('auth');
Route::get('/admin/expense/{branch}','Page\PageController@expense')->middleware('auth');
Route::get('/admin/tuition_fees/{branch}','Page\PageController@tuition_fees')->middleware('auth');
Route::get('/admin/discounts/{branch}','Page\PageController@discounts')->middleware('auth');
Route::get('/admin/ra_files/{branch}','Page\PageController@ra_files')->middleware('auth');
//records

//page route



//books
Route::post('/book/proceed/add_book','Book\BookController@add_book')->middleware('auth');
Route::post('/book/proceed/update_book','Book\BookController@update_book')->middleware('auth');

Route::get('/data/fetch-book-data/{book_id}','Book\BookDataFetchController@fetch_book_data')->middleware('auth');

//books

//Discounts

Route::post('/discount/proceed/update_discount','Discount\DiscountController@update_discount')->middleware('auth');
Route::post('/discount/proceed/add_discount','Discount\DiscountController@add_discount')->middleware('auth');
//Discounts


//tuition fee

Route::post('/tuition/proceed/update_tuition','Tuition\TuitionController@update_tuition')->middleware('auth');
Route::post('/tuition/proceed/add_tuition','Tuition\TuitionController@add_tuition')->middleware('auth');
//tuition fee


// hris
Route::get('/HRIS/record/{branch}','HRIS\EmployeeController@hris')->middleware('auth');

Route::get('/hris/datafetch/{user_id}','HRIS\UserDataFetchController@user_data')->middleware('auth');
Route::post('/email_available/checkemail','HRIS\UserDataFetchController@checkmail')->name('email_available.checkemail');

Route::get('/hris/users_list','HRIS\UserController@user_list')->middleware('auth');
Route::post('/hris/proceed/add_user','HRIS\UserController@add_user')->middleware('auth');
Route::post('/hris/proceed/update_user','HRIS\UserController@update_user')->middleware('auth');
Route::delete('/hris/proceed/delete_user','HRIS\UserDataFetchController@delete_user')->middleware('auth');
// hris



Route::post('/proceed/transact/reserve','cbrc\ReservationController@proceed_reserve_student')->middleware('auth');
Route::get('/fetch-student/{program}/{branch}','Student\StudentDatafetchController@fetch_student')->middleware('auth');
Route::get('/fetch_student_if_enrolled_or_reserved/{data}','Student\StudentDatafetchController@fetch_student_if_enrolled_or_reserved')->middleware('auth');
Route::get('/fetch_tuition_by_category/{category_id}/{program}/{branch}','Tuition\TuitionDataFetchController@fetch_tuition_by_category')->middleware('auth');
Route::get('/fetch_discount_by_program/{program}/{branch}','Discount\DiscountDataetchController@fetch_discount')->middleware('auth');
Route::get('/fetch_discount_by_discount/{discount_id}/{branch}','Discount\DiscountDatafetchController@fetch_discount_data')->middleware('auth');
//foor let
Route::get('/fetch_tuition_by_category_let/{category_id}/{program}/{major_id}/{branch}','Tuition\TuitionDataFetchController@fetch_tuition_by_category_let')->middleware('auth');

Route::post('/new_payment/proceed/make_transact_enroll','cbrc\PaymentController@proceed_enroll_student')->middleware('auth');
Route::post('/new_payment/proceed/make_transact_add_payment','cbrc\PaymentController@proceed_add_payment')->middleware('auth');
Route::post('/new_payment/proceed/make_transact_enroll_reserve','cbrc\PaymentController@proceed_enroll_reserved_student')->middleware('auth');



Route::get('/fetch_check_student_status/{student_id}/{branch}','Student\StudentDatafetchController@fetch_check_student_status')->middleware('auth');
//all involved with make transaction navigation



//expense

Route::get('/finance/add_expense_category/{branch}','Finance\ExpenseController@add_expense_category')->middleware('auth');

Route::get('/finance/fetch_sub_category/{category_id}','Finance\ExpenseDataFetchController@fetch_sub_category')->middleware('auth');
Route::post('/proceed/insert_expense','Finance\ExpenseController@proceed_insert_expense')->middleware('auth');

Route::post('/proceed/add_expense_category','Finance\ExpenseController@proceed_insert_category')->middleware('auth');
Route::post('/proceed/add_expense_sub_category','Finance\ExpenseController@proceed_insert_sub_category')->middleware('auth');



Route::post('/proceed/insert_remit','Finance\RemitController@proceed_insert_remit')->middleware('auth');
Route::post('/proceed/insert_budget','Finance\BudgetController@proceed_insert_budget')->middleware('auth');
//expense






//Student Record
Route::get('/enrolled_list/{branch}','Student\StudentViewController@enrolled_list')->middleware('auth');
Route::get('/reserved_list/{branch}','Student\StudentViewController@reserved_list')->middleware('auth');
Route::get('/dropped_list/{branch}','Student\StudentViewController@dropped_list')->middleware('auth');
Route::get('/scholar_list/{branch}','Student\StudentViewController@scholar_list')->middleware('auth');

Route::get('/student-summary/{id}','Student\StudentViewController@student_summary')->middleware('auth');


Route::get('/enroll_reserved/{card_id}/{student_id}','Student\StudentViewController@enroll_reserved_student')->middleware('auth');
Route::get('/reserve_student/{student_id}','Student\StudentViewController@reserve_student')->middleware('auth');
Route::get('/enroll_student/{student_id}','Student\StudentViewController@enroll_student')->middleware('auth');


Route::get('/card_payments/{card_id}','Student\StudentDatafetchController@card_payments');
Route::get('/payment-info/{payment_id}','Student\StudentDatafetchController@payment_information')->middleware('auth');

Route::get('/fetch_major','Student\StudentDataFetchController@fetch_major')->middleware('auth');

Route::get('/data/fetch-category','Student\StudentDatafetchController@fetch_category')->middleware('auth');
Route::get('/data/fetch-tuition/{category_id}/{program}/{branch}','Tuition\TuitionDataFetchController@fetch_tuition')->middleware('auth');
Route::get('/data/fetch-tuition-data/{tuition_id}','Tuition\TuitionDataFetchController@fetch_tuition_data')->middleware('auth');

Route::get('/data/fetch-discount/{program}/{branch}','Discount\DiscountDataFetchController@fetch_discount')->middleware('auth');
Route::get('/data/fetch-discount-data/{discount_id}','Discount\DiscountDataFetchController@fetch_discount_data')->middleware('auth');

//post

Route::post('/proceed/save_student','cbrc\AddEnrolleeController@save_student')->middleware('auth');
Route::post('/proceed/reserve','Student\StudentTransactionController@proceed_reserve_student')->middleware('auth');

Route::post('/proceed/enroll_reserved_student','Student\StudentTransactionController@proceed_enroll_reserved_student')->middleware('auth');
Route::post('/proceed/proceed_add_payment','Student\StudentTransactionController@proceed_add_payment')->middleware('auth');
Route::post('/proceed/enroll','Student\StudentTransactionController@proceed_enroll_student')->middleware('auth');
//Student Record




//expenses

Route::get('/expense-test','payment\ExpenseDataController@index');

//added by mike
//==========end=====
Route::get('500', function()
{
	abort(500);
});


//member 

// Route::get('dashboard/{bid}','Dashboard\DashboardController@branch_dashboard');

//bagong dashboard


Route::get('/dashboard/branch/{branch}','Dashboard\DashboardController@mainXbranch_dashboard');