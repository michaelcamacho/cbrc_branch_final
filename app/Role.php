<?php

namespace App;


use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    //
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'role_user';
}
