<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ExpenseSetup extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $guarded = [];
}
