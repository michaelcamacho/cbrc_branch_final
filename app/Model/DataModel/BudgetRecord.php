<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class BudgetRecord extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'budgets';
    protected $fillable = [
        'id',
        'category'

    ];
}
