<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class extends Model
{
    protected $fillable[
    	'program',
		'class',
		'aka',
    ];
}
