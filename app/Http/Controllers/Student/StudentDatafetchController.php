<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use DB;
use Alert;
use Auth;
use App\Model\DataModel\Tuition;
use App\Model\AdminSettings;
use App\Branch;
use App\Category;
use App\CbrcStudent;
use App\StudentEnrollmentCard;
use App\PaymentInfo;
use App\Program;
use App\Major;

class StudentDatafetchController extends Controller
{
   
public function fetch_category(){
    $fetch_category = Category::all();
    return response()->json($fetch_category);
 }


 //student data fetch
 public function fetch_student( $program , $branch ){
   
$branch = Branch::where('branch_name' , '=' ,$branch)->value('id');


  $student = CbrcStudent::leftJoin('programs','programs.id','=','cbrc_students.program')
        ->leftJoin('majors','majors.id','=','cbrc_students.major')
        ->select('cbrc_students.*','programs.program_name','majors.major')
        ->where('cbrc_students.program' , '=' , $program)
        ->where('cbrc_students.branch' , '=' , $branch)
        ->get();

  return response()->json($student);
}



public function fetch_student_if_enrolled_or_reserved( $data ){
  
$info = explode('_',$data);
  $latest_card = StudentEnrollmentCard::leftJoin('card_status','card_status.id','=','student_enrollment_card.status')
  ->select('student_enrollment_card.*','card_status.status As status_name')
  ->where('student_id','=',$info[0])
  ->where('season','=',$info[1])
  ->where('year','=',$info[2])
  ->first();

  if(!$latest_card){
    $message = "no card";

  }else{

    $message = "exist";
  }

  return response()->json([
    'message' => $message,
    'latest_card' => $latest_card
  ]);
}




public function fetch_check_student_status( $student_id , $branch ){
  
$settings = AdminSettings::findorfail(1);

    $latest_card = StudentEnrollmentCard::where('season','=', $settings->season)
    ->where('year','=',$settings->year)
    ->where('student_id','=',$student_id)
    ->first();
  
    $student = CbrcStudent::where('id' , '=' , $student_id)->first();
  

    if(!$latest_card){
      //option 1 means for enrollment 
      return response()->json([
        'option' => 1,
        'message' => "no card",
        'student' => $student,
        ]);
    }else{
      
        if($latest_card->status == 1){
          
          //option 2 means for new payment student status is enrolled
          $option = 2;
          
          $total_amount = StudentEnrollmentCard::where('id' , '=' , $latest_card->id)->value('balance');
          if($total_amount == 0){
            $option = 4;
          }


          return response()->json([
            'option' => $option,
            'message' => "exist",
            'total_amount' => $total_amount,
            'status' => $latest_card->status,
            'latest_card' => $latest_card,
            ]);    
        }
        else if($latest_card->status == 2){
          
          $firstPayment = PaymentInfo::where('card_id' , '=' , $latest_card->id)->first();
          //option 3 means for enrollment student status is reserved
          return response()->json([
            'option' => 3,
            'message' => "exist",
            'status' => $latest_card->status,
            'latest_card' => $latest_card,
            'firstPayment' => $firstPayment,
            ]);    
        }

    }
  
  
  } 






 public function payment_information ($payment_id){
    
    $payment_info  = PaymentInfo::where('id','=',$payment_id)->get();
    
    return response()->json($payment_info);
   }

   public function card_payments($card_id){
    $row_counter=1;
    $card_payments = PaymentInfo::where('card_id','=',$card_id)->orderBy('id' , 'desc')->get();
      
      $card_info = DB::connection('satelite_branch_db_connection')->table('student_enrollment_card')
      ->leftJoin('cbrc_students','cbrc_students.id','=','student_enrollment_card.student_id')
      ->leftJoin('majors','majors.id','=','student_enrollment_card.major_id')
      ->leftJoin('categories','categories.id','=','student_enrollment_card.category_id')
      ->leftJoin('card_status','card_status.id','=','student_enrollment_card.status')
      ->leftJoin('programs','programs.id','=','student_enrollment_card.program_id')
      ->leftJoin('discounts','discounts.id','=','student_enrollment_card.discount_category_id')
      ->select('student_enrollment_card.*','card_status.status As status_name','cbrc_students.first_name As first_name','cbrc_students.middle_name As middle_name','cbrc_students.last_name As last_name','categories.category_name As category_name')
      ->where('student_enrollment_card.id' , '=' , $card_id)
      ->get();



      return response()->json([

          'row_counter' => $row_counter,
          'card_payments' => $card_payments,
          'card_info' => $card_info,
      ]);
  }


//student data fetch



//fetch major
public function fetch_major(){
  $major = Major::all();
  return response()->json($major);
}
}
