<?php

namespace App\Http\Controllers\Tuition;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Category;
use App\Branch;
use App\CbrcStudent;
use App\StudentEnrollmentCard;
use App\Model\AdminSettings;
use App\PaymentInfo;
use App\Program;
use App\Reservationfee;
use App\Model\DataModel\Tuition;

class TuitionDataFetchController extends Controller
{
    
  


  public function fetch_tuition_data($tuition_id){
                
    
    $tuition_data = Tuition::leftJoin('categories','tuitions.category','=','categories.id')
    ->leftJoin('programs','tuitions.program_id','=','programs.id')
    ->leftJoin('branches','branches.id','=','tuitions.branch_id')
    ->select('tuitions.*','branches.branch_name As branch_name','categories.category_name As category_name' , 'programs.program_name As program_name' )
    ->where('tuitions.id','=',$tuition_id)->get();

    
     return response()->json($tuition_data);
  }



  
  public function fetch_tuition($category_id , $program , $branch){
   $branch_id = Branch::where('branch_name', '=' , $branch)->first();

   $fetch_tuition = Tuition::leftJoin('categories','tuitions.category','=','categories.id')
   ->leftJoin('programs','tuitions.program_id','=','programs.id')
   ->select('tuitions.*','categories.category_name As category_name' , 'programs.program_name As program_name' )
   ->where('tuitions.branch_id','=',$branch_id->id)
   ->where('tuitions.program_id','=',$program)
   ->where('tuitions.category','=',$category_id)->get();

   
    return response()->json($fetch_tuition);
 }
  
 



 public function fetch_tuition_by_category($category_id , $program , $branch){
  $branch_id = Branch::where('branch_name', '=' , $branch)->first();

  $fetch_tuition = Tuition::join('categories','tuitions.category','=','categories.id')
  ->join('programs','tuitions.program_id','=','programs.id')
  ->select('tuitions.*','categories.category_name As category_name' , 'programs.program_name As program_name' )
  ->where('tuitions.branch_id','=',$branch_id->id)
  ->where('tuitions.program_id','=',$program)
  ->where('tuitions.category','=',$category_id)->first();

  
   return response()->json($fetch_tuition);
}


public function fetch_tuition_by_category_let($category_id , $program, $major_id , $branch){
  $branch_id = Branch::where('branch_name', '=' , $branch)->first();
  if($major_id == 1){
  
  $fetch_tuition = Tuition::leftJoin('categories','tuitions.category','=','categories.id')
  ->leftJoin('programs','tuitions.program_id','=','programs.id')
  ->leftJoin('majors','tuitions.major_id','=','majors.id')
  ->select('tuitions.*','categories.category_name As category_name' , 'majors.major As major_name','programs.program_name As program_name' )
  ->where('tuitions.major_id','=',$major_id)
  ->where('tuitions.branch_id','=',$branch_id->id)
  ->where('tuitions.program_id','=',$program)
  ->where('tuitions.category','=',$category_id)
  ->first();
 
 }else{
  $fetch_tuition = Tuition::latest('id')->leftJoin('categories','tuitions.category','=','categories.id')
  ->leftJoin('programs','tuitions.program_id','=','programs.id')
  ->select('tuitions.*','categories.category_name As category_name' ,'programs.program_name As program_name' )
  ->where('tuitions.branch_id','=',$branch_id->id)
  ->where('tuitions.program_id','=',$program)
  ->where('tuitions.category','=',$category_id)
  ->first(); 
 }
   return response()->json($fetch_tuition);
}

}
