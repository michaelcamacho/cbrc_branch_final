<?php

namespace App\Http\Controllers\cbrc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Program;
use App\Model\AdminSettings;
use App\Branch;
use App\CbrcStudent;
use App\Major;
use DB;
use Auth;
use Alert;
class AddEnrolleeController extends Controller
{
    public function __construct()
    {
    
         $this->middleware('role:Member|Admin');
        
    }
    public function add_enrollee($branch){
        $program = Program::all();
        $Branches = $this->list_branches();
        return view('member.add-enrollee')->with('bid','')
        ->with('Branches',$Branches)->with('branch',$branch)->with('program',$program)->with('brancher',Auth::user()->branch);
}

    public function getSeasonYearAndStatus(Request $request){
         $program = $request->input('program');
        $yearSeason = AdminSettings::select('Season','Year')->where('branch',Auth::user()->branch)
        ->where('Account',strtolower(Auth::user()->role))->get();
         $major = DB::connection('satelite_branch_db_connection')->table('majors')
         ->join('programs','programs.id', '=', 'majors.program_id')
         ->select('majors.major','majors.aka')
         ->where('programs.id', $program)
         ->get();
         $datasofmajorandyearseason = [
             'major' => $major,
             'yearseason' => $yearSeason,
         ];

         
        return response()->json($datasofmajorandyearseason);
    }

    public function save_student(Request $request){
        $input = $request->except(['_token']);
        $date = date('Y-m-d');
        $branch_id = Branch::where('branch_name','=', strtolower($input['branch']))->first();
      
        CbrcStudent::insert([
            "branch" => $branch_id->id,
            "last_name" => $input['last_name'],
            "first_name" => $input['middle_name'],
            "middle_name" => $input['first_name'],
            "username" => $input['username'],
            "password" => $input['password'],
            // "course" => '',
            "major" => $input['major'],
            "program" => $input['program'],
            "school" => $input['school'],
            "noa_no" => $input['noa_no'],
            "birthdate" => $input['birthdate'],
            "contact_no" => $input['contact_no'],
            "email" => $input['email'],
            "take" => $input['no_take'],
            "address" => $input['address'],
            "contact_person" =>  $input['contact_person'],
            "contact_details" => $input['contact_details'],
            "registration" => $date,
        ]);

        Alert::success('Success!', 'New student has been registered.');
        return redirect('/new_enrollee/'.$input['branch']);
    }
}