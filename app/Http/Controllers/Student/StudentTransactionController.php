<?php

namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Category;
use App\Branch;
use App\CbrcStudent;
use App\StudentEnrollmentCard;
use App\Model\AdminSettings;
use App\PaymentInfo;
use App\Program;
use App\Reservationfee;
use App\Model\DataModel\Tuition;


class StudentTransactionController extends Controller
{
   
    public function proceed_enroll_reserved_student(Request $request){
            

        
        $registration_date=date('M-d-Y');
        $input = $request->except(['_token']);
        $status = 1;

        $card_balance = $input['total_due'] -  $input['amount_paid'] ;

        StudentEnrollmentCard::where('id','=',$input['card_id'])->update([
            'date_registered' => $registration_date ,
            'facilitation_price' => $input['facilitation_fee'] , 
            'tuition_fee_price' => $input['tuition_fee_price'] , 
            'discount_category_id' => $input['drop-down-discount'] , 
            'balance' => $card_balance, 
            'discount' => $input['discount_amount'] , 
            'category_id' => $input['drop-down-category'] , 
            'section' => $input['drop-down-section'] , 
            'status' => $status,
        ]);

        
        PaymentInfo::insert([
            'payment_date' => $registration_date ,
            'payment_no' => 2,
            'card_id' => $input['card_id'],
            'amount_paid' => $input['amount_paid'] ,
            'balance_before' => $input['total_due'] ,
            'balance_after' => $card_balance ,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 ,
        ]);



        return redirect('/student-summary/'.$input['student_id']);
    }



    public function proceed_enroll_student(Request $request){
        $season_year = AdminSettings::findorfail(1);    

        
        $registration_date=date('M-d-Y');
        $input = $request->except(['_token']);
        $status = 1;
        $card_balance = $input['total_due'] -  $input['amount_paid'] ;

        StudentEnrollmentCard::insert([
            'date_registered' => $registration_date ,
            'student_id' => $input['student_id'],
            'program_id' => $input['hidden_program_id'],
            'major_id' => $input['hidden_major_id'],
            'facilitation_price' => $input['facilitation_fee'] , 
            'tuition_fee_price' => $input['tuition_fee_price'] , 
            'discount_category_id' => $input['drop-down-discount'] , 
            'balance' => $card_balance, 
            'discount' => $input['discount_amount'] , 
            'category_id' => $input['drop-down-category'] , 
            'section' => $input['drop-down-section'] , 
            'season' => $season_year->season,
            'year' => $season_year->year,
            'status' => $status,
        ]);

        
        $latest_card = StudentEnrollmentCard::latest('id')
        ->where('student_id','=',$input['student_id'])->first();
       
        
        
        PaymentInfo::insert([
            'payment_date' => $registration_date ,
            'payment_no' => 1,
            'card_id' => $latest_card->id,
            'amount_paid' => $input['amount_paid'] ,
            'balance_before' => $input['total_due'] ,
            'balance_after' => $card_balance ,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 ,
        ]);



        return redirect('/student-summary/'.$input['student_id']);
    }







    public function proceed_reserve_student(Request $request){
                 
        $status = 2;
        $registration_date=date('M-d-Y');
        $input = $request->except(['_token']);
        

        StudentEnrollmentCard::insert([
            'date_registered' => $registration_date ,
            'student_id' => $input['student_id'] ,
            'category_id' => $input['drop-down-category'] ,
            'season' => $input['drop-down-season'] ,
            'year' => $input['drop-down-year'] ,
            'status' => $status,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 
        ]);

        $latest_card = StudentEnrollmentCard::latest('id')
        ->where('student_id','=',$input['student_id'])->first();
       

        
        PaymentInfo::insert([
            'payment_date' => $registration_date ,
            'payment_no' => 1,
            'card_id' => $latest_card->id,
            'amount_paid' => $input['amount_paid'] ,
            'balance_before' => 0 ,
            'balance_after' => 0 ,
            'created_by_id' => Auth::user()->id, 
            'deleted' => 0 
        ]);


        return redirect('/student-summary/'.$input['student_id']);
    }



    public function proceed_add_payment(Request $request){

        $payment_date=date('M-d-Y');
        $input = $request->except(['_token']);

    
        
            $card_balance = $input['add_payment_card_balance'] -  $input['amount_paid'] ;

            StudentEnrollmentCard::where('id','=',$input['add_payment_card_id'])->update([

                'balance' => $card_balance, 
            ]);


            $last_payment = PaymentInfo::latest('id')
            ->where('card_id','=',$input['add_payment_card_id'])->first();


            PaymentInfo::insert([
                'payment_date' => $payment_date ,
                'payment_no' => $last_payment->payment_no + 1 ,
                'card_id' => $input['add_payment_card_id'],
                'amount_paid' => $input['amount_paid'] ,
                'balance_before' => $input['add_payment_card_balance'],
                'balance_after' => $card_balance,
                'created_by_id' => Auth::user()->id, 
                'deleted' => 0 
            ]);


        return redirect('/student-summary/'.$input['add_payment_student_id']); 
    }
}
