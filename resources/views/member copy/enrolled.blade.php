@extends('main')
@section('title')
Enrolled Records
@stop
@section('css')
<style type="text/css">
.form-control:disabled, .form-control[readonly] {
color: #000;
}
@media print {
.content-header, .left-side, .main-header, .main-sidebar, .no-print {
display: none!important;
}
.table {
border-collapse: collapse !important;
}
td{
color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
<!-- Page Title Area -->
<div class="container-fluid">
<div class="row page-title clearfix">
<div class="page-title-left">
<h6 class="page-title-heading mr-0 mr-r-5">Enrolled Records</h6>

</div>
<!-- /.page-title-left -->
<div class="page-title-right d-none d-sm-inline-flex">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="#">Dashboard</a>
</li>
<li class="breadcrumb-item">Records</li>
<li class="breadcrumb-item active">Enrolled Records</li>
</ol>
</div>
<!-- /.page-title-right -->
</div>
<!-- /.page-title -->
</div>
<!-- /.container-fluid -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="container-fluid">
<div class="widget-list">
<div class="row">
<div class="col-md-12 widget-holder">
<div class="widget-bg">
<div class="widget-body clearfix">
<h5 class="box-title mr-b-0">Enrolled Records</h5>
<br/>

</div>

<div class="widget-body clearfix">
<h3>Season 1</h3>
<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
<thead>

<tr>
<th>Name</th>
<th>CBRC ID</th>
<th>Program</th>
<th>Section</th>
<th>Category</th>
<th>Contact #</th>
<th>Email</th>
<th>Address</th>
<th>Facilitation</th>
<th>Birthdate</th>
<th>School</th>
<th>Major</th>
<th>Noa #</th>
<th>Take</th>
<th>Contact Person</th>
<th>Emergency #</th>
<th>Date Registered</th>
<th>Tuition Fee</th>
<th>Amount Paid</th>
<th>Total Balance</th>
<th>Facilitation Fee</th>
<th>Discount</th>
</tr>

</thead><tbody>

{{-- season 1 --}}
@if(isset($let))
@foreach($let as $lets)
@if($lets->season == "Season 2")
<tr>
<td>{{$lets->last_name}}, {{$lets->first_name}} {{$lets->middle_name}}</td>
<td>{{$lets->cbrc_id}}</td>
<td>{{$lets->course}}</td>
<td>{{$lets->section}}</td>
<td>{{$lets->category}}</td>
<td>{{$lets->contact_no}}</td>
<td>{{$lets->email}}</td>
<td>{{$lets->address}}</td>
<td>{{$lets->facilitation}}</td> 
<td>{{$lets->birthdate}}</td>
<td>{{$lets->school}}</td>
<td>{{$lets->major}}</td>
<td>{{$lets->noa_no}}</td>
<td>{{$lets->take}}</td>
<td>{{$lets->contact_person}}</td>
<td>{{$lets->contact_details}}</td>
<td>{{\Carbon\Carbon::parse($lets->created_at)->format('M-d-Y')}}</td>
@foreach($s1Summary as $s1Sum)
@if($lets->id == $s1Sum->student_id & $s1Sum->program == "LET" )
<td>{{$s1Sum->tuition_fee}}</td>
<td>{{$s1Sum->amount_paid}}</td>
<td>{{$s1Sum->total_balance}}</td>
<td>{{$s1Sum->facilitation_fee}}</td>
<td>{{$s1Sum->discount}}</td>
@endif
@endforeach
</tr>
@endif
@endforeach
@endif


</tbody>
</table>

</div>

<!-- /.widget-bg -->
</div>
<!-- /.widget-holder -->
</div>
<!-- /.row -->
</div>
<!-- /.widget-list -->
</div>
<!-- /.container-fluid -->
</main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
$('table.display').DataTable();
});

$(document).ready(function() {
$('.record').addClass('active');
$('.record').addClass('collapse in');
});

</script>
@stop