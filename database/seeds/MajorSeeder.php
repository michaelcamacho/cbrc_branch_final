<?php

use Illuminate\Database\Seeder;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'BEED',
            'aka' => 'BEED',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'MATHEMATICS',
            'aka' => 'MATH',
        ]);

         DB::table('majors')->insert([
            'program' => 'lets',
            'major' => 'ENGLISH',
            'aka' => 'ENGLISH',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'FILIPINO',
            'aka' => 'FILIPINO',
        ]);

         DB::table('majors')->insert([
            'program' => 'lets',
            'major' => 'TLE',
            'aka' => 'TLE',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'BIOLOGICAL SCIENCE',
            'aka' => 'BIO SCI',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'PHYSICAL SCIENCE',
            'aka' => 'PHY SCI',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'SOCIAL SCIENCE',
            'aka' => 'SOC SCI',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'MAPEH',
            'aka' => 'MAPEH',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'VALUES',
            'aka' => 'VALUES',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'AGRICULTURE AND FISHERY ARTS',
            'aka' => 'AFA',
        ]);

         DB::table('majors')->insert([
        	'program' => 'lets',
            'major' => 'UFO',
            'aka' => 'UFO',
        ]);

    }
}
