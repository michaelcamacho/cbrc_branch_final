<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class ExpenseProgramList extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = "expense_program_list";

    protected $fillable = [
    "id",
    "program_name"
    ];
}
