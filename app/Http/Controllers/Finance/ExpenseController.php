<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DataModel\Expense;
use App\Model\DataModel\ExpenseCategory;
use App\Model\DataModel\ExpenseSubcategory;
use App\Model\AdminSettings;
use App\Branch;
use Alert;
use Auth;

class ExpenseController extends Controller
{
    public function __construct()
    {
            $this->middleware('auth');
            $settings = AdminSettings::findOrFail(1);
            $this->year = $settings->year;
            $this->settings = $settings;
            $main_id = Branch::select('main_branchid')->get();
            $this->Branches = Branch::whereNotIn('id', $main_id)->get();
    }
    

    public function proceed_insert_expense( Request $request ){
        $date=date('M-d-Y');
        $input = $request->except(['_token']);
        $branch_id = Branch::where("branch_name" , "=" , strtolower(request("branch")) )->first();

        Expense::insert([
            "date" => $date , 
            "branch_id" => $branch_id->id,
            "program_id" => request("program"), 
            "subcategory_id" =>  request("sub_category"),
            "amount" => request("amount"),
            "remarks" => request("remarks"),
            "author" => Auth::user()->id,
            "season" => request("season"),
            "year" => request("year"),
        ]);



        Alert::success('Success!', 'Expense saved.');
        return redirect("/new_expense/" . strtolower(request("branch")));
    }



    public function add_expense_category($branch){

        $expense_category =ExpenseCategory::all();
        
        return view('finance.add_expense_category')
        ->with('Branches',$this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('expense_category' , $expense_category)
        ->with('branch' , $branch);
    }

    public function proceed_insert_category(Request $request){
        $branch = $request->input('branch');
        $input = $request->except(['_token']);

        ExpenseCategory::insert([
            "category" => $input['category']
        ]);
      
        Alert::success('Success!', 'New Expense Category Saved.');
       
      return redirect('/finance/add_expense_category/'.$branch);
    }
    
    
    public function proceed_insert_sub_category(Request $request){
        $branch = $request->input('branch');
        $input = $request->except(['_token']);
        
        ExpenseSubcategory::insert([
            "category_id" => $input['category'],
            "sub_category" => $input['sub_category']
        ]);
        Alert::success('Success!', 'New Expense Sub Category Saved.');
       
      return redirect('/finance/add_expense_category/'.$branch);
    }
    

}
