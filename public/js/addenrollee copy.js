//FUNCTIONS in BUTTON
let season;
let year;
//global setting
const config = async(data,url,customizefunction) =>{
    let setting ={
        method:'POST',
        body:JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    };
    const response = await fetch(url,setting);
    customizefunction(response,data);
}
//

    const getYearAndSeason = async (response,data) =>{
        let res = await response.json();
        console.log(res);
        let majors = res.major;
        let SeasonYears = res.yearseason;
                majors.forEach((major,index)=>{
                    $('#major').append(`<option value="${major.aka}">${major.major}</option>`);
                });

            const SeasonFunction = (season) =>{
                if(season == "Season 1"){return "Season 2"}else{return "Season 1"};
            }
            
                SeasonYears.forEach((SeasonYear,index)=>{
                    year = SeasonYear.Year;
                    season = SeasonYear.Season;
                    $('#year').append(`<option value="${SeasonYear.Year}">${SeasonYear.Year}</option><option id='year' value="${parseInt(SeasonYear.Year)+parseInt(1)}">${parseInt(SeasonYear.Year)+parseInt(1)}</option><option id='year' value="${parseInt(SeasonYear.Year)+parseInt(2)}">${parseInt(SeasonYear.Year)+parseInt(2)}</option><option id='year' value="${parseInt(SeasonYear.Year)+parseInt(3)}">${parseInt(SeasonYear.Year)+parseInt(3)}</option><option id='year' value="${parseInt(SeasonYear.Year)+parseInt(4)}">${parseInt(SeasonYear.Year)+parseInt(4)}</option>`);
                    $('#season').append(`<option value="${SeasonYear.Season}">${SeasonYear.Season}</option><option id='seasons' value="${SeasonFunction(SeasonYear.Season)}">${SeasonFunction(SeasonYear.Season)}</option>`);
                });
                $('#STATUS').val("");
                $('.status').append(`<font style="color:blue;">Enrollee</font>`);
    }

    const getstatus = (data) =>{
        console.log(data)
            if(season == data.seasonid && year == data.yearid){
                $('#STATUS').empty();
                $('#STATUS').val("enrolled");
                $('.status').append(`<font style="color:blue;">Enrollee</font>`);
            
            }else{
                $('#STATUS').empty();
                $('#STATUS').val("Reserve");
                $('.status').append(`<font style="color:red;">Reserve</font>`);
            };
    }
//BUTTONS 
$('#program').change(()=>{
    $('#button-prevent').prop('disabled',false);
    $('#major').empty();
    $('#season').empty();
    $('#year').empty();
    $('.status').empty();
    const program = {
        program: $('#program').children("option:selected").val()
    };
    config(program,'/getseasonyearandstatus',getYearAndSeason);
});


$('#season').on('change',()=>{
    $('.status').empty();
    let value = {
    seasonid:$('#season').val(),
    yearid:$('#year').val(),
    };
    getstatus(value);
});
$('#year').on('change',()=>{
    $('.status').empty();
    let value = {
    seasonid:$('#season').val(),
    yearid:$('#year').val(),
    };
    getstatus(value);
});
