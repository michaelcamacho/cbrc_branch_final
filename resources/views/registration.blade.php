<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pace.css') }}">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ONLINE REGISTRATION | CBRC - {{ isset($_COOKIE['system_title']) ? $_COOKIE['system_title'] : '' }}</title>
    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/material-icons/material-icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/mono-social-icons/monosocialiconsfont.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/vendors/feather-icons/feather.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/select2/4.0.5/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/jqvmap/1.5.1/jqvmap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/public.css') }}" rel="stylesheet" type="text/css">
    <!--  SweetAlert2  -->
      <script src="{{ asset('assets/sweetalert2/sweetalert2.min.js') }}"></script>
      <link href="{{ asset('assets/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/styles.css" rel="stylesheet') }}" />
    <link href="{{ asset('css/regi.css') }}" rel="stylesheet" />


    <meta name="author" content="cipherfusionphilippines.com" />
        <link rel="canonical" href="https://www.cipherfusionphilippines.com/" />
        <meta property="fb:admins" content="100001809531980">
        <meta property="fb:app_id" content="206704316521457">
        <meta property="og:image" content="https://cbrc-novaliches.com/img/fblogo.png"/>
        <meta property="og:locale" content="en_PH" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="CBRC Novaliches" />
        <meta property="og:description" content="The BIGGEST and LEADING Brand in TEST PREPARATION" />
        <meta property="og:url" content="https://cbrc-novaliches.com/cbrc/online-registration" />
        <meta property="og:site_name" content="CBRC Novaliches" />
        <!-- Mobile Specific Metas
                ================================================== -->
                <meta name="viewport" content="user-scalable=yes, initial-scale=1, maximum-scale=1, width=device-width">

                <meta name="application-name" content="CBRC Bicol"/>
                        <meta name="msapplication-square70x70logo" content="/img/small.jpg"/>
                        <meta name="msapplication-square150x150logo" content="/img/medium.jpg"/>
                        <meta name="msapplication-wide310x150logo" content="/img/wide.jpg"/>
                        <meta name="msapplication-square310x310logo" content="/img/large.jpg"/>
                        <meta name="msapplication-TileColor" content="#000000"/>
                <script type="text/javascript">
                    cdn_url = 'https://cbrc-novaliches.com/';
                </script>

                
    <!-- Head Libs -->
    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
    <script data-pace-options='{ "ajax": false, "selectors": [ "img" ]}' src="{{ asset('assets/js/pace.min.js') }}"></script>

    <style type="text/css">
    .date[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
    }
        .date[type=date]:focus::-webkit-datetime-edit {
        color: #495057 !important;
    }
    </style>
</head>

<body class="sidebar-light sidebar-expand navbar-brand-dark" id="data">
<div id="wrapper" class="wrapper">

@include('template.header-registration')

<div class="content-wrapper">
<main class="regi-main clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Online Registration</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Online Registration</a>
                            </li>
                            
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix" id="register">
                                    <h5 class="box-title mr-b-0">Personal Information</h5>
                                   <br/>
                                    <form class="form-material" id="form">
                                        @csrf

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="branch" id="branch" required>
                                                    <option value="" disable="true" selected="true">--- Select Branch ---</option>
                                                    @if(isset($branch))
                                                     @foreach($branch as $branches)
                                                    <option value="{{$branches->aka}}">{{$branches->branch_name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                    <label>Branch</label>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="last_name" type="text" id="last_name" required>
                                                    <label>Last Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="first_name" type="text" id="first_name" required>
                                                    <label>First Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="middle_name" id="middle_name" type="text" required>
                                                    <label>Middle Name &nbsp&nbsp(NM for nomiddle name)</label>
                                                </div>
                                            </div>
                                        </div>

                                       

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control date" name="birthdate" id="birthdate" type="date" required>
                                                    <label>Birth Date</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="contact_no" type="text" id="contact_no" required>
                                                    <label>Contact Number</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="address" type="text" id="address" required>
                                                    <label>Address</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="email" type="email" id="email" required>
                                                    <label>Email</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <br/><br/>
                                        <h5 class="box-title mr-b-0"> Academic Background</h5>
                                        <br/><br/>

                                         <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="school" type="text" id="school" required>
                                                    <label>School</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="program" id="program" required>
                                                        <option value="" disable="true" selected="true">--- Select Program ---</option>
                                                        @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->aka}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="major" id="major">
                                                        <option value="">----</option>
                                                    </select>
                                                    <label>Major</label>
                                                </div>
                                            </div>
                                        </div>

                                        

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                     <input class="form-control" name="take" type="text" id="take">
                                                    <label>Take</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                 <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                        </div>

                                        <br/><br/>
                                        <h5 class="box-title mr-b-0"> Contact Person In Case Of Emergency</h5>
                                        <br/><br/>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="contact_person" id="contact_person" required>
                                                    <label>Contact Person Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="contact_details" id="contact_details" required>
                                                    <label>Contact Person Number</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                 <div class="form-group">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                       
                                       <br/>
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary save" type="submit" id="button-prevent">Register</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>




</div>

@include('template.footer')

</div>

    <script src="{{ asset('assets/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/popper.js/1.14.3/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/metisMenu/2.7.9/metisMenu.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/countup.js/1.9.2/countUp.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/moment.js/2.22.2/moment.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/Chart.js/2.7.2/Chart.min.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js') }}"></script>
  
    <script src="{{ asset('assets/ajax/libs/jqvmap/1.5.1/jquery.vmap.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/jqvmap/1.5.1/maps/jquery.vmap.usa.js') }}"></script>
    <script src="{{ asset('assets/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/template.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('/js/prevent.js') }}"></script>
    <script type="text/javascript">

        $('#program').on('change', function(e){
            console.log(e);

            var program = e.target.value;

            $.get('/json-major?program=' + program,function(data){
                console.log(data);
                
                 $('#major').empty();
                 $('#major').append('<option value="" disable="true" selected="true">----</option>');
                 
                 $.each(data, function(index, majorObj){
                    $('#major').append('<option value="'+ majorObj.aka +'">'+ majorObj.major + '</option>');
                })
            });
        });
    
     $('#form').submit(function(event){
        event.preventDefault();
        event.stopPropagation();
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('branch', $('#branch').val());
                formData.append('last_name', $('#last_name').val());
                formData.append('first_name', $('#first_name').val());
                formData.append('middle_name', $('#middle_name').val());
                formData.append('birthdate', $('#birthdate').val());
                formData.append('contact_no', $('#contact_no').val());
                formData.append('address', $('#address').val());
                formData.append('email', $('#email').val());
                formData.append('school', $('#school').val());
                formData.append('program', $('#program').val());
                formData.append('major', $('#major').val());
                formData.append('take', $('#take').val());
                formData.append('contact_person', $('#contact_person').val());
                formData.append('contact_details', $('#contact_details').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               swal({
              title: 'Are you sure?',
              text: "Please double check all the Information you gave before proceeding.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, register me now!'
                }).then(function () {

                $.ajax({
                  url: "{{ url('/new-register') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success : function(data) {
                      swal({
                          title: data.title,
                          text: data.message,
                          type: data.check
                        }).then(function(){

                        $('#register form')[0].reset();
                        window.location = "/thank-you";
                      })
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          
                      })
                  }
                           
                    });
                });
            });

    </script>
    
</body>

</html>