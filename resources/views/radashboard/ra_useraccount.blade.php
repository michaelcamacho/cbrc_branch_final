@extends('main')
@section('title')
 RA User Accounts
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<style type="text/css">
  .raSpace{
    margin-top: 20px;
  }
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">  
        <div class="container raSpace">
            <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                           <div class="card-body">
                             <div class="form-group">
                                <h4 style="display:inline-block"> RA User Accounts</h4>
                                <input type="hidden" value="novaliches" id="branchName">
                                <button type="button" class="btn btn-primary" style="float:right; display:inline-block;" data-toggle="modal" data-target="#createUser">Add</button>

                                {{-- ADD USER MODAL --}}
                                

                                  {{-- RA USER TABLE --}}
                                <table class="table table-striped ">
                                    <thead>
                                        <tr>
                                            <td>Role</td>
                                            <td>Username</td>
                                            <td>Email</td>
                                            <td>Name</td>
                                            <td>Action</td>
                                            <td style="display:none;"></td>
                                        </tr>
                                    </thead>
                                    <tbody id="raUserTable">
                                        
                                    </tbody>
                                </table>
                            </div>  
                        </div>        
                    </div>  
               </div>
            </div>
        </div>
    </div>
</main>
{{-- RA CREATE ACCOUNT --}}

<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Ra Account</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id = "user-form">

                <input type="hidden" name="role" id="role" value="Ra" />
                <div class="form-group">
                        <label for="branch" class="col-form-label">Ra Branch</label>
                        <select class="form-control" name="branch" id="branch">
                            <option value="" selected="true" disabled>Select Branch</option>
                            @foreach ($branches as $branch)
                        <option value="{{$branch->aka}}">{{$branch->branch_name}}</option>
                                
                            @endforeach
                        </select>
                </div>
            <div class="form-group">
              <label for="Name" class="col-form-label">Name:</label>
              <input type="text" class="form-control" id="fullname" required>
            </div>
              <div class="form-group">
                    <label for="Email" class="col-form-label">Email:</label>
                    <input type="Email" class="form-control" id="Email" required>
                  </div>
              <div class="form-group">
                <label for="Username" class="col-form-label">Username:</label>
                <input type="text" class="form-control" id="Username" required>
              </div>
              <div class="form-group">
                <label for="Password" class="col-form-label">Password:</label>
                <input type="password" class="form-control" id="Password" required>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="create-account">Create Account</button>
              </div>
          </form>
        </div>
       
      </div>
    </div>
  </div>
        
{{-- RA UPDATE ACCOUNT --}}
   
  <div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update Ra Account</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id = "user-form">
                    <input type="hidden" name="role" id="role" value="Ra" />
                    <input type="hidden" name="id" id="updateid">
                    <div class="form-group">
                      <label for="Branch" class="col-form-label">Branch:</label>
                      <input type="text" class="form-control" id="updatebranch" disabled required>
                    </div>
                <div class="form-group">
                  <label for="Name" class="col-form-label">Name:</label>
                  <input type="text" class="form-control" id="updateName" required>
                </div>
                  <div class="form-group">
                        <label for="Email" class="col-form-label">Email:</label>
                        <input type="Email" class="form-control" id="updateEmail" required>
                      </div>
                  <div class="form-group">
                    <label for="Username" class="col-form-label">Username:</label>
                    <input type="text" class="form-control" id="updateUsername" required>
                  </div>
                  </div>
                  <div class="form-group">
                    <label for="Password" class="col-form-label">Password:</label>
                    <input type="text" class="form-control" id="updatePassword" required>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update-account">Update Account</button>
                  </div>
              </form>
            </div>
           
          </div>
        </div>
      </div>

  @stop

  @section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">

function deluser(id){

  const uid = id.value;

  var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}

  swal({
              title: 'Are you sure?',
              text: "You were about to delete the selected user, this account cannot be reverted.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, Delete it now!'
                }).then(function (){

                  $.ajax({
                          url: 'https://cbrc.solutions/api/auth/login',
                          method: 'POST',
                          data: auth
                  }).done(function(response){

                    var apiToString = JSON.stringify(response);
                    var apiTokenParse = JSON.parse(apiToString);
                    var apiToken = apiTokenParse.access_token;
                   
                      $.get('https://cbrc.solutions/api/main/user-delete/'+uid+'?token='+apiToken, function(response){
                        console.log(response);
                        if(response.msg == 'success'){

                            swal({
                                title: 'Delete',
                                text: 'User Successfully Deleted',
                                type: 'success'
                            }).then(function () {
                                location.reload();
                            })
               }
          });
      });
  })  
}


$(window).on('load',function(){


    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
   $.ajax({
   url: 'https://cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;
     var branch = $('#branchName').val();



     $.get('https://cbrc.solutions/api/main/ra/users/novaliches?token='+apiToken,function(data){
                $.each(data,function(index,raUserData){

                  console.log(raUserData);
                    $('#raUserTable').append('<tr><td id="role_'+raUserData.id+'">'+ raUserData.role +'</td>'+
                    '<td id="username_'+raUserData.id+'">'+ raUserData.username +'</td>'+
                    '<td id="email_'+raUserData.id+'">'+ raUserData.email +'</td>'+
                    '<td id="name_'+raUserData.id+'">'+ raUserData.name +'</td>'+
                    '<td><a href="#" class="btn btn-success btnEdit" data-toggle="modal" data-target="#updateUser" '+
                    'id="'+raUserData.id+'">Update</a> <button class="btn btn-danger" type="button" value="'+raUserData.id+'" onclick="deluser(this)">Delete</button<</td><td id="branch_'+raUserData.id+'" style="display:none;">'+ raUserData.branch +'</td>'+
                    '</tr>')
                     
                      $('.btnEdit').on('click',function(){
                            var updateid = $(this).attr('id');
                            var role = $('#role_' + updateid).text();
                            var username = $('#username_' + updateid).text();
                            var email = $('#email_' + updateid).text();
                            var name = $('#name_' + updateid).text();
                            var branch = $('#branch_' + updateid).text();
                            var branch = $('#branch_' + updateid).text();
                            
                            $('#updateid').val(updateid);
                            $('#updateUsername').val(username);
                            $('#updateEmail').val(email);
                            $('#updateName').val(name);
                            $('#updatebranch').val(branch);
                      });
                });
            });
        });
     });

$('#create-account').on('click', function(){

    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
 
   $.ajax({
   url: 'https://cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;

    //ajax post here send user details

    const userdetails = {

        "roles":$('#role').val(),
        "username":$('#Username').val(),
        "password":$('#Password').val(),
        "email":$('#Email').val(),
        "name":$('#fullname').val(),
        "branch":$('#branch').val(),
    } 
    
        $.ajax({
        url: 'https://cbrc.solutions/api/main/create-user?token='+apiToken,
        method: 'POST',
        data: userdetails
         }).done(function(response){   
                console.log(response.msg);
                if(response.msg == 'username'){
                    swal({
                          title: 'Username',
                          text: 'Username Already Exist',
                          type: 'error'
                        }).then(function () {
                            $('#Username').focus();
                        })
                }

                if(response.msg == 'email'){
                    swal({
                          title: 'Email',
                          text: 'Email Address Already Exist',
                          type: 'error'
                        }).then(function () {
                            $('#Email').focus();
                        })
                }

                if(response.msg == 'success'){
                    swal({
                          title: 'Success',
                          text: 'User Successfully Created!',
                          type: 'success'
                        }).then(function () {
                          location.reload();
                        })
                }
            });
      });
  });

  $('#update-account').on('click', function(){
        var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
            $.ajax({
            url: 'https://cbrc.solutions/api/auth/login',
            method: 'POST',
            data: auth
          }).done(function(response){
            var apiToString = JSON.stringify(response);
            var apiTokenParse = JSON.parse(apiToString);
            var apiToken = apiTokenParse.access_token;  

            const updateuser = {
            "userid":$("#updateid").val(), 
            "username":$('#updateUsername').val(),
            "email":$('#updateEmail').val(),
            "name":$('#updateName').val(),

            } 
          $.ajax({
            url: 'https://cbrc.solutions/api/main/update-user?token='+apiToken,
            method: 'POST',
            data: updateuser
          }).done(function(response){
            console.log(response.msg);
                if(response.msg == 'username'){
                        swal({
                              title: 'Username',
                              text: 'Username Already Exist',
                              type: 'error'
                            }).then(function () {
                                $('#updateUsername').focus();
                            })
                    }

                    if(response.msg == 'email'){
                        swal({
                              title: 'Email',
                              text: 'Email Address Already Exist',
                              type: 'error'
                            }).then(function () {
                                $('#updateEmail').focus();
                            })
                    }

                    if(response.msg == 'success'){
                        swal({
                              title: 'Success',
                              text: 'User Successfully Updated!',
                              type: 'success'
                            }).then(function () {
                                location.reload();
                            })
                    }
            });
        });
    });

  
</script>



@stop