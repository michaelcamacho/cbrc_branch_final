@extends('main')
@section('title')
REMIT
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Cash Remit</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Cash Remit</h5>
                                   <br/>
                                   <h5> <span class="pull-right card_status"></span></h5>
                                        <br>
                                        <br>
                                        <br>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="/proceed/insert_remit">
                                        @csrf
                                    <input type="hidden" name="branch" value="{{$branch}}">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" id="date" name="date" readonly>
                                                    <label>Date</label>
                                               </div>
                                            </div>
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="category" id="category" required>
                                                        
                                                        
                                                        <option value="" class='selector'>--- Select Cash Category ---</option>
                                                    <option value="1">Book Cash</option>
                                                    <option value="2">Enrollee Cash</option>
                                                            
                                                  </select>
                                                    <label>Cash Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                            <select class="form-control" name="season" id="season" >
                                                                <option value="" class='selector1'>--- Select Season ---</option>
                                                                <option value="Season 1">Season 1</option>
                                                                <option value="Season 2">Season 2</option>
                                                            </select>
                                                    <label>Season</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                       <select class="form-control" name="year" id="year" >
                                                        <option value="" class='selector1'>--- Select Year ---</option>
                                                      <option value="2020">2020</option>
                                                      <option value="2021">2021</option>
                                                      <option value="2022">2022</option>
                                                      <option value="2023">2023</option>
                                                      <option value="2024">2024</option>
                                                      <option value="2025">2025</option>
                                                      
                                                  </select>
                                                    <label>Season</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                       
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <input class="form-control decimal" name="amount" type="number" id="amount" maxlength="11" required>
                                                       
                                                    <label>Amount</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" name="remarks" id="remarks">
                                                    <label>Remarks</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" id="submit">Remit...</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
@stop