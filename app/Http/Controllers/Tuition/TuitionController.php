<?php

namespace App\Http\Controllers\Tuition;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Category;
use App\Branch;
use App\Major;
use App\Program;
use App\Model\DataModel\Tuition;
class TuitionController extends Controller
{
   
   

    public function add_tuition(Request $request){
        $input = $request->except(['_token']);
        $branch_id = Branch::where('branch_name' , '=' ,strtolower($input['branch']))->first();
        $major=0;
        if($input['program'] == 1){
        $major = $input['major'];    
        }      
        $proceed = 1;
		

    $existent = Tuition::where('branch_id','=',$branch_id->id)->where('program_id','=',$input['program'])->where('category','=',$input['category'])->get();


if($input['program'] == 1 ){

    if(count($existent) == 2){
        $proceed = 0;

    }elseif(count($existent) == 0){
    }else{

        if($existent[0]->major_id != $input['major'] ){
            
        }
        
    }
    
}else{
    if(count($existent) == 1){
        $proceed = 0;
    }
}

    if($proceed == 1 ){
        Tuition::insert([
            'branch_id' => $branch_id->id ,
            'program_id' => $input['program'],
            'category' => $input['category'], 
           'major_id' => $major,
            'tuition_fee' => $input['tuition_fee'], 
            'facilitation_fee' => $input['facilitation_fee'], 
       
            
        ]);


            Alert::success('Success!', 'New Tuition added.');
        
            return redirect('/admin/tuition-fees');
        
        }else{
        
            Alert::error('Sorry!', 'Tuition by Category Already Exist..<br>You may Update The Existing Category <br> Thank You');
        
            return redirect('/admin/tuition-fees');
                
        }


    }



    public function update_tuition(Request $request){
        $input = $request->except(['_token']);
        Tuition::where('id','=',$input['item_id'])->update([

            'tuition_fee' => $input['tuition_fee'], 
            'facilitation_fee' => $input['facilitation_fee'], 
            
        ]);

        Alert::success('Success!', 'New Tuition Fee Updated.');
        
        return redirect('/admin/tuition-fees');
    }
}
