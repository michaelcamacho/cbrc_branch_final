<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{
    protected $connection = 'satelite_branch_db_connection';
   
    protected $guard = [];


    public function expense_subcategory(){

        return $this->hasMany(ExpenseSubcategory::class,'category_id');
    }


    
}
