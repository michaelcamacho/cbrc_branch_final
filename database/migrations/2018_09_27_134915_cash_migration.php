<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('cashes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_id');
            $table->string('season','50');
            $table->string('year','50');
            $table->float('cash')->nullable();
            $table->timestamps();
        });

       

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashes');
        
    }
}
