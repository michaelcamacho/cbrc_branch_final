<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class BooksOrderList extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'books_order_list';
}
