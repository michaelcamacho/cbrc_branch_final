@extends('main')
@section('title')
Newsfeed
@stop

<style>
  @import url(https://fonts.googleapis.com/css?family=Quicksand);
.content{
    width: 100%;
    height: 700px;
    overflow: hidden;
}
.sub-content{
    width: 100%;
    height: 700px;
    overflow: auto;
    -ms-overflow-style: none

}
.titlebody{
    width: 98%;
    height: 700px;
    padding-right: 100px;
    overflow: scroll;
  
    
}

.sub-content::-webkit-scrollbar {
    width: 0;
    height: 0;
}

.post-content{
    width: 100%;
    max-height: 350px;
    margin-top: 10px;
}


.popup img{
    transform: scale(1.1);
}
    
  </style>

@section('main-content')
<link rel="stylesheet" href="/css/lightbox.min.css">


<main class="main-wrapper clearfix">
    <div class="container">
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <h4 style="margin-bottom: 0px;">CBRC NEWSFEED</h4>
          <hr style="margin-top: 0px;">
        </div>
        <div class="col-md-1"></div>
      </div>
      <div class="content">
        <div class="sub-content">
          <div class="row" id="titlebody">
        </div>
      </div>
       
    </div>
  </div> <!-- end of container -->                        
</main>       
@endsection


@section('js')
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="/js/lightbox-plus-jquery.min.js"></script>



<script>
  $(window).on('load', function(){
    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
    $.ajax({
      url: 'https://cbrc.solutions/api/auth/login',
      method: 'POST',
      data: auth
    }).done(function(response){
      var apiToString = JSON.stringify(response);
      var apiTokenParse = JSON.parse(apiToString);
      var apiToken = apiTokenParse.access_token;

      $.get('https://cbrc.solutions/api/main/fetch-post?token=' + apiToken, function(data){
        console.log(data);
        $.each(data, function (index, objPost){
            console.log(objPost);
            $('#titlebody').append('<div class="col-md-1"></div>'+
        '<div class="col-md-10" style="padding-right: 0px;">'+
           '<div class="card shadow-sm p-3 mx-sm-1 p-3 mt-10" style="margin-top: 10px;margin-bottom: 0px;">'+
              '<div class="container">'+
                '<div class="row">'+
                  '<div class="col-md-4"><img id="yow" style="max-height:200px; width:200px;"src="https://cbrc.solutions/upload/'+objPost.image+'"></div>'+
                  '<div class="col-md-8" style="padding-left:0px;"><p style="font-weight:bold;font-size:20px;margin-bottom:0px;color:#0e1111;">'+objPost.title+'</p><p style="color:#414a4c;">'+objPost.lastdate+' day ago &#x2022; <i class="fas fa-globe-africa"></i></p><p style="color:#232b2b;">'+objPost.body+'</p></div>'+
                  '</div></div></div></div>'+
                '<div class="col-md-1"></div>')
             });
        });
        
    });
    
  });

</script>
@endsection