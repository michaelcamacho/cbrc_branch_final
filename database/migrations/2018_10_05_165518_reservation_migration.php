<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservationMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id')->nullable();
            $table->string('name')->nullable();
            $table->string('branch')->nullable();
            $table->string('program')->nullable();
            $table->string('prog')->nullable();
            $table->string('school')->nullable();
            $table->string('email')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('reservation_fee')->nullable();
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');

    }
}
