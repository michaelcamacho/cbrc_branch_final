<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class BooksStockMovementRecord extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'books_stock_movement_record';
}
