$('.print').click(function() {
    var rcp = $( "#receipt" ).val() + 1;
    var date = $( "#date" ).val();
    var branch = $( "#branch" ).val();
    var name = $( "#student" ).val();
    var program = $( "#program" ).val();
    var total_amount = parseFloat($( "#total_amount" ).val());
    var amount_paid = parseFloat($( "#amount_paid" ).val());


        if (program == 'lets' ){
        program = 'LET';
        }
        if (program == 'nles' ){
            program = 'NLE';
        }
        if (program == 'crims' ){
            program = 'Criminology';
        }
        if (program == 'civils' ){
            program = 'Civil Service';
        }
         if (program == 'psycs' ){
            program = 'Psychometrician';
        }
        if (program == 'nclexs' ){
            program = 'NCLEX';
        }
        if (program == 'ielts' ){
            program = 'IELTS';
        }
        if (program == 'socials' ){
            program = 'Social Work';
        }
        if (program == 'agris' ){
            program = 'Agriculture';
        }
        if (program == 'mids' ){
            program = 'Midwifery';
        }

        if (program == 'onlines' ){
            program = 'Online Only';
        }

        if(isNaN(total_amount) == false && isNaN(amount_paid) == false && name != null){
            if(total_amount <= amount_paid || amount_paid == 0 ){
        var winPrint = window.open('', '', 'left=0,top=0,width=1000,height=800,toolbar=0,scrollbars=0,status=0');
        winPrint.document.write('<!DOCTYPE html><head><title>Book Payment Receipt</title>');
        winPrint.document.write('<style type="text/css">');
        winPrint.document.write('#print-table{border-collapse: collapse;}');
        winPrint.document.write('#print-table, tr, td {border: 1px solid black;}#td1{padding:12px 100px 12px 20px;font-size: 13px;font-weight: 600;}#td2{padding:10px 20px 12px 360px;font-size: 13px;font-weight: 600;}');
        winPrint.document.write('#logo{width:30%;}#title{font-size:12px;margin: 5px 0 5px 0;}#detail{margin: 10px 0px 10px 0px;}');
        winPrint.document.write('h2{color: #000;}.print-table{border: 1 #000;}#rcp{position:absolute; right:0; display:none;}#rcv{position:relative;}</style></head>');
        winPrint.document.write('<body><center><img src="../img/cbrc-logo2.png" id="logo"></center><h2 style="margin-top:5px;margin-bottom:5px;">Student Copy</h2><h4 id="rcp">receipt #: '+ rcp +'</h4><h4 id="detail">Date: '+ date +'<br/>Name: '+ name +'<br/>Branch: '+ branch +'<br/>Progam: '+ program +'</h4>');
        winPrint.document.write('<h4>Total Amount: '+ total_amount + '</h4>');
        winPrint.document.write('<table id="print-table">');
        winPrint.document.write('<tr><td id="td1">Book </br></br>');
        
        $('input[name^="book_title"]').each(function() {
            var book = $(this).val();
            winPrint.document.write(book + '</br>');
        });


        winPrint.document.write('</br>TOTAL AMOUNT: </br>AMOUNT PAID: </td>');
        winPrint.document.write('<td id="td2" style="text-align:right;">Price </br></br>');
        
        $('input[name^="price"]').each(function() {
        var price = $(this).val();
        winPrint.document.write(price + '</br>');
        });


        winPrint.document.write('</br>'+total_amount+'</br>'+amount_paid+'</td></tr></table>');
        winPrint.document.write('<h4 id="rcv">_____________________<br/>Received by:</h4>');
        winPrint.document.write('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');
        winPrint.document.write('<center><p id="title">Book Payment Receipt</p></center>');
        winPrint.document.write('<center><img src="../img/cbrc-logo2.png" id="logo"></center>');
        winPrint.document.write('<h2 style="margin-top:5px;margin-bottom:5px;">Branch Copy</h2>');
        winPrint.document.write('<h4 id="rcp">receipt #: '+ rcp +'</h4><h4 id="detail">Date: '+ date +'<br/>Name: '+ name +'<br/>Branch: '+ branch +'<br/>Progam: '+ program +'</h4>');
        winPrint.document.write('<h4>Total Amount: '+ total_amount + '</h4>');
        winPrint.document.write('<table id="print-table">');
        winPrint.document.write('<tr><td id="td1">Book </br></br>');

        $('input[name^="book_title"]').each(function() {
            var book = $(this).val();
            winPrint.document.write(book + '</br>');
        });
        
        winPrint.document.write('</br>TOTAL AMOUNT: </br>AMOUNT PAID: </td>');
        winPrint.document.write('<td id="td2" style="text-align:right;">Price </br></br>');
        $('input[name^="price"]').each(function() {
        var price = $(this).val();
        winPrint.document.write(price + '</br>');});
        winPrint.document.write('</br>'+total_amount+'</br>'+amount_paid+'</td></tr></table>');
        winPrint.document.write('<h4 id="rcv">_____________________<br/>Received by:</h4>');

        winPrint.document.close();
        
        setTimeout(function() {
        winPrint.focus();
        winPrint.print();
        winPrint.close();
        }, 250);
        }
        }
        });


        
    

