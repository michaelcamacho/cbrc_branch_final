$('.print').click(function() {
    var rcp = $( "#receipt" ).val() + 1;
    var date = $( "#date" ).val();
    var transfer_to = $( "#transfer_to" ).val();
    var branchsender = $( "#branchsender" ).val();
    var remark = $( "#remark" ).val();
    var total_quantity = parseFloat($( "#total_quantity" ).val());
   
      
        var winPrint = window.open('', '', 'left=0,top=0,width=1000,height=800,toolbar=0,scrollbars=0,status=0');
        winPrint.document.write('<!DOCTYPE html><head><title>Book Transfer Receipt</title>');
        winPrint.document.write('<style type="text/css">');
        winPrint.document.write('#print-table{border-collapse: collapse;}');
        winPrint.document.write('#print-table, tr, td {border: 1px solid black;}#td1{padding:12px 100px 12px 20px;font-size: 13px;font-weight: 600;}#td2{padding:10px 20px 12px 360px;font-size: 13px;font-weight: 600;}');
        winPrint.document.write('#logo{width:30%;}#title{font-size:12px;margin: 5px 0 5px 0;}#detail{margin: 10px 0px 10px 0px;}');
        winPrint.document.write('h2{color: #000;}.print-table{border: 1 #000;}#rcp{position:absolute; right:0; display:none;}#rcv{position:relative;}</style></head>');
        winPrint.document.write('<body><center><img src="../img/cbrc-logo2.png" id="logo"></center><h2 style="margin-top:5px;margin-bottom:5px;">Reciver Copy</h2><h4 id="rcp">receipt #: '+ rcp +'</h4><h4 id="detail">Date: '+ date +'<br/>Remark: '+ remark +'<br/>Branch Sender: '+ branchsender +'<br/>Transferred To '+ transfer_to +'</h4>');
        winPrint.document.write('<h4>Total Quantities: '+ total_quantity + '</h4>');
        winPrint.document.write('<table id="print-table">');
        winPrint.document.write('<tr><td id="td1">Book Title</br></br>');
        
        $('input[name^="book_title"]').each(function() {
            var book = $(this).val();
            winPrint.document.write(book + '</br>');
        });


        winPrint.document.write('</br>TOTAL AMOUNT: </td>');
        winPrint.document.write('<td id="td2" style="text-align:right;">Quantities </br></br>');
        
        $('input[name^="quantities"]').each(function() {
        var quantities = $(this).val();
        winPrint.document.write(quantities + '</br>');
        });


        winPrint.document.write('</br>'+total_quantity+'</td></tr></table>');
        winPrint.document.write('<h4 id="rcv">_____________________<br/>Received by:</h4>');
        winPrint.document.write('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');
        winPrint.document.write('<center><p id="title">Book Transfer Receipt</p></center>');
        winPrint.document.write('<center><img src="../img/cbrc-logo2.png" id="logo"></center>');
        winPrint.document.write('<h2 style="margin-top:5px;margin-bottom:5px;">Sender Copy</h2>');
        winPrint.document.write('<h4 id="rcp">receipt #: '+ rcp +'</h4><h4 id="detail">Date: '+ date +'<br/>Remark: '+ remark +'<br/>Branch Sender: '+ branchsender + '<br/>Transferred :to '+ transfer_to +'</h4>');
        winPrint.document.write('<h4>Total Quantities: '+ total_quantity + '</h4>');
        winPrint.document.write('<table id="print-table">');
        winPrint.document.write('<tr><td id="td1">Book Title</br></br>');

        $('input[name^="book_title"]').each(function() {
            var book = $(this).val();
            winPrint.document.write(book + '</br>');
        });
        
        winPrint.document.write('</br>TOTAL AMOUNT: </td>');
        winPrint.document.write('<td id="td2" style="text-align:right;">Quantities </br></br>');
        $('input[name^="quantities"]').each(function() {
        var quantities = $(this).val();
        winPrint.document.write(quantities + '</br>');});
        winPrint.document.write('</br>'+total_quantity+'</td></tr></table>');
        winPrint.document.write('<h4 id="rcv">_____________________<br/>Received by:</h4>');

        winPrint.document.close();
        
        setTimeout(function() {
        winPrint.focus();
        winPrint.print();
        winPrint.close();
        }, 250);
        
        });


        
    

