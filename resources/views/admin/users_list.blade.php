@extends('main')
@section('title')
Users
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
  <div class="container-fluid">
      <div class="row page-title clearfix">
          <div class="page-title-left">
              <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
          </div>
          <div class="page-title-right d-none d-sm-inline-flex">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item">Records</li>
                  <li class="breadcrumb-item active">Users</li>
              </ol>
          </div>
      </div>
  </div>
  <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                  <div class="widget-body clearfix">
                      <h5 class="box-title mr-b-0">Users</h5>
                     <br/>        
                  </div>
                  <div class="widget-body clearfix">
        
      <div class="add">
      <a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#add-user">Add New User</a>
      </div>

        @foreach($Branches As $item)
        <div id="sale-table">
        <br>
        <h6>{{$item->branch_name}} User List</h6>
                    
                        <table id="user" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Role</th>
                                    <th>Branch</th>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    @if($item->id !== Auth::user()->branch_id)
                                    <th>Update</th>
                                    @endif
                                    @if($item->id !== Auth::user()->branch_id)
                                    <th>Delete</th>
                                    @endif
                                    <th>Password</th>
                                  </tr>          
                                </thead>
                                <tbody>
                                    @if(isset($user))
                                        @foreach($user as $users)
                                        @if($users->branch_id == $item->id)
                                              <tr id="row_{{$users->id}}">
                                                    <td>{{$users->id}}</td>
                                                    <td id="role_name_{{$users->id}}">{{$users->display_name}}</td>
                                                    <td id="branch_{{$users->id}}">{{$users->branch}}</td>
                                                    <td id="name_{{$users->id}}">{{$users->name}}</td>
                                                    <td id="username_{{$users->id}}">{{$users->username}}</td>
                                                    <td id="email_{{$users->id}}">{{$users->email}}</td>
                                                    @if($item->id !== Auth::user()->branch_id)
                                                    <td><a href="#" class="btn btn-success edit" id="{{$users->id}}" data-toggle="modal" data-target="#update-user">Update</a></td>
                                                    @endif
                                                    @if($item->id !== Auth::user()->branch_id)
                                                    <td><button class="btn btn-danger delete" user-id="{{$users->id}}">Delete</button></td>
                                                    @endif
                                                    <td id="password_{{$users->id}}">{{$users->password}}</td>  
                                                    <input type="hidden" id="role_{{$users->id}}" value="{{$users->role_name}}">
                                                    <input type="hidden" id="role_id_{{$users->id}}" value="{{$users->role_id}}">
                                              </tr>
                                            @endif
                                          @endforeach
                                          @endif
                                      </tbody>           
                                    </table>
                                  </div>
                          @endforeach



                      </div>
                 </div>
              </div>
          </div>
       </div>
     </div>
  </main>

<!-- ====================== User Create Account ============================================ --> 
 <div id="add-user" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                      <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                      <div class="modal-body">
                          
                        <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                          </div>
              <form id="user-form" method="POST" action="/hris/proceed/add_user">
                @csrf
             
{{-- branch option --}}
            <div class="form-group">   
              @if($main_branch != "")     
              <input id="branch" type="hidden" class="form-control" name="branch" value="{{ Auth::user()->branch_id}}">
              @else
              <label for="branch">Branch</label>        
              <select class="form-control" name="branch" id="branch">
                <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                @if(isset($Branches))
                @foreach($Branches as $item)
                     
                              <option value="{{$item->id}}">{{$item->branch_name}}</option>
                             
                      @endforeach
                  @endif
              </select>
              @endif
            </div>
{{-- branch option --}}
              


                <div class="form-group">
                  <input type="hidden" name="role" id="role" value="Admin" >
                {{--  <label for="role">Role</label>
                 <select class="form-control" name="role" id="role">
                    <option value="0" disable="true" selected="true">--- Select Role ---</option>
                    @if(isset($role))
                        @foreach($role as $roles)
                       
                                <option value="{{$roles->name}}">{{$roles->display_name}}</option>
                               
                        @endforeach
                    @endif
                </select> --}}
                </div>

              <div class="form-group">
                  <input id="name" type="text" class="form-control" name="name" required placeholder="Name">
                  
              </div>
              <br/>

              <div class="form-group">
                <input id="email" type="email"  name="email" class="form-control" required placeholder="Email" data-validation="email">
                <span id="error_email"></span>
                </div>
              <br/>

              <div class="form-group">
                <input id="username" type="text" class="form-control" name="username" required placeholder="Username" autocomplete="false">
                <span id="error_username"></span>

              </div>
              <br/>
              <div class="form-group">
                <input id="password" type="text" class="form-control" name="password" required placeholder="Password" autocomplete="new-password">
              </div>
              <br/>
              <div class="row">
                <!-- /.col -->
                <div class="col-12 text-center">
                  {{-- <a href="#" class="btn btn-rounded btn-success ripple save" name=>Register</a> --}}
              <input type="submit" class="btn btn-info" name="submit" value="save">
                </div>
                <!-- /.col -->
              </div>
              
            </form>
          </div>
      </div>                    
  </div>
</div>

<!-- ============================== Update userAccount ============================================ -->
<div id="update-user" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
      <div class="modal-content">
        <a href="#" class="close" id="clear_role_update_form" data-dismiss="modal" aria-hidden="true">×</a>
            <div class="modal-body">
              <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
              </div>
              <form id="user-form" method="POST" action="/hris/proceed/update_user">
                @csrf
                <div class="form-group">
                    <input type="hidden" class="form-control" name="id" id="update_id" readonly>                    
                    <input type="hidden" class="form-control" name="role_id" id="update_role_id" readonly>                    
                </div>
                 
{{-- branch option --}}
            <div class="form-group">   
                @if($main_branch != "")     
                <input id="update_branch" type="hidden" class="form-control" name="branch" value="{{ Auth::user()->branch_id}}">
                @else
                <label for="branch">Branch</label>        
                <select class="form-control" name="branch" id="update_branch">
                  <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                  @if(isset($Branches))
                  @foreach($Branches as $item)
                       
                                <option value="{{$item->id}}">{{$item->branch_name}}</option>
                               
                        @endforeach
                    @endif
                </select>
                @endif
              </div>
  {{-- branch option --}}
  <div class="form-group">
      <input type="hidden" name="role" id="update_role" value="Admin" >
    {{--  <label for="role">Role</label>
     <select class="form-control" name="role" id="role">
        <option value="0" disable="true" selected="true">--- Select Role ---</option>
        @if(isset($role))
            @foreach($role as $roles)
           
                    <option value="{{$roles->name}}">{{$roles->display_name}}</option>
                   
            @endforeach
        @endif
    </select> --}}
    </div>
        <br>
                <div class="form-group">
                  <label for="update_name">Name</label>
                  <input id="update_name" type="text" class="form-control" name="name" required>      
                </div>
                <div class="form-group">
                  <label for="update_email">Email</label>
                  <input id="update_email" type="email" name ="email" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="update_username">Username</label>
                  <input id="update_username" type="text" class="form-control" name="username" required autocomplete="false">       
                </div>
                  <div class="form-group">
                      <label for="password">Password</label>
                       <input id="update_password" type="password" class="form-control" name="password"  autocomplete="new-password">
                  </div>
                <br/>
                    <div class="row">
                      <div class="col-12 text-center">
                        {{-- <a href="#" class="btn btn-rounded btn-success ripple update" data-dismiss="modal">Update</a> --}}
                       <input type="submit" name="submit" class="btn btn-info" value="submit">
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/user.js') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
    $('table.display').DataTable();
} );

 


$(document.body).on('click','.edit',function(){
  user_id = $(this).attr('id');
 
    $.get('/hris/datafetch/' + user_id ,function(item){
      console.log(item);

      $('#update_id').val(item.id);
      $('#update_role_id').val(item.role_id);
      $('#update_role').val(item.role);
      $('#update_branch').val(item.branch_id);
      $('#update_name').val(item.name);
      $('#update_username').val(item.username);
      $('#update_email').val(item.email); 
          
      });
});

    
 $('#user').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    let postid = $(this).attr("user-id");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                
               $.ajax({
               type: 'DELETE',
               url: "{{ url('/hris/proceed/delete_user') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: {id:postid,"_token":'{{ csrf_token() }}'},
               dataType: 'json',
               success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
          });
          });
          });
    
      

   
</script>

@stop