<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
	protected $connection = 'satelite_branch_db_connection';
	protected $fillable = [
    'program',
	'major',
	'aka',
	];
}
