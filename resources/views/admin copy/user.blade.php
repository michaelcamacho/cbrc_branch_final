@extends('main')
@section('title')
Users
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
  <div class="container-fluid">
      <div class="row page-title clearfix">
          <div class="page-title-left">
              <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
          </div>
          <div class="page-title-right d-none d-sm-inline-flex">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item">Records</li>
                  <li class="breadcrumb-item active">Users</li>
              </ol>
          </div>
      </div>
  </div>
  <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                  <div class="widget-body clearfix">
                      <h5 class="box-title mr-b-0">Users</h5>
                     <br/>        
                  </div>
                  <div class="widget-body clearfix">
                    <div id="sale-table">
                        <table id="user" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                            <div class="add">
                            <a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#add-user">Add New User</a>
                            </div>
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Role</th>
                                    <th>Branch</th>
                                    <th>Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                    <th>Password</th>
                                  </tr>          
                                </thead>
                                <tbody>
                                   @if(isset($user))
                                      @foreach($user as $users)
                                        <tr id="row_{{$users->id}}">
                                            <td>{{$users->id}}</td>
                                            <td id="role_{{$users->id}}">{{$users->display_name}}</td>
                                            <td id="branch_{{$users->id}}">{{$users->branch}}</td>
                                            <td id="name_{{$users->id}}">{{$users->name}}</td>
                                            <td id="username_{{$users->id}}">{{$users->username}}</td>
                                            <td id="email_{{$users->id}}">{{$users->email}}</td>
                                             <td><a href="#" class="btn btn-success edit" id="edit_{{$users->id}}" data-toggle="modal" data-target="#update-user">Update</a></td>
                                            <td><button class="btn btn-danger delete" user-id="{{$users->id}}">Delete</button></td>
                                            <td id="password_{{$users->id}}">{{$users->password}}</td>  
                                            <input type="hidden" id="role_name_{{$users->id}}" value="{{$users->role_name}}">
                                        </tr>
                                        @endforeach
                                      @endif
                                </tbody>           
                          </table>
                        </div>
                      </div>
                 </div>
              </div>
          </div>
       </div>
     </div>
  </main>

<!-- ====================== User Create Account ============================================ --> 
 <div id="add-user" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="user-form">

                        {{-- <div class="form-group">
                            <select class="form-control" name="branch" id="branch">
                                <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                <option value="Main">Main (Administrator)</option>
                                @if(isset($branch))
                                    @foreach($branch as $branches)
                                <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div> --}}
                <div class="form-group">
                <select class="form-control" name="role" id="role">
                    <option value="0" disable="true" selected="true">--- Select Role ---</option>
                    @if(isset($role))
                        @foreach($role as $roles)
                        @if( strpos($roles->name, 'ra_') === false)
                                @if($roles->name == 'Member')
                                <option value="{{$roles->name}}">Manager</option>
                                @else
                                <option value="{{$roles->name}}">{{$roles->display_name}}</option>
                                @endif
                            @endif
                        @endforeach
                    @endif
                </select>
                </div>
                <br>
                <div class="form-group">
                <input id="name" type="text" class="form-control" name="name" required placeholder="Name">
                
              </div>
              <br/>

              <div class="form-group">
                <input id="email" type="email" class="form-control" required placeholder="Email" data-validation="email">
                <span id="error_email"></span>
                </div>
              <br/>

              <div class="form-group">
                <input id="username" type="text" class="form-control" name="username" required placeholder="Username" autocomplete="false">
                <span id="error_username"></span>

              </div>
              <br/>
              <div class="form-group">
                <input id="password" type="text" class="form-control" name="password" required placeholder="Password" autocomplete="new-password">
              </div>
              <br/>
              <div class="row">
                <!-- /.col -->
                <div class="col-12 text-center">
                  <a href="#" class="btn btn-rounded btn-success ripple save" data-dismiss="modal">Register</a>
                </div>
                <!-- /.col -->
              </div>
              {{ csrf_field() }}
            </form>
          </div>
      </div>                    
  </div>
</div>

<!-- ============================== Update userAccount ============================================ -->
<div id="update-user" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
      <div class="modal-content">
        <a href="#" class="close" id="clear_role_update_form" data-dismiss="modal" aria-hidden="true">×</a>
            <div class="modal-body">
              <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
              </div>
                <form id="user-form">
                <div class="form-group">
                    <input type="hidden" class="form-control" name="id" id="update_id" readonly>                    
                </div>
                <div class="form-group">
                  <select class="form-control" name="role-update" id="role-update">
                      
                       @if(isset($role))
                        @foreach($role as $roles)
                        @if( strpos($roles->name, 'ra_') === false)
                                @if($roles->name == 'Member')
                                <option value="{{$roles->name}}">Manager</option>
                                @else
                                <option value="{{$roles->name}}">{{$roles->display_name}}</option>
                                @endif
                            @endif
                        @endforeach
                    @endif
                      
                  </select>
                </div>   
                <div class="form-group">
                  <label for="update_name">Name</label>
                  <input id="update_name" type="text" class="form-control" name="name" required>      
                </div>
                <div class="form-group">
                  <label for="update_email">Email</label>
                  <input id="update_email" type="email" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="update_username">Username</label>
                  <input id="update_username" type="text" class="form-control" name="username" required autocomplete="false">       
                </div>
                  <div class="form-group">
                      <label for="password">Password</label>
                       <input id="update_password" type="password" class="form-control" name="password" required autocomplete="new-password">
                  </div>
                <br/>
                    <div class="row">
                      <div class="col-12 text-center">
                        <a href="#" class="btn btn-rounded btn-success ripple update" data-dismiss="modal">Update</a>
                      </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/user.js') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
   

 
$('#update-user').on('hide.bs.modal', function (e) {
    $("select[name=role-update] option:last").remove();
});


$(document.body).on('click','.edit',function(){

  window.edit_id = $(this).attr('id').replace('edit_','');
  role = $('#role_'+edit_id).text();
  branch = $('#branch_'+edit_id).text();
  name = $('#name_'+edit_id).text();
  username = $('#username_'+edit_id).text();
  email = $('#email_'+edit_id).text();
  role_name = $('#role_name_'+edit_id).val();
  
 
  $('#update_id').val(edit_id);
    $("#role-update").append("<option value="+role_name+" selected>"+role+"</option>");
  $('#update_branch').val(branch);
  $('#update_name').val(name);
  $('#update_username').val(username);
  $('#update_email').val(email);

 });

     $('.save').on('click',function(){
              
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('role', $('#role').val());
                formData.append('name', $('#name').val());
                formData.append('email', $('#email').val());
                formData.append('username', $('#username').val());
                formData.append('password', $('#password').val());
                formData.append('branch', $('#branch').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/insert-user') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                      console.log(data);
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-user form')[0].reset();
                        location.reload();

                    },

                        error : function(data){
                            swal({
                                title: 'Oops...',
                                text: data.message,
                                type: 'error',
                                timer: '1500'
                            })
                        }
                               
              });
            });

         $('.update').on('click',function(){
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', $('#update_id').val());
                formData.append('role', $('#role-update').val());
                formData.append('name', $('#update_name').val());
                formData.append('email', $('#update_email').val());
                formData.append('username', $('#update_username').val());
                formData.append('password', $('#update_password').val());
                formData.append('branch', $('#update_branch').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/update-user') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                    console.log(data);
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-user form')[0].reset();
                        location.reload();

                    }
                               
              });
            });
            
            
            
         $('.delete').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    let postid = $(this).attr("user-id");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                
               $.ajax({
               type: 'DELETE',
               url: "{{ url('/delete-user') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: {id:postid,"_token": "{{ csrf_token() }}"},
               dataType: 'json',
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

      $('#user').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    let postid = $(this).attr("user-id");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                
               $.ajax({
               type: 'DELETE',
               url: "{{ url('/delete-user') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: {id:postid,"_token": "{{ csrf_token() }}"},
               dataType: 'json',
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });




             
          
//check if username and email exist
$(document).ready(function(){

$('#email').blur(function(){
 var error_email = '';
 var email = $('#email').val();
 var _token = $('input[name="_token"]').val();
 var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 if(!filter.test(email))
 {    
  $('#error_email').html('<label class="text-danger">Invalid Email</label>');
  $('#email').addClass('has-error');
 }
 else
 {
  $.ajax({
   url:"{{ route('email_available.checkemail') }}",
   method:"POST",
   data:{email:email, _token:_token},
   success:function(result)
   {
    if(result == 'unique')
    {
     $('#error_email').html('<label class="text-danger">Email being used</label>');
     $('#email').removeClass('has-error');
    
    }
    else
    { 
     $('#error_email').html('<label class="text-success">Email Available</label>');
     $('#email').addClass('has-error');
     
    }

   }
  })
 }
});


$('#username').blur(function(){
 var error_username = '';
 var username = $('#username').val();
 var _token = $('input[name="_token"]').val();
 
 
  $.ajax({
   url:"{{ route('username_available.checkusername') }}",
   method:"POST",
   data:{username:username, _token:_token},
   success:function(result)
   {
    if(result == 'unique')
    {
     $('#error_username').html('<label class="text-danger">username being used</label>');
     $('#username').removeClass('has-error');
    }
    else
    { 
     $('#error_username').html('<label class="text-success">username Available</label>');
     $('#username').addClass('has-error');
    }

   }
  })
 });
});//end of check email and username if exist


$(document.body).on('click','.update',function(){
  var admin = "Admin";
  var Member = "Member";
     var edit_id = $(this).attr('id');
     var role = $('#role_'+edit_id).text();
     var branch = $('#branch_'+edit_id).text();
      var name = $('#name_'+edit_id).text();
      var username = $('#username_'+edit_id).text();
      var email = $('#email_'+edit_id).text();

     if(role == "Manager"){
      $('#role-update').empty();
      $('#role-update').append('<option value="'+Member+'">'+role+'</option><option value="Admin">Administrator</option>'); 
       }else{
      $('#role-update').empty();
      $('#role-update').append('<option value="'+admin+'">'+role+'</option><option value="Member">Manager</option>');
  
    }

    $('#update_id').val(edit_id);
    $('#update_branch').val(branch);
    $('#update_name').val(name);
    $('#update_username').val(username);
    $('#update_email').val(email);

});
</script>

@stop