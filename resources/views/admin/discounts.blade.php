@extends('main')
@section('title')
 Discounts
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Discount Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Discount Records</h5>
                                   <br/>
                                   
                                </div>
            <div class="widget-body clearfix">
                    <div class="add">
                            <a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#add-discount">Add New Discount</a>
                            </div>
            @foreach($branch_table_count As $table_count)
            <div id="tuition-table">
                
            <br>

            
                <h6>{{$table_count->branch_name}} Discount List</h6>
            <table  class="table table-bordered table-hover display nowrap margin-top-10 w-p100" id="" style="width: 100% !important">
                       
                             <thead>
                               
                                <tr>
                                    <th>Branch</th>
                                    <th>Program</th>
                                    <th>Category</th>
                                    <th>Discount Category</th>
                                    <th>Amount</th>
                                    @if($table_count->id == Auth::user()->branch_id)
                                        <th>Update</th>
                                    @endif
                                    {{-- <th>Delete</th> --}}
                                </tr>
                               
                            </thead>
                            <tbody>
                        @if(isset($discounts))
                            @foreach($discounts as $item)
                                @if($item->branch_id == $table_count->id)
                            <tr>
                                <td>{{$item->branch_name}}</td>
                                <td>{{$item->program_name}}</td>
                                <td>{{$item->category_name}}</td>
                                <td>{{$item->discount_category}}</td>
                                <td>{{$item->discount_amount}}</td>
                                @if($table_count->id == Auth::user()->branch_id)
                                    <td><a href="#" class="btn btn-success edit" id="{{$item->id}}" data-toggle="modal" data-target="#update-discount">Update</a></td>
                                @endif
                                    {{-- <td><button class="btn btn-danger delete" user-id="{{$item->id}},{{$item->branch}}">Delete</button></td> --}}
                            </tr>
                               @endif
                            @endforeach
                        @endif
        
                                    
                            </tbody>           
                            
                        </table>
                    </div>
                @endforeach
        <div id="add-discount" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form method="POST" action="/discount/proceed/add_discount" >
                               @csrf
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                {{-- branch option --}}
                            <div class="form-group">   
                                    @if($main_branch != "")     
                                    <input id="update_branch" type="hidden" class="form-control" name="branch" value="{{ Auth::user()->branch_id}}">
                                    @else
                                    <label for="branch">Branch</label>        
                                    <select class="form-control" name="branch" id="update_branch">
                                    <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                    @if(isset($Branches))
                                    @foreach($Branches as $item)
                                        
                                                    <option value="{{$item->id}}">{{$item->branch_name}}</option>
                                                
                                            @endforeach
                                        @endif
                                    </select>
                                    @endif
                                </div>
                {{-- branch option --}}
            
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="program">
                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->id}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label for="category">Category</label>
                                        <select class="form-control" name="category" id="category">
                                            <option value="" disable="true" selected="true">--- Select Category ---</option>
                                            @if(isset($category))
                                            @foreach($category as $categories)
                                            <option value="{{$categories->id}}">{{$categories->category_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Discount Category</label>
                                    <input class="form-control" type="text" name="discount_category" id="discount_category" required>
                                </div>
                                <div class="form-group">
                                    <label for="discount_amount">Amount</label>
                                    <input class="form-control decimal" type="text" name="discount_amount" id="discount_amount" required>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <input type="submit" name="submit" value="save" class="btn btn-info">
                                      </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

            <div id="update-discount" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="discount-form" method="POST" action="/discount/proceed/update_discount">
                                @csrf
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="item_id" id="update_id" readonly>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="update_program">
                                        <option value="" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->id}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" name="category" id="update_category">
                                        <option value="" disable="true" selected="true">--- Select Category ---</option>
                                        @if(isset($category))
                                        @foreach($category as $categories)
                                        <option value="{{$categories->id}}">{{$categories->category_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Discount Category</label>
                                    <input class="form-control" type="text" name="discount_category" id="update_discount_category" required>
                                </div>
                                <div class="form-group">
                                    <label for="discount_amount">Amount</label>
                                    <input class="form-control decimal" type="text" name="discount_amount" id="update_discount_amount" required>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <input type="submit" name="submit" value="update" class="btn btn-info">
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/discount.js') }}"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('table.display').DataTable();
} );





    $('.update').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', $('#update_id').val());
                formData.append('branch', $('#update_branch').val());
                formData.append('program', $('#update_program').val());
                formData.append('category', $('#update_category').val());
                formData.append('discount_category', $('#update_discount_category').val());
                formData.append('discount_amount', $('#update_discount_amount').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/update-discount') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-discount form')[0].reset();
                        location.reload();

                    }
                               
              });
            });
    $('#discount').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-discount') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

    $('.delete').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-discount') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
        });
       $('.decimal').keypress(function(evt){
            return (/^[0-9]*\~?[0-9]*$/).test($(this).val()+evt.key);
        });
</script>
@stop