<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class Remit extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $fillable = [
        'date',
        'category',
        'season',
        'amount',
        'remarks',
        'author',

    ];
}
