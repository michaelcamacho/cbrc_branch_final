<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\DataModel\ExpenseSubcategory;
class ExpenseDataFetchController extends Controller
{
    

    public function fetch_sub_category($category_id){

        $fetch_sub_category = ExpenseSubcategory::where("category_id" , "=" , $category_id )->get();

        
        return response()->json($fetch_sub_category);
    }
}
