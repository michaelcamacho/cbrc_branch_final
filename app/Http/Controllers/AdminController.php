<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Branch;
use App\Program;
use App\Model\DataModel\BooksInventorie;
use App\Model\DataModel\BookSale;
use App\Model\DataModel\Budget;
use App\Model\DataModel\Dropped;
use App\Model\DataModel\Expense;
use App\Model\DataModel\Receivable;
use App\Model\DataModel\Tuition;
use App\Model\DataModel\PettyCash;
use App\facilitation;
use DB;
use Auth;
use App\User;
use App\Role;
use App\Major;


use App\Model\ExpenseSetup;
use App\Model\AdminSettings;

class AdminController extends Controller
{

public function __construct()
	{
		$this->middleware('role:admin');	
	}



public function total_program(){

 		$let_1_sale=NovalichesS1Sale::where('program','=','LET')->sum('amount_paid');

    	$nle_1_sale=NovalichesS1Sale::where('program','=','NLE')->sum('amount_paid');

		$crim_1_sale=NovalichesS1Sale::where('program','=','Criminology')->sum('amount_paid');

		$civil_1_sale=NovalichesS1Sale::where('program','=','Civil Service')->sum('amount_paid');

		$psyc_1_sale=NovalichesS1Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$nclex_1_sale=NovalichesS1Sale::where('program','=','NCLEX')->sum('amount_paid');

		$ielts_1_sale=NovalichesS1Sale::where('program','=','IELTS')->sum('amount_paid');

		$social_1_sale=NovalichesS1Sale::where('program','=','Social')->sum('amount_paid');

		$agri_1_sale=NovalichesS1Sale::where('program','=','Agriculture')->sum('amount_paid');

		$mid_1_sale=NovalichesS1Sale::where('program','=','Midwifery')->sum('amount_paid');

		$online_1_sale=NovalichesS1Sale::where('program','=','Online Only')->sum('amount_paid');

		$let_2_sale=NovalichesS2Sale::where('program','=','LET')->sum('amount_paid');

    	$nle_2_sale=NovalichesS2Sale::where('program','=','NLE')->sum('amount_paid');

		$crim_2_sale=NovalichesS2Sale::where('program','=','Criminology')->sum('amount_paid');

		$civil_2_sale=NovalichesS2Sale::where('program','=','Civil Service')->sum('amount_paid');

		$psyc_2_sale=novalichesS2Sale::where('program','=','Psychometrician')->sum('amount_paid');

		$nclex_2_sale=NovalichesS2Sale::where('program','=','NCLEX')->sum('amount_paid');

		$ielts_2_sale=NovalichesS2Sale::where('program','=','IELTS')->sum('amount_paid');

		$social_2_sale=NovalichesS2Sale::where('program','=','Social')->sum('amount_paid');

		$agri_2_sale=NovalichesS2Sale::where('program','=','Agriculture')->sum('amount_paid');

		$mid_2_sale=NovalichesS2Sale::where('program','=','Midwifery')->sum('amount_paid');

		$online_2_sale=NovalichesS2Sale::where('program','=','Online Only')->sum('amount_paid');

		$let = 	NovalichesLet::where('status','=','Enrolled')->count();

		$nle = 	NovalichesNle::where('status','=','Enrolled')->count();

		$crim = NovalichesCrim::where('status','=','Enrolled')->count();

		$civil= NovalichesCivil::where('status','=','Enrolled')->count();

		$psyc = NovalichesPsyc::where('status','=','Enrolled')->count();

		$nclex = NovalichesNclex::where('status','=','Enrolled')->count();

		$ielts = NovalichesIelt::where('status','=','Enrolled')->count();

		$social = NovalichesSocial::where('status','=','Enrolled')->count();

		$agri = NovalichesAgri::where('status','=','Enrolled')->count();

		$mid = NovalichesMid::where('status','=','Enrolled')->count();

		$online = NovalichesOnline::where('status','=','Enrolled')->count();

	return view ('admin.total-program')
	->with('let_1_sale',$let_1_sale)
	->with('nle_1_sale',$nle_1_sale)
	->with('crim_1_sale',$crim_1_sale)
	->with('civil_1_sale',$civil_1_sale)
	->with('psyc_1_sale',$psyc_1_sale)
	->with('nclex_1_sale',$nclex_1_sale)
	->with('ielts_1_sale',$ielts_1_sale)
	->with('social_1_sale',$social_1_sale)
	->with('agri_1_sale',$agri_1_sale)
	->with('mid_1_sale',$mid_1_sale)
	->with('online_1_sale',$online_1_sale)
	->with('let_2_sale',$let_2_sale)
	->with('nle_2_sale',$nle_2_sale)
	->with('crim_2_sale',$crim_2_sale)
	->with('civil_2_sale',$civil_2_sale)
	->with('psyc_2_sale',$psyc_2_sale)
	->with('nclex_2_sale',$nclex_2_sale)
	->with('ielts_2_sale',$ielts_2_sale)
	->with('social_2_sale',$social_2_sale)
	->with('agri_2_sale',$agri_2_sale)
	->with('mid_2_sale',$mid_1_sale)
	->with('online_2_sale',$online_1_sale)
	->with('let',$let)
	->with('nle',$nle)
	->with('crim',$crim)
	->with('civil',$civil)
	->with('psyc',$psyc)
	->with('nclex',$nclex)
	->with('ielts',$ielts)
	->with('social',$social)
	->with('agri',$agri)
	->with('mid',$mid)
	->with('online',$online);
	

}

public function total_agri(){
	$program = "Agriculture";
	$a_prog = NovalichesAgri::where('status','=','Enrolled')->get();


	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_civil(){
	$program = "Civil Service";
	$a_prog = NovalichesCivil::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_crim(){
	$program = "Criminology";
	$a_prog = NovalichesCrim::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_ielts(){
	$program = "IELTS";
	$a_prog = NovalichesIelt::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_let(){
	$program = "LET";
	$a_prog = NovalichesLet::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_nclex(){
	$program = "NCLEX";
	$a_prog = NovalichesNclex::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_nle(){
	$program = "NLE";
	$a_prog = NovalichesNle::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_psyc(){
	$program = "Psychometrician";
	$a_prog = NovalichesPsyc::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_social(){
	$program = "Social Work";
	$a_prog = NovalichesSocial::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_mid(){
	$program = "Midwifery";
	$a_prog = NovalichesMid::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_online(){
	$program = "Online Only";
	$a_prog = NovalichesOnline::where('status','=','Enrolled')->get();

	return view ('admin.total-enrollee')
	->with('a_prog',$a_prog)
	->with('program',$program);
}

public function total_dropped(){

	$a_let=NovalichesLet::where('status','=','Dropped')->get();

    $a_nle=NovalichesNle::where('status','=','Dropped')->get();

    $a_crim=NovalichesCrim::where('status','=','Dropped')->get();

    $a_civil=NovalichesCivil::where('status','=','Dropped')->get();

    $a_psyc=NovalichesPsyc::where('status','=','Dropped')->get();

    $a_nclex=NovalichesNclex::where('status','=','Dropped')->get();

    $a_ielt=NovalichesIelt::where('status','=','Dropped')->get();

    $a_social=NovalichesSocial::where('status','=','Dropped')->get();

    $a_agri=NovalichesAgri::where('status','=','Dropped')->get();

    $a_mid=NovalichesMid::where('status','=','Dropped')->get();

    $a_online=NovalichesOnline::where('status','=','Dropped')->get();

    return view ('admin.total-dropped')
    ->with('a_let',$a_let)
    ->with('a_nle',$a_nle)
    ->with('a_crim',$a_crim)
    ->with('a_civil',$a_civil)
    ->with('a_psyc',$a_psyc)
    ->with('a_nclex',$a_nclex)
    ->with('a_ielt',$a_ielt)
    ->with('a_social',$a_social)
    ->with('a_agri',$a_agri)
    ->with('a_mid',$a_mid)
    ->with('a_online',$a_online);
}

public function total_scholars(){

	$a_let=NovalichesLet::where('category','=','Scholar')->get();

    $a_nle=NovalichesNle::where('category','=','Scholar')->get();

	$a_crim=NovalichesCrim::where('category','=','Scholar')->get();

	$a_civil=NovalichesCivil::where('category','=','Scholar')->get();

	$a_psyc=NovalichesPsyc::where('category','=','Scholar')->get();

	$a_nclex=NovalichesNclex::where('category','=','Scholar')->get();

	$a_ielt=NovalichesIelt::where('category','=','Scholar')->get();

	$a_social=NovalichesSocial::where('category','=','Scholar')->get();

	$a_agri=NovalichesAgri::where('category','=','Scholar')->get();

	$a_mid=NovalichesMid::where('status','=','Scholar')->get();

    $a_online=NovalichesOnline::where('status','=','Scholar')->get();


    return view ('admin.total-scholars')
    ->with('a_let',$a_let)
    ->with('a_nle',$a_nle)
    ->with('a_crim',$a_crim)
    ->with('a_civil',$a_civil)
    ->with('a_psyc',$a_psyc)
    ->with('a_nclex',$a_nclex)
    ->with('a_ielt',$a_ielt)
    ->with('a_social',$a_social)
    ->with('a_agri',$a_agri)
    ->with('a_mid',$a_mid)
    ->with('a_online',$a_online);;
}

public function tuition_fees(){

	$a_tuition = Tuition::all();
    $branch = Branch::all();
	$program = Program::all();
	
	return view ('admin.tuition-fees')
	->with('a_tuition',$a_tuition)
	->with('branch',$branch)
	->with('program',$program);
}
public function insert_tuition(Request $request){

	$input = $request->except(['_token']);
	$branch = $input['branch'];
	

	if($branch == 'Novaliches'){
		
		$existent1 = NovalichesTuition::where('program','=',$input['program'])->where('category','=',$input['category'])->first();
		
		if($existent1 == null){
			NovalichesTuition::insert($input);

			return response()->json([
            'success' => true,
            'status'  => 'success',
            'result'  => 'Success!',
            'message' => 'New tuition has been added',
        ]);
		}

		if($existent1 != null) {
			return response()->json([
            'success' => true,
            'status'  => 'error',
            'result'  =>'Failed!',
            'message' => 'Tution already exist',
        ]);
		}
		
	}

}

public function update_tuition(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Novaliches'){
		NovalichesTuition::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'tuition_fee' 		=>	$input['tuition_fee'],
			'facilitation_fee' 	=>	$input['facilitation_fee'],
			'season'			=>	$input['season'],
			'year'				=> $input['year'],
		]);
	}
}

public function delete_tuition(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Novaliches'){

              NovalichesTuition::where('id','=',$input['id'])->delete();
           
    }

              return response()->json([
            'success' => true,
            'message' => '1 tuition has been deleted',
        ]);
      
         }

public function discounts(){
	$branch = Branch::all();
	$a_discount = NovalichesDiscount::all();
	$program = Program::all();
	return view ('admin.discounts')
	->with('a_discount',$a_discount)
	->with('branch',$branch)
	->with('program',$program);
}

public function insert_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Novaliches'){
		NovalichesDiscount::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New discount has been added',
        ]);

}

public function update_discount(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Novaliches'){
		NovalichesDiscount::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'category' 			=>	$input['category'],
			'discount_category' =>	$input['discount_category'],
			'discount_amount' 	=>	$input['discount_amount'],
		]);
	}
	

return response()->json([
            'success' => true,
            'message' => '1 discount has been updated',
        ]);

}

public function delete_discount(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Novaliches'){

              NovalichesDiscount::where('id','=',$input['id'])->delete();
    }

       return response()->json([
            'success' => true,
            'message' => '1 discount has been deleted',
        ]);
      
         }
    

public function total_books(){
	$branch = Branch::all();
	$a_book = NovalichesBooksInventorie::all();

    $a_sale = NovalichesBooksSale::all();

	$program = Program::all();
	return view ('admin.total-books')
	->with('a_book',$a_book)
	->with('a_sale',$a_sale)
	->with('branch',$branch)
	->with('program',$program);
}

public function insert_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];

	if($branch == 'Novaliches'){
		NovalichesBooksInventorie::insert($input);
	}

return response()->json([
            'success' => true,
            'message' => 'New book has been added',
        ]);

}

public function update_book(Request $request){

$input = $request->except(['_token']);
$branch = $input['branch'];
$id = $input['id'];

	if($branch == 'Novaliches'){
		NovalichesBooksInventorie::where('id','=',$id)->update([
			'program' 			=>	$input['program'],
			'book_title' 		=>	$input['book_title'],
			'price' 			=>	$input['price'],
			'available' 		=>	$input['available'],
		]);
	}
	

return response()->json([
            'success' => true,
            'message' => 'Book inventories has been updated',
        ]);

}

public function delete_book(Request $request){
		$input = $request->except(['_token']);

		if($input['branch'] == 'Novaliches'){

              NovalichesBooksInventorie::where('id','=',$input['id'])->delete();

    }

    return response()->json([
            'success' => true,
            'message' => '1 book record has been deleted',
        ]);
}

public function total_expense(){

	$a_sale = NovalichesExpense::all();
	
	
	return view ('admin.total-expense')
	->with('a_sale',$a_sale);
}


function populateScoreCard(){

    $date = date('M-d-Y');
   
    for($x=1; $x <= 2; $x++){
    
    $scorecard_last = NovalichesScoreCards::where('season',"=",$x)->orderby('date','desc')->value('date');
    $scorecard_last_count = NovalichesScoreCards::where('season',"=",$x)->orderby('date','desc')->count();
    $scorecard_count = NovalichesScoreCards::where('season',"=",$x)->count();
    //check latest tranaction month in scorecard table
    
	$last1 = $scorecard_last;
	if(date("M-d-Y",strtotime($scorecard_last)) !== date("M-d-Y",strtotime($date))){
		if($scorecard_count == 0 || $scorecard_last_count== 0){
		}else{
			$today= date_create($date);
			$last= date_create($last1);
			$diff=date_diff($today,$last);
			$till = $diff->format("%a");
			
				
				for($y = 1; $y<=$till; $y++){
				
				
				$last1	= date('M-d-Y', strtotime("+1 day", strtotime($last1)));
			

					 //populate missing days					
					 if(date("M-d-Y",strtotime($last1)) !== date("M-d-Y",strtotime($scorecard_last))){
						 NovalichesScoreCards::create([
							 'year' => date("Y",strtotime($last1)),
							 'date' => $last1,
							 'season' => $x,
							 ]);
							 
							}
							
						}
                }//end of else
                }//end of check date
    //end of check latest tranaction month in scorecard table
			}//end of loopfor season 1 to 2
}//end of populate score card




public function expense_setup(Request $request){

	$branch = $request->input('branch');
	$Season = $request->input('season');
	$Year = $request->input('year');

	$setup = ExpenseSetup::where('Branch','=',$branch)->count();
	$id = ExpenseSetup::where('Branch','=',$branch)->value('id');

	if ($setup == 0) {
		//new settings
		$Set = New ExpenseSetup;
		$Set->Branch = $branch;
		$Set->Season = $Season;
		$Set->Year = $Year;
		$Set->save();

		return "save";
	} else {
		
		$updset = ExpenseSetup::find($id);
		$updset->Branch = $branch;
		$updset->Season = $Season;
		$updset->Year = $Year;
		$updset->save();
		return "update";
	}
	

}

public function dash_settings($param){

		$branch = Branch::all();
		
		
        
        
    return view('admin.admin-settings')
    ->with('branch',$branch)
    ->with('param',$param);
    
}

public function save_adsettings(Request $request){
    
    $param = $request->input('param');
    $branch = $request->input('branch');
	$Season = $request->input('season');
	$Year = $request->input('year');
    
    
   	$setup = AdminSettings::where('Branch','=',$branch)->where('Account','=',$param)->count();
	$id = AdminSettings::where('Branch','=',$branch)->where('Account','=',$param)->value('id');
    
	if ($setup == 0) {
		//new settings
		$Set = New AdminSettings;
		$Set->Account = $param;
		$Set->Branch = $branch;
		$Set->Season = $Season;
		$Set->Year = $Year;
		$Set->save();

     return Redirect()->back()->with(['message' => 'Setting has been Set']);
        // 	return response()->json([
        //     'success' => true,
        //     'message' => 'New Setting has been set',
        // ]);
		
	} else {
		
		$updset = AdminSettings::find($id);
		$updset->Account = $param;
		$updset->Branch = $branch;
		$updset->Season = $Season;
		$updset->Year = $Year;
		$updset->save();
		
		 return Redirect()->back()->with(['message' => 'Setting has been Updated']);
// 			return response()->json([
//             'success' => true,
//             'message' => 'Setting has been Updated',
//         ]);
		
	}
    

}

public function attendance(){

	$program = Program::all();
	 $sy = date("Y")+1;
     $years = range($sy,2010);
     $sbranch = $this->sbranch;

	return view('radashboard.attendance')
		->with('years',$years)
		->with('program',$program)
		->with('sbranch',$sbranch);


}


public function evaluation(){
	  $sy = date("Y")+1;
      $years = range($sy,2010);
      $sbranch = $this->sbranch;

	return view('radashboard.evaluation')
	->with('years',$years)
	->with('sbranch',$sbranch);
}

public function filter_exam(){
	$program = Program::all();
	  $sy = date("Y")+1;
      $years = range($sy,2010);

	return view('radashboard.filter_monitoring_exam')
	->with('years',$years)
	->with('program',$program);
}

public function exam_monitoring(Request $request){
	$season = $request->input('season');
	$year = $request->input('year');
	$program = $request->input('program');
	$major = $request->input('major');
	$branch = $this->branch;


	return view('radashboard.exam_monitoring')
	->with('season',$season)
	->with('year',$year)
	->with('program',$program)
	->with('major',$major)
	->with('branch', $branch);

}

public function create_user(){
	
	$branches = Branch::all();
	return view('radashboard.ra_useraccount')
	->with('branches',$branches);
}


public function expense_settings(){
	$branch = Branch::all();
	return view('admin.expense-settings')->with('branch',$branch);
}

public function lecturer(){
	return view('radashboard.lecturer');
}
public function online_completion(){
	return view('radashboard.online_completion');
}
public function result_analysis(){
	return view('radashboard.result_analysis');
	
}
// ==================== Newsfeed  ========================

public function newsfeed(){
    return view('admin.newsfeed');
    } 

   public function task_todolist(){
   	return view('admin.taskTodoList');
   } 

// ==================== Task image ==========================================

   public function task_image(Request $request){
  	    $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $fileNameToStore = time(). '_'.$imageName;
        $image->move('task_images',$fileNameToStore);
        // $imagePath = public_path('upload/$imageName');
        return response()->json($fileNameToStore);

	}        
}
