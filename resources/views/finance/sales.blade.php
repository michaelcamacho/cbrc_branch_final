@extends('main')
@section('title')
sales
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>

@stop
@section('main-content')

<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} sales</h6>
                    
                </div>
            </div>
        </div>
        <!-- /.page-title -->
        
        <div class="row" >
            <div class="widget-list">
            <div class="col-md-12 widget-holder">    
                    @include('finance.finance_nav')
            </div>
            </div>
        </div>

        <!-- page content strarts here -->
        <div class="container-fluid">
                <div class="widget-list">

                    <div class="row" >
                    @foreach( $program as $prog)
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                
                                <h6>{{$prog->program_name}}</h6>
                                    <table  class="table table-bordered table-hover display nowrap margin-top-10 w-p100" id="" style="width: 100% !important">
                                        <thead>
                                        <tr>
                                            <th>Trans.#</th>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Balance</th>
                                            <th>Discount</th>
                                            <th>Tuition</th>
                                            <th>Facilitation Fee</th>
                                            <th>Category</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $card as $item)
                                            @if($item->program == $prog->id)
                                                <tr>
                                                    <td>00{{$item->id}}</td>
                                                    <td>{{$item->date_registered}}</td>
                                                    <td>{{$item->first_name . ' ' . $item->middle_name . ' ' . $item->last_name}}</td>
                                                    <td>{{$item->amount_paid}}</td>
                                                    <td>{{$item->balance}}</td>
                                                    <td>{{$item->discount}}</td>
                                                    <td>{{$item->tuition_fee_price}}</td>
                                                    <td>{{$item->facilitation_price}}</td>
                                                    <td>{{$item->category_name}}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                              </table>

                            </div>
                        </div>
                        @endforeach
                    </div><!-- page row ends here --> 


                </div><!-- page content-fluid ends here --> 
        </div>
        <!-- page content ends here -->        
    </main>
    <!-- end of main -->
@stop

@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
 
    $('table.display').DataTable();
} );

</script>




@stop