<?php

namespace App\Http\Middleware;

use Closure;
use App\Tenant;
use App\Users;
use Auth;

class RedirectSubdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if(isset($_COOKIE['db_name'])){

            \Config::set('database.connections.satelite_branch_db_connection', array(
                'driver'    => 'mysql', 
                'host'      => '127.0.0.1',
                'database'  =>  $_COOKIE['db_name'],
                'username'  =>  'root',
                'password'  =>  '',
                'charset'   => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'strict'    => false,
                'options'   => [                                
                    \PDO::ATTR_EMULATE_PREPARES => true
                    ]
                ));     
        }




        // Extract the subdomain from URL
        list($subdomain) = explode('.', $request->getHost(), 2);
       
       
        // Retrieve requested tenant's info from database. If not found, abort the request.
        $tenant = Tenant::where('slug', $subdomain)->firstOrFail();

        setcookie('slug', $tenant->slug, time() + (86400 * 30), "/"); // 86400 = 1 day
        setcookie('db_name', $tenant->db_name, time() + (86400 * 30), "/"); // 86400 = 1 day
        setcookie('system_title', $tenant->system_title, time() + (86400 * 30), "/"); // 86400 = 1 day
       


        return $next($request);
    }
}
