@extends('main')
@section('title')
ADD BUDGET
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Add Budget</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Add Budget</h5>
                                   <br/>
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-new-budget'}}">
										@csrf
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" id="date" name="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="category" id="category" required>
                                                        <option value="" disable="true" selected="true">--- Select Category ---</option>
                                                        <option value="Initial Budget">Initial Budget</option>
                                                        <option value="Salary">Salary</option>
                                                        <option value="Tax">Taxes and Licences</option>
                                                        <option value="Insurance">Insurance</option>
                                                        <option value="Utilities">Utilities</option>
                                                        <option value="Daily Expense">Daily Expense</option>
                                                        <option value="Lecturer">Lecturer</option>
                                                        <option value="Other">Other</option>
                                                        
                                                    </select>
                                                    <label>Budget Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                           
                                        </div>

                                       
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <input class="form-control" name="amount" type="number" id="amount" min="0" step="0.01">
                                                       
                                                    <label>Amount</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" name="remarks" id="remarks">
                                                    <label>Remarks</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" id="button-prevent">Add New Budget</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.add-budget').addClass('active');
});

</script>
@stop