@extends('main')
@section('title')
Financial Report
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: 0px;
}

@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}

#financialReportPage{
    box-shadow: 3px 3px 3px 3px;
    width: 1000px;
    color:black;
    padding-top:10px;
    padding-left:60px;
    padding-right:60px;
    padding-bottom:60px;
    max-width: 1000px;
}

#programlist{
   
    margin-left:60px;
}

#receiptSeason1{
    
    text-align:right;
    position:absolute;
    margin-left:260px;
}
#receiptSeason2{
    text-align:right;
    position:absolute;
    margin-left:400px;
    
}

#ReceiptBooks{
    text-align:right;
    position:absolute;
}
#totReceiptBooks{
    
    margin-left:92px;
}


#disbursmentlist{
    
    width:500px;
    margin-left:60px;
}
#operatingexpense{
    
    width:500px;
    margin-left:30px;
}
#operatingexpensedataS1{
    position:absolute;
    text-align:right;
    width:100px;
    margin-left:440px;
    margin-top:-285px;
}

#operatingexpensedataS2{
    position:absolute;
    text-align:right;
    width:100px;
    margin-left:600px;
    margin-top:-285px;
}
#administrativeexpense{
    
    width:500px;
    margin-left:30px;
}
#administrativeexpensedataS1{
    position:absolute;
    text-align:right;
    width:200px;
    margin-left:340px;
    margin-top:-314px;
}

#administrativeexpensedataS2{
    position:absolute;
    text-align:right;
    width:300px;
    margin-left:400px;
    margin-top:-314px;
}
#otherdisbursment{
    
    margin-left:30px;
}
#otherdisbursmentdataS1{
    position:absolute;
    text-align:right;
    width:200px;
    margin-left:340px;
    margin-top:-50px;
}
#otherdisbursmentdataS2{
    position:absolute;
    text-align:right;
    width:200px;
    margin-left:500px;
    margin-top:-50px;
}
#totDisburseNetExcess{
    width:500px;
    text-align:right;
    margin-left:40px;
}

#netincome{
    text-align:right;
    width:200px;
    margin-top:-50px;
    margin-left:500px;
}

#totbookSale{
    text-align:right;
    position:absolute;
    margin-left:540px;
    margin-top:-290px;
}
#totReciept{
    position:absolute;
    margin-left:60px;
}

#totRevenue{
    position:absolute;
    margin-left:680px;
    margin-top:0px;
}

#line1,
#line2,
#line3
#line4{
background-color:#475a68;
border-width:2px;
width:880px;
}

#line2{
    position:absolute;
    margin-top: 30px;
}

#line3{
    position:absolute;
    margin-top: 0px;
    margin-left: 300px;
    width: 500px;
}

#line4{
    position:absolute;
    margin-top: 0px;
    margin-left: 60px;
    width: 600px;
}

#netIncomelbl{
    position:absolute;
    margin-top: -25px;
    margin-left: 710px;

}
</style>

<link rel="stylesheet" style="text/css" href="{{ asset('css/financialreport.css') }}" />
@stop
@section('main-content')

<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} Financial Report</h6>
                    
                </div>
            </div>
        </div>
        <!-- /.page-title -->
        
        <div class="row" >
            <div class="widget-list">
                    <div class="col-md-12 widget-holder">
                        
                        @include('finance.finance_nav')
            
                    </div>
            </div>
        </div>

        <!-- page content strarts here -->
        <div class="container-fluid">
                <div class="widget-list">
                    <div class="row" >
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                
                                <!--content start -->
                                <div class="widget-body clearfix">
                                    
                                    <input type="button" class="btn btn-primary" value="Download" onclick="downloadPdf()" />
                                <br>
                                <br>
                                    <div class="row"> 
                                        <label style="margin-top:10px;margin-right:10px"> Year </label>
                                        <select  class="form-control col-md-2   year"name="year" id="year">

                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                        </select>

                                 </div>          
                                            <br/><br/>

                                            <div style="background:white" id="financialReportPage">
                        
                                            <br/>
                                            
                                            <b>  
                                                Carl Balita Review Center<br/>										
                                                {{ $branch }} Branch<br/>										
                                                Statement of Reciepts & Disbursements<br/>										
                                                As of &nbsp;
                                            </b>	
                                            
                                                <hr id="line1"/>                                        
                                            
                                            <br/>                                        
                                                <b>RECIEPTS:</b>
                                                <br/> 
                                                <div id="receiptSeason1">
                                                <b>Season 1</b><br/>
                                                  
                                                </div><!-- end of receiptSeason1 --> 
                                            
                                                <div id="receiptSeason2">
                                                <b>Season 2</b><br/>
                                               
                                                </div><!-- end of receiptSeason2 --> 
                                            
                                                <div id="programlist">  				 						
                                                <b>Review Fee</b>	        <br/>						
                                                LET							<br/>
                                                NLE 						<br/>
                                                Criminology					<br/>		
                                                Civil Service				<br/>		
                                                Psychometrician				<br/>			
                                                NCLEX						<br/>	
                                                IELTS						<br/>	
                                                Social Service				<br/>			
                                                Agriculture					<br/>		
                                                Midwifery					<br/>		
                                                Online Only					<br/>		
                                                </div><!-- end of programlist --> 
                                                <hr id="line2" />
                                                <hr id="line4" />

                                                <div id="totbookSale">  <b>Books</b><br/>
                                                   
                                                </div>
                                                
                                               
                                                
                                            <br/><br/>
                                            
                                            <b>DISBURSEMENTS:</b>
                                            
                                            <br/>									
                                            
                                            <div id="disbursmentlist">
                                                <b>Operating Expenses</b>
                                                <br/>									
                                                    <div id="operatingexpense">
                                                    Professional Fee			                    <br/>
                                                    Lecturer Allowance - Food and Transpo			<br/>	
                                                    Lecturer - Accommodation				        <br/> 				
                                                    Review Materials								<br/>
                                                    Facilitation Fee 								<br/>
                                                    Final Coaching								    <br/>
                                                    Rental - Venue				                    <br/>			
                                                    Marketing Expenses				                <br/>				
                                                    Courrier and Shipments				            <br/>				
                                                    Advertisement & Promotional Expenses			<br/>		
                                                    Representation				    		        <br/>
                                                    </div>
                                               
                                                <div id="operatingexpensedataS1">
                                                        <b>Season 1</b><br>
                                                      
                                                </div>
                                                <!-- end of operatingexpense --> 
                                                <div id="operatingexpensedataS2">
                                                        <b>Season 2</b><br>
                                                    </div>
                                                <!-- end of operatingexpense --> 

                                                <br/>
                                                <b>Administrative Expenses</b>					<br/>					
                                                    <div id="administrativeexpense">
                                                    Salaries & Wages				            <br/>				
                                                    Employee Gov't Contribution				    <br/>				
                                                    Rental - Office				                <br/>			
                                                    Light & Water				                <br/>			
                                                    Retainer's Fee								<br/>
                                                    PT & Communication				            <br/>			
                                                    Repairs & Maintenance				        <br/>			
                                                    Transportation Expense				        <br/>				
                                                    Supplies				                    <br/>				
                                                    Taxes, Licenses & Fees				        <br/> 				
                                                    Office Equipment				            <br/> 				
                                                    Furniture and Fixtures				        <br/>				
                                                    Miscellaneous (others, interest-bank)		<br/> 				
                                                    </div>
                                            
                                                <div id="administrativeexpensedataS1">
                                                    </div>
                                                <!-- end of administrativeexpense --> 

                                                <div id="administrativeexpensedataS2">
                                                          </div>
                                               <!-- end of administrativeexpense --> 

                                                <br/>	
                                                <b>Other Disbursements	</b>
                                                <br/>	
                                                <div id="otherdisbursment">							
                                                    Books<br/>								
                                                    Loans<br/>	
                                                </div> 
                                                
                                                <div id="otherdisbursmentdataS1">
                                                </div>
                                                <!-- end of administrativeexpense --> 
                                                <div id="otherdisbursmentdataS2">
                                               </div>
                                               <!-- end of administrativeexpense --> 

                                            </div><!-- end of disbusment -->                                      
                                                                                      
                                            <br/>	
                                            <div id="totDisburseNetExcess">                                       
                                            <b >Total Disbursements:</b><br/>									
                                            	                                       
                                            <b >Net Excess/(Deficit)</b>						 
                                            </div>
                                            <hr id="line3"/>
                                            <div id="netincome">
                                                
                                                <br/>
                                                
                                              
                                                <br/>
                                                
                                            </div>		
                                            <b id="netIncomelbl">Net Income</b>
                                            <br/>
                                            <br/>
                                           
                                    </div><!-- financialReportPage end -->


                        

                                </div>
                                <!-- content end -->
                            </div>
                        </div>
                    </div><!-- page row ends here --> 
                </div><!-- page content-fluid ends here --> 
        </div>
        <!-- page content ends here -->        
    </main>
    <!-- end of main -->
@stop

@section('js')

<script type="text/javascript">
    $(document).ready(function() {
  $('.reports').addClass('active');
  $('.reports').addClass('collapse in');

  });

</script>




@stop