<?php

namespace App\Http\Controllers\cbrc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Alert;
use Auth;
use App\Branch;
use App\Model\AdminSettings;
use App\CbrcStudent;
use App\StudentEnrollmentCard;
use App\PaymentInfo;

class ReservationController extends Controller
{

    public function proceed_reserve_student(Request $request){
		
			$request->all();
			$status = 2;
			$registration_date=date('M-d-Y');
			$input = $request->except(['_token']);
			
			
	
			StudentEnrollmentCard::insert([
				'date_registered' => $registration_date ,
				'student_id' => $input['student_id'] ,
				'season' => $input['season'] ,
				'year' => $input['year'] ,
				'status' => $status,
				'created_by_id' => Auth::user()->id, 
				'deleted' => 0 
			]);
	
			$latest_card = StudentEnrollmentCard::latest('id')
			->where('student_id','=',$input['student_id'])->first();
		   
	
			
			PaymentInfo::insert([
				'payment_date' => $registration_date ,
				'payment_no' => 1,
				'card_id' => $latest_card->id,
				'amount_paid' => $input['amount_paid'] ,
				'balance_before' => 0 ,
				'balance_after' => 0 ,
				'created_by_id' => Auth::user()->id, 
				'deleted' => 0 
			]);
	
	
		
	
			Alert::success('Success!', 'Student Reserved.');
    	return redirect('/new_reservation/'. request('branch'));

    }
}
