@extends('main')
@section('title')
BULLETIN BOARD
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
  @media print {
  .navbar-nav, .navbar, .page-title, .breadcrumb, .footer, #topnav, .page-title-box, .d-print-none, #fbmessenger ,#fbmessenger-mobile , .header{
    display: none;
    margin: 0;
    padding: 0;
  }
  .left {
    display: none;
  }
  .content, .page-content-wrapper, .wrapper {
    margin-top: 0;
    padding-top: 0;
  }
  .content-page {
    margin-left: 0;
    margin-top: 0;
  }
  .topbar, .footer, .side-menu, .dataTables_filter, .dataTables_length, .dataTables_info, .dataTables_paginate {
    display: none;
    margin: 0;
    padding: 0;
  }
  .content-page > .content {
    margin: 0;
  }
}
</style>

@stop

@section('main-content')
<main class="main-wrapper clearfix">
     <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">                      
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item" ><a href="bulletin">Notification</a></li>
                             <li class="breadcrumb-item active">Task History</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>  
                <!-- /.page-title -->
            </div>
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                   <h4 class="text-center">Task Summary</h4>
                </div>
              </div>
            </div>
            <div class="container">            
                <table class="table table-striped" id="myTable">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th width ="60%">Description</th>
                      <th>Finish Date</th>

                    </tr>
                  </thead>
                  <tbody id="taskhistory">
                    
                  </tbody>
                </table>
              </div>
                 <div class="d-print-none">
                    <div class="pull-right">
                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i></a>
                        <a href="/bulletin" class="btn btn-primary waves-effect waves-light">Back</a>
                    </div>
                </div>        
          </main>


@stop
@section('js')
  
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('/js/data-table.js') }}"></script>
  
  <script type="text/javascript">  
    $(window).on('load',function(){
     var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
      $.ajax({
         url: 'https://beta.cbrc.solutions/api/auth/login',
        method: 'POST',
        data: auth
      }).done(function(response){
      var apiToString = JSON.stringify(response);
      var apiTokenParse = JSON.parse(apiToString);
      var apiToken = apiTokenParse.access_token;
      console.log(apiToken);

      $.get('https://cbrc.solutions/api/main/taskhistory/Novaliches?token='+apiToken,function(data){
          $.each(data,function(index, ObjHistory){
            console.log(ObjHistory);
              $('#taskhistory').append('<tr><td>'+ObjHistory.title+'</td><td>'+ObjHistory.description+'</td><td>'+ObjHistory.task_done+'</td></tr>');
               $('#myTable').DataTable();
          });
      });
    });

  });  

  </script>
@stop