<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\CbrcStudent;
use App\Branch;
use auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
     public function __construct()
	{
		$this->middleware('auth');
		
	}

	public function student_program($aka,$branch){

		$branch_name = lcfirst($branch);
		$student = CbrcStudent::where('program','=',$aka)
		->where('branch','=',$branch_name)
		->get();

		return $student;
	}



	public function main_branch_id()
        {
        return Branch::findOrFail(Auth::user()->branch_id)->main_branchid;
        }

	public function list_of_branches(){

		return Branch::all();
	}
}
