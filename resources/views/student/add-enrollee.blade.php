@extends('main')
@section('title')
ADD ENROLLEE
@stop
@section('css')
<style type="text/css">
	.date[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
	.date[type=date]:focus::-webkit-datetime-edit {
    color: #495057 !important;
}


fieldset.info_separator {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    width:100%;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.info-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto;
    padding:0 10px;
    border-bottom:none;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Add Enrollee</h6>
                        
                    </div>
                   
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                            
                                <div class="widget-body clearfix">

                                    <fieldset class="info_separator">
                                            <legend class="info-border">Personal Information</legend>
                                  
                                   <br/>
                    {{-- <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{Auth::user()->position. 'insert-enrollee'}}"> --}}
                        <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="/proceed/save_student">
                                        @csrf
                                        <input type="hidden" name="branch" value="{{ $branch }}">

                                        
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="last_name" type="text" required>
                                                    <label>Last Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="first_name" type="text" required>
                                                    <label>First Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="middle_name" type="text" required>
                                                    <label>Middle Name  &nbsp&nbsp(NM for no middle name)</label>
                                                </div>
                                            </div>
                                        </div>

                                       

                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                	<input class="form-control date" name="birthdate" type="date" required>
                                                    <label>Birth Date</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <input class="form-control decimal" name="contact_no" type="text" onkeypress="return isNumber(event);" maxlength="11" required>
                                                    <label>Contact Number</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <input class="form-control" name="address" type="text" required>
                                                    <label>Address</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <select class="form-control" name="section" id="section" required>
                                                        
                                                        <option value="" selected>-- Select Section --</option>
                                                        <option value="Section 1">Section 1</option>
                                                        <option value="Section 2">Section 2</option>
                                                        <option value="Section 3">Section 3</option>
                                                        <option value="Section 4">Section 4</option>
                                                        <option value="Section 5">Section 5</option>
                                                        <option value="Section 6">Section 6</option>
                                                        <option value="Section 7">Section 7</option>
                                                        <option value="Section 8">Section 8</option>
                                                        <option value="Section 9">Section 9</option>
                                                        <option value="Section 10">Section 10</option>
                                                    </select>
                                                    
                                                    <label>Section</label>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <input class="form-control" name="email" type="email" required>
                                                    <label>Email</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <input class="form-control" name="username" type="text">
                                                    <label>User Name</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <input class="form-control" name="password" type="text">
                                                    <label>Password</label>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    
                            <fieldset class="info_separator">
                                    <legend class="info-border">Academic Background</legend>
                                        <br/>

                                         <div class="row">

                                            <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>School</label>
                                                        <input class="form-control" name="school" type="text" required>
                                                    </div>
                                                    
                                                       
                                            </div>


                                            <div class="col-lg-12">
                                            <div class="row">
                                                   
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label>Program</label>
                                                            <select class="form-control" name="program" id="program" required>
                                                                
                                                                @if(isset($program))
                                                                @foreach($program as $programs)
                                                                <option value="{{$programs->id}}">{{$programs->program_name}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                            <label>Major</label>
                                                            <select class="form-control" name="major" id="major" >
                                                            <option value="0">-- select a major --</option>
                                                            @if(isset($major))
                                                                @foreach($major as $majors)
                                                                <option value="{{$majors->id}}">{{$majors->major}}</option>
                                                                @endforeach
                                                            @endif
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                                <label>NOA #</label> 
                                                                <input class="form-control" name="noa_no" type="text">
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="col-lg-3">
                                                        <div class="form-group">
                                                                <label># of Take </label> 
                                                                <input class="form-control" name="no_take" type="text" required>
                                                        </div>        
                                                    </div>

                                            </div>  
                                            </div>
                                                    
                                                
                                        </div>
                                    </fieldset>

                                    <fieldset class="info_separator">
                                    <legend class="info-border">Contact Person In Case Of Emergency</legend>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Contact Person Name</label>
                                                    <input class="form-control" type="text" name="contact_person" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Contact Person Number</label>
                                                    <input class="form-control decimal" type="text" name="contact_details" maxlength="11" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                 <div class="form-group">
		                                            
		                                        </div>
                                            </div>
                                        </div>
                                        
                                        
                                      
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary pull-right" id="button-prevent" type="submit">Add New Student</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </fieldset>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script src="{{asset('js/addenrollee.js')}}"></script>
<script type="text/javascript">


$(document).ready(function() {
  $('.add-enrollee').addClass('active');
});


//
// $('#program').on('change', function(e){
//             console.log(e);

//             var program = e.target.value;

//             $.get('/json-major?program=' + program,function(data){
//                 console.log(data);
                
//                  $('#major').empty();
//                  $.each(data, function(index, majorObj){
//                     $('#major').append('<option value="'+ majorObj.aka +'">'+ majorObj.major + '</option>');
//                 })
//             });
//         });
        $('.decimal').keypress(function(evt){
                        return (/^[0-9]*\~?[0-9]*$/).test($(this).val()+evt.key);
                    });
                    
                    $("#section").on('change',function(){
                        $('.selector').remove();
                    })
                    $("#program").on('change',function(){
                        $('.selector1').remove();
        });
                    
</script>
@stop