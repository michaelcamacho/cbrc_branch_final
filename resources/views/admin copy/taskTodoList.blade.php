@extends('main')
@section('title')
 Task Todo List
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Space+Mono&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
<style type="text/css">

  .tableRecords th{
    font-weight: normal; 
    font-family: 'Space Mono', monospace;
    font-weight: bold;
  }
  #title{
    margin-bottom: 0px;
    font-size: 18px;
    font-family: 'Quicksand', sans-serif;
     font-weight: bold;  
  }
  #desc{
    font-family: 'Quicksand', sans-serif;

  }

</style> 
@stop
@section('main-content')
<main class="main-wrapper clearfix">
  <div class="container">
    <div class="row"> 
      <div class="col-md-8">
         <div class="card shadow-sm p-3  mx-sm-1 p-3" style="margin-top: 10px; margin-bottom: 10px;">
              <div class="col-md-12">     
          <p style="font-size: 18px;font-weight: bold;">TASK TODO LIST <span id="dateToday" style="float: right;"></span></p>
          <hr>
            <div class="col-md-12">  
            <div id="taskBody" class="text-center"> 
              </div>
                  <div class="form-group purple-border">
                  <label for="exampleFormControlTextarea4">Remarks</label>
                  <textarea class="form-control" id="remarks" rows="3"></textarea>
                </div>
          
            <form action="/task_image" method="POST"  enctype="multipart/form-data"
            class="dropzone" 
            id="my-awesome-dropzone" style="border-color: blue; border: 2px dashed rgb(0, 135, 247); background: white;">
              @csrf      
              <div class="dz-message">Drop files here or click to upload.</div>  
            </form>
               <button type="button" class="btn btn-primary btn-block" id="submitTask" style="margin-top: 10px;">submit</button>
            
           </div>
        </div>
      </div>       
    </div>
  </div>
</div> <!-- end of container -->
</main>

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
 <script src="{{asset('assets/dropzone/dist/dropzone.js')}}"></script>
<script type="text/javascript">
   $(window).on('load',function(){
  var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
  $.ajax({
     url: 'https://cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = 
     JSON.stringify(response);
  var apiTokenParse = JSON.parse(apiToString);
  var apiToken = apiTokenParse.access_token;
   var today = new Date();
        var dd = String(today.getDate());
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        } 
         if(mm<10) {
            mm = '0'+mm
        }    
        today = mm +'-'+ dd +'-' +yyyy;
        $('#dateToday').text(today);



        $.get('https://cbrc.solutions/api/main/tasks/Novaliches?token='+apiToken,function(data){
              $.each(data, function (index, ObjCarousel) {
               $('#taskBody').empty();
                  $('#taskBody').append('<input id="branch" value="'+ObjCarousel.Branch_ID+'" type="hidden"></input><input id="taskId" value="'+ObjCarousel.Task_id+'" type="hidden"></input><p id="title">'+ObjCarousel.title+'</p><p id="desc">'+ObjCarousel.description+'</p>')
          });   
      });
 
 

Dropzone.options.myAwesomeDropzone = {
  addRemoveLinks: true,
  init: function() {
  }
};

$('#submitTask').on('click',function(){
     var imgg = $('#my-awesome-dropzone')[0].dropzone.getAcceptedFiles();
        var id = $('#taskId').val();
        var branch = $('#branch').val();
        var remarks = $('#remarks').val();
        var dateToday = $('#dateToday').text();
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

      var imageStore=[];
        for (var i = 0; i < imgg.length; i++) {
           var imageupload = imgg[i].name;
            imageStore.push(imageupload);
        }

        var formdata = new FormData();
        formdata.append('_token',csrf_token);
        formdata.append('task_id',id);
        formdata.append('image',imageStore);
        formdata.append('branch',branch);
        formdata.append('remarks',remarks);
        formdata.append('dateToday',dateToday);

         $.ajax({
          type: 'POST',
          url:  "https://cbrc.solutions/api/main/doneTask?token=" + apiToken,
          data:formdata,
          processData:false,
          contentType: false,
         }).done(function(response){
          swal('Task Completed!');
          window.location.href = "bulletin";
         });
      });
    });
  });  

</script>
@stop