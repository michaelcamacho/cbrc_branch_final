<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
	protected $connection = 'satelite_branch_db_connection';
  protected $guarded = [];
  
    protected $fillable = [
    	'id',
    	'employee_no',
    	'last_name',
    	'first_name',
    	'middle_name',
    	'birthdate',
    	'gender',
    	'status',
    	'address',
    	'email',
    	'contact_no',
        'contact_person',
        'contact_details',
    	'position',
    	'employment_status',
    	'rate',
    	'date_hired',
    	'sss',
    	'phil_health',
    	'pag_ibig',
    	'tin',
        'cover_image'
    ];
}
