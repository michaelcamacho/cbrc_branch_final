$('#program').on('change', function(e){
    console.log(e);

     var program = e.target.value;
     var branch = $('#branch').val().toLowerCase();

     $.get('json-student?program=' + program,function(data){
         console.log(data);

          $('#student').empty();
          $('#student').append('<option value="" disable="true" selected="true">--- Select Student ---</option>');

           $.each(data, function(index, studentObj){
             $('#student').append('<option value="'+ studentObj.school + '*'+ studentObj.last_name +', '+ studentObj.first_name+'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
         });
          
     });
});