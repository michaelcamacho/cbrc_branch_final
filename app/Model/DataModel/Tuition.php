<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class Tuition extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'tuitions';
}
