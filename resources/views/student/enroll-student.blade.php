@extends('main')
@section('title')
Student Summary
@stop
@section('css')
<style type="text/css">
.form-control:disabled, .form-control[readonly] {
color: #000;
}
@media print {
.content-header, .left-side, .main-header, .main-sidebar, .no-print {
display: none!important;
}
.table {
border-collapse: collapse !important;
}
td{
color: #000;
}

}
fieldset.info_separator {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    width:100%;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.info-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto;
    padding:0 10px;
    border-bottom:none;
}


</style>

<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
<!-- Page Title Area -->
<div class="container-fluid">
<div class="row page-title clearfix">
    
    <div class="page-title-left">
    <h6 class="page-title-heading mr-0 mr-r-5">Enroll Student</h6>
    </div>




</div>
<!-- /.page-title -->
</div>
<!-- /.container-fluid -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="container-fluid">
<div class="widget-list">
<div class="row">
<div class="col-md-12 widget-holder">
<div class="widget-bg p-5">

        @foreach($student as $data)
        <a href="{{'/student-summary/'.$data->id }}"  class="btn btn-info" >Return to Student Summary</a>
        @endforeach

        <br>
       


<div class="row col-lg-12">
    <div class="col-lg-6">
                    <fieldset class="info_separator">
                        <legend class="info-border">Student Information</legend>
                            @foreach($student as $data)
                                
                            <b>Name:    {{ $data->first_name . ' ' . $data->middle_name . ' ' . $data->last_name  }}</b><br>
                            <b >Program: {{ $data->program_name }}</b><br>
                            <b >Major:   {{ $data->major }}</b><br>
                            <b>Program: {{ $data->program_name }}</b><br>

                            <input type="hidden" form="student_form" value="{{$data->program}}" id="hidden_program_id"name="hidden_program_id" >
                            <input type="hidden" form="student_form" value="{{$data->program}}" id="hidden_major_id"name="hidden_major_id" >
                            @endforeach
                  



                    <div >
                            <label for="">Section</label>
                            <div class="form-group">
                                <select form="student_form" class="form-control" name="drop-down-section" type="text" id="drop-down-section" >
                                    <option value="0"  >--- Select Section ---</option>
                                    <option value="Section 1"  >Section 1</option>
                                    <option value="Section 2"  >Section 2</option>
                                    <option value="Section 3"  >Section 3</option>
                                    <option value="Section 4"  >Section 4</option>
                                    <option value="Section 5"  >Section 5</option>
                                    <option value="Section 6"  >Section 6</option>
                                    <option value="Section 7"  >Section 7</option>
                                    <option value="Section 8"  >Section 8</option>
                                    <option value="Section 9"  >Section 9</option>
                                    <option value="Section 10" >Section 10</option>
                                </select>
                            </div>
                        </div>
                
                        <div >
                            <label>Student Category</label>
                            <div class="form-group">
                                <select form="student_form" class="form-control" name="drop-down-category" id="drop-down-category" >
                              
                                    @foreach($student_category as $category)
                                     <option value="{{$category->id}}">{{ $category->category_name }}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                    
                        <div >
                            <label for="">Tuition Fee</label>
                            <div class="form-group">
                                <select form="student_form" class="form-control" name="drop-down-tuition"  id="drop-down-tuition" >
                                    <option value="0"  class="cat" selected>-- Select Tuition Fee --</option>
                                </select>
                            </div>
                        </div>
                
                
                        <div>
                            <label for="">Discount</label>
                            <div class="form-group">
                                <select form="student_form" class="form-control" name="drop-down-discount"  id="drop-down-discount" >
                                    <option value="0"   selected>--- Select Discount ---</option>                
                                </select>
                            </div>
                        </div>
                    </fieldset>
    </div>
    <div class="col-lg-6">

        <form id="student_form" method="POST" action="/proceed/enroll">
        <input type="hidden" name="student_id" value="{{$data->id}}">
        @csrf

<fieldset class="info_separator" style="font-family:digital-clock-font; font-size:16px;">
    <legend class="info-border">Student Information</legend>   
    
    <div style="text-align:right">
        Tuition Fee:<input style="border:0;text-align:right;" name="tuition_fee_price" id="tuition_fee_price"/><br>        
        facilitation Fee:<input style="border:0;text-align:right;" name="facilitation_fee" id="facilitation_fee"/><br>
        Discount Amount:<input style="border:0;text-align:right;" name="discount_amount" value="0" id="discount_amount"/><br>
        <hr style="margin:0px; border:1.2px solid gray">
        Total Due:<input style="border:0;text-align:right;" name="total_due" id="total_due"/>
    </div>      
    <br><br>
        
        <br>

        <div class="row col-lg-12" >
            <div class=" col-lg-6" >
                <label for=""> 
                    Amount Paid: 
                </label>
                    <input type="number"  style="text-align: center" name="amount_paid" placeholder="0.00">
                
            </div>
            <div class="col-lg-6 " style="margin-top:-30px;">   
        
                <button  id="compute" class="btn btn-info pull-right">Compute</button>  
                <br>
                <br>    
                <input type="submit"  value="Enroll Student" name="submit" href="#" class="btn btn-info pull-right" />
            </div>
        </div>
        
</fieldset>
      
    </div>
</div>
<!-- /.row -->


     
</form>

<!-- /.widget-bg -->
</div>
<!-- /.widget-holder -->
</div>
<!-- /.row -->
</div>
<!-- /.widget-list -->
</div>
<!-- /.container-fluid -->
</main>

@stop
@section('js')
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
$('table.display').DataTable();
});

$(document).ready(function() {
$('.record').addClass('active');
$('.record').addClass('collapse in');
});




$('#drop-down-category').on('load change', function(e){
    var program_id = $('#hidden_program_id').val();
    var category_id = $(this).val();

                $('#tuition_fee_price').val(0);
                $('#facilitation_fee').val(0);
                $('#discount_amount').val(0);

        $.get('/data/fetch-tuition/'+category_id+'/'+program_id ,function(data){
                $('#drop-down-tuition').empty();
                $('#drop-down-tuition').append('<option value="0">-- Select Tuition Fee --</option>');
            $.each(data, function(index, tuition){
                $('#drop-down-tuition').append('<option value="'+tuition.id+'">'+tuition.tuition_name+'</option>');
            });
        });


    $.get('/data/fetch-discount/'+category_id+'/'+program_id ,function(data){
                
                $('#drop-down-discount').empty();
                $('#drop-down-discount').append('<option value="0">-- Select Discount Fee --</option>');
            $.each(data, function(index, discount){
                $('#drop-down-discount').append('<option value="'+discount.id+'">'+discount.discount_category+'</option>');
            });
        });

});
jQuery(window).on("load", function(){
    $('#drop-down-category').change()
});




$('#drop-down-tuition').on('change', function(e){
    var category_id = $('#drop-down-category').val();
    var program_id = $('#hidden_program_id').val();

       $.get('/data/fetch-discount/'+category_id+'/'+program_id ,function(data){
            

                $('#drop-down-discount').empty();
                $('#drop-down-discount').append('<option value="0">-- Select Discount Fee --</option>');
            $.each(data, function(index, discount){
                $('#drop-down-discount').append('<option value="'+discount.id+'">'+discount.discount_category+'</option>');
            });
        });
});



$('#drop-down-tuition').on('change', function(e){   
    var tuition_id = $(this).val();


       $.get('/data/fetch-tuition-data/'+tuition_id,function(data){
             
                $('#tuition_fee_price').val(0);
                $('#facilitation_fee').val(0);
                $('#discount_amount').val(0);
                $('#total_due').val(0);

            $.each(data, function(index, tuition){
                $('#tuition_fee_price').val(tuition.tuition_fee);
                $('#facilitation_fee').val(tuition.facilitation_fee);
            });
        });
});





$('#drop-down-discount').on('change', function(e){
    var discount_id = $(this).val();
   
      $.get('/data/fetch-discount-data/'+discount_id,function(data){
               $('#discount_amount').val('');
               $('#total_due').val(0);
   
        $.each(data, function(index, discount){
               $('#discount_amount').val(discount.discount_amount);
               console.log(discount);   
        });
       });

});


$('#compute').on('click', function(){

var tuition = parseInt($("#tuition_fee_price").val());
var facilitation = parseInt($("#facilitation_fee").val());
var discount = parseInt($("#discount_amount").val());

    total_amount = (tuition + facilitation)  - discount;
    $("#total_due").val(total_amount);
    
        

});


</script>
@stop