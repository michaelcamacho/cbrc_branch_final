<?php

use Illuminate\Database\Seeder;

class ViewSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_settings')->insert([
        	'id' => '2',
            'Account' => 'member',
            'Branch' => 'Novaliches',
            'Season'   => 'Season 1',
            'Year'   => '2019',
        ]);

        DB::table('admin_settings')->insert([
        	'id' => '3',
            'Account' => 'admin',
            'Branch' => 'Novaliches',
            'Season'   => 'Season 1',
            'Year'   => '2019',
        ]);

        DB::table('expense_setups')->insert([
        	'id' => '1',
            'Branch' => 'Novaliches',
            'Season'   => 'Season 2',
            'Year'   => '2019',
        ]);


    }
}
