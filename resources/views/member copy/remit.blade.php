@extends('main')
@section('title')
Cash Remit Records
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Cash Remit Records</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Cash Remit Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Cash Remit Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="exam" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Trans. #</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>Season</th>
                            <th>Amount</th>
                            <th>Remarks</th>
                            <th>Author</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($remit))
                            @foreach($remit as $remits)
                        <tr>
                            <td>00{{$remits->id}}</td>
                            <td>{{$remits->date}}</td>
                            <td>{{$remits->category}}</td>
                            <td>{{$remits->season}}</td>
                            <td>{{$remits->amount}}</td>
                            <td>{{$remits->remarks}}</td>
                            <td>{{$remits->author}}</td>
                           
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
           
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
});

</script>
@stop