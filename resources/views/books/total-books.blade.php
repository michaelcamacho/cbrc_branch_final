@extends('main')
@section('title')
 Books
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Books Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Books Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="book-table">
                                    <table id="book" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                        <div class="add">
                                        <a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#add-book">Add Books</a>
                                        </div>
                     <thead>
                       
                        <tr>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Price</th>
                            <th>Stock</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($a_book))
                            @foreach($a_book as $a_books)
                        <tr id="row_{{$a_books->id}}">
                            <td id="a_branch_{{$a_books->id}}">{{$a_books->branch}}</td>
                            <td id="a_program_{{$a_books->id}}">{{$a_books->program}}</td>
                            <td id="a_book_title_{{$a_books->id}}">{{$a_books->book_title}}</td>
                            <td id="a_book_author_{{$a_books->id}}">{{$a_books->book_author}}</td>
                            <td id="a_price_{{$a_books->id}}">{{$a_books->price}}</td>
                            <td id="a_available_{{$a_books->id}}">{{$a_books->available}}</td>
                            <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$a_books->id}}" data-toggle="modal" data-target="#update-book">Update</a></td>
                            <td><button class="btn btn-danger delete" user-id="{{$a_books->id}},{{$a_books->branch}}">Delete</button></td>
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>
            </div>
           
            
        <div id="add-book" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="book-form">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <select class="form-control" name="branch" id="branch" required>
                                        <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                        @if(isset($branch))
                                        @foreach($branch as $branches)
                                        <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="program" required>
                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                <label for="book_title">Book Title</label>
                                <select class="form-control" name="book_title" id="book_title" required>
                                    <option value="0" disable="true" selected="true">--- Select Book Title ---</option>
                                </select>
                                </div>
                                <div class="form-group">
                                    <label for="book_author">Book Author</label>
                                    <input class="form-control" type="text" name="book_author" id="book_author" required>
                                </div>
                                <div class="form-group">
                                    <label for="price">Book Price</label>
                                    <input class="form-control decimal" type="text" maxlength="12" name="price" id="price" required>
                                </div>
                                <div class="form-group">
                                    <label for="available">Total books stocks</label>
                                    <input class="form-control decimal1" type="text" name="available" id="available" required>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple save" data-dismiss="modal">Save</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

            <div id="update-book" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="book-form-update">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="branch" id="update_id" readonly required>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="update_branch">Branch</label>
                                    <select class="form-control" name="update_branch" id="update_branch" required>
                                        <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                        @if(isset($branch))
                                        @foreach($branch as $branches)
                                        <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="update_program">Program</label>
                                    <select class="form-control" name="update_program" id="update_program" required>
                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label for="update_book_title">Book Title</label>
                                        <select class="form-control" name="update_book_title" id="update_book_title" required>
                                            <option value="0">-- Select book --</option>
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label for="update_author">Book Author</label>
                                    <input class="form-control" type="text" name="update_author" id="update_author" required>
                                </div>
                                <div class="form-group">
                                    <label for="price">Book Price</label>
                                    <input class="form-control" type="number" name="price" id="update_price" required>
                                </div>
                                <div class="form-group">
                                    <label for="available">Current Stock</label>
                                    <input class="form-control" type="number" name="available" id="update_available" disabled>
                                </div>
                                <div class="row col-md-12">
                                    <div class="form-group col-md-6">
                                    <label for="stockInput">Enter Quantity </label>
                                    <input class="form-control decimal1" maxlength="12" type="text" id="stockInput" placeholder="0" value="0" name="stockInput"  />
                                  
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                    <label for="stockInput">Add or Remove</label>
                                    <select class="form-control" name="option" id="option" required>
                                            <option value="add"  selected="true">Add</option>
                                            <option value="remove" >Remove</option>
                                          
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="newStock"></label>
                                    <input class="form-control" type="number"   name="newStock" id="update_newStock" readonly/>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple update button_update" data-dismiss="modal">Update</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->

            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <br/>
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Books Sales Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="book-table">
                                    <table id="sale" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                     <thead>
                       
                        <tr>
                            <th>Trans. #</th>
                            <th>Date</th>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Amount</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($a_sale))
                            @foreach($a_sale as $a_sales)
                        <tr id="row_{{$a_sales->id}}">
                            <td>{{$a_sales->id}}</td>
                            <td>{{$a_sales->date}}</td>
                            <td>{{$a_sales->branch}}</td>
                            <td>{{$a_sales->program}}</td>
                            <td>{{$a_sales->name}}</td>
                            <td>{{$a_sales->book_title}}</td>
                            <td>{{$a_sales->amount}}</td>
                            
                        </tr>
                       @endforeach
                            @endif


                    </tbody>           
                    
                </table>
            </div>

             </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/book.js') }}"></script>
<script type="text/javascript">

    $('.save').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('branch', $('#branch').val());
                formData.append('program', $('#program').val());
                formData.append('book_title', $('#book_title').val());
                formData.append('book_author', $('#book_author').val());
                formData.append('price', $('#price').val());
                formData.append('available', $('#available').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/insert-book') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-book form')[0].reset();
                        location.reload();

                    }
                               
              });
            });

    $('.update').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', $('#update_id').val());
                formData.append('branch', $('#update_branch').val());
                formData.append('program', $('#update_program').val());
                formData.append('author', $('#update_author').val());
                formData.append('book_title', $('#update_book_title').val());
                formData.append('price', $('#update_price').val());
                formData.append('available', $('#update_newStock').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/update-book') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-book form')[0].reset();
                        location.reload();

                    }
                               
              });
            });
    $('.delete').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-book') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

    $('#book').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-book') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });


   
    
    $('#stockInput, #option ').on('keyup change wheel',function(){
        var x = 0;

            if($('#option').val() == "remove"){

        if( parseInt($('#stockInput').val()) > parseInt($('#update_available').val()) ){
            $('#stockInput').val("") ; 
        }else{

            var x =  parseInt($('#update_available').val()) - parseInt($('#stockInput').val()) ;
        }

            }
            else if($('#option').val() == "add"){
                
        var x =  parseInt($('#update_available').val()) + parseInt($('#stockInput').val()) ;
               
                }
        console.log(x);
        $('#update_newStock').val(x);
    });

    

   var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"};
         

         $('#program').on('change', function(e){
            console.log(e);
            var program = $("#program").val();
            
            $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   
            console.log(urlToken);

                    $.getJSON('https://cbrc.solutions/api/main/books?token='+urlToken,function(data){
                    
                                $('#book_title').empty();
                                $('#book_title').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');
                                    $.each(data, function (i) {
                                        $.each(this, function (key, value) {
                                        
                                            console.log(value.program_name + " = " + program);
                                            if(value.program_name == program ){
                                                

                                             $('#book_title').append('<option value="'+ value.Title +'">'+  value.Title +' </option>');
                                            }
                                        
                                        });
                                    });

                });//end of on change getJSON
                });
        });//end of on change #program


         
         $('#update_program').on('change', function(e){
            console.log(e);
            var program = $("#update_program").val();

            $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   
            console.log(urlToken);

            $.getJSON('https://cbrc.solutions/api/main/books?token='+urlToken,function(data){
            
            $('#update_book_title').empty();
            $('#update_book_title').append('<option value="0" disable="true" selected="true">--- Select Book ---</option>');

                $.each(data, function (i) {
                    $.each(this, function (key, value) {
                    
                        console.log(program);
                        console.log(value.program_name);
                        if(value.program_name == program ){
                            
                        $('#update_book_title').append('<option value="'+ value.Title +'">'+  value.Title +' </option>');
                    }                    
                });
            });
            });

                });//end of on change getJSON
               
        });//end of on change #update_program


         $('#update-book').on('focus',function(e){
            var x =  parseInt($('#update_available').val()) + parseInt($('#stockInput').val());
                    console.log(x);
                    $('#update_newStock').val(x);
            console.log(e);
            var program = $("#update_program").val();
            var book_title = $("#update_book_title").val();

            $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   
            console.log(urlToken);

            $.getJSON('https://cbrc.solutions/api/main/books?token='+urlToken,function(data){
            
                $('#update_book_title').empty();
                $('#update_book_title').append('<option value="'+ book_title +'">'+  book_title +' </option>');
                $.each(data, function (i) {
                    console.log(data);
                    $.each(this, function (key, value) {
                    
                        console.log(program);
                        console.log(value.program_name);
                        if(value.program_name == program ){
                            
                        $('#update_book_title').append('<option value="'+ value.Title +'">'+  value.Title +' </option>');
                    }                    
                });
            });
            });
            });
          

                });//end of on change getJSON

            $('#update_book_title , #book_title').on('change',function(e){
            console.log(e);
            var program = $("#update_program").val();
            var book_title = $("#update_book_title").val();

            $.ajax({
              url: 'https://cbrc.solutions/api/auth/login',
              method: 'POST',
              data: auth
              }).done(function(response){
              var tokenResponse = JSON.stringify(response);
              var token = JSON.parse(tokenResponse);
              var urlToken = token.access_token;   
            console.log(urlToken);
            $.getJSON('https://cbrc.solutions/api/main/books?token='+urlToken,function(data){
          
            $.each(data, function (i) {
                    $.each(this, function (key, value) {
                    
                        
                        if(value.Title == $('#book_title').val() ){
                        console.log('this is the data: = ' + this);
                        console.log('this is from Author:' + value.Author);
                        var author = value.Author;
                        $('#update_author').val(author);
                            $('#book_author').val(author);
                        }
                    
                    });
                });
                });
               
                
                });

        });//end of on change #update_program

        $('.decimal').keypress(function(evt){
            return (/^[0-9]*\.?[0-9]*$/).test($(this).val()+evt.key);
        });  
        $('.decimal1').keypress(function(evt){
            return (/^[0-9]*\~?[0-9]*$/).test($(this).val()+evt.key);
        });  
</script>
@stop