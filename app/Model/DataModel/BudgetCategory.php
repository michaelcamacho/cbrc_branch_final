<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class BudgetCategory extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'budget_categories';
    protected $fillable = [
        'id',
        'category'

    ];
}
