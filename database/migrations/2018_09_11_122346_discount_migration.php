<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiscountMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch_id');
            $table->string('program_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('discount_category')->nullable();
            $table->string('discount_amount')->nullable();

            $table->timestamps();

        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('discounts');      

    }
}
