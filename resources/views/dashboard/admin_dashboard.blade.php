@extends('main')
@section('title')
DASHBOARD
@stop
@section('main-content')

@if ($flash = session('log'))
<input type="hidden" value="{{$flash}}" id="logstat" />
<input type="hidden" value="" id="ip" />
<input type="hidden" value="" id="branch" />
<input type="hidden" value="" id="user" />
<input type="hidden" value="" id="user_id" />

@endif
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Summary</h6>
                        {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index-2.html">Dashboard</a>
                            </li>
                            {{--<li class="breadcrumb-item active">Home</li>--}}
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->



            <div class="container-fluid">
                
                @if($flash = session('log'))
                <input type="hidden" value="{{$flash}}" id="logstat" />
                @endif

              

                {{-- {{$data_dashboard->branch_name}} --}}
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                    </div>

                    <div class="widget-holder col-lg-4">
    
                    </div>
                    <!-- /.widget-holder -->
                    @foreach($data_dashboards as $data_dashboard)
                    <div class="widget-holder widget-sm col-md-4 widget-full-height season1card">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Sales for Season {{$data_dashboard->season}}</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                        <span class="counter">
                                        {{($data_dashboard->total_tuition_fee + $data_dashboard->total_facilitation)}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    @endforeach
                    <div class="widget-holder widget-sm col-md-4 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Facilitation Balance</p>
                                        <span class="counter-title-a d-block">
                                            Php
                                        <span class="counter">
                                        {{$other_data_dashboard->total_facilitation or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{ url('/total-program')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View List</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                     <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse bg-primary">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Enrollees</p>
                                        <span class="counter-title-a d-block">
                                            
                                            <span class="counter">
                                            {{$other_data_dashboard->total_enrollee or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Records</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    

                     <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Expenses</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$total_expense or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{url('total-expense')}}" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Receivables</p>
                                        <span class="counter-title-a d-block">
                                        Php
                                        <span class="counter">
                                        {{$other_data_dashboard->total_receivable or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col-md-3 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Discounts</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$other_data_dashboard->total_discount or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    
                 
                  
                    <!-- /.widget-holder -->
                </div>
                <!-- /.widget-list -->


                {{-- if main logged in --}}
                Season 1
            <div class="row">
                
                    @foreach($season1_other as $ob)
                <div class="widget-holder widget-sm col-md-3 col-lg-3 widget-full-height">
                        
                        <div class="widget-bg text-inverse" style="background: #e8b335">
                               
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">{{$ob->main_branch}}</p>
                                        
                                        Total Sale
                                        <span class="counter">
                                        {{($ob->total_facilitation + $ob->total_tuition_fee)}}
                                        </span><br>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    @endforeach
                </div>
                Season 2
                <div class="row">
                        @foreach($season2_other as $ob1)
                        <div class="widget-holder widget-sm col-md-3 col-lg-3 widget-full-height">
                                
                                <div class="widget-bg text-inverse" style="background: #e8b335">
                                       
                                    <div class="widget-body">
                                        <div class="counter-w-info media">
                                            <div class="media-body w-50">
                                                <p class="text-muted mr-b-5 fw-600">{{$ob1->main_branch}}</p>
                                                
                                                Total Sale
                                                <span class="counter">
                                                {{($ob1->total_facilitation + $ob1->total_tuition_fee)}}
                                                </span><br>
                                                <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">Branches</div>
                                            </div>
                                            <!-- /.media-body -->
                                            <div class="pull-right align-self-center">
                                                <div class="mr-t-20">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.counter-w-info -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            @endforeach
                    </div>

                    Expenses
                    <div class="row">
                            @foreach($expense_per_branch as $exp1)
                            <div class="widget-holder widget-sm col-md-3 col-lg-3 widget-full-height">
                                    
                                    <div class="widget-bg text-inverse" style="background: #e8b335">
                                           
                                        <div class="widget-body">
                                            <div class="counter-w-info media">
                                                <div class="media-body w-50">
                                                    <p class="text-muted mr-b-5 fw-600">{{$exp1->branch_name}}</p>
                                                    
                                                    Total Expense
                                                    <span class="counter">
                                                    {{$exp1->total_expense_per_branch}}
                                                    </span><br>
                                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">Branches</div>
                                                </div>
                                                <!-- /.media-body -->
                                                <div class="pull-right align-self-center">
                                                    <div class="mr-t-20">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.counter-w-info -->
                                        </div>
                                        <!-- /.widget-body -->
                                    </div>
                                    <!-- /.widget-bg -->
                                </div>
                                @endforeach
                        </div>


                    Enrollee
                    <div class="row">
                            @foreach($enrollee_per_branch as $enr)
                            <div class="widget-holder widget-sm col-md-3 col-lg-3 widget-full-height">
                                    
                                    <div class="widget-bg text-inverse" style="background: #e8b335">
                                           
                                        <div class="widget-body">
                                            <div class="counter-w-info media">
                                                <div class="media-body w-50">
                                                    <p class="text-muted mr-b-5 fw-600">{{$enr->branch_name}}</p>
                                                    
                                                    Total Enrollee
                                                    <span class="counter">
                                                    {{$enr->total_enrollee_per_branch}}
                                                    </span><br>
                                                    <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">Branches</div>
                                                </div>
                                                <!-- /.media-body -->
                                                <div class="pull-right align-self-center">
                                                    <div class="mr-t-20">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.counter-w-info -->
                                        </div>
                                        <!-- /.widget-body -->
                                    </div>
                                    <!-- /.widget-bg -->
                                </div>
                                @endforeach
                        </div>
                        
                    
            </div>
        </main>


@stop
@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
    

<script type="text/javascript">
    $('.admin-dashboard').addClass('active');

</script>

<script>
        $( window ).on("load", function() {
                const stat = $('#logstat').val();
                if (stat != null) {
                     console.log(stat);
                     //this get function fetch the authenticated user 
                      
                        $.get('/fetch-userinfo/'+stat, function(data){
                            var formData = new FormData();
                            $.getJSON('https://api.ipify.org?format=json', function(ips){
                                    formData.append('ipaddress',ips.ip);
                                       formData.append('action','Login');
                                        $('#ip').val(ips.ip); 
                                          $.each(data, function(index,objuser){              
                                              $('#branch').val('Novaliches');
                                              $('#user').val(objuser.name+'-Administrator');
                                              $('#user_id').val(objuser.id);
                                                formData.append('branch',objuser.branch);
                                                formData.append('user',objuser.name);
                                                formData.append('user_id',objuser.id);
                                            }); 
                                        }); 
                            
                                    });
                                }
                            });
        
            </script>
        <script>
            setTimeout(function(){
                    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"};       
                    $.ajax({
                      url: 'https://cbrc.solutions/api/auth/login',
                      method: 'POST',
                      data: auth
                      }).done(function(response){
                      var tokenResponse = JSON.stringify(response);
                      var token = JSON.parse(tokenResponse);
                      var urlToken = token.access_token;   
                      
                      console.log(urlToken);
                             var items = {          
                            "action":"Login",
                            "branch":$('#branch').val(),
                            "user":$('#user').val(),
                            "user_id":$('#user_id').val(),
                            "ipaddress":$('#ip').val(),
                                
                            };
                            
                               var FormToString = JSON.stringify(items);
                      var formToJsons = JSON.parse(FormToString);
                      var str = JSON.stringify(formToJsons);
                      var formToJson = JSON.parse(str);
                        $.ajax({
                            url: 'https://cbrc.solutions/api/main/system-log?token='+urlToken,
                            type: 'POST',
                            data: formToJson,
                                }).done(function(response){
                                     console.log(response);
                            });
                         });             
                    
                }, 2000);           
        </script>
@stop
