@extends('main')
@section('title')
Lecturer Evaluation
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
.btn-group {
    top: -14px;
}
div.dataTables_wrapper div.dataTables_info {
    display: none;
}
#table-header{
    float: right;
}
#lec3_filter{
    display: none;
}
#titleLecturer{
    font-size: 15px;
    font-weight: bold;
}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<div id="secret">

</div>
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{$sbranch}}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Lecturer Evaluation</li>
                            <li class="breadcrumb-item active">Lecturer Evaluation Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>

            <div class="container">
                  <div class="row">
                    <div class="col-md-6">
                       <div class="card shadow-sm p-3  mx-sm-1 p-3" style="margin-top: 10px; margin-bottom: 10px;">
                            <div class="col-md-12"> 
                              <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;">EVALUATION REPORT</p>
                            <hr>
                            <div class="form-group">
                                <label>Branch</label>
                                <input class="form-control" type="text" value="{{$sbranch}}" name="branch" id="branch">    
                            </div>
                            <div class="form-group">
                                <label>Season</label>
                                <select class="form-control"  name="season" id="season" >
                                <option value="0" selected="true" disabled = "true">--- Select Season ---</option>
                                 <option value="Season 1">Season 1</option>
                                 <option value="Season 2">Season 2</option>
                              </select>
                                
                            </div>
                             <div class="form-group">
                                <label>Year</label>
                                <select class="form-control yearSearch" name="year" id="year" required>
                                  <option value="0" selected="true">--- Select Year ---</option>
                                    @foreach($years as $year)
                                        <option value="{{$year}}">{{$year}}</option>
                                    @endforeach

                              </select>
                            </div>
                            <div class="form-group">
                                <label>Lecturer</label>
                                <select class="form-control" name="program" id="FilterLecturer" required >
                                  <option value="0" selected="true" disabled = "true">--- Select Lecturer ---</option>
                                </select>                 
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <select class="form-control"  name="class" id="Filtersubj" readonly>
                                <option value="0" selected="true" disabled = "true">--- Select Subject ---</option>
                              </select>
                            </div>
                       
                             <div class="form-group">
                                <button class="btn btn-success evaluateLecturer" id="evallec" style="width: 100%;" >Search Lecturer</button>
                            </div>
                      </div>
                    </div>       
                  </div>

                   <div class="col-md-6">
                       <div class="card shadow-sm p-3  mx-sm-1 p-3" style="margin-top: 10px; margin-bottom: 10px;">
                            <div class="col-md-12"> 
                              <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;">LECTURER INFORMATION</p>
                            <hr>
                            <p id="titleLecturer"><i class="fa fa-tv"></i> BRANCH : <span class="branch_info" style="color:red;"></span></p>
                            <p id="titleLecturer"><i class="fa fa-user"></i> LECTURER : <span class="lecturer_info" style="color:red;"></span></p>
                            <p id="titleLecturer"><i class="fa fa-bookmark"></i> SUBJECT :<span class="subject_info" style="color:red;"></span></p>
                            <p id="titleLecturer"><i class="fa fa-calendar-plus"></i> SEASON : <span class="season_info" style="color:red;"></span></p>
                            <p id="titleLecturer"><i class="fa fa-calendar-check"></i> YEAR : <span class="year_info" style="color:red;"></span></p>
                           
                  </div>
                </div>
              </div> <!-- end of container -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <p style="font-size: 18px;font-weight: bold;margin-bottom: 5px;">EVALUATION RESULT</p>
                                    <hr>
                                </div>
                                <div class="widget-body clearfix">  
                                <div class="col-12">
                                <div class="panel panel-default">
                                                  
                                <div class="table-responsive">        
                                    <p style="font-size: 18px;font-weight: bold;">Evaluation Question</p>
                                        <table id="lec2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important"> 
                                            <tr>
                                                <th class="cell-question">Question</th>
                                                <th class="cell">Excellent</th>
                                                <th class="cell">Good</th>
                                                <th class="cell">Fair</th>
                                                <th class="cell">Poor</th>
                                                <th class="cell">Very Poor</th>
                                                <th class="cell">Total</th>
                                            </tr>       
                                            <tr>
                                                <td class="cell-question"><p>Comprehensiveness – shows knowledge of the subject matter</p></td>
                                                <td class="cell"><input type="text" name="qA" id="excellentA" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="goodA" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="fairA" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="poorA" value="0" class="form-control"></td>                                                                         </td>
                                                <td class="cell"><input type="text" name="qA" id="verypoorA" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalA" value="0" class="form-control"></td>
                                            </tr>
                                            <tr>
                                                <td><p>Substantiveness – delivers enough and substantial concepts</p></td>
                                                <td class="cell"><input type="text" name="qB" id="excellentB" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qB" id="goodB" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qB" id="fairB" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qB" id="poorB" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qB" id="verypoorB" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalB" value="0" class="form-control"></td>
                                            </tr>

                                            <tr>
                                                <td><p>Relevance – provides topics that are relevant to the actual exam</p></td>
                                                <td class="cell"><input type="text" name="qC" id="excellentC"  value="0"class="form-control"></td>
                                                <td class="cell"><input type="text" name="qC" id="goodC" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qC" id="fairC"  value="0"class="form-control"></td>
                                                <td class="cell"><input type="text" name="qC" id="poorC" value="0" class="form-control"></td>
                                                <td  class="cell"><input type="text" name="qC" id="verypoorC" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalC" value="0" class="form-control"></td>
                                            </tr>
                                            <tr>
                                                <td><p>Organization – executes the topic in an organized manner</p></td>
                                                <td class="cell"><input type="text" name="qD" id="excellentD" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qD" id="goodD" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qD" id="fairD" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qD" id="poorD" value="0" class="form-control"></td>
                                                <td  class="cell"><input type="text" name="qD" id="verypoorD" value="0" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalD" value="0" class="form-control"></td>

                                            </tr>
                                                <td><strong>MASTERY</strong></td>
                                                <td colspan="6"></td>                             
                                            </tr>
                                            <tr>
                                                <td><p>Authority –  exhibits confidence and authority in presenting the topic</p></td>
                                                <td class="cell"><input type="text" name="qE" value="0" id="excellentE" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qE" value="0" id="goodE" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qE" value="0" id="fairE" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qE" value="0" id="poorE" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qE" value="0" id="verypoorE" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalE" value="0" class="form-control"></td>

                                        </tr>
                                        <tr>
                                            <td><p>Ability to Answer Questions – able to answer students questions satisfactorily</p></td>
                                            <td class="cell"><input type="text" name="qF" value="0" id="excellentF" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qF" value="0" id="goodF" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qF" value="0" id="fairF" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qF" value="0" id="poorF" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qF" value="0" id="verypoorF" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qA" id="totalF" value="0" class="form-control"></td>

                                            
                                        </tr>
                                        <tr>
                                            <td> <p>Preparedness – well-prepared to discuss the lecture</p></td>
                                            <td class="cell"><input type="text" name="qG" value="0" id="excellentG" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qG" value="0" id="goodG" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qG" value="0" id="fairG" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qG" value="0" id="poorG" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qG" value="0" id="verypoorG" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qA" id="totalG" value="0" class="form-control"></td>                                  
                                        </tr>
                                        <tr>
                                            <td><strong>DELIVERY</strong></td>
                                            <td colspan="6"></td>
                                        </tr>
                                        <tr>
                                            <td><p>Enthusiasm  - shows energy that stimulates participation</p></td>
                                            <td class="cell"><input type="text" name="qH" value="0" id="excellentH" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qH" value="0" id="goodH" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qH" value="0" id="fairH" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qH" value="0" id="poorH" class="form-control"></td>
                                            <td  class="cell"><input type="text" name="qH" value="0" id="verypoorH" class="form-control"> </td>
                                            <td class="cell"><input type="text" name="qA" id="totalH" value="0" class="form-control"></td>
                                        </tr>
                                        <tr>
                                            <td><p>Pacing – moves to one topic to another understandable to the students</p></td>
                                            <td class="cell"><input type="text" name="qI" value="0" id="excellentI" class="form-control" ></td>
                                            <td class="cell"><input type="text" name="qI" value="0" id="goodI" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qI" value="0" id="fairI" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qI" value="0" id="poorI" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qI" value="0" id="verypoorI" class="form-control"> </td>
                                            <td class="cell"><input type="text" name="qA" id="totalI" value="0" class="form-control"></td>

                                        </tr>
                                        <tr>
                                            <td><p>Voice Projection –has effetive and clear voice</p></td>

                                            <td class="cell"><input type="text" name="qJ" value="0" id="excellentJ" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qJ" value="0" id="goodJ" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qJ" value="0" id="fairJ" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qJ" value="0" id="poorJ" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qJ" value="0" id="verypoorJ" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qA" id="totalJ" value="0" class="form-control"></td>

                                            
                                        </tr>
                                        <tr>
                                            <td><p>Approach/ Strategy – has dynamic technique in discussing</p></td>
                                            <td class="cell"><input type="text" name="qK" value="0" id="excellentK" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qK" value="0" id="goodK" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qK" value="0" id="fairK" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qK" value="0" id="poorK" class="form-control"></td>
                                            <td class="cell"><input type="text" name="qK" value="0" id="verypoorK" class="form-control"></td> 
                                            <td class="cell"><input type="text" name="qA" id="totalK" value="0" class="form-control"></td>
                                        </tr>
                                        <tr>
                                                <td>
                                                  <strong>MOTIVATIONAL</strong>
                                                </td>
                                                <td colspan="6"></td>
                                                
                                              </tr>
                                              <tr>
                                                <td><p>Inspiration – inspires me to strive harder and makes the topic more interesting</p></td>
                                                <td class="cell"><input type="text" name="qL" value="0" id="excellentL" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qL" value="0" id="goodL" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qL" value="0" id="fairL" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qL" value="0" id="poorL" class="form-control"></td>
                                                <td  class="cell"><input type="text" name="qL" value="0" id="verypoorL" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalL" value="0" class="form-control"></td>

                                                
                                              </tr>
                                              <tr>
                                                <td>
                                                  <p>Self-Esteem – boosts me to be a better person</p>
                                                </td>
                                                <td class="cell"><input type="text" name="qM" value="0" id="excellentM" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qM" value="0" id="goodM" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qM" value="0" id="fairM" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qM" value="0" id="poorM" class="form-control"></td>
                                                <td  class="cell"><input type="text" name="qM" value="0" id="verypoorM" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalM" value="0" class="form-control"></td>

                                               
                                              </tr>
                                              <tr>
                                                <td>
                                                  <p>Authenticity – shows sincerity and authenticity in presenting himself/herself</p>
                                                </td>
                                                <td class="cell"><input type="text" name="qN" value="0" id="excellentN" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qN" value="0" id="goodN" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qN" value="0" id="fairN" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qN" value="0" id="poorN" class="form-control"></td>
                                                <td  class="cell"><input type="text" name="qN" value="0" id="verypoorN" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalN" value="0" class="form-control"></td>

                                              </tr>
                                              <tr>
                                                    <td><strong>IMAGING</strong></td>
                                                    <td colspan="6"></td>
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <p>Appearance & Grooming – shows neatness, proper posture, and an aesthetic appeal</p>
                                                    </td>
                                                    <td class="cell"><input type="text" name="qO" value="0" id="excellentO" class="form-control"></td>
                                                    <td class="cell"> <input type="text" name="qO" value="0" id="goodO" class="form-control"></td>
                                                    <td class="cell"> <input type="text" name="qO" value="0" id="fairO" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qO" value="0" id="poorO" class="form-control"></td>
                                                    <td  class="cell"><input type="text" name="qO" value="0" id="verypoorO" class="form-control"> </td>
                                                <td class="cell"><input type="text" name="qA" id="totalO" value="0" class="form-control"></td>

                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <p>Character & Personality - shows uniqueness and carried himself/herself very well</p>
                                                    </td>
                                                    <td class="cell"><input type="text" name="qP" value="0" id="excellentP" class="form-control" required></td>
                                                    <td class="cell"><input type="text" name="qP" value="0" id="goodP" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qP" value="0" id="fairP" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qP" value="0" id="poorP" class="form-control"></td>
                                                <td class="cell"><input type="text" name="qA" id="totalP" value="0" class="form-control"></td>

                                                    <td  class="cell">
                                                        <input type="text" name="qP" value="0" id="verypoorP" class="form-control"> 
                                                    </td>
                                                    
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <p>Impact – has stage presence, charisma, and a contagious aura</p>
                                                    </td>
                                                    <td class="cell"><input type="text" name="qQ" value="0" id="excellentQ" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qQ" value="0" id="goodQ" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qQ" value="0" id="fairQ" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qQ" value="0" id="poorQ" class="form-control"></td>
                                                    <td class="cell"><input type="text" name="qQ" value="0" id="verypoorQ" class="form-control"> </td>
                                                    <td class="cell"><input type="text" name="qA" id="totalQ" value="0" class="form-control"></td>

                                                  </tr>
                                                  <tr>
                                                      <td class="text-center"><b>Likes</b></td>
                                                      <td class ="text-center" colspan="6"><b>Dislike</b></td>
                                                 </tr>
                                                 <tr>
                                                     <td>
                                                          <ul id="Likes">
                                                             
                                                            
                                                          </ul>
                                                      </td>
                                                      <td colspan="6" >
                                                          <ul id="Dislike">
                                                               

                                                       
                                                          </ul>
                                                      </td>
                                                </tr>
                                                 <tr>
                                                      <td class="text-center"colspan="7"><b>Comments</b></td>
                                                  </tr>
                                                  <tr>
                                                      <td colspan="7">
                                                            <ul id="Comments">
                                                                
                                                            </ul>
                                                      </td>
                                                  </tr>
                                                </table>                 
                                         </div>                 
                                 </div>
                                </div>
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
        </main>

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/evaluate.js') }}"></script>
@include('js.evaluation')
<script src="{{ asset('/js/lecturer-evaluation.js') }}"></script>

<script type="text/javascript">

    function setbid(branch){

         var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
         $.ajax({
                   url: 'https://cbrc.solutions/api/auth/login',
                    method: 'POST',
                    data: auth
                }).done(function(response){
                     var apiToString = JSON.stringify(response);
                     var apiTokenParse = JSON.parse(apiToString);
                     var apiToken = apiTokenParse.access_token;
                        $('#secret').empty();
                        $('#secret').append('<input type="hidden" id="apitoken" value="'+apiToken+'"/>');
                    $.get('https://cbrc.solutions/api/main/get-bid/'+branch+'?token='+apiToken, function(bid){
                            
                            $('#secret').append('<input type="hidden" id="bid" value="'+bid+'"/>');
                            

                          })
                    });
                  }

                  //end set bid

setbid('novaliches');

$('#year').on('change',function(d){

    const branch_id = $('#bid').val();
    const y = d.target.value;
    const s = $('#season').val();
    const token = $('#apitoken').val();

        var lec = {
            "Season":s,
            "Year":y,
            "branch":"novaliches",
            "Program":"lets"
        }
     $.ajax({
        url:'https://cbrc.solutions/api/main/lecturer-monitoring?token='+token,
         method: 'POST',
        data: lec,
    }).done(function(response){
        $.each(response, function(index, tempData){
            $('#FilterLecturer').append('<option value="'+tempData.lec_id+'">'+tempData.Lastname+' '+tempData.Firstname+'</option>');
        });

    });
});

$('#FilterLecturer').on('change',function(d){

    const branch_id = $('#bid').val();
    const lec_id = d.target.value;
    const season = $('#season').val();
    const token = $('#apitoken').val();
    const year = $('#year').val();

    $.get('https://cbrc.solutions/api/main/get-lecsubject/'+branch_id+'/'+season+'/'+year+'/'+lec_id+'?token='+token, function(data){
        $.each(data,function(index, subj){
            console.log(subj);
            $('#Filtersubj').append('<option value="'+subj.subject_id+'">'+subj.Subject+'</option>')
        });
    });

});


$('#evallec').on('click',function(){



    const season = $('#season').val();
    const col = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q"];
    const year = $('#year').val();
    const lecid = $('#FilterLecturer').val();
    const subject = $('#Filtersubj').val();
    const bid = $('#bid').val();
    const token = $('#apitoken').val();
    const branch = $('#branch').val();
    const lecturer = $('#FilterLecturer option:selected').text();

    $('.branch_info').text(branch);
    $('.year_info').text(season);
    $('.season_info').text(year);
    $('.subject_info').text(year);
    $('.lecturer_info').text(lecturer);





for (let i = 0; i < col.length; i++) {
    // console.log(col[i]);
    $.get('https://cbrc.solutions/api/main/eval-count/'+lecid+'/q'+col[i]+'/'+season+'/'+year+'/'+subject+'/'+bid+'?token='+token, function(data){
        var total = 0;
        $.each(data, function(index,objeval){

            $('#'+objeval.id).val(objeval.Count);
            total += Number(objeval.Count);
    
            $('#total'+col[i].toUpperCase()).val(total);

        });
    });
}

const lecdata = {
    "Season":season,
    "Year":year,
    "bid":bid,
    "lecturer":lecid,
    "subject":subject
}
    $.ajax({
        url:'https://cbrc.solutions/api/main/eval-records?token='+token,
        method:'POST',
        data:lecdata,
        }).done(function(response){

            $.each(response, function(index, eval){
                console.log(eval);
                if(eval.LIKE != null){
                    $('#Likes').append(' <li>'+eval.LIKE+'</li>');
                        }
                        if (eval.DISLIKE != null)  {
                            $('#Dislike').append('<li>'+eval.DISLIKE+'</li>');
                        }
                         if (eval.COMMENT != null)  {
                            $('#Comments').append('<li>'+eval.COMMENT+'</li>');
                        }
                    
            });
        });
});

</script>


@stop