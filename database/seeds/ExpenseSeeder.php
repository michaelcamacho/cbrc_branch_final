<?php

use Illuminate\Database\Seeder;

class ExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expenses_category')->insert([
        	'category' => 'Salary',
            'sub_category' => '',
        ]);
       	DB::table('expenses_category')->insert([
        	'category' => 'Tax',
            'sub_category' => '',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Insurance',
            'sub_category' => '',
        ]);
        DB::table('expenses_category')->insert([
            'category' => 'Loans',
            'sub_category' => '',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Allowance',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Gasoline',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Gifts',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Meals',
        ]);
        
        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Professional Fee',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Transportation',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Marketing',
            'sub_category' => 'Posters | Signage | Flyers',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Rent',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Electricity',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Water',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Internet',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Venue',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Sound System',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Utilities',
        	'sub_category' => 'Telephone',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Transportation',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Load',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Office Supply',
        ]);
        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Gasoline',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Meals',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Waybill',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Maintenance',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Daily Expense',
            'sub_category' => 'Allowance',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Daily Expense',
            'sub_category' => 'Grocery',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Daily Expense',
        	'sub_category' => 'Others',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Professional Fee',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Allowance',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Reimbursement',
        ]);

        DB::table('expenses_category')->insert([
        	'category' => 'Lecturer',
        	'sub_category' => 'Tax',
        ]);
        DB::table('expenses_category')->insert([
            'category' => 'Lecturer',
            'sub_category' => 'Transportation',
        ]);
        DB::table('expenses_category')->insert([
            'category' => 'Lecturer',
            'sub_category' => 'Accommodation',
        ]);
        DB::table('expenses_category')->insert([
            'category' => 'Contribution',
            'sub_category' => 'SSS',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Contribution',
            'sub_category' => 'Pag-ibig',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Contribution',
            'sub_category' => 'PhilHealth',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Investment',
            'sub_category' => 'Franchise Fee',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Investment',
            'sub_category' => 'Office Equipment',
        ]);

        DB::table('expenses_category')->insert([
            'category' => 'Investment',
            'sub_category' => 'Furniture & Fixture',
        ]);

        DB::table('petty_cashes')->insert([
            'petty_cash' => '0',
            
        ]);
        
    }
}
