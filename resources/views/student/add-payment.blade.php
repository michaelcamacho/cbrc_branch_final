@extends('main')
@section('title')
Student Summary
@stop
@section('css')
<style type="text/css">
.form-control:disabled, .form-control[readonly] {
color: #000;
}
@media print {
.content-header, .left-side, .main-header, .main-sidebar, .no-print {
display: none!important;
}
.table {
border-collapse: collapse !important;
}
td{
color: #000;
}

}


</style>

<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
<!-- Page Title Area -->
<div class="container-fluid">
<div class="row page-title clearfix">
    
    <div class="page-title-left">
    <h6 class="page-title-heading mr-0 mr-r-5">Add Payment</h6>
    </div>





</div>
<!-- /.page-title -->
</div>
<!-- /.container-fluid -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="container-fluid">
<div class="widget-list">
<div class="row">
<div class="col-md-12 widget-holder">
<div class="widget-bg p-5">





</div>
<!-- /.widget-bg -->
</div>
<!-- /.widget-holder -->
</div>
<!-- /.row -->
</div>
<!-- /.widget-list -->
</div>
<!-- /.container-fluid -->
</main>

@stop
@section('js')
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
$('table.display').DataTable();
});

$(document).ready(function() {
$('.record').addClass('active');
$('.record').addClass('collapse in');
});


</script>
@stop