<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Branch;
use App\Model\DataModel\BudgetRecord;

use Alert;
use Auth;
class BudgetController extends Controller
{
    public function proceed_insert_budget(Request $request){
        $input = $request->except(['_token']);

        $branch_id = Branch::where('branch_name', '=' , $input['branch'] )->first();
        
        


        BudgetRecord::insert([
            "branch_id" => $branch_id->id,
            "date" => $request['date'],
            "category" => $input['category'],
            "season" => $input['season'],
            "year" => $input['year'],
            "amount" => $input['amount'],
            "remarks" => $input['remarks'],
            "author" => Auth::user()->id
        ]);
       
        Alert::success('Success!', 'Budget saved.');
        return redirect('/new_budget/'.$input['branch']);

    }
}
