@extends('main')
@section('title')
Score Card
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side,  .main-header, .main-sidebar, .no-print {
    display: none!important;
}
} 

#trTitle{
        visibility: hidden;
        position:absolute;
}
#score-card tbody{
    height:200px;
}
.darkblue{
    color:#497EC3;
}
.blue{
    color:#1D549D;
}
.white{
    background: white;
}
.DTFC_LeftBodyLiner {
  overflow: hidden;
  border:none;
}

/* table settings ends here*/
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <!-- Page Title Area -->
    <div class="container-fluid">
        <div class="row page-title clearfix">
            <div class="page-title-left">
                <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} branch Score Card Season 1 </h6>
                
            </div>
            <!-- /.page-title-left -->
            <div class="page-title-right d-none d-sm-inline-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item">Records</li>
                    <li class="breadcrumb-item active">Score Card</li>
                </ol>
            </div>

            <!-- /.page-title-right -->
        </div>
        <!-- /.page-title -->
    </div>
    <!-- /.container-fluid -->
    <!-- =================================== -->
    <!-- Different data widgets ============ -->
    <!-- =================================== -->
    <div class="container-fluid">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                            
                        <body>
                                    
                                            <table id="score-card"data-scroll-y="300" class="DTFC_LeftBodyLiner table table-hover   text-center display nowrap margin-top-10 w-p100" style="width:100%">
                                                    
                                                    <thead>
                                                        <tr id="trTitle">
                                                            <th colspan="35">CBRC {{ $branch }} SCORE CARD SEASON 1</th>
                                                        </tr>
                                                        <tr id="trTitle">
                                                                <th colspan="35">printed date{{ date('M-d-Y') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2"></th>
                                                            <th colspan="13">TEACHER</th>
                                                            
                                                            <th colspan="3">NURSING</th>
                                                            <th colspan="3">CRIMINOLOGY</th>
                                                            <th colspan="3">CIVIL SERVICE</th>
                                                            <th colspan="3">PSYCOMETRICIAN</th>
                                                            <th colspan="3">NCLEX</th>
                                                            <th colspan="3">IELTS</th>
                                                            <th colspan="3">SOCIAL WORK</th>
                                                            <th colspan="3">AGRICULTURE</th>
                                                            <th colspan="3">MIDWIFERY</th>
                                                            <th colspan="3">ONLINE</th>
                                                        </tr>
                                                    
                                                        <tr>
                                                            <th >DATE</th>
                                                            <th >DAY</th>
                                                            <th>BEED</th>
                                                            <th>BSED</th>
                                                            <th>TOTAL LET</th>
                                                            <th>MATH</th>
                                                            <th>TLE</th>
                                                            <th>ENGLISH</th>
                                                            <th>FILIPINO</th>
                                                            <th>BIO SCI</th>
                                                            <th>PHY SCI</th>
                                                            <th>SOC SCI</th>
                                                            <th>MAPEH</th>
                                                            <th>VALUES</th>
                                                            <th>AFA</th>
                                                            <th>UFO</th>

                                                            <th>TOTAL NLE</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL CRIM</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>

                                                            <th>TOTAL CS</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>

                                                            <th>TOTAL PSYC</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL NCLEX</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL IELTS</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL SOCIAL</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL AGRI</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL MID</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                            
                                                            <th>TOTAL ONLINE</th>
                                                            <th>1ST TIMER</th>
                                                            <th>RETAKER</th>
                                                        </tr>
                                                        
                                                        
                                                        <tr class="blue">
                                                            <th>OVERALL</th>
                                                            <th>-</th>
                                                            <th>{{ $tot_beed  }}</th>
                                                            <th>{{ $fin_tot_bsed }}</th>
                                                            <th>{{ $fin_tot_let }}</th>
                                                            <th>{{ $tot_math }}</th>
                                                            <th>{{ $tot_tle }}</th>
                                                            <th>{{ $tot_english }}</th>
                                                            <th>{{ $tot_filipino }}</th>
                                                            <th>{{ $tot_biosci }}</th>
                                                            <th>{{ $tot_physci }}</th>
                                                            <th>{{ $tot_socsci }}</th>
                                                            <th>{{ $tot_mapeh }}</th>
                                                            <th>{{ $tot_values }}</th>
                                                            <th>{{ $tot_afa }}</th>
                                                            <th>{{ $tot_ufo }}</th>

                                                            <th>{{$tot_nle}}</th>
                                                            <th>{{$tot_nle_1stTimers}}</th>
                                                            <th>{{$tot_nle_retakers}}</th>

                                                            <th>{{$tot_crim}}</th>
                                                            <th>{{$tot_crim_1stTimers}}</th>
                                                            <th>{{$tot_crim_retakers}}</th>

                                                            <th>{{$tot_civil}}</th>
                                                            <th>{{$tot_civil_1stTimers}}</th>
                                                            <th>{{$tot_civil_retakers}}</th>

                                                            <th>{{$tot_psyc}}</th>
                                                            <th>{{$tot_psyc_1stTimers}}</th>
                                                            <th>{{$tot_psyc_retakers}}</th>

                                                            <th>{{$tot_nclex}}</th>
                                                            <th>{{$tot_nclex_1stTimers}}</th>
                                                            <th>{{$tot_nclex_retakers}}</th>

                                                            <th>{{$tot_ielts}}</th>
                                                            <th>{{$tot_ielts_1stTimers}}</th>
                                                            <th>{{$tot_ielts_retakers}}</th>

                                                            <th>{{$tot_social}}</th>
                                                            <th>{{$tot_social_1stTimers}}</th>
                                                            <th>{{$tot_social_retakers}}</th>

                                                            <th>{{$tot_agri}}</th>
                                                            <th>{{$tot_agri_1stTimers}}</th>
                                                            <th>{{$tot_agri_retakers}}</th>

                                                            <th>{{$tot_mid}}</th>
                                                            <th>{{$tot_mid_1stTimers}}</th>
                                                            <th>{{$tot_mid_retakers}}</th>

                                                            <th>{{$tot_online}}</th>
                                                            <th>{{$tot_online_1stTimers}}</th>
                                                            <th>{{$tot_online_retakers}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                {{-- @foreach($scorecard as $scdata)
                                                    <tr>
                                                        <td class="white ">{{ date("m-d-Y" , strtotime($scdata['date'])) }}</td>
                                                        <td class="white ">{{ date("D" , strtotime($scdata['date'])) }}</td>                             
                                                        <td class="darkblue"><b>{{ $scdata['beed'] }}</b></td>
                                                        <td class="darkblue"><b>{{
                                                            $tot_bsed = $scdata['math'] +
                                                            $scdata['tle'] + $scdata['english'] +
                                                            $scdata['filipino'] + $scdata['biosci'] +
                                                            $scdata['physci'] + $scdata['mapeh'] +
                                                            $scdata['socsci'] + $scdata['values'] +
                                                            $scdata['afa'] + $scdata['ufo'] 
                                                        }}</b></td>
                                                        <td><b>{{ $tot_let = $tot_bsed +  $scdata['beed'] }}</b></td>
                                                        <td>{{ $scdata['math'] }}</td>
                                                        <td>{{ $scdata['tle'] }}</td>
                                                        <td>{{ $scdata['english'] }}</td>
                                                        <td>{{ $scdata['filipino'] }}</td>
                                                        <td>{{ $scdata['biosci'] }}</td>
                                                        <td>{{ $scdata['physci'] }}</td>
                                                        <td>{{ $scdata['socsci'] }}</td>
                                                        <td>{{ $scdata['mapeh'] }}</td>
                                                        <td>{{ $scdata['values'] }}</td>
                                                        <td>{{ $scdata['afa'] }}</td>
                                                        <td>{{ $scdata['ufo'] }}</td>

                                                        <td class="darkblue"><b>{{ $tot_nle = $scdata['nles_1stTimers'] +  $scdata['nles_retakers']  }}</b></td>
                                                        <td>{{ $scdata['nles_1stTimers'] }}</td>
                                                        <td>{{ $scdata['nles_retakers'] }}</td>

                                                        
                                                        <td class="darkblue"><b>{{ $tot_crim = $scdata['crims_1stTimers'] +  $scdata['crims_retakers'] }}</b></td>
                                                        <td>{{ $scdata['crims_1stTimers'] }}</td>
                                                        <td>{{ $scdata['crims_retakers'] }}</td>

                                                        
                                                        <td class="darkblue"><b>{{ $tot_civil = $scdata['civils_1stTimers'] +  $scdata['civils_retakers'] }}</b></td>
                                                        <td>{{ $scdata['civils_1stTimers'] }}</td>
                                                        <td>{{ $scdata['civils_retakers'] }}</td>
                                                        
                                                        <td class="darkblue"><b>{{ $tot_psyc = $scdata['psycs_1stTimers'] +  $scdata['psycs_retakers'] }}</b></td>
                                                        <td>{{ $scdata['psycs_1stTimers'] }}</td>
                                                        <td>{{ $scdata['psycs_retakers'] }}</td>
                                                        
                                                        <td class="darkblue"><b>{{ $tot_nclex = $scdata['nclexes_1stTimers'] +  $scdata['nclexes_retakers'] }}</b></td>
                                                        <td>{{ $scdata['nclexes_1stTimers'] }}</td>
                                                        <td>{{ $scdata['nclexes_retakers'] }}</td>
                                                        
                                                        <td class="darkblue"><b>{{ $tot_ielts = $scdata['ielts_1stTimers'] +  $scdata['ielts_retakers'] }}</b></td>
                                                        <td>{{ $scdata['ielts_1stTimers'] }}</td>
                                                        <td>{{ $scdata['ielts_retakers'] }}</td>
                                                        
                                                        <td class="darkblue"><b>{{ $tot_social = $scdata['socials_1stTimers'] +  $scdata['socials_retakers'] }}</b></td>
                                                        <td>{{ $scdata['socials_1stTimers'] }}</td>
                                                        <td>{{ $scdata['socials_retakers'] }}</td>                     
                                                        
                                                        <td class="darkblue"><b>{{ $tot_agri = $scdata['agris_1stTimers'] +  $scdata['agris_retakers'] }}</b></td>
                                                        <td>{{ $scdata['agris_1stTimers'] }}</td>
                                                        <td>{{ $scdata['agris_retakers'] }}</td>
                                                    
                                                        <td class="darkblue"><b>{{ $tot_mid = $scdata['mids_1stTimers'] +  $scdata['mids_retakers'] }}</b></td>
                                                        <td>{{ $scdata['mids_1stTimers'] }}</td>
                                                        <td>{{ $scdata['mids_retakers'] }}</td>
                                                    
                                                        <td class="darkblue"><b>{{ $tot_online = $scdata['onlines_1stTimers'] +  $scdata['onlines_retakers'] }}</b></td>
                                                        <td>{{ $scdata['onlines_1stTimers'] }}</td>
                                                        <td>{{ $scdata['onlines_retakers'] }}</td>
                                                    </tr>
                                                    
                                                
                                                        
                                                    <div id="trTitle">{{ $weekly_beed = $weekly_beed + $scdata['beed'] }}
                                                    {{  $weekly_bsed = $weekly_bsed + $tot_bsed }}
                                                    {{  $weekly_let = $weekly_beed + $weekly_bsed }}
                                                    {{  $weekly_math = $weekly_math + $scdata['math']  }}
                                                    {{  $weekly_tle = $weekly_tle + $scdata['tle']  }}
                                                    {{  $weekly_english = $weekly_english + $scdata['english']  }}
                                                    {{  $weekly_filipino = $weekly_filipino + $scdata['filipino']  }}
                                                    {{  $weekly_biosci = $weekly_biosci + $scdata['biosci']  }}
                                                    {{  $weekly_physci = $weekly_physci + $scdata['physci']  }}
                                                    {{  $weekly_socsci = $weekly_socsci + $scdata['socsci'] }}
                                                    {{  $weekly_mapeh = $weekly_mapeh + $scdata['mapeh']  }}
                                                    {{  $weekly_values = $weekly_values + $scdata['values']  }}
                                                    {{  $weekly_afa = $weekly_afa + $scdata['afa']  }}
                                                    {{  $weekly_ufo = $weekly_ufo + $scdata['ufo']  }}

                                                    {{  $weekly_tot_nle_1stTimers = $weekly_tot_nle_1stTimers + $scdata['nle_1stTimers']  }}
                                                    {{  $weekly_tot_nle_retakers = $weekly_tot_nle_retakers + $scdata['nle_retakers']  }}
                                                    {{  $weekly_tot_nle = $weekly_tot_nle_1stTimers + $weekly_tot_nle_retakers  }}
                                                                                                                
                                                    {{  $weekly_tot_crim_1stTimers = $weekly_tot_crim_1stTimers + $scdata['crim_1stTimers']  }}
                                                    {{  $weekly_tot_crim_retakers = $weekly_tot_crim_retakers + $scdata['crim_retakers']  }}
                                                    {{  $weekly_tot_crim = $weekly_tot_crim_1stTimers + $weekly_tot_crim_retakers  }}<
                                                    
                                                    {{  $weekly_tot_civil_1stTimers = $weekly_tot_civil_1stTimers + $scdata['civil_1stTimers']  }}
                                                    {{  $weekly_tot_civil_retakers = $weekly_tot_civil_retakers + $scdata['civil_retakers']  }}
                                                    {{  $weekly_tot_civil = $weekly_tot_civil_1stTimers + $weekly_tot_civil_retakers  }}
                                                    
                                                    {{  $weekly_tot_psyc_1stTimers = $weekly_tot_psyc_1stTimers + $scdata['psyc_1stTimers']  }}
                                                    {{  $weekly_tot_psyc_retakers = $weekly_tot_psyc_retakers + $scdata['psyc_retakers']  }}
                                                    {{  $weekly_tot_psyc = $weekly_tot_psyc_1stTimers + $weekly_tot_psyc_retakers  }}
                                                    
                                                    {{  $weekly_tot_nclex_1stTimers = $weekly_tot_nclex_1stTimers + $scdata['nclex_1stTimers']  }}
                                                    {{  $weekly_tot_nclex_retakers = $weekly_tot_nclex_retakers + $scdata['nclex_retakers']  }}
                                                    {{  $weekly_tot_nclex = $weekly_tot_nclex_1stTimers + $weekly_tot_nclex_retakers  }}
                                                    
                                                    {{  $weekly_tot_ielts_1stTimers = $weekly_tot_ielts_1stTimers + $scdata['ielts_1stTimers']  }}
                                                    {{  $weekly_tot_ielts_retakers = $weekly_tot_ielts_retakers + $scdata['ielts_retakers']  }}
                                                    {{  $weekly_tot_ielts = $weekly_tot_ielts_1stTimers + $weekly_tot_ielts_retakers  }}
                                                    
                                                    {{  $weekly_tot_social_1stTimers = $weekly_tot_social_1stTimers + $scdata['social_1stTimers']  }}
                                                    {{  $weekly_tot_social_retakers = $weekly_tot_social_retakers + $scdata['social_retakers']  }}
                                                    {{  $weekly_tot_social = $weekly_tot_social_1stTimers + $weekly_tot_social_retakers  }}

                                                    {{  $weekly_tot_agri_1stTimers = $weekly_tot_agri_1stTimers + $scdata['agri_1stTimers']  }}
                                                    {{ $weekly_tot_agri_retakers = $weekly_tot_agri_retakers + $scdata['agri_retakers']  }}
                                                    {{ $weekly_tot_agri = $weekly_tot_agri_1stTimers + $weekly_tot_agri_retakers  }}

                                                    {{  $weekly_tot_mid_1stTimers = $weekly_tot_mid_1stTimers + $scdata['mid_1stTimers']  }}
                                                    {{ $weekly_tot_mid_retakers = $weekly_tot_mid_retakers + $scdata['mid_retakers']  }}
                                                    {{ $weekly_tot_mid = $weekly_tot_mid_1stTimers + $weekly_tot_mid_retakers  }}

                                                    {{  $weekly_tot_online_1stTimers = $weekly_tot_online_1stTimers + $scdata['online_1stTimers']  }}
                                                    {{ $weekly_tot_online_retakers = $weekly_tot_online_retakers + $scdata['online_retakers']  }}
                                                    {{ $weekly_tot_online = $weekly_tot_online_1stTimers + $weekly_tot_online_retakers  }}
                                                    
                                                </div>
                                              
                                                    @if( date("D" , strtotime($scdata['date'])) == $Sun or $scorecard_last['id'] == $scdata['id'] )      

                                                    <tr class="darkblue">
                                                        
                                                        <td class="white "><b>WEEKLY</b></td>
                                                        <td class="white "><b>-</b></td>
                                                        <td><b>{{ $weekly_beed }}</b></td>
                                                        <td><b>{{ $weekly_bsed }}</b></td>
                                                        <td><b>{{ $weekly_let }}</b></td>
                                                        <td><b>{{ $weekly_math }}</b></td>
                                                        <td><b>{{ $weekly_tle }}</b></td>
                                                        <td><b>{{ $weekly_english }}</b></td>
                                                        <td><b>{{ $weekly_filipino }}</b></td>
                                                        <td><b>{{ $weekly_biosci }}</b></td>
                                                        <td><b>{{ $weekly_physci }}</b></td>
                                                        <td><b>{{ $weekly_socsci }}</b></td>
                                                        <td><b>{{ $weekly_mapeh }}</b></td>
                                                        <td><b>{{ $weekly_values }}</b></td>
                                                        <td><b>{{ $weekly_afa }}</b></td>
                                                        <td><b>{{ $weekly_ufo }}</b></td>

                                                        <td><b>{{ $weekly_tot_nle }}</td>
                                                        <td><b>{{ $weekly_tot_nle_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_nle_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_crim }}</td>
                                                        <td><b>{{ $weekly_tot_crim_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_crim_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_civil }}</td>
                                                        <td><b>{{ $weekly_tot_civil_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_civil_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_psyc }}</b></td>
                                                        <td><b>{{ $weekly_tot_psyc_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_psyc_retakers }}</b></td>


                                                        <td><b>{{ $weekly_tot_nclex }}</b></td>
                                                        <td><b>{{ $weekly_tot_nclex_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_nclex_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_ielts }}</b></td>
                                                        <td><b>{{ $weekly_tot_ielts_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_ielts_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_social }}</b></td>
                                                        <td><b>{{ $weekly_tot_social_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_social_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_agri }}</b></td>
                                                        <td><b>{{ $weekly_tot_agri_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_agri_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_mid }}</b></td>
                                                        <td><b>{{ $weekly_tot_mid_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_mid_retakers }}</b></td>

                                                        <td><b>{{ $weekly_tot_online }}</b></td>
                                                        <td><b>{{ $weekly_tot_online_1stTimers }}</b></td>
                                                        <td><b>{{ $weekly_tot_online_retakers }}</b></td>
                                                    </tr>


                                                    <div id="trTitle" >
                                                            {{ $weekly_beed = 0 }}
                                                            {{  $weekly_bsed = 0 }}
                                                            {{  $weekly_let = 0 }}
                                                            {{  $weekly_math = 0  }}
                                                            {{  $weekly_tle = 0  }}
                                                            {{  $weekly_english = 0 }}
                                                            {{  $weekly_filipino = 0 }}
                                                            {{  $weekly_biosci = 0 }}
                                                            {{  $weekly_physci = 0 }}
                                                            {{  $weekly_socsci = 0 }}
                                                            {{  $weekly_mapeh = 0 }}
                                                            {{  $weekly_values = 0 }}
                                                            {{  $weekly_afa = 0 }}
                                                            {{  $weekly_ufo = 0 }}

                                                            {{  $weekly_tot_nle_1stTimers = 0 }}
                                                            {{  $weekly_tot_nle_retakers = 0 }}
                                                            {{  $weekly_tot_nle = 0 }}
                                                                                                                        

                                                            {{  $weekly_tot_crim_1stTimers = 0  }}
                                                            {{  $weekly_tot_crim_retakers = 0 }}
                                                            {{  $weekly_tot_crim = 0  }}
                                                            

                                                            {{  $weekly_tot_civil_1stTimers = 0}}
                                                            {{  $weekly_tot_civil_retakers = 0 }}
                                                            {{  $weekly_tot_civil = 0 }}
                                                            
                                                            {{  $weekly_tot_psyc_1stTimers = 0}}
                                                            {{  $weekly_tot_psyc_retakers = 0 }}
                                                            {{  $weekly_tot_psyc = 0 }}
                                                            
                                                            {{  $weekly_tot_nclex_1stTimers = 0}}
                                                            {{  $weekly_tot_nclex_retakers = 0 }}
                                                            {{  $weekly_tot_nclex = 0 }}

                                                            {{  $weekly_tot_ielts_1stTimers = 0}}
                                                            {{  $weekly_tot_ielts_retakers = 0 }}
                                                            {{  $weekly_tot_ielts = 0 }}

                                                            {{  $weekly_tot_social_1stTimers = 0}}
                                                            {{  $weekly_tot_social_retakers = 0 }}
                                                            {{  $weekly_tot_social = 0 }}

                                                            {{  $weekly_tot_agri_1stTimers =0 }}
                                                            {{ $weekly_tot_agri_retakers = 0  }}
                                                            {{ $weekly_tot_agri = 0  }}

                                                            {{  $weekly_tot_mid_1stTimers =0 }}
                                                            {{ $weekly_tot_mid_retakers = 0  }}
                                                            {{ $weekly_tot_mid = 0  }}

                                                            {{  $weekly_tot_online_1stTimers =0 }}
                                                            {{ $weekly_tot_online_retakers = 0  }}
                                                            {{ $weekly_tot_online = 0  }}
                                                            
                                                        </div>
                                                    @endif
                                                @endforeach --}}
                                                        
                                    </tbody>
                                        
                                
                                </table>
                        </body>
                        
                        
                    </div>
                    <!-- /.widget-bg -->
                </div>
                <!-- /.widget-holder -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.widget-list -->
    </div>
        <!-- /.container-fluid -->
    </main>

@stop
@section('js')



<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/datatable/buttons.print.min.js') }}"></script>
<script src="{{ asset('/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ asset('/datatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
          
        

        $(document).ready(function() {
             $('.record').addClass('active');
             $('.record').addClass('collapse in');
        });

        $(document).ready(function() {
            
    
            var table2 = $('#score-card').DataTable( {
                "scrollX":true,
                scrollCollapse: true,
                fixedColumns:{
                    leftColumns: 2,
                    },
                "paging": false,
                "order": false,
                dom: 'Bfrtip',
                buttons: [
                'excel',
                ]
         });
        
                
        });
   
             
        

</script>

@stop