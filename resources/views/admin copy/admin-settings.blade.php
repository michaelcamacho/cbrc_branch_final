@extends('main')
@section('title')
Admin Settings
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5"></h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Data Settings</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">SAVE {{strtoupper($param)}} SETTINGS</h5>
                                   <br/>
                                    @if (Session::has('message'))
                                       <div class="alert alert-success" role="alert">
                                           {{Session::get('message')}}
                                       </div
                                    @endif
                                    <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" action="/save-adsettings">
                                        @csrf
                                        
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" name="param" id="param" value="{{strtoupper($param)}}" readonly>
                                                    <label>Dashboard</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="branch" id="branch" required>
                                                        <option value="" disable="true" selected="true">--- Select branch ---</option>
                                                               @if(isset($branch))
                                                                @foreach($branch as $branches)
                                                                <option value="{{$branches->branch_name}}">{{ucwords($branches->aka)}}</option>
                                                                @endforeach
                                                                @endif
                                                      
                                                    </select>
                                                    <label>Branch</label>
                                                </div>
                                            </div>
                                            </div>
                                       

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group" id="eto">
                                                     <select class="form-control" name="season" id="season">
                                                                    <option value="" disable="true" selected="true">--- Select Season ---</option>
                                                                    <option value="Season 1">Season 1</option>
                                                                    <option value="Season 2">Season 2</option>
                                                                </select>
                                                    <label>Season</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   <select class="form-control" name="year" id="year" required>
                                                                    <option value="" disable="true" selected="true">--- Select Season ---</option>
                                                                    <option value="2018">2018</option>
                                                                    <option value="2019">2019</option>
                                                                    <option value="2018">2020</option>
                                                                    <option value="2019">2021</option>
                                                                    <option value="2018">2022</option>
                                                                </select>
                                                    <label>Year</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                        </div>
                                        

                                       
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" id="button-prevent">Save Settings</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
         const param = $('#param').val();
        
        if(param == 'ADMIN'){
            $('#branch').val('Main');
            $('#branch').attr('readonly', true);
            //\ $('#eto').hide();
             
            
        }else{
            
            $("#branch option[value='Main']").remove();
        }
        
 
    });
</script>
@stop