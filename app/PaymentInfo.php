<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    protected $connection = 'satelite_branch_db_connection';
    protected $table = 'student_payment_transactions';
    protected $fillable = [

        'id',
        'payment_no',
        'card_id',
        'amount_paid',
        'balance_before',
        'balance_after',
        'transaction_date',
        'created_by',
        'deleted',
        'remarks',
    ];
}
