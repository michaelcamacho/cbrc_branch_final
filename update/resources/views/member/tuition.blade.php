@extends('main')
@section('title')
Tuition & Discount Records
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Tuition Records</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Tuition Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Tuition Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Tuition Fee</th>
                            <th>Facilitation Fee</th>
                            <th>Season</th>
                            <th>Year</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($tuition))
                            @foreach($tuition as $tuitions)
                        <tr>
                            <td>{{$tuitions->program}}</td>
                            <td>{{$tuitions->category}}</td>
                            <td>{{$tuitions->tuition_fee}}</td>
                            <td>{{$tuitions->facilitation_fee}}</td>
                            <td>{{$tuitions->season}}</td>
                            <td>{{$tuitions->year}}</td>
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Discount Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="exam" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Discount Category</th>
                            <th>Amount</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($discount))
                            @foreach($discount as $discounts)
                        <tr>
                            <td>{{$discounts->program}}</td>
                            <td>{{$discounts->category}}</td>
                            <td>{{$discounts->discount_category}}</td>
                            <td>{{$discounts->discount_amount}}</td>
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
});

</script>
@stop