<?php

namespace App\Http\Controllers\Discount;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use Alert;
use App\Category;
use App\Branch;
use App\Major;
use App\Program;
use App\Model\DataModel\Discount;

class DiscountController extends Controller
{
   


    public function add_discount(Request $request){
        $input = $request->except(['_token']);

        $exist = Discount::where('program_id' , '=' , $input['program'])
        ->where('branch_id' , '=' , $input['branch'])
        ->where('discount_category' , '=' , $input['discount_category'])
        ->get();

        if(count($exist) < 1){

            Discount::insert([

                'branch_id' => $input['branch'], 
                'program_id' => $input['program'], 
                'category_id' => $input['category'], 
                'discount_category' => $input['discount_category'], 
                'discount_amount' => $input['discount_amount'], 
                
            ]);
    
        Alert::success('Success!', 'New Discount Successfully added.');
        return redirect('/admin/discounts');
        }
        
        Alert::error('Error!', 'Discount Already Exist.');
        return redirect('/admin/discounts');
    }


     


    public function update_discount(Request $request){
        $input = $request->except(['_token']);

        Discount::where('id','=',$input['item_id'])->update([

            'program_id' => $input['program'], 
            'category_id' => $input['category'], 
            'discount_category' => $input['discount_category'], 
            'discount_amount' => $input['discount_amount'], 
            
        ]);

        Alert::success('Success!', 'Discount Fee Updated.');
        
        return redirect('/admin/discounts');
    }
    
         
    
}
