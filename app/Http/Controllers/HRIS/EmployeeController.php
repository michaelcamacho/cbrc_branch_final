<?php

namespace App\Http\Controllers\HRIS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\DataModel\Employee;

use Auth;

class EmployeeController extends Controller
{
    
    public function hris($branch){
        return view('hris.hris_record')
        ->with('branch' , $branch);
    }

    public function add_employee(){
        $branch = Auth::user()->branch;
        return view('hris.add_employee')
        ->with('branch' , $branch);
    }
    public function proceed_add_employee(Request $request){
        $branch = Auth::user()->branch;
        $input = $request->except(['_token']);
return $input;

        return view('hris.add_employee')
        ->with('branch' , $branch);
    }
}
