<?php

namespace App\Model\DataModel;

use Illuminate\Database\Eloquent\Model;

class ExpenseSubcategory extends Model
{
    protected $connection = 'satelite_branch_db_connection';
   
    protected $guard = [];


    public function ExpenseCat(){

        return $this->belongsTo(ExpenseCategory::class,'id');
    }

    public function expenses_transaction(){

        return $this->hasMany(Expense::class,'subcategory_id');
    }


    
}
