@extends('main')
@section('title')
Budget Records
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
  <div class="container-fluid">
      <div class="row page-title clearfix">
          <div class="page-title-left">
              <h6 class="page-title-heading mr-0 mr-r-5">Budget Records</h6>        
          </div>
          <div class="page-title-right d-none d-sm-inline-flex">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a>
                  </li>
                  <li class="breadcrumb-item">Records</li>
                  <li class="breadcrumb-item active">Budget Records</li>
              </ol>
          </div>
      </div>
    </div>
    <div class="container-fluid">
        <div class="widget-list">
            <div class="row">
                <div class="col-md-12 widget-holder">
                    <div class="widget-bg">
                        <div class="widget-body clearfix">
                            <h5 class="box-title mr-b-0">Budget Records</h5>
                           <br/>            
                        </div>
                        <div class="widget-body clearfix">
                      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                        <thead>        
                            <tr>
                                <th>Trans #</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Category</th>
                                <th>Remarks</th>
                            </tr>                     
                        </thead>
                        <tbody>
                             @if(isset($budget))
                                @foreach($budget as $budgets)
                            <tr>
                                <td>00{{$budgets->id}}</td>
                                <td>{{$budgets->date}}</td>
                                <td>{{$budgets->amount}}</td>
                                <td>{{$budgets->category}}</td>
                                <td>{{$budgets->remarks}}</td>
                            </tr>
                                  @endforeach
                               @endif       
                        </tbody>                           
                      </table>
                    @role('Admin')
                    <br/>
                    <a class="btn btn-danger clear" id="clear">Clear table</a>
                    @endrole
                  </div>      
              </div>
            </div>
          </div>
        </div>
    </div>
</main>

@stop
@section('js')
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
});
$('#clear').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');

          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('program', 'lets');
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-budget'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
        });
</script>
@stop