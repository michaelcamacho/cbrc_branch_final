@extends('main')
@section('title')
 Lecturer Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Space+Mono&display=swap" rel="stylesheet">
<style type="text/css">
  .tableRecords th{
    font-weight: normal; 
    font-family: 'Space Mono', monospace;
    font-weight: bold;
  }
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
          <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                          <div class="card" >
                              <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                                LECTURER REPORTS
                              </div>
                                <div class="card-body">
                                   <div class="form-row">
                                      <div class="form-group col-md-3">
                                         <select id="cs" name ="cs"  class="form-control seasonSearch" required>
                                                 <option value="0" selected="true" disabled = "true"> Select Season</option>           
                                                 <option value="Season 1">Season 1</option>  
                                                 <option value="Season 2">Season 2</option>
                                              </select> 
                                        </div>
                                      <div class="form-group col-md-3">
                                         <select id="cs" name ="cs" class="form-control yearSearch" required>
                                             <option value="0" selected="true" disabled = "true"> Select Year</option>           
                                              @foreach($years as $years)
                                                <option>
                                                   {{$years}}
                                                </option>  
                                             @endforeach
                                          </select>  
                                      </div>
                                       <div class="form-group col-md-3">
                                         <select id="cs" name ="cs" class="form-control programSearch" required>
                                             <option value="0" selected="true" disabled = "true"> Select Programs</option>           
                                              @foreach($programs as $programs)
                                                <option value="{{$programs->aka}}">
                                                   {{$programs->program_name}}
                                                </option>  
                                             @endforeach
                                          </select>  
                                          <input type="hidden" value="{{ $branch }}" name="branch" id="branch">
                                      </div>
                                       <div class="form-group col-md-3">
                                        <button type="button" class="btn btn-primary btn-block" id="btn-filter">Find</button>
                                      </div>
                                   </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>


         <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                          <div class="card" >
                              <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                               VIEW LECTURER REPORTS
                              </div>
                                <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped tableRecords" id="myTable">
                                              <thead>
                                                <tr>
                                                  <th scope="col" class="text-center">DATE LECTURER</th>
                                                  <th scope="col" class="text-center">LECTURER</th>
                                                  <th scope="col" class="text-center">SUBJECT</th>
                                                  <th scope="col" class="text-center">TIME START</th>
                                                  <th scope="col" class="text-center">START AM BREAK</th>
                                                  <th scope="col" class="text-center">END AM BREAK</th>
                                                  <th scope="col" class="text-center">START LUNCH</th>
                                                  <th scope="col" class="text-center">END LUNCH</th>
                                                  <th scope="col" class="text-center">START PM BREAK</th>
                                                  <th scope="col" class="text-center">END PM BREAK</th>
                                                  <th scope="col" class="text-center">END CLASS</th>
                                                </tr> 
                                              </thead>
                                              <tbody id="stdlist">
                                          
                                              </tbody>
                                          </table>                  
                                        </div>
                                   </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>

      </div>  
  </main>

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

   $('#btn-filter').on('click',function(){
    var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
   $.ajax({
   url: 'https://cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;
     

     var season = $('.seasonSearch').val();
     var year = $('.yearSearch').val();
     var programs = $('.programSearch').val();
     var branch = $('#branch').val();

     console.log(programs);

  var formData = {
        "Season":season,      
        "Year": year,        
        "Program": programs,
        "major":  year,          
        "branch":  branch, 
      };

      $.ajax({
        url: "https://cbrc.solutions/api/main/lecturer-monitoring?token="+apiToken,
        method: 'POST',
        data: formData,
            }).done(function(response){ 
            $('#stdlist').empty(); 
        $.each(response, function(index, Lecturer) {
          $('#stdlist').append('<tr><td class="text-center">'+Lecturer.Lecture_Date+'</td><td class="text-center">'+Lecturer.Lastname+
          '</td><td class="text-center">'+Lecturer.Subject+'</td><td class="text-center">'+Lecturer.TimeIn+'</td><td class="text-center">'+Lecturer.Am_BreakIn+
          '</td><td class="text-center">'+Lecturer.Am_BreakOut+'</td><td class="text-center">'+Lecturer.LunchIn+
          '</td><td class="text-center">'+Lecturer.LunchOut+'</td><td class="text-center">'+Lecturer.Pm_BreakIn+'</td><td class="text-center">'+Lecturer.Pm_BreakOut+
          '</td><td class="text-center">'+Lecturer.TimeOut); 
      
        });
      });
   });
});

</script>
@stop 