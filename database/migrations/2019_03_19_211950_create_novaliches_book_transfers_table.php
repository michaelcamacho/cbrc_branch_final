<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovalichesBookTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_transfers', function (Blueprint $table) {
            $table->increments('id');

            
            $table->string('book_transId');
            $table->string('book_program');
            $table->string('book_major');

            $table->string('book_author');
            $table->string('book_title');
            $table->string('book_quantity');
            $table->string('book_stockInitial');
            $table->string('book_stockFinal');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_transfers');
    }
}
