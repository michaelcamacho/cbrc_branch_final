@extends('main')
@section('title')
NEW RESERVATION
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
<link href="{{ asset('css/select.css') }}" rel="stylesheet" />
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                            <h6 class="box-title mr-b-0">New Reservation</h6>
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">New Reservation</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5> <span class="pull-right student_status"></span></h5>

                                   <br/>
                                   <br/>
                                   <br/>
                                   <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="{{'/proceed/transact/reserve'}}">
                                    @csrf
                                        {{-- student info --}}
                                        <input type="hidden" name="branch" id="branch_name" value="{{$branch}}">
                                        <input type="hidden" id="id" value=""> 
                                        {{-- student info --}}
                                       
                                       
                                         <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="program" id="program" required>
                                                        <option value="0" disable="true" selected="true" onchange="" class="selector">--- Select Program ---</option>
                                                        @if(isset($program))
                                                        @foreach($program as $programs)
                                                        <option value="{{$programs->id}}">{{$programs->program_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" value="{{ $date }}" name="date" id="date" readonly>
                                                        <label>Date</label>
    
                                                    </div>
                                                </div>
                                            
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group input-has-value">
                                                   <select class="form-control js-example-basic-single" name="student_id" id="student" required>
                                                        <option value="" disable="true"  selected="true">--- Select Student ---</option>
                                                       
                                                    </select>
                                                    <label>Student Name</label>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                               
                                            </div>
                                        </div>

                                           

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group input-has-value">
                                             
                                                   <select class="form-control" name="season" id="season">
                                                       <option value=""> Select  a season</option>
                                                       <option value="1"> Season 1 </option>
                                                       <option value="2"> Season 2 </option>
                                                   </select>
                                                    <label>Season</label>
                                                </div>
                                            </div>
                                            
                                        
                                            <div class="col-lg-3">
                                                <div class="form-group input-has-value">
                                                    <select class="form-control" name="year" id="year">
                                                        <option value=""> select a year </option>
                                                        <option value="2020"> 2020 </option>
                                                        <option value="2021"> 2021 </option>
                                                        <option value="2022"> 2022 </option>
                                                        <option value="2023"> 2023 </option>
                                                        <option value="2024"> 2024 </option>
                                                        <option value="2025"> 2025 </option>
                                                    </select>
                                                    <label>Year</label>
                                                </div>
                                            </div>
                                            
                                        </div>
                                         
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control decimal" id="amount_paid" name="amount_paid" type="text" maxlength="12" required>
                                                    <label>Amount Paid</label>
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary proceed" type="submit" id="button-prevent">Add New Reservation</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')




<script src="{{ asset('/js/prevent.js') }}"></script>
<script src="{{ asset('/js/moment.js') }}"></script>
{{--<script src="{{ asset('/js/book-fetch.js') }}"></script>--}}
<script type="text/javascript">
    $(document).ready(function() {
  $('.new-reservation').addClass('active');
});


    $('#program').on('change', function(e){
            
            var program = e.target.value;
            var branch = $('#branch_name').val();
                console.log(program);

            $.get('/fetch-student/' + program + '/' + branch,function(data){
                console.log(data);

                 $('#student').empty();
                 $('#student').append('<option value="" disable="true" selected="true">--- Select Student ---</option>');

                $('#amount').val('');

                  $.each(data, function(index, studentObj){
                    $('#student').append('<option value="'+ studentObj.id +'">'+ studentObj.last_name +', '+ studentObj.first_name+' '+ studentObj.middle_name+'</option>');
                });
                 

            });
        });



        $('#year').on('change', function(e){
            
            var student_id = $('#student').val();
            var season = $('#season').val();
            var year = $('#year').val();
            var result = 'no result';
            $.get('/fetch_student_if_enrolled_or_reserved/' + student_id +'_'+ season +'_'+ year,function(data){
                if(data.message == "exist"){
        $.each(data,function(i,item){
                if(item.status){
                    $('.proceed').attr('disabled' , 'disabled');
                    $('.student_status').text('Status: Sudent is ' + item.status_name);
                }
        });
                }else{
                    $('.proceed').removeAttr('disabled');
                    $('.student_status').text('');
                }

                console.log(data)
             
            });
        });

</script>
<script>
 

</script>

@stop