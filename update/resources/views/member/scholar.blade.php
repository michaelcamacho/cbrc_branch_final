@extends('main')
@section('title')
Scholar Records
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Scholars Records</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Scholars Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Scholars Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                    <table id="example-en" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC ID</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>School</th>
                            <th>Contact #</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($let))
                            @foreach($let as $lets)
                        <tr>
                            <td>{{$lets->last_name}}, {{$lets->first_name}} {{$lets->middle_name}}</td>
                            <td>{{$lets->cbrc_id}}</td>
                            <td>{{$lets->course}}</td>
                            <td>{{$lets->category}}</td>
                            <td>{{$lets->school}}</td>
                            <td>{{$lets->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($nle))
                            @foreach($nle as $nles)
                        <tr>
                            <td>{{$nles->last_name}}, {{$nles->first_name}} {{$nles->middle_name}}</td>
                            <td>{{$nles->cbrc_id}}</td>
                            <td>{{$nles->course}}</td>
                            <td>{{$nles->category}}</td>
                            <td>{{$nles->school}}</td>
                            <td>{{$nles->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($crim))
                            @foreach($crim as $crims)
                        <tr>
                            <td>{{$crims->last_name}}, {{$rims->first_name}} {{$rims->middle_name}}</td>
                            <td>{{$crims->cbrc_id}}</td>
                            <td>{{$crims->course}}</td>
                            <td>{{$crims->category}}</td>
                            <td>{{$crims->school}}</td>
                            <td>{{$crims->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($civil))
                            @foreach($civil as $civils)
                        <tr>
                            <td>{{$civils->last_name}}, {{$vils->first_name}} {{$vils->middle_name}}</td>
                            <td>{{$civils->cbrc_id}}</td>
                            <td>{{$civils->course}}</td>
                            <td>{{$civils->category}}</td>
                            <td>{{$civils->school}}</td>
                            <td>{{$civils->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($psyc))
                            @foreach($psyc as $psycs)
                        <tr>
                            <td>{{$psycs->last_name}}, {{$sycs->first_name}} {{$sycs->middle_name}}</td>
                            <td>{{$psycs->cbrc_id}}</td>
                            <td>{{$psycs->course}}</td>
                            <td>{{$psyc->category}}</td>
                            <td>{{$psycs->school}}</td>
                            <td>{{$psycs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($nclex))
                            @foreach($nclex as $nclexs)
                        <tr>
                            <td>{{$nclexs->last_name}}, {{$lexs->first_name}} {{$lexs->middle_name}}</td>
                            <td>{{$nclexs->cbrc_id}}</td>
                            <td>{{$nclexs->course}}</td>
                            <td>{{$nclexs->category}}</td>
                            <td>{{$nclexs->school}}</td>
                            <td>{{$nclexs->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($ielt))
                            @foreach($ielt as $ielts)
                        <tr>
                            <td>{{$ielts->last_name}}, {{$elts->first_name}} {{$elts->middle_name}}</td>
                            <td>{{$ielts->cbrc_id}}</td>
                            <td>{{$ielts->course}}</td>
                            <td>{{$ielts->category}}</td>
                            <td>{{$ielts->school}}</td>
                            <td>{{$ielts->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($social))
                            @foreach($social as $socials)
                        <tr>
                            <td>{{$socials->last_name}}, {{$ials->first_name}} {{$ials->middle_name}}</td>
                            <td>{{$socials->cbrc_id}}</td>
                            <td>{{$socials->course}}</td>
                            <td>{{$socials->category}}</td>
                            <td>{{$socials->school}}</td>
                            <td>{{$socials->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                        @if(isset($agri))
                            @foreach($agri as $agris)
                        <tr>
                            <td>{{$agris->last_name}}, {{$gris->first_name}} {{$gris->middle_name}}</td>
                            <td>{{$agris->cbrc_id}}</td>
                            <td>{{$agris->course}}</td>
                            <td>{{$agris->category}}</td>
                            <td>{{$agris->school}}</td>
                            <td>{{$agris->contact_no}}</td>
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
});

</script>
@stop