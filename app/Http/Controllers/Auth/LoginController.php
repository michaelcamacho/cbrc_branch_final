<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectAfterLogout = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

public function authenticated()
{
        $uid = auth()->user()->id;
    
        if(auth()->user()->hasRole('Admin'))
    {
           return redirect('/dashboard')->with('log',$uid);
    } 
    
    if(auth()->user()->hasRole('Member'))
    {
        return redirect('/dashboard/'.strtolower(auth()->user()->branch))->with('log',$uid);
    }

    
    // else{
       
    //     return auth()->user()->id;
    // } 
   
    
}

public function username(){
    $loginType = request()->input('username');

    $this->username = filter_var($loginType, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

    request()->merge([$this->username => $loginType]);

    return property_exists($this, 'username') ? $this->username : 'email';
}


}
