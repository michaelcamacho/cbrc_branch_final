@extends('main')
@section('title')
expenses
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;    
}


/* Styles go here */

.tree, .tree ul {
    margin:0;
    padding:0;
    list-style:none
}
.tree ul {
    margin-left:1em;
    position:relative
}
.tree ul ul {
    margin-left:.5em
}
.tree ul:before {
    content:"";
    display:block;
    width:0;
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    border-left:1px solid
}
.tree li {
    margin:0;
    padding:0 1em;
    line-height:2em;
    color:#369;
    font-weight:700;
    position:relative
}
.tree ul li:before {
    content:"";
    display:block;
    width:10px;
    height:0;
    border-top:1px solid;
    margin-top:-1px;
    position:absolute;
    top:1em;
    left:0
}
.tree ul li:last-child:before {
    background:#fff;
    height:auto;
    top:1em;
    bottom:0
}
.indicator {
    margin-right:5px;
}
.tree li a {
    text-decoration: none;
    color:#369;
}
.tree li button, .tree li button:active, .tree li button:focus {
    text-decoration: none;
    color:#369;
    border:none;
    background:transparent;
    margin:0px 0px 0px 0px;
    padding:0px 0px 0px 0px;
    outline: 0;
}
</style>
{{-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/> --}}
@stop
@section('main-content')

<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} expenses</h6>
                    
                </div>
            </div>
        </div>
        <!-- /.page-title -->
        
      <a class="btn btn-info" href="/finance/expenses">back</a>

        <!-- page content strarts here -->
        <div class="container-fluid">
                <div class="widget-list">
                  <br>
                    <div class="row" >
                        
                        {{-- expense category --}}
                        <div class="col-md-6 widget-holder">
                            <div class="widget-bg">
                            <h6 class="text-center">Expense Category</h6>                                
                    
                            <ul id="tree3">
                                @foreach( $expense_category as $category)
                                <li>
                                    <i class="material-icons">add</i>
                                        <a href="#">
                                        {{$category->category}}</a>
                                    <ul>    
                                        @foreach ($category->expense_subcategory as $sub)
                                            <li>
                                            
                                                {{$sub->sub_category}}
                                            
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>    
                                @endforeach
                            </ul>
                          
                            
                            
                            </div>
                        </div>
                        {{-- expense category --}}
                        {{-- expense sub category --}}
                        <div class="col-md-6 widget-holder">
                        <div class="widget-bg">

                                <ul class="nav nav-tabs">
                                        <li><a href="#a" class="btn btn-info" data-toggle="tab">add new category</a></li>
                                        <li><a href="#b" class="btn btn-info" data-toggle="tab">add new sub category</a></li>
                                        <li><a href="#c" class="btn btn-info" data-toggle="tab">edit categories</a></li>
                                </ul>
                                     
                                     <div class="tab-content">
                                        
                                        <div class="tab-pane active" id="a">
                                            <h6>add new category</h6>
                                            <form method="POST" action="/proceed/add_expense_category"  data-toggle="validator" >
                                              @csrf
                                            <input type="hidden" name="branch" value="{{$branch}}">
                                                
                                                <label for="category">
                                                    category name
                                                </label>
                                                
                                                <div class="row">
                                                        <div class="col-md-8">
                                                            <input class="form-control" name="category" type="text">
                                                        </div>
                                                  
                                                        <div class="col-md-4">
                                                            <input name="submit" class="btn btn-info  form-control" type="submit">
                                                        </div>
                                                </div>
                                            
                                            </form>
                                        </div>

                                        <div class="tab-pane" id="b">
                                                <h6>add sub category</h6>
                                                <form method="POST" action="/proceed/add_expense_sub_category"  data-toggle="validator" >
                                                  @csrf
                                                  <input type="hidden" name="branch" value="{{$branch}}">
                                             
                                                  <label for="category">
                                                          
                                                        </label>
                                                        
                                                        <div class="row">
                                                                <div class="col-md-8">
                                                                    <select class="form-control" name="category" id="category">
                                                                        <option value="">--select a category--</option>
                                                                        @foreach( $expense_category as $category)
                                                                        <option value="{{$category->id}}">{{$category->category}}</option>
                                                                        
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-md-8">
                                                                    <input class="form-control" name="sub_category" type="text">
                                                                </div>
                                                          
                                                                <div class="col-md-4">
                                                                    <input name="submit" class="btn btn-info  form-control" type="submit">
                                                                </div>
                                                        </div>
                                            </form>
                                        </div>

                                        <div class="tab-pane" id="c">
                                            <h6>edit category</h6>
                                            <form method="POST" action="/proceed/edit_expense_category"  data-toggle="validator" >
                                              @csrf
                                              <input type="hidden" name="branch" value="{{$branch}}">
                                         
                                              <label for="category">
                                                      
                                                    </label>
                                                    
                                                    <div class="row">
                                                            <div class="col-md-8">
                                                                <select class="form-control" name="category" id="category">
                                                                    <option value="">--select a category--</option>
                                                                    @foreach( $expense_category as $category)
                                                                    <option value="{{$category->id}}">{{$category->category}}</option>
                                                                    
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="row">
                                                            <div class="col-md-8">
                                                                <input class="form-control" name="sub_category" type="text">
                                                            </div>
                                                      
                                                            <div class="col-md-4">
                                                                <input name="change " class="btn btn-info  form-control" type="submit">
                                                            </div>
                                                    </div>
                                        </form>
                                        </div>
                                        
                                    </div>
                                
                            </div>
                        </div>
                        {{-- expense sub category --}}


                    </div><!-- page row ends here --> 
                </div><!-- page content-fluid ends here --> 
        </div>
        <!-- page content ends here -->        
    </main>
    <!-- end of main -->
@stop

@section('js')

<script type="text/javascript">

$(document).ready(function() {
  $('.reports').addClass('active');
  $('.reports').addClass('collapse in');
});


</script>



<script>
    $.fn.extend({
      treed: function(o) {

        var openedClass = 'glyphicon-minus-sign';
        var closedClass = 'glyphicon-plus-sign';

        if (typeof o != 'undefined') {
          if (typeof o.openedClass != 'undefined') {
            openedClass = o.openedClass;
          }
          if (typeof o.closedClass != 'undefined') {
            closedClass = o.closedClass;
          }
        };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function() {
          var branch = $(this); //li with children ul
          branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
          branch.addClass('branch');
          branch.on('click', function(e) {
            if (this == e.target) {
              var icon = $(this).children('i:first');
              icon.toggleClass(openedClass + " " + closedClass);
              $(this).children().children().toggle();
            }
          })
          branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
        tree.find('.branch .indicator').each(function() {
          $(this).on('click', function() {
            $(this).closest('li').click();
          });
        });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function() {
          $(this).on('click', function(e) {
            $(this).closest('li').click();
            e.preventDefault();
          });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function() {
          $(this).on('click', function(e) {
            $(this).closest('li').click();
            e.preventDefault();
          });
        });
      }
    });

     //Initialization of treeviews

    $('#tree1').treed();

    $('#tree2').treed({
      openedClass: 'glyphicon-folder-open',
      closedClass: 'glyphicon-folder-close'
    });

    $('#tree3').treed({
      openedClass: 'glyphicon-chevron-right',
      closedClass: 'glyphicon-chevron-down'
    });
  </script>
@stop