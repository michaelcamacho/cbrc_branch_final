$('.print').click(function() {
    var rcp = $( "#receipt" ).val() + 1;
    var date = $( "#date" ).val();
    var name = $( "#student" ).val();
    var student = name.split("*");
    var student_name = student[0];
    var program = $( "#program" ).val();
    var cat = $( "#category" ).val();
    var branch = $( "#branch" ).val();

    var tuitions = parseFloat($( "#tuition" ).val());
    var facilitation = parseFloat($( "#facilitation" ).val());

    var tuition = tuitions + facilitation;

    var balance = parseFloat($("#balance").val());
    var total_amount = parseFloat($( "#total_amount" ).val());
    var amount_paid = parseFloat($( "#amount_paid" ).val());

    var discount = $( "#discount" ).val();
    if(discount != 0){
    var disc = discount.split(",");
    var dis = disc[1] +'-----'+disc[0];
    }
    if(discount == 0){
    
    var dis = 0;
    }

        
        if (isNaN(balance) == false){

            if(total_amount > amount_paid){
              balance = total_amount - amount_paid; 
            }

            if(total_amount <= amount_paid){
                balance = balance - amount_paid;

                if(balance < 0){
                    balance = 0;
                }
            }
        }


        if (isNaN(balance) == true){

            if(total_amount >= amount_paid){
                balance = total_amount - amount_paid; 
            }

            if(total_amount <= amount_paid){

                balance = 0;
            }
        }

        if (isNaN(tuition) == true){

            tuition = 0;
        }

        if (program == 'lets' ){
        program = 'LET';
        }
        if (program == 'nles' ){
            program = 'NLE';
        }
        if (program == 'crims' ){
            program = 'Criminology';
        }
        if (program == 'civils' ){
            program = 'Civil Service';
        }
         if (program == 'psycs' ){
            program = 'Psychometrician';
        }
        if (program == 'nclexs' ){
            program = 'NCLEX';
        }
        if (program == 'ielts' ){
            program = 'IELTS';
        }
        if (program == 'socials' ){
            program = 'Social Work';
        }
        if (program == 'agris' ){
            program = 'Agriculture';
        }
        if (program == 'mids' ){
            program = 'Midwifery';
        }

        if (program == 'onlines' ){
            program = 'Online Only';
        }

        if(cat == 0){
            cat = "";
        }
        if(isNaN(total_amount) == false && isNaN(amount_paid) == false && name != null){
            var winPrint = window.open('', '', 'left=0,top=0,width=1000,height=800,toolbar=0,scrollbars=0,status=0');
        
        winPrint.document.write('<!DOCTYPE html><head><title>Payment Receipt</title>');
        winPrint.document.write('<style type="text/css">');
        winPrint.document.write('#print-table{border-collapse: collapse;}');
        winPrint.document.write('#print-table, tr, td {border: 1px solid black;}#td1{padding:12px 100px 12px 20px;font-size: 13px;font-weight: 600;}#td2{padding:10px 20px 12px 360px;font-size: 13px;font-weight: 600;}');
        winPrint.document.write('#logo{width:30%;}#title{font-size:12px;margin: 5px 0 5px 0;}#detail{margin: 10px 0px 10px 0px;}');
        winPrint.document.write('h2{color: #000;}.print-table{border: 1 #000;}#rcp{position:absolute; right:0; display:none;}#rcv{position:relative;}</style></head>');
        winPrint.document.write('<body><center><img src="../img/cbrc-logo2.png" id="logo"></center><h2 style="margin-top:5px;margin-bottom:5px;">Student Copy</h2><h4 id="rcp">receipt #: '+ rcp +'</h4><h4>Date: '+ date +'<br/>Name: '+ student_name +'<br/>Branch: '+ branch +'<br/>Progam: '+ program +'<br/>Category: '+ cat +'</h4>');
        winPrint.document.write('<h4>Tuition Fee: '+ tuition + '</h4>');
        winPrint.document.write('<table id="print-table">');
        winPrint.document.write('<tr><td id="td1">Current Balance: <br/> Discount: <br/> Amount Paid: <br/> Balance Due: <br/></td>');
        winPrint.document.write('<td id="td2" style="text-align:right;">' +total_amount + '</br>' + dis + '<br/>' + amount_paid +'<br/>' + balance + '<br/></td></tr></table>');
        winPrint.document.write('<h4 id="rcv">_____________________<br/>Received by:</h4>');
        winPrint.document.write('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');
        winPrint.document.write('<center><p id="title">Payment Receipt</p></center>');
        winPrint.document.write('<center><img src="../img/cbrc-logo2.png" id="logo"></center>');
        winPrint.document.write('<h2 style="margin-top:5px;margin-bottom:5px;">Branch Copy</h2>');
        winPrint.document.write('<h4 id="rcp">receipt #: '+ rcp +'</h4><h4>Date: '+ date +'<br/>Name: '+ student_name +'<br/>Branch: '+ branch +'<br/>Progam: '+ program +'<br/>Category: '+ cat +'</h4>');
        winPrint.document.write('<h4>Tuition Fee: '+ tuition + '</h4>');
        winPrint.document.write('<table id="print-table">');
        winPrint.document.write('<tr><td id="td1">Current Balance: <br/> Discount: <br/> Amount Paid: <br/> Balance Due: <br/></td>');
        winPrint.document.write('<td style="text-align:right;" id="td2">' +total_amount + '</br>' + dis + '<br/>' + amount_paid +'<br/>' + balance + '<br/></td></tr></table>');
        winPrint.document.write('<h4 id="rcv">_____________________<br/>Received by:</h4>');
        winPrint.document.close();
        
        setTimeout(function() {
        winPrint.focus();
        winPrint.print();
        winPrint.close();
        }, 250);

    }
        });


        
    

