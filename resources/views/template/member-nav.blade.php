  <aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
            <!-- User Details -->
            <div class="side-user">
                <figure class="side-user-bg" style="background-image: url(assets/demo/user-image-cropped.jpg)">
                    <img src="assets/demo/user-image-cropped.jpg" alt="" class="d-none">
                </figure>
                <div class="col-sm-12 text-center p-0 clearfix">
                    <div class="d-inline-block pos-relative mr-b-10">
                        <span class="avatar-text">{{substr(Auth::user()->name,0,1)}}</span>
                        <figure class="avatar-img thumb-sm mr-b-0 d-none">
                            <img src="assets/demo/users/user1.jpg" class="rounded-circle" alt="">
                        </figure>
                    </div>
                    <!-- /.d-inline-block -->
                    <div class="lh-14 mr-t-5 sidebar-collapse-hidden">
                        <h6 class="hide-menu side-user-heading">{{ Auth::user()->name }}</h6><small class="hide-menu">{{ Auth::user()->email}}</small>
                    </div>
                </div>
                <!-- /.col-sm-12 -->
            </div>
            <!-- /.side-user -->
            <!-- Sidebar Menu -->
            <nav class="sidebar-nav">
                <ul class="nav in side-menu">

                    
                                    <li class="current-page dashboard"><a href="{{ url('/dashboard'.'/'.strtolower(Auth::user()->branch))}}"><i class="list-icon material-icons">home</i> <span class="hide-menu">Dashboard</span></a></li>
                                  
                                    <li class="current-page new_enrollee"><a href="{{ url('/new_enrollee'.'/'.strtolower(Auth::user()->branch))}}">  <i class="list-icon material-icons">add</i><span class="hide-menu">New Enrollee</span></a></li>
                                    <li class="current-page new_payment"><a href="{{ url('/new_payment'.'/'.strtolower(Auth::user()->branch))}}"> <i class="list-icon material-icons">add</i><span class="hide-menu">New Payment</span></a></li>
                                    <li class="current-page new_reservation"><a href="{{ url('/new_reservation'.'/'.strtolower(Auth::user()->branch))}}"> <i class="list-icon material-icons">add</i><span class="hide-menu">New Reservation</span></a></li>
                                    <li class="current-page new_expense"><a href="{{ url('/new_expense'.'/'.strtolower(Auth::user()->branch))}}"> <i class="list-icon material-icons">add</i><span class="hide-menu">New Expense</span></a></li>
                                    <li class="current-page new_budget"><a href="{{ url('/new_budget'.'/'.strtolower(Auth::user()->branch))}}"> <i class="list-icon material-icons">add</i><span class="hide-menu">New Budget</span></a></li>
                                    <li class="current-page new_book_payment"><a href="{{ url('/new_book_payment'.'/'.strtolower(Auth::user()->branch))}}"> <i class="list-icon material-icons">add</i><span class="hide-menu">New Book Payment</span></a></li>
                                    <li class="current-page new_cash_remit"><a href="{{ url('/new_cash_remit'.'/'.strtolower(Auth::user()->branch))}}"> <i class="list-icon material-icons">add</i><span class="hide-menu">New Cash Remit</span></a></li>


                                    <li class="menu-item-has-children reports"><a href="javascript:void(0);">
                                            <i class="list-icon material-icons">apps</i> <span class="hide-menu">Reports</span></a>  
                                            <ul class="list-unstyled sub-menu reports">
                                           
                            
                                            <li><a href="{{ url('/today_transactions/'.strtolower(Auth::user()->branch))}}">Transactions</a>
                                            </li>
                                            
                                                <li><a href="{{ url('/score_card/'.strtolower(Auth::user()->branch))}}">Score Card</a>
                                            </li>
                                                <li><a href="{{ url('/financial_report/'.strtolower(Auth::user()->branch))}}">Financial</a>
                                            </li>
                                                
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item-has-children ad-Hris">
            
                                            <a href="javascript:void(0);">
                                                    <i class="list-icon material-icons">people_outline</i> 
                                                    <span class="hide-menu">HRIS</span>
                                            </a>                    
                                            
                                            <ul class="list-unstyled sub-menu evaluate">
                                                
                                                <li><a href="{{ url('/hris/users_list')}}">System Users</a></li>
                                                <li><a href="{{ url('/hris/ra-useraccount')}}">RA User</a></li>
                                                <li><a href="{{ url('/hris/employee-record')}}">Employee Records</a></li>
                                            </ul>
                                        </li>
        


                                    <li class="menu-item-has-children records"><a href="javascript:void(0);"><i class="list-icon material-icons">folder</i> <span class="hide-menu">Records</span></a>                    
                                    <ul class="list-unstyled sub-menu records">
                                            <li class="current-page students"><a href="{{ url('/master_list/'. strtolower(Auth::user()->branch))}}"><i class="list-icon material-icons">group</i> <span class="hide-menu">Students</span></a></li>
                                            <li class="current-page books"><a href="{{ url('/admin/books')}}"><i class="list-icon material-icons">import_contacts</i> <span class="hide-menu">Books</span></a></li>
                                            <li class="current-page finance"><a href="{{ url('/admin/finance')}}"><i class="list-icon material-icons">local_atm</i> <span class="hide-menu">Finance</span></a></li>
                                            <li class="current-page tuition_fees"><a href="{{ url('/admin/tuition_fees')}}"><i class="list-icon material-icons">folder_open</i> <span class="hide-menu">Tuition Fees</span></a></li>
                                            <li class="current-page discounts"><a href="{{ url('/admin/discounts')}}"><i class="list-icon material-icons">folder</i> <span class="hide-menu">Discounts</span></a></li>
                                            <li class="current-page RA_dashboard"><a href="{{ url('/admin/ra_files')}}"><i class="list-icon material-icons">poll</i><span class="hide-menu">RA Files</span></a></li>
                                        </ul>
                                    </li>       

              
                                    <li class="menu-item-has-children others"><a href="javascript:void(0);"><i class="list-icon material-icons">folder</i> <span class="hide-menu">Others</span></a>                    
                                        <ul class="list-unstyled sub-menu others">
                                            <li><a href="{{ url('/passers_template')}}">Passer's Template</a></li>
                                            <li><a href="{{url('/bulletin')}}">Bulletin Board</a></li>
                                        </ul>
                                    </li>
                                
                              
                </ul>        
            </nav>
            <!-- /.sidebar-nav -->
        </aside>