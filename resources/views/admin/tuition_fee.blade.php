@extends('main')
@section('title')
 Tuition Fees
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Tuition Fees Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Tuition Fees Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
    
        <a href="#" class="btn btn-primary ripple mr-3 pull-right   " data-toggle="modal" data-target="#add-tuition">Add New Tuition Fee</a>
    
    

       
        @foreach($branch_table_count As $item)
        
        <div id="tuition-table">
        <br>
        <h6>{{$item->branch_name}} Tuition List</h6>
            
                <table  class="table table-bordered table-hover display nowrap margin-top-10 w-p100" id="" style="width: 100% !important">
                     <thead>               
                        <tr>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Tuition Fee</th>
                            <th>Facilitation Fee</th>
                            <th>Season</th>
                            <th>Year</th>
                            @if($item->id == Auth::user()->branch_id)
                            <th>Update</th>
                            @endif
                            {{-- <th>Delete</th> --}}
                        </tr>       
                    </thead>
                    <tbody>
                    @if(isset($tuition))
                        @foreach($tuition as $tuitions)
                            @if($tuitions->branch_id == $item->id)
                                <tr>
                                    <td>{{$tuitions->branch_name}}</td>
                                    <td>{{$tuitions->program_name}}</td>
                                    <td>{{$tuitions->category_name}}</td>
                                    <td>{{$tuitions->tuition_fee}}</td>
                                    <td>{{$tuitions->facilitation_fee}}</td>
                                    <td>{{$tuitions->season}}</td>
                                    <td>{{$tuitions->year}}</td>
                                    @if($item->id == Auth::user()->branch_id)
                                    <td><a href="#" class="btn btn-success edit" id="{{$tuitions->id}}" data-toggle="modal" data-target="#update-tuition">Update</a></td>
                                    @endif
                                    {{-- <td><button class="btn btn-danger delete" user-id="{{$item->id}},{{$item->branch_name}}">Delete</button></td> --}}
                                </tr>
                            @endif
                       @endforeach
                    @endif

                            
                    </tbody>           
                    
                </table>
            </div>
            @endforeach
        <div id="add-tuition" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="tuition-form" method="POST" action="/tuition/proceed/add_tuition">
                               @csrf
                                <div class="form-group">
                           


                            {{-- branch option --}}
                            <div class="form-group">   
                                    @if($main_branch != "")     
                                    <input id="update_branch" type="hidden" class="form-control" name="branch" value="{{ Auth::user()->branch_id}}">
                                    @else
                                    <label for="branch">Branch</label>        
                                    <select class="form-control" name="branch" id="update_branch">
                                    <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                    @if(isset($Branches))
                                    @foreach($Branches as $item)
                                        
                                                    <option value="{{$item->id}}">{{$item->branch_name}}</option>
                                                
                                            @endforeach
                                        @endif
                                    </select>
                                    @endif
                                </div>
                            {{-- branch option --}}


                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        
                                    <div class="col-md-7">
                                        <label for="program">Program</label>
                                        <select class="form-control" name="program" id="program">
                                            <option value="" disable="true" selected="true">--- Select Program ---</option>
                                            @if(isset($program))
                                            @foreach($program as $programs)
                                            <option value="{{$programs->id}}">{{$programs->program_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <label for="major">Major</label>
                                        <select class="form-control" name="major" id="major" >
                                            <option value="0" disable="true" selected="true">--- Select Major ---</option>
                                            <option value="1">BEED</option>
                                            <option value="2">BSED</option>
                                        </select>
                                    </div>
                               
                               
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" name="category" id="category">
                                            <option value="" disable="true" selected="true">--- Select Category ---</option>
                                            @if(isset($category))
                                            @foreach($category as $categories)
                                            <option value="{{$categories->id}}">{{$categories->category_name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                </div>
                                <div class="form-group">
                                    <label for="tuition">Tuition Fee</label>
                                    <input class="form-control decimal" type="text" min="0" name="tuition_fee" id="tuition_fee" required>
                                </div>
                                <div class="form-group">
                                    <label for="facilitation_fee">Facilitation Fee</label>
                                    <input class="form-control decimal" type="text" min="0" name="facilitation_fee" id="facilitation_fee" required>
                                </div>
                              
                                                        
                                <div class="text-center mr-b-30">
                                    <input type="submit" name="submit" class="btn btn-info" value="Save">
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

            <div id="update-tuition" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="tuition-form-update" method="POST" action="/tuition/proceed/update_tuition">
                                @csrf
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="item_id" id="update_id" readonly>
                                    
                                </div>
                             
                                <div class="form-group">
                                    <label for="tuition">Tuition Fee</label>
                                    <input class="form-control decimal" type="text" name="tuition_fee" id="update_tuition_fee" required>
                                </div>
                                <div class="form-group">
                                    <label for="facilitation_fee">Facilitation Fee</label>
                                    <input class="form-control decimal" type="text" name="facilitation_fee" id="update_facilitation_fee" required>
                                </div>
                             
                                                        
                                <div class="text-center mr-b-30">
                                    <input type="submit" name="submit" value="update" class="btn btn-info">
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>
        

@stop
@section('js')

    <script src="{{ asset('/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/tuition.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table.display').DataTable();
} );

      $('.decimal').keypress(function(evt){
            return (/^[0-9]*\~?[0-9]*$/).test($(this).val()+evt.key);
        });
</script>
@stop