@extends('main')
@section('title')
 Attendance Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Space+Mono&display=swap" rel="stylesheet">
<style type="text/css">
  .tableRecords th{
    font-weight: normal; 
    font-family: 'Space Mono', monospace;
    font-weight: bold;
  }

</style>
@stop
@section('main-content')

<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">


       <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                          <div class="card" >
                              <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                              ATTENDANCE REPORTS
                              </div>
                                <div class="card-body">
                                   <div class="form-row">
                                      <div class="form-group col-md-3">
                                         <select id="cs" name ="cs"  class="form-control seasonSearch" required>
                                                 <option value="0" selected="true" disabled = "true"> Select Season</option>           
                                                 <option value="Season 1">Season 1</option>  
                                                 <option value="Season 2">Season 2</option>
                                              </select> 
                                        </div>
                                      <div class="form-group col-md-3">
                                         <select id="cs" name ="cs" class="form-control yearSearch" required>
                                             <option value="0" selected="true" disabled = "true"> Select Year</option>           
                                             @foreach($years as $years)
                                                <option>
                                                {{$years}}
                                                </option>  
                                             @endforeach
                                          </select>  
                                      </div>
                                       <div class="form-group col-md-3">
                                         <select id="cs" name ="cs" class="form-control programSearch" required>
                                             <option value="0" selected="true" disabled = "true"> Select Programs</option>           
                                             @foreach($program as $program)
                                                <option value="{{$program->aka}}">
                                                  {{$program->program_name}}
                                                </option>  
                                             @endforeach
                                          </select>  
                                          <input type="hidden" value="{{ $sbranch }}" name="branch" id="branch">
                                      </div>
                                       <div class="form-group col-md-3">
                                        <button type="button" class="btn btn-primary btn-block" id="btn-filter">Find</button>
                                      </div>
                                   </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>

         <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                          <div class="card">
                              <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                               <p style="margin-bottom: 0px;">VIEW ATTENDANCE REPORTS</p>
                              </div>
                                <div class="card-body">

                                        <div class="table-responsive">
                                            <table class="table table-striped tableRecords" id="myTable">
                                              <thead>
                                                <tr>
                                                   <th class="text-center">ID Number</th>
                                                    <th class="text-center">Student Name</th>
                                                    <th class="text-center">School</th> 
                                                    <th class="text-center">Total Days</th>
                                                    <th class="text-center">Terminal</th>
                                                </tr> 
                                              </thead>
                                              <tbody id="stdlist">
                                          
                                              </tbody>
                                          </table>                  
                                        </div>
                                   </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>
            </main>

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

   $('#btn-filter').on('click',function(){
       var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
        $.ajax({
            url: 'https://cbrc.solutions/api/auth/login',
            method: 'POST',
            data: auth
            }).done(function(response){
             var apiToString = JSON.stringify(response);
             var apiTokenParse = JSON.parse(apiToString);
             var apiToken = apiTokenParse.access_token;
             console.log(apiToken);

            var season = $('.seasonSearch').val();
            var year = $('.yearSearch').val();
            var programs = $('.programSearch').val();
            var branch = $('#branch').val();
        
          console.log(season);
          console.log(year);  
          console.log(programs);
          console.log(branch);

               var formData = {
                "Season":season,      
                "Year": year,        
                "Program": programs,         
                "branch":  branch, 
              };

              $.ajax({
                  url:"https://cbrc.solutions/api/main/attendance-report?token="+apiToken,
                  method: 'POST',
                  data: formData,
                }).done(function(response){
                  $('#stdlist').empty();
                  $.each(response, function(index, attendance){
                  $('#stdlist').append('<tr><td class="text-center">'+attendance.cbrc_id+'</td><td class="text-center">'+attendance.Lastname+'</td><td class="text-center">'+attendance.School+'</td><td class="text-center">'+attendance.totaldays+'</td><td class="text-center">'+attendance.terminal+'</td></tr>');

                  });
              
              });
          });

   });

    
</script>
@stop