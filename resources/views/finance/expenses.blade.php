@extends('main')
@section('title')
expenses
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;    
}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')

<main class="main-wrapper clearfix">
        <!-- Page Title Area -->
        <div class="container-fluid">
            <div class="row page-title clearfix">
                <div class="page-title-left">
                    <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }} expenses</h6>
                    
                </div>
            </div>
        </div>

            <div class="widget-holder">    
                        
                
                    @include('finance.finance_nav')    
                
                
            <a class="btn btn-info pull-right " href="/finance/add_expense_category/{{ strtolower($branch) }}">add expense category</a>
            </div>
                
                <!-- /.page-title -->
        <div class="widget-holder">
            <div class="widget-bg">
                
                <br>
            <a class="btn btn-info pull-right " href="/new_expense/{{$branch}}">add expense</a>
            <br>
            <br>
            <table  class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Remarks</th>
                            <th>Season</th>
                            <th>Year</th>
                            <th>Author</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($expense as $item)
                            
                        <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->remarks }}</td>
                        <td>{{ $item->season }}</td>
                        <td>{{ $item->year }}</td>
                        <td>{{ $item->author }}</td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

       
                     
    </main>
    <!-- end of main -->
@stop

@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
  $('.reports').addClass('active');
  $('.reports').addClass('collapse in');
});


</script>



@stop