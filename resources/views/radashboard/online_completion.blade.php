@extends('main')
@section('title')
 Online Completion
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Space+Mono&display=swap" rel="stylesheet">
<style type="text/css">
  .tableRecords th{
    font-weight: normal; 
    font-family: 'Space Mono', monospace;
    font-weight: bold;
  }
</style>


@stop
@section('main-content')


<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
          <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                          <div class="card" >
                              <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                                ONLINE COMPLETION REPORTS
                              </div>
                                <div class="card-body">
                                   <div class="form-row">
                                      <div class="form-group col-md-2">
                                         <select id="cs" name ="cs"  class="form-control seasonSearch" required>
                                                 <option value="0" selected="true" disabled = "true"> Select Season</option>           
                                                 <option value="Season 1">Season 1</option>  
                                                 <option value="Season 2">Season 2</option>
                                              </select> 
                                        </div>
                                      <div class="form-group col-md-2">
                                         <select id="cs" name ="cs" class="form-control yearSearch" required>
                                             <option value="0" selected="true" disabled = "true"> Select Year</option>           
                                            @foreach($years as $year)
                                                <option>
                                                   {{$year}}
                                                </option>  
                                              @endforeach
                                          </select>  
                                      </div>
                                      <div class="form-group col-md-2">
                                         <select id="cs" name ="cs" class="form-control programSearch" required>
                                             <option value="0" selected="true" disabled = "true"> Select Programs</option>           
                                              @foreach($programs as $programs)
                                                <option value=" {{$programs->aka}}">
                                                   {{$programs->program_name}}
                                                </option>  
                                              @endforeach
                                          </select>  
                                      </div>
                                       <div class="form-group col-md-2">
                                         <select id="cs" name ="cs" class="form-control studentMajor" required>
                                           <option value="0" selected="true" disabled = "true"> Select Major</option>           
                                            <option value="BEED"> BEED </option>
                                            <option value="BEED"> BSED </option>  
                                        </select>  
                                      </div>
                                       <div class="form-group col-md-4">
                                        <button type="button" class="btn btn-primary btn-block" id="btn-filter">Find</button>
                                      </div>
                                      <input type="hidden" name="branch" id="branch" value="{{$branch}}">
                                   </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>


         <div class="container" style="margin-top: 10px;">
            <div class="row">
                    <div class="col-12">
                          <div class="card" >
                              <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                               VIEW ONLINE COMPLETION REPORTS
                              </div>
                                <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover tableRecords" id="myTable">
                                              <thead>
                                                <tr>
                                                 <th scope="col" class="center">CBRC_ID</th>
                                                  <th scope="col" class="center">STUDENT</th>
                                                  <th scope="col" class="center">MAJOR</th>
                                                  <th scope="col" class="center">GEN_ED</th>
                                                  <th scope="col" class="center">PRO_ED</th>
                                                  <th scope="col" class="center">STATUS</th>
                                                </tr> 
                                              </thead>
                                              <tbody id="stdlist">
                                          
                                              </tbody>
                                          </table>                  
                                        </div>
                                   </div>
                                </div>
                            </div>
                       </div>
                    </div>
                </div>

      </div>  
  </main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">


    $('#btn-filter').on('click',function(){
     var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
     $.ajax({
    url: 'https://beta.cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
    }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;
 
    var season = $('.seasonSearch').val();
    var year = $('.yearSearch').val();
    var program = $('.programSearch').val();
    var major = $('.studentMajor').val();
    var branch = $('#branch').val();

    

      var formData = {
        "Season" : season,
        "Year" : year,
        "Program" : program,
        "Major" : major,
        "branch" : branch,
      }
      $.ajax({
        url:"https://cbrc.solutions/api/main/online-completion?token="+apiToken,
        method:"POST",
        data: formData,
      }).done(function(response){
        $.each(response, function(index, Completion) {
          $('#stdlist').append('<tr><td class="text-center">'+Completion.cbrc_id+'</td><td class="text-center">'+Completion.Lastname+', '
          +Completion.Firstname+' '+Completion.Middlename+'</td><td class="text-center">'+Completion.Major+'</td><td class="text-center">'
          +Completion.GenEd+'</td><td class="text-center">'+Completion.ProfEd+'</td><td class="text-center">'+Completion.Status)
        });
      });

      //  $.get('https://cbrc.solutions/api/main/online-completion/'+season+'/'+year+'/'+major+'?token='+apiToken,function(data){  
      //  $('#stdlist').empty();
      //    $.each(data, function (index, studentCompletion) {
      //           $('#myTable').dataTable();
      //     });
      //  });
   
   });
});
</script>

@stop