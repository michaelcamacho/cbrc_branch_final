<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StudentEnrollmentCard;
use App\Model\DataModel\Expense;
use App\Model\DataModel\BudgetRecord;
use App\Model\AdminSettings;
use DB;
use App\Branch;
use Auth;
class DashboardController extends Controller
{
    private $year;
    private $Branches;
   
    public function __construct()
    {
            $this->middleware('auth');

            $settings = AdminSettings::findOrFail(1);
            $this->year = $settings->year;
        

    }

    public function admin_dashboard(){

        // dd($this->ifAdminValidate1());
       
        if($this->main_branch_id() == ""){
        $budget = BudgetRecord::where('year' , '=' , $this->year)->sum('amount');
        $total_expense = Expense::where('year' , '=' , $this->year)->sum('amount');
        $enrollee = StudentEnrollmentCard::join('cbrc_students as b', 'b.id','student_enrollment_card.student_id')
        ->where('student_enrollment_card.year' , '=', $this->year)
        ->count();
        // dd($this->enrolleeForAdmin());
                return view('dashboard/admin_dashboard')
                        ->with('main_branch',$this->main_branch_id())
                        ->with('Branches',$this->list_of_branches())
                        ->with('enrollee' , $enrollee)
                        ->with('budget' , $budget)
                        ->with('enrollee_per_branch',$this->enrolleeForAdmin())
                        ->with('expense_per_branch',$this->expenseForAdmin())
                        ->with('total_expense' , $total_expense)
                        ->with('data_dashboards',$this->ifAdminValidate())
                        ->with('other_data_dashboard',$this->ifAdminValidate1())
                        ->with('season1_other',$this->ifAdminValidate2())
                        ->with('season2_other',$this->ifAdminValidate3());
        }
                if($this->main_branch_id() != "")
                        {
                                $Branches = Branch::all();
                                $selfBranch = Branch::findOrFail(Auth::user()->branch_id)->aka;
                                return redirect('dashboard/'.$selfBranch);
                        }
        }

        // public function main_branch_id()
        // {
        // return Branch::findOrFail(Auth::user()->branch_id)->main_branchid;
        // }
//member
        public function branch_dashboard($branch){

        $bid = Branch::where('branch_name','=',$branch)->value('id');

        // dd($this->branchseason1($bid));
        $budget = BudgetRecord::where('year' , '=' , $this->year)->where('branch_id' , '=' , $bid)->sum('amount');
        $expense = Expense::where('year' , '=' , $this->year)->where('branch_id' , '=' , $bid)->sum('amount');
        $enrollee = StudentEnrollmentCard::join('cbrc_students as b', 'b.id','student_enrollment_card.student_id')
        ->where('student_enrollment_card.year' , '=', $this->year)
        ->where('branch' , '=' , $bid)
        ->groupby('student_enrollment_card.season')
        ->count();
        return view('dashboard.member-dashboard')
                ->with('main_branch',$this->main_branch_id())
                ->with('s1s',$this->branchseason1($bid))
                ->with('s2s',$this->branchseason2($bid))
                ->with('Branches' , $this->Branches)
                ->with('budget',$budget)
                ->with('expense',$expense)
                ->with('enrollee',$enrollee)
                ->with('data_dashboards',$this->receivableForMember($bid));
        

}
//member

        
        
//     public function member_dashboard(){
    
        // $Branches = Branch::all();
        // return view('dashboard/member_dashboard')
        // ->with('main_branch',$this->main_branch_id())
//                 ->with('Branches' , $Branches);
//         }

        public function mainbranch()
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select('sec.season',DB::raw('sum(sec.tuition_fee_price) as total_tuition_fee'),DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),DB::raw('sum(sec.facilitation_price) as total_facilitation'),DB::raw('count(sec.id) as total_enrollee'))
                ->where('sec.year', $this->year)
                // ->groupby('sec.season')
                ->first(); 
        }

        public function expenseForAdmin()
        {
                return  DB::connection('satelite_branch_db_connection')->table('expenses as exp')
                ->LeftJoin('branches as b', 'b.id','exp.branch_id')
                ->select(DB::raw('sum(exp.amount) as total_expense_per_branch'),'b.branch_name as branch_name')
                ->where('exp.year' ,'=', $this->year)
                ->groupBy('b.id')
                ->get();

        }
        public function enrolleeForAdmin()
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select(DB::raw('count(sec.id) as total_enrollee_per_branch'),'b.branch_name')
                ->where('sec.year', $this->year)
                ->groupBy('b.id')
                ->get();
        }
        public function receivableForAdmin()
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select(DB::raw('sum(sec.tuition_fee_price) as total_tuition_fee'),'b.branch_name as main_branch',DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),DB::raw('sum(sec.facilitation_price) as total_facilitation'),'cs.branch','sec.season','sec.year',DB::raw('count(sec.id) as total_enrollee'))
                ->where('sec.year', $this->year)
                ->groupBy('sec.season')
                ->get();
        }
        
        public function receivableForMember($bid)
        {
                
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
               ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
               ->RightJoin('branches as b', 'b.id','cs.branch')
               ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
               ->select('b.branch_name',DB::raw('(sum(sec.facilitation_price) + sum(sec.tuition_fee_price))  as total_sale'),DB::raw('sum(sec.tuition_fee_price) as total_tuition'),DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),'cs.branch','sec.year')
               ->where('b.id', $bid)
               ->where('sec.year', $this->year)
        //        ->groupBy('sec.season','sec.year')
               ->first(); 
        }
        
        public function ifAdminValidate()
        {
               return (Auth::user()->role == 'Admin' | Auth::user()->role =='Developer') ? $this->receivableForAdmin() : $this->receivableForMember() ;
        }

        public function ifAdminValidate1()
        {
               return (Auth::user()->role == 'Admin' | Auth::user()->role =='Developer') ? $this->mainbranch() : '' ;
        }
        
        public function mainbranchseason1()
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select(DB::raw('sum(sec.tuition_fee_price) as total_tuition_fee'),'b.branch_name as main_branch',DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),DB::raw('sum(sec.facilitation_price) as total_facilitation'),'cs.branch','sec.season','sec.year',DB::raw('count(sec.id) as total_enrollee'))
                ->where('sec.year', $this->year)
                ->where('sec.season',1)
                ->groupBy('b.id')
                ->get();
        }
        public function mainbranchseason2()
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select(DB::raw('sum(sec.tuition_fee_price) as total_tuition_fee'),'b.branch_name as main_branch',DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),DB::raw('sum(sec.facilitation_price) as total_facilitation'),'cs.branch','sec.season','sec.year',DB::raw('count(sec.id) as total_enrollee'))
                ->where('sec.year', $this->year)
                ->where('sec.season',2)
                ->groupBy('b.id')
                ->get();
        }
        public function branchseason1($bid)
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select(DB::raw('sum(sec.tuition_fee_price) as total_tuition_fee'),'b.branch_name as main_branch',DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),DB::raw('sum(sec.facilitation_price) as total_facilitation'),'cs.branch','sec.season','sec.year',DB::raw('count(sec.id) as total_enrollee'))
                ->where('sec.year', $this->year)
                ->where('b.id', $bid)
                ->where('sec.season',1)
                ->first();
        }
        public function branchseason2($bid)
        {
                return  DB::connection('satelite_branch_db_connection')->table('student_enrollment_card as sec')
                ->LeftJoin('cbrc_students as cs', 'cs.id','sec.student_id')
                ->RightJoin('branches as b', 'b.id','cs.branch')
                ->RightJoin('student_payment_transactions as spt','spt.card_id', 'sec.id')
                ->select(DB::raw('sum(sec.tuition_fee_price) as total_tuition_fee'),'b.branch_name as main_branch',DB::raw('sum(spt.amount_paid) as total_amount_paid'),DB::raw('sum(sec.balance)as total_receivable'),DB::raw('sum(sec.discount) as total_discount'),DB::raw('sum(sec.facilitation_price) as total_facilitation'),'cs.branch','sec.season','sec.year',DB::raw('count(sec.id) as total_enrollee'))
                ->where('sec.year', $this->year)
                ->where('b.id', $bid)
                ->where('sec.season',2)
                ->first();
        }

        public function ifAdminValidate2()
        {
               return (Auth::user()->role == 'Admin' | Auth::user()->role =='Developer') ? $this->mainbranchseason1() : '' ;
        }

        public function ifAdminValidate3()
        {
               return (Auth::user()->role == 'Admin' | Auth::user()->role =='Developer') ? $this->mainbranchseason2() : '' ;
        }

        //bagong dashbaord dito




        public function mainXbranch_dashboard($branch)
        {
                
                return view('Dashboard.admin_branch_dashboard')
                ->with('Branches',$this->list_of_branches())
                ->with('branch',$branch);
        }
}
