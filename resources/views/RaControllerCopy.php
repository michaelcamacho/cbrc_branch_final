<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CbrcLecturer;
use App\CbrcLecturerSchedule;
use App\CbrcSubject;
use App\Program;
use App\ExamCategory;
use DB;
use App\Branch;
use App\ExamResult;


class RaControllerReports extends Controller
{
    
       public function __construct()
    {
        $this->middleware('cors');
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    public function lecturer($season,$year,$program,$branch){
        $lecturerdata = DB::table('cbrc_lecturer_schedules')
        ->leftjoin('cbrc_lecturers', 'cbrc_lecturers.id', '=', 'cbrc_lecturer_schedules.lec_id')
        ->leftjoin('cbrc_subjects', 'cbrc_subjects.id', '=', 'cbrc_lecturer_schedules.subject_id')
        ->leftjoin('programs', 'programs.program_id', '=', 'cbrc_lecturer_schedules.program_id')
        ->leftjoin('branches','branches.Branch_ID','=','cbrc_lecturer_schedules.Branch_ID')
        ->select('cbrc_lecturer_schedules.*', 'cbrc_lecturers.Lastname', 'cbrc_subjects.Subject','branches.Branch_Name')
        ->where('cbrc_lecturer_schedules.Season', '=', $season)
        ->where('cbrc_lecturer_schedules.year', '=', $year)
        ->where('programs.program_name', '=', $program)
        ->where('branches.Branch_Name','=', $branch)
        ->get();
       return response()->json($lecturerdata);
    }
    
    public function exam(){
        $examcategory = ExamCategory::all();
        return response()->json($examcategory);
    }
    
    
    public function exam_table(Request $request){
       $season = $request->input('season');
       $year = $request->input('year');
       $program = $request->input('program');
       $major = $request->input('major');
       $branch = $request->input('branch');
       $examcat = $request->input('examcat');

       
     if($program == 'lets'){
      if($major == 'BEED'){
        $examData = DB::table('exam_results')
       ->leftjoin('cbrc_students','cbrc_students.id','=','exam_results.std_id')
       ->leftjoin('exam_categories','exam_categories.id','=','exam_results.exam_id')
       ->select('exam_results.*','cbrc_students.*','exam_categories.ExamCategory')
       ->where('cbrc_students.Season','=',$season)
       ->where('cbrc_students.Year','=',$year)
       ->where('cbrc_students.Major','=',$major)
       ->where('cbrc_students.Branch_Name', '=',$branch)
       ->where('exam_categories.ExamCategory', '=', $examcat)
       ->get();
       
          
      }else{
        $examData = DB::table('exam_results')
       ->leftjoin('cbrc_students','cbrc_students.id','=','exam_results.std_id')
       ->leftjoin('exam_categories','exam_categories.id','=','exam_results.exam_id')
       ->select('exam_results.*','cbrc_students.*','exam_categories.ExamCategory')
       ->where('cbrc_students.Season','=',$season)
       ->where('cbrc_students.Year','=',$year)
       ->where('cbrc_students.Major','!=','BEED')
       ->where('cbrc_students.Branch_Name', '=',$branch)
       ->where('exam_categories.ExamCategory', '=', $examcat)
       ->get();
      }
       }else{
        $examData = DB::table('exam_results')
       ->leftjoin('cbrc_students','cbrc_students.id','=','exam_results.std_id')
       ->leftjoin('exam_categories','exam_categories.id','=','exam_results.exam_id')
       ->select('exam_results.*','cbrc_students.*','exam_categories.ExamCategory')
       ->where('cbrc_students.Season','=',$season)
       ->where('cbrc_students.Year','=',$year)
       ->where('cbrc_students.Program','=',$Program)
       ->where('cbrc_students.Branch_Name', '=',$branch)
       ->where('exam_categories.ExamCategory', '=', $examcat)
       ->get();
      }
       return response()->json($examData);
       
    }
}
