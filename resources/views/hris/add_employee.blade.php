@extends('main')
@section('title')
ADD EMPLOYEE
@stop
@section('css')
<style type="text/css">
  .date[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
  .date[type=date]:focus::-webkit-datetime-edit {
    color: #495057 !important;
}
input[type="file"]{
  display: none;
}
#upload{
  cursor: pointer;
  background-color:#f57327;
  color: #fff;

}
@media only screen and (min-width: 320px) {
  #upload {
     margin-left: 18px;
    padding: 9px 29px; 
  }
}
@media only screen and (min-width: 360px) {
  #upload {
     margin-left: 20px;
    padding: 10px 45px; 
  }
}

@media only screen and (min-width: 375px) {
  #upload {
     margin-left: 22px;
    padding: 9px 52px; 
  }
}

@media only screen and (min-width: 384px) {
  #upload {
     margin-left: 24px;
    padding: 10px 54px; 
  }
}
@media only screen and (min-width: 411px) {
  #upload {
     margin-left: 30px;
    padding: 10px 60px; 
  }
}
@media only screen and (min-width: 768px) {
  #upload {
     margin-left: 19px;
    padding: 10px 35px; 
  }
}

@media only screen and (min-width: 1024px) {
  #upload {
     margin-left: 24px;
    padding: 10px 63px; 
  }
}

@media only screen 
and (min-width : 1824px) {
  #upload{
      margin-left: 75px;
    padding: 10px 60px; 
  }
}


@media only screen 
and (min-width : 1280px) {
  #upload{
      margin-left: 53px;
    padding: 10px 60px; 
  }
}


input[type="button"]{
   background-color: #4CAF50;
  border: none;
  color: white;
  padding: 10px 60px;
  text-decoration: none;
  margin: 4px 2px;
  cursor: pointer;
}
</style>

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Add New Employee</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <form class="form-material" method="POST" action="/proceed/add_employee" data-toggle="validator" enctype="multipart/form-data" id="form">
                                     @csrf
                                     <input type="hidden" name="filename" id="filename">
                                     <input type="hidden" name="token" id="token" value="">
                                     <input type="hidden" name="is_upload" id="is_upload" value="">
                                      <input type="hidden" name="image" id="image">
                                      <div class="container">
                                        <div class="row">
                                          <div class="col-md-8">
                                            <div class="row">
                                              <div class="col-md-12">
                                                <h6>PERSONAL INFORMATION</h6>
                                              </div>
                                               <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="last_name" type="text"  maxlength="50" required>
                                                    <label>Last Name</label>
                                                </div>
                                          </div>
                                          <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="first_name" type="text" maxlength="50" required>
                                                    <label>First Name</label>
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="middle_name" maxlength="50" type="text" required>
                                                    <label>Middle Name </label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="address" maxlength="50" type="text" required>
                                                    <label>Address</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="number" name="contact_no" min="0" class="form-control"  maxlength="50" required>
                                                    <label>Contact Number</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="email" name="email" class="form-control"  maxlength="50" required="email" required>
                                                    <label>Email</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control date" name="birthdate"  maxlength="50" type="date" required>
                                                    <label>Birth Date</label>
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="gender" id="gender"  maxlength="50" required="">
                                                        <option value="">----</option>
                                                        <option value="MALE">Male</option>
                                                        <option value="FEMALE">Female</option>
                                                    </select>
                                                    <label>Gender</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="status" id="status" required="">
                                                        <option value="">----</option>
                                                        <option value="SINGLE">Single</option>
                                                        <option value="MARRIED">Married</option>
                                                        <option value="WIDOWED">Widowed</option>
                                                        <option value="DIVORSED">Divorsed</option>
                                                        <option value="SEPARATED">Separated</option>
                                                    </select>
                                                    <label>Status</label>
                                                </div>
                                            </div>
                                            </div>
                                          </div>
                                          <div class="col-md-4">
                                            <div class="row">
                                                  <div class="col-md-12 text-center" style="margin-top: 25px;margin-bottom: 15px;">
                                                    <img class="profile-pic  rounded-circle" id = "imgDisplay" style="height: 200px;max-width: 80%;" src="/cover_images/download.png">
                                                  </div>
                                                  <div class="col-md-12">
                                                    <label>
                                                      <span id="upload" class="text-center">Upload Files</span>
                                                      <input class="file-upload" id="cover_image" name="cover_image"type="file" accept="image/*" style="margin-top: 15px;margin-left: 50px;" />
                                                      </label>
                                                  </div>
                                                  
                                                </div>
                                            </div>
                                          </div>
                                        </div> <!-- end container of information -->
                                        <div class="container">
                                          <div class="row">
                                            <div class="col-md-12">
                                           <h6>EMPLOYMENT INFORMATION</h6>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="employee_no" type="text" required="">
                                                    <label>Employee Number</label>
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="position" type="text">
                                                    <label>Position</label>
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="employment_status" id="employment_status">
                                                        <option value="">----</option>
                                                        <option value="REGULAR">Regular</option>
                                                        <option value="PROBATIONARY">Probationary</option>
                                                        <option value="PART TIME">Part Time</option>
                                                    </select>
                                                    <label>Employment Status</label>
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" name="rate" type="number" min =0>
                                                    <label>RATE</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control date" id= "date_hired" name="date_hired" type="date" required>
                                                    <label>Date Hired</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" name="branch_Name" id="branch_name">
                                                        <option value="">----</option>
                                                        @if(isset($branch_name))
                                                          @foreach($branch_name as $branches)
                                                            <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                                          @endforeach
                                                        @endif
                                                    </select>
                                                    <label>Branch</label>
                                                </div>
                                            </div> 
                                          </div>
                                        </div><!-- end of container Employee record -->
                                          <div class="container">
                                            <div class="row">
                                              <div class="col-md-12">
                                                  <h6>GOVERNMENT IDENTIFICATION NUMBER</h6> 
                                              </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control decimal" type="text" maxlength="13" name="sss" required>
                                                    <label>SSS</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control decimal" type="text" min="0" name="phil_health"  maxlength="12" required>
                                                    <label>PhilHealth</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <input class="form-control decimal" type="text" min="0" name="pag_ibig" maxlength="12" required>
                                                    <label>PagIbig</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control decimal" type="text" min="0" name="tin"  maxlength="12" required>
                                                    <label>TIN</label>
                                                </div>
                                            </div>
                                            </div>
                                          </div> <!-- end of container government number -->
                                          <div class="container">
                                            <div class="row">
                                              <div class="col-md-12">
                                                <h6>INCASE OF EMERGENCY</h6>
                                              </div>
                                                 <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="contact_person" required>
                                                    <label>Contact Person Name</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control decimal" type="text" min="0" name="contact_details"  maxlength="11" required>
                                                    <label>Contact Person Number</label>
                                                </div>
                                            </div>
                                            <div class="col-md-8 form-actions btn-list">
                                               <button class="btn btn-primary" id="button-prevent" type="submit">Add New Employee</button>
                                                
                                            </div>
                                            </div>
                                         </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>



@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script src="{{asset('/js/webcam.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
      $("#branch_name option[value='Main']").remove();
  $('.add-enrollee').addClass('active');
});

<script>
    $(document).ready(function(){
        $('#form').submit(function(e){
            var form = this;
            var fileInput = $(this).find("input[type=file]")[0],
            file = fileInput.files && fileInput.files[0];

            if( file ) {
                var img = new Image();

                img.src = window.URL.createObjectURL( file );

                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;

                    window.URL.revokeObjectURL( img.src );

                    if( width == 600 && height == 600 ) {
                        form.submit();
                    }
                    else {
                       swal({
                          title: 'Oops...',
                          text: 'Image is too large',
                          type: 'error',
                          timer: '3000'
                      });
                      $(':input[type="submit"]').removeAttr("disabled");
                    }
                };
            }
            else {
              form.submit();
            }            
            e.preventDefault();
        });
    });
</script>

<script type="text/javascript">
$(document).ready(function() {
    var readURL = function(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }   
    $(".file-upload").on('change', function(e){
        $('#is_upload').val(0);
        $('#filename').val(e.target.files[0].name);
        readURL(this);
    });   

     $('.decimal').keypress(function(evt){
      return (/^[0-9]*\~?[0-9]*$/).test($(this).val()+evt.key);
        });
      });    


</script>
@stop