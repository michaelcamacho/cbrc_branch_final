@extends('main')
@section('title')
ADD NEW EXPENSE
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                  </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                    <h5> <span class="pull-right card_status"></span></h5>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Add New Expense</h5>
                                   <br/>
                                <form class="form-material" method="POST" data-toggle="validator" enctype="multipart/form-data" id="form" action="/proceed/insert_expense">
                                        @csrf


                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="branch" id="branch" value="{{ $branch }}"> 
                                                    <input type="hidden" name="season" id="season" value="{{ $expense_setting->season }}"> 
                                                    <input type="hidden" name="year" id="year" value="{{ $expense_setting->year }}"> 
                                                    <input class="form-control" type="text" 
                                                    value="{{ $date }}" id="date" name="date" readonly>
                                                    <label>Date</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                  
                                                </div>
                                            </div>
                                           
                                        </div>



                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" id="program" name="program" required>
                                                        <option value="" class='selector' >--- Select Program ---</option>
                                                        
                                                        @if(isset($expense_program_list))
                                                            @foreach($expense_program_list as $program_list)
                                                                <option value="{{$program_list->id}}">{{$program_list->program_name}}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>
                                                    <label>Program</label>
                                                </div>
                                            </div>
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <select class="form-control" name="category" id="category" required>
                                                        <option value="" disable="true" selected="true" class='selector1'>--- Select Category ---</option>
                                                       
                                                        @if(isset($expense_category))
                                                            @foreach($expense_category as $category_list)
                                                                <option value="{{$category_list->id}}">{{$category_list->category}}</option>
                                                            @endforeach
                                                        @endif


                                                    </select>
                                                    <label>Expense Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                           
                                        </div>

                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   <select class="form-control sub_category" name="sub_category" id="sub_category">
                                                        <option value="" disable="true" selected="true">--- Select Sub Category ---</option>
                                                       
                                                    </select>
                                                    <label id="lblSubCategory">Expense Sub Category</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                     
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                     <input class="form-control decimal" name="amount" type="text" maxlength="11">
                                                       
                                                    <label>Amount</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group val">
                                                    
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input class="form-control" name="remarks" id="remarks">
                                                    <label>Remarks</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                   
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <div class="form-actions btn-list">
                                            <button class="btn btn-primary" type="submit" name="submit" id="button-prevent">Add New Expense</button>
                                            {{--<button class="btn btn-outline-default" type="button">Cancel</button>--}}
                                        </div>
                                    </form>
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script src="{{ asset('/js/prevent.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.add-expense').addClass('active');
});



$('#category').on('change', function(e){
$('#amount').val('');
$('#remarks').val('');
var category_id = e.target.value;

$.get('/finance/fetch_sub_category/' + category_id,function(data){
                console.log(data);

                 $('#sub_category').empty();
                 $('#sub_category').append('<option value="" disable="true" selected="true">--- Select Sub Category ---</option>');

                $('#amount').val('');

                  $.each(data, function(index, subCategory){
                    $('#sub_category').append('<option value="'+ subCategory.id +'">'+ subCategory.sub_category+'</option>');
                });
                 

            });
});

</script>
    <script>
        const minorbug = () =>{
                                $('.decimal').keypress(function(evt){
                                    return (/^[0-9]*\.?[0-9]*$/).test($(this).val()+evt.key);
                                }); 
                                $("#program").on('change',function(){
                                    $('.selector').remove();
                                });
                                $("#category").on('change',function(){
                                    $('.selector1').remove();
                                });
                                $("#sub_category").on('change',function(){
                                    $('.selector2').remove();
                                });
        }
        minorbug();
    </script>
@stop