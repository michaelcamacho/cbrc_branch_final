//---------------------FUNCTION------------------------------
        //-------------------FIX FUNCTION---------------------------------
              const configs = async(data,urls,enteryourfunction) =>{
                const setting = {
                  method:'POST',
                  headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                      },
                  body:JSON.stringify(data),
                };
                const response = await fetch(urls,setting);
                enteryourfunction(response,data);
              };

          //--------------------GETSTUDENT NAME AND ID--------------------
              const studentName = async(response,data) =>{
                $('#total_amount').val("") 
                $('#facilitation').val('');
                $('#tuition').val('');
                $('#discount').prop('disabled',true);
                $('#category').prop('disabled',true);
                $('#category').empty();
                $('#category').append(`<option value="0" disable="true" >--- Select Category ---</option>`);
                $('#balance').val("");
                $('#dr').val("");
                $('#reserve').val("");
                $('.balance').removeClass("input-has-value");
                $('#discount').append(`<option value="0" disable="true" >Select category first.</option>`);
                $('.cat').attr('selected','selected');
                const res = await response.json();  
                  try{
                        
                        if(res.length > 0 ){
                          $('#student').empty();
                          $('#student').append(`<option value="" disable="true" >--- Select Student ---</option>`);
                          res.forEach((data1,index)=>{
                            $('#student').append(`<option value="${data1.cbrc_id}" disable="true" >[CBRC ID:${data1.cbrc_id}] / ${data1.last_name}, ${data1.first_name}, ${data1.middle_name}</option>`);
                          });
                        }else{
                          $('#student').empty();
                          $('#student').append(`<option style='color:red' value="" disable="true" selected="true" >No Enrollee on this program</option>`);
                        };
                    }catch(err){
                      console.log(err.res);
                    };
              }

              //---------------------addpayment--------------------
              const AddPayment = async(response,data) => {
                  const res = await response.json();
                  try {
                    window.location.href = res
                  } catch(err) {
                    console.log(err.res);
                  };
                
                
              };
              //-----------------ShowDiscount----------------------
              const Discount = async(response,data) =>{
                $('#total_amount').val("")
                $('#facilitation').addClass("input-has-value");
                $('.val').addClass("input-has-value");
                $('#facilitation').addClass("input-has-value");
                $('#tuition').addClass("input-has-value");
                $('#tuition').val($('#category').children("option:selected").attr('id'))
                $('#facilitation').val($('#category').children("option:selected").attr('class'))
                $('.cat').removeAttr('selected')
                $('#discount').empty();
                const res = await response.json();
                try{
                  if(res.length > 0 ){
                    $('#discount').append(`<option value="0">--- Select Category ---</option>`);
                    res.forEach((discount,index)=>{
                      $('#discount').append(`<option value="${discount.id}" class="${discount.discount_amount}">${discount.discount_category}</option>`);
                    });
                    }else{
                      $('#discount').append(`<option value="0" selected="true" >No discount available in this category.</option>`);
                    }
                }catch(err){
                  return console.log(err.response);
                }
              };
              //-----------------getBalance-----------------------
              const getBalance = async(response,data) =>{
                const res = await response.json();
                $('#total_amount').val("")
                $('#facilitation').val('');
                $('#tuition').val('');
                let balance = res.bal
                let category = res.cat
                
                try{
                  $('#category').empty();
                  $('#category').append(`<option value="0">--- Select Category ---</option>`);
                  category.forEach((categories,index)=>{
                    $('#category').append(`<option value="${categories.id}" id="${categories.tuition_fee}" class="${categories.facilitation_fee}" >${categories.category_name}</option>`);
                  });
                  
                  if(data.student_id != "" ){
                    if(balance.balance == null | balance.balance == 0){
                      $('#category').removeAttr('disabled');
                      $('#discount').removeAttr('disabled');
                    }else{
                        $('#category').prop('disabled',true);
                        $('#discount').prop('disabled',true);
                      }
                  }else{
                    $('#category').prop('disabled',true);
                    $('#discount').prop('disabled',true);
                  }
                  $('#balance').val(balance.balance);
                  $('#reserve').val(balance.reservation_fee);
                  $('#dr').val(balance.registration);
                  $('.balance').addClass("input-has-value");


              }catch(err){
                $('#balance').val("");
                $('#reserve').val("");
                $('#dr').val("");
              }
              console.log('test')
              if($('#dr').val() !=""){
                console.log('try')
                $('#reserve').val('');
              }
              $('.cat').attr('selected','selected');
              $('#discount').empty();
              $('#discount').append(`<option value="0" disable="true" >Select category first.</option>`);
              }
              //-------------------Formulas-------------------------
              calculate = (number1,number2) =>{
                if(number1 == "" & number2== ""){
                  return 0;
                };
                return parseFloat(number1) + parseFloat(number2);
              }
              bal = (number1) =>{
                if(number1 == ""){
                  return 0;
                };
                return parseFloat(number1);
              }
              discount = (value) =>{
                if(value == null){
                  return parseFloat(0);
                }
                return parseFloat(value);
              }
//-----------------------Triggers--------------

          //  In the above code this display student name when 
          //  choicing program such as; LET, CRIM and  etc..
    $("#program").change(()=>{
      $('#amount_paid').prop('disabled',true);
      $('#amount_paid').val("");
      $("#discount").val("");
      let data = {
        'program':$('#program').children("option:selected").val()
        };
      configs(data,'/requesting-studentdatainPayment',studentName);
    });
          // this is for passing data inside controller
    $('#button-prevent').click(()=>{
      const validatedate = (value) =>{
        if(value == ""){
          var d = new Date();
          var months = ["Jan","Feb","Mar","April","May","June","July","Aug","Sept","Oct","Nov","Dec"]
          return months[d.getMonth()]+'-'+d.getDay()+'-'+d.getFullYear();
        }else{
          return value;
        }
      }
      const data = {
        date:validatedate($('#dr').val()),
        program:$('#program').val(),
        student:$('#student').val(),
        category:$('#category').val(),
        discount:$('#discount').val(),
        total_amount:$('#total_amount').val(),
        facilitation:$('#facilitation').val(),
        // total_amount:$('#total_amount').val(),
        // total_amount:$('#total_amount').val(),
        // total_amount:$('#total_amount').val(),
        amount_paid:$('#amount_paid').val(),
      };
      if(data.program == "0" | data.student == "" | data.amount_paid == "" && data.total_amount == "" ){
        swal('Invalid Input');
      }else if(data.amount_paid =="0" | data.amount_paid == ""){
        swal('0 Amount paid is not valid');
      }else if(data.total_amount == "" | data.total_amount == "0"){
        swal('Click Compute.....');
      }else{
        swal({
          title: "Are you sure?",
          text: "This will add in your inventory as data.",
          showCancelButton: true,
          confirmButtonText: "Yes",
          cancelButtonText: "No",
      }).then(val=>{
          if(val){
            configs(data,'/payment/insert-studentPayment',AddPayment);
          }
        }).catch((err)=>{

        });
      }
    });
          //get discount in specific program and category
    $('#category').change(()=>{
      $('#amount_paid').val("");
      const data = {
        category:$('#category').val(),
        program:$('#program').children("option:selected").val()
      };
      configs(data,'/requesting-programdiscount',Discount);
    });
          //Student change
    $('#student').change(()=>{
      
      $('#amount_paid').val("");
      $('#amount_paid').removeAttr('disabled')
      let data = {
        student_id:$('#student').val(),
        program:$('#program').val(),
      }
      configs(data,'/payment/getBalance',getBalance)
    });    

    //Compute Total

    $('#compute').click(()=>{
      $('#amount_paid').val("");
        $('.total').addClass("input-has-value");
          
        $('#total_amount').val(bal($('#balance').val())+calculate($('#tuition').val(),$('#facilitation').val())-$('#reserve').val() - discount($('#discount').children("option:selected").attr('class')));
          if($('#total_amount').val()==0){
            $('.total').removeClass("input-has-value");
            $('#total_amount').val("");
          }
      });
      //amountPaid
      $('#amount_paid').on('change',()=>{
        let totalAmount = bal($('#balance').val())+calculate($('#tuition').val(),$('#facilitation').val())-$('#reserve').val() - discount($('#discount').children("option:selected").attr('class'));
        let facifee = parseFloat($('#facilitation').val());
        if(totalAmount < $('#amount_paid').val()){
         $('#amount_paid').val(totalAmount);
       }
       if(facifee > $('#amount_paid').val()){
        $('#amount_paid').val(facifee);
      }
        if($('#amount_paid').val() == '0'){
          $('#amount_paid').val("")
        }
      });
      //discounts change
     $('#discount').on('change',()=>{
      let facifee = parseFloat($('#facilitation').val());
      $('.dis').addClass("input-has-value");

       $('#amount_paid').val(facifee);
     });

                                //NOTES:
//  I ADDED TABLE NAME WITH COLUMN NAME OF category_name and category_id,
//  I also edited column of discount_category and changed it to category_id for joint function.