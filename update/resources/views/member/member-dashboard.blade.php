@extends('main')
@section('title')
DASHBOARD
@stop
@section('dashboard')
active
@stop
@section('main-content')
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Summary</h6>
                        {{--<p class="page-title-description mr-0 d-none d-md-inline-block">statistics, charts and events</p>--}}
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{Auth::user()->position. 'dashboard'}}">Dashboard</a>
                            </li>
                            {{--<li class="breadcrumb-item active">Home</li>--}}
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder col-lg-4">
    
                    </div>
                    <!-- first boxes -->

                    <div class="widget-holder widget-sm col-md-6 widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Sales For Season 1</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                        <span class="counter">
                                        {{$s1_sale or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{Auth::user()->position}}./sales-program" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background:#85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Sales For Season 2</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                        <span class="counter">
                                        {{$s2_sale or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{Auth::user()->position}}./sales-program" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <div class="widget-holder widget-sm col-md-4 widget-full-height">
                        <div class="widget-bg text-inverse bg-primary">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Cash On hand - Season 1 </p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                        <span class="counter">
                                        {{$s1_cash or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="#" class="btn btn-link btn-underlined btn-xs btn-white fs-11"></a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-md-4 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Cash On Hand - Season 2</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                            <span class="counter">
                                            {{$s2_cash or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="#" class="btn btn-link btn-underlined btn-xs btn-white fs-11"></a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                    <div class="widget-holder widget-sm col-md-4 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Cash From Books</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                            <span class="counter">
                                            {{$book_cash or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="" class="btn btn-link btn-underlined btn-xs btn-white fs-11"></a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col-lg-4 col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse bg-primary" >
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Receivables</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                        <span class="counter">
                                        {{$receivable or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-lg-4 col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Expenses</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                            <span class="counter">
                                            {{$expense or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <a href="{{Auth::user()->position}}./expense" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-lg-4 col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Petty Cash</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$pettycash or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{Auth::user()->position}}./add-budget" class="btn btn-link btn-underlined btn-xs btn-white fs-11">Add Budget For Petty Cash</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    
                </div>
                <!-- /.widget-list -->
            </div>

            <div class="container-fluid">
                <div class="widget-list row">
                    <div class="widget-holder widget-full-height widget-flex col-lg-8">
                        
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder col-lg-4">
    
                    </div>
                   
                    <!-- second boxes -->

                    <div class="widget-holder widget-sm col-lg-3 col-md-6 widget-full-height">
                        <div class="widget-bg bg-primary text-inverse">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Enrollees</p>
                                        <span class="counter-title-a d-block">
                                            
                                        <span class="counter">
                                        {{$enrollee or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{Auth::user()->position}}./sales-program" class="btn btn-link btn-underlined btn-xs fs-11 btn-yellow text-white">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                    <!-- /.widget-holder -->
                    <div class="widget-holder widget-sm col-lg-3 col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #85d1f2">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Unpaid Students</p>
                                        <span class="counter-title-a d-block"><span class="counter">
                                        {{$unpaid or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-lg-3 col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6bd8a1">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Discounts</p>
                                        <span class="counter-title-a d-block">
                                            Php 
                                            <span class="counter">
                                            {{$discount or 0}}
                                            </span></span>
                                        <!-- /.counter-title --> <div class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement From Branches</div>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>

                     <div class="widget-holder widget-sm col-lg-3 col-md-6 widget-full-height">
                        <div class="widget-bg text-inverse" style="background: #6a55ee">
                            <div class="widget-body">
                                <div class="counter-w-info media">
                                    <div class="media-body w-50">
                                        <p class="text-muted mr-b-5 fw-600">Total Book Sales</p>
                                        <span class="counter-title-a d-block">
                                        Php 
                                        <span class="counter">
                                        {{$book or 0}}
                                        </span></span>
                                        <!-- /.counter-title --> <a href="{{Auth::user()->position}}./books" class="btn btn-link btn-underlined btn-xs btn-white fs-11">View Statement</a>
                                    </div>
                                    <!-- /.media-body -->
                                    <div class="pull-right align-self-center">
                                        <div class="mr-t-20">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.counter-w-info -->
                            </div>
                            <!-- /.widget-body -->
                        </div>
                        <!-- /.widget-bg -->
                    </div>
                   
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>

@stop
@section('js')
<script type="text/javascript">
    $(document).ready(function() {
  $('.dashboard').addClass('active');
   $('.branches').addClass('active');
  $('.branches').addClass('collapse in');
 
});

</script>
@stop
