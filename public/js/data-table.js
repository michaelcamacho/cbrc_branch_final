//[Data Table Javascript]

//Project:	Superieur Admin - Responsive Admin Template
//Primary use:   Used only for the Data Table

$(function () {
    "use strict";

    $('#example1').DataTable();
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
    });
	
	var table = $('#example').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		],
		"order": [[ 0, "desc" ]],
		
	} );
	
	var table = $('.display2').DataTable( {
		responsive: true,
		"scrollX": true,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		 'pdf' 
		
		],
		"order": [[ 0, "desc" ]],
		
	} );
	var table = $('.display').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		],
		"order": [[ 0, "desc" ]],
		
	} );
	
	var table = $('#example').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		],
		"order": [[ 0, "desc" ]],
		
	} );
	var table = $('#booktransact').DataTable( {
		"order": [[ 0, "desc" ]],
	} );

	var table = $('#exam2').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		],
		"order": [[ 0, "desc" ]],
		
	} );

	var table = $('#example-en').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": [[ 0, "asc" ],[ 2, "asc" ]],
		
	} );

	var table = $('#example-res').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": [[ 1, "asc" ],[ 0, "asc" ]],
		
	} );

	var table = $('#lec').DataTable( {
		responsive: false,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": false,
		"paging": false,
		"searching": false,
		exportOptions : {
            columns: ':visible'
        },

		
	} );

	var table = $('#total-en').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": [[ 2, "asc" ],[ 0, "asc" ]],
		
	} );

	var table = $('#lec2').DataTable( {
		responsive: false,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		
	} );

	var table = $('#lec3').DataTable( {
		responsive: false,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		smart: true,	
	} );

	var table = $('#example-en2').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		],
		"order": [[ 0, "asc" ]],
		
	} );

	var table2 = $('#exam').DataTable( {
		responsive: true,
		"scrollX": true,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-let').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-nle').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-crim').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-civil').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-psyc').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-nclex').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-ielts').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-social').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-agri').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-mid').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	var table2 = $('#report-online').DataTable( {
		responsive: true,
		"scrollX": true,
		"scrollX": true,
		"order": false,
		"paging": false,
		"searching": false,
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf','csv', 
		
		]
		
	} );

	
	$('#tickets').DataTable({
	  'paging'      : true,
	  'lengthChange': false,
	  'searching'   : false,
	  'ordering'    : true,
	  'info'        : true,
	  'autoWidth'   : false,
	});
	
	$('#productorder').DataTable({
	  'paging'      : true,
	  'lengthChange': true,
	  'searching'   : true,
	  'ordering'    : true,
	  'info'        : true,
	  'autoWidth'   : false,
	});
	
	//--------Individual column searching
	
    // Setup - add a text input to each footer cell
    $('#example5 tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#example5').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	
	
	//---------------Form inputs
	var table = $('#example6').DataTable();
 
    $('button').click( function() {
        var data = table.$('input, select').serialize();
       
        return false;
    } );
	
	var table = $('#new-payment-table').DataTable( {
		responsive: true,
		"scrollX": true,
	} );
	

//new added by mike

var table = $('#student').DataTable( {
	responsive: true,
	"dom": '<"pull-left"f><"pull-right"l>tip',
	'searching': false,
	'lengthChange': false,
	"order": [[ 0, "desc" ]] ,
	
} );


var table = $('#card_information_tb').DataTable( {
	'responsive': true,
	"bInfo" : false,
	'bPaginate': false,
	'searching': false,
	'lengthChange': false,
} );

var table = $('#student1').DataTable( {
	responsive: true,
	"dom": '<"pull-left"f><"pull-right"l>tip',
	'lengthChange': false,
} );


//new added by mike




	
  }); // End of use strict