<?php

namespace App\Http\Controllers\Page;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Branch;
use App\Category;
use App\CbrcStudent;
use App\Major;
use App\StudentEnrollmentCard;
use App\Tenant;
use App\Program;

use App\Model\AdminSettings;
use App\Model\ExpenseSetup;

use App\Model\DataModel\Cash;
use App\Model\DataModel\Tuition;
use App\Model\DataModel\Discount;
use App\Model\DataModel\BudgetCategory;
use App\Model\DataModel\Expense;
use App\Model\DataModel\ExpenseCategory;
use App\Model\DataModel\ExpenseProgramList;
use App\Model\DataModel\ExpenseSubCategory;

use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;


class PageController extends Controller
{
    private $year;
    private $Branches;
   
    public function __construct()
    {
            $this->middleware('auth');
            $settings = AdminSettings::findOrFail(1);
            $this->year = $settings->year;
            $this->settings = $settings;
            $main_id = Branch::select('main_branchid')->get();
            $this->Branches = Branch::whereNotIn('id', $main_id)->get();
    }
    


    public function new_enrollee($branch)
    {

        $program = Program::all();
        $major = Major::all();
        
        return view('student.add-enrollee')
        ->with('main_branch',$this->main_branch_id())
        ->with('Branches' , $this->Branches)
        ->with('program' , $program)
        ->with('major' , $major)
        ->with('branch' , $branch);

    }


    public function new_payment($branch)
    {
        $date = date('Y-m-d');
        $program = Program::all();
        $categories = Category::all();
        
           
        return view('make_transaction.new_payment')
        ->with('settings' , $this->settings)
        ->with('main_branch',$this->main_branch_id())
        ->with('Branches' , $this->Branches)
        ->with('categories' , $categories)
        ->with('program' , $program)
        ->with('year' , $this->year)
        ->with('branch' , $branch)
        ->with('date' , $date);
    }

    
    public function new_reservation($branch)
    {
        $date = date('Y-m-d');
        $program = Program::all();
       return view('make_transaction.new_reservation')
        ->with('Branches' , $this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('year' , $this->year)
        ->with('program' , $program)
        ->with('date' , $date)
        ->with('branch' , $branch);        
    }
    
    public function new_expense($branch)
    {
        $date = date('Y-m-d');
        $expense_setting = ExpenseSetup::where('branch','=',$branch)->first();
   
        $expense = Expense::all();
        $expense_program_list = ExpenseProgramList::all();
        $expense_category = ExpenseCategory::all();
    
        return view('make_transaction.add_expense')
        ->with('expense_program_list' , $expense_program_list)
        ->with('Branches' , $this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('expense_category' , $expense_category)
        ->with('expense_setting' , $expense_setting)
        ->with('date' , $date)
        ->with('branch' , $branch);
    }

    public function new_budget($branch)
    {
        $date = date('Y-m-d');
        $expense_setting = ExpenseSetup::where('branch','=', $branch )->first();
        $branch_id = Branch::where('branch_name', '=' , $branch)->first();
        $budget_categories = BudgetCategory::all();
        
        return view('make_transaction.add_budget')
        ->with('budget_categories' , $budget_categories)
        ->with('date' , $date)
        ->with('main_branch',$this->main_branch_id())
        ->with('expense_setting' , $expense_setting)
        ->with('Branches' , $this->Branches)
        ->with('branch' , $branch);
    }

    
    public function new_book_payment($branch)
    {
        
      return "Book Payment";
    }

     

    public function new_cash_remit($branch){
        
        $date = date('Y-m-d');
        $branch_id = Branch::where('branch_name', '=' , $branch)->first();
        $expense_setting = ExpenseSetup::where('branch','=', $branch )->first();
        
        return view('make_transaction.new_remit')
        ->with('expense_setting' , $expense_setting)
        ->with('date' , $date)
        ->with('Branches' , $this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('branch' , $branch);
    }


    // reports
    public function today_transactions($branch)
    {
        return view('reports.today_transaction')
        ->with('date' , date('Y-m-d'))
        ->with('Branches' , $this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('branch' , $branch);
    }
    public function yesterday_transactions($branch)
    {
        return view('reports.yesterday_transaction')
        ->with('date' , date('Y-m-d'))
        ->with('Branches' , $this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('branch' , $branch);
    }
    public function score_card($branch)
    {
        return "score card";
        
    }
    public function financial_report($branch)
    {
        return "financial report";

    }
    // reports



// records


public function master_list($branch)
{
    $row_counter=1;
    
    $student = CbrcStudent::leftJoin('programs','cbrc_students.program','=','programs.id')
    ->select('cbrc_students.*','programs.program_name')
    ->where('cbrc_students.branch','=' , Auth::user()->branch_id )
    ->get();

    return view ('student.master_list')
    ->with('Branches',$this->Branches)
    ->with('main_branch',$this->main_branch_id())
    ->with('row_counter',$row_counter)
    ->with('student',$student)
    ->with('branch',$branch);
}

public function books()
{
    return "books";

}


public function sales($branch)
{

$program = Program::all();
$card = Program::rightJoin('cbrc_students as std','std.program','=','programs.id')
->rightJoin('student_enrollment_card as sec','std.id','=','sec.student_id')
->rightJoin('categories as cat','sec.category_id','=','cat.id')
->select('sec.id','std.program','sec.date_registered','cat.category_name','std.first_name','std.middle_name','std.last_name','sec.tuition_fee_price','sec.facilitation_price','sec.discount','sec.balance','programs.program_name',DB::raw('((sec.tuition_fee_price + sec.facilitation_price) - sec.discount) - sec.balance as amount_paid'))
->where('std.branch','=' , $branch )
->get();


    return view('finance.sales')
    ->with('program',$program)
    ->with('card',$card)
    ->with('branch',$branch)
    ->with('Branches',$this->Branches)
    ->with('main_branch',$this->main_branch_id());

}
public function recievables($branch)
{
    return view('finance.recievables')
    ->with('branch',Auth::user()->branch)
    ->with('Branches',$this->Branches)
    ->with('main_branch',$this->main_branch_id());

}
public function finance($branch)
{
    return view('finance.finance')
    ->with('branch',Auth::user()->branch)
    ->with('Branches',$this->Branches)
    ->with('main_branch',$this->main_branch_id());

}

public function expense($branch)
{


        $date = date('Y-m-d');
        $branch_id = Branch::where('branch_name', '=' , $branch )->first();
        $expense_category = ExpenseCategory::all();

        $Branches = Branch::all();
        $expense = DB::connection('satelite_branch_db_connection')
        ->table('expenses')
        ->leftjoin('users','users.id','=','expenses.author')
        ->select('expenses.*' , 'users.name as author')
        ->where('expenses.branch_id','=', $branch_id->id)->get();
        
        // return $expense->expense_subcategory;


        return view('finance.expenses')
        ->with('expense_category' , $expense_category)
        ->with('expense' , $expense)
        ->with('Branches',$this->Branches)
        ->with('main_branch',$this->main_branch_id())
        ->with('date' , $date)
        ->with('branch',$branch);


}






public function tuition_fees()
{

    $category = Category::all();
    $program =  program::all();
    $major =  Major::all();
    $tuition = '';
    $branch_table_count ='';

	if($this->main_branch_id() == ''){
        
    $branch_table_count =Branch::all();
        $tuition = Tuition::leftJoin('categories','tuitions.category','=','categories.id')
        ->leftJoin('programs','tuitions.program_id','=','programs.id')
        ->leftJoin('branches as b','b.id','=','tuitions.branch_id')
        ->select('tuitions.*','b.branch_name As branch_name','categories.category_name As category_name' , 'programs.program_name As program_name' )
        // ->where('tuitions.year', $this->year)
        ->groupBy('b.id')
        ->get();

        // dd($tuition);
    }
    elseif($this->main_branch_id() != ''){
        
    $branch_table_count =Branch::where('id' , '=' , Auth::user()->branch_id)->get();
        $tuition = Tuition::leftJoin('categories','tuitions.category','=','categories.id')
        ->leftJoin('programs','tuitions.program_id','=','programs.id')
        ->leftJoin('branches','branches.id','=','tuitions.branch_id')
        ->select('tuitions.*','branches.branch_name As branch_name','categories.category_name As category_name' , 'programs.program_name As program_name' )
        ->where('tuitions.branch_id', '=' , Auth::user()->branch_id)
        ->get();
    }
    
    return view('admin.tuition_fee')
    ->with('major' , $major)
    ->with('category' , $category)
    ->with('program' , $program)
    ->with('tuition' , $tuition)
    ->with('branch_table_count',$branch_table_count)
    ->with('main_branch',$this->main_branch_id())
	->with('Branches',$this->list_of_branches());
    }







    public function discounts()
    {
        
        $branch_table_count ='';
        $Branches = Branch::all(); 
        $category = Category::all();
        $program =  program::all();
        $major =  Major::all();
     
    if($this->main_branch_id() == ''){
  
        $branch_table_count =Branch::all();
        $discounts = Discount::leftJoin('categories','discounts.category_id','=','categories.id')
        ->leftJoin('programs','discounts.program_id','=','programs.id')
        ->leftJoin('branches','branches.id','=','discounts.branch_id')
        ->select('discounts.*','branches.branch_name As branch_name','categories.category_name As category_name' , 'programs.program_name As program_name' )
        ->get();
    }
    elseif($this->main_branch_id() != ''){

        $branch_table_count =Branch::where('id' , '=' , Auth::user()->branch_id)->get();
        $discounts = Discount::leftJoin('categories','discounts.category_id','=','categories.id')
        ->leftJoin('programs','discounts.program_id','=','programs.id')
        ->leftJoin('branches','branches.id','=','discounts.branch_id')
        ->select('discounts.*','branches.branch_name As branch_name','categories.category_name As category_name' , 'programs.program_name As program_name' )
        ->where('discounts.branch_id', '=' , Auth::user()->branch_id)
        ->get();
    }
        return view('admin.discounts')
        ->with('major' , $major)
        ->with('category' , $category)
        ->with('program' , $program)
        ->with('discounts' , $discounts)
        ->with('branch_table_count' , $branch_table_count)
        ->with('main_branch',$this->main_branch_id())
        ->with('Branches',$this->list_of_branches());
    }
    
    public function ra_files()
    {
            return "ra_files";
    }

// records




//reports



//reports



}
