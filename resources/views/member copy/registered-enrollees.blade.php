@extends('main')
@section('title')
{{$prog}} 
@stop
@section('css')
<style type="text/css">
    .form-control:disabled, .form-control[readonly] {
    color: #000;
}
@media print {
    .content-header, .left-side, .main-header, .main-sidebar, .no-print {
    display: none!important;
}
 .table {
border-collapse: collapse !important;
}
td{
    color: #000;
}

}

</style>
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">{{ $branch }}</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item">Programs</li>
                            <li class="breadcrumb-item active">{{$prog}} Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">{{$prog}} Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">

                               
                        <div class="col-md-6">

                        <form method="POST" data-toggle="validator" enctype="multipart/form-data" action="{{Auth::user()->position. 'csv-enrollee'}}">
                        @csrf
                       <div class="input-group">
                        
                          <span class="input-group-btn">
                            <span class="btn btn-info" onclick="$(this).parent().find('input[type=file]').click();">Import CSV</span>

                            <input name="csv" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" accept=".csv">
                            <input type="hidden" name="program" value="{{$prog}}">
                          </span>

                          <span class="form-control"></span>
                          <input class="btn btn-success" type="submit" value="Upload">
                        </div>
                        </form>

                        </div>
                                
                               
                    <table id="example-en2" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">

                    <thead>
                       
                        <tr>
                            <th>Name</th>
                            <th>CBRC #</th>
                            <th>Program</th>
                            <th>Section</th>
                            <th>Registration</th>
                            <th>Date Registered</th>
                            <th>Contact #</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Birthdate</th>
                            <th>School</th>
                            <th>Major</th>
                            <th>Take</th>
                            <th>NOA #</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Contact Person</th>
                            <th>Emergency #</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($enrollee))
                            @foreach($enrollee as $enrollees)
                        <tr>
                            <td id="name_{{$enrollees->id}}">{{$enrollees->last_name}}, {{$enrollees->first_name}}, {{$enrollees->middle_name}}</td>
                            <td id="cbrc_id_{{$enrollees->id}}">{{$enrollees->cbrc_id}}</td>
                            <td id="program_{{$enrollees->id}}">{{$enrollees->program_name}}</td>
                            <td id="section_{{$enrollees->id}}">{{$enrollees->section}}</td>
                            <td>{{$enrollees->registration}}</td>
                            <td>{{\Carbon\Carbon::parse($enrollees->created_at)->format('M-d-Y')}}</td>
                            <td id="contact_no_{{$enrollees->id}}">{{$enrollees->contact_no}}</td>
                            <td id="contact_no_{{$enrollees->id}}">
                                @if($enrollees->status != '')
                                    {{$enrollees->status}}
                                @else
                                     <font color='red'>Not enrolled</font>
                                @endif
                            </td>
                            <td id="email_{{$enrollees->id}}">{{$enrollees->email}}</td>
                            <td id="address_{{$enrollees->id}}">{{$enrollees->address}}</td>
                            
                            <td id="birthdate_{{$enrollees->id}}">{{$enrollees->birthdate}}</td>
                            <td id="school_{{$enrollees->id}}">{{$enrollees->school}}</td>
                            <td id="major_{{$enrollees->id}}">{{$enrollees->major}}</td>
                            <td id="take_{{$enrollees->id}}">{{$enrollees->take}}</td>
                            <td id="noa_no_{{$enrollees->id}}">{{$enrollees->noa_no}}</td>
                            <td id="username_{{$enrollees->id}}">{{$enrollees->username}}</td>
                            <td id="password_{{$enrollees->id}}">{{$enrollees->password}}</td>
                            <td id="contact_person_{{$enrollees->id}}">{{$enrollees->contact_person}}</td>
                            <td id="contact_details_{{$enrollees->id}}">{{$enrollees->contact_details}}</td>
                            <td>
                                <a href="#" class="btn btn-success edit" id="edit_{{$enrollees->id}}" data-toggle="modal" data-target="#update-student">Update</a>
                            </td>
                            <td>
                                <button class="btn btn-danger delete" id="delete_{{$enrollees->id}}" user-id="{{$enrollees->id}},{{$enrollees->course}}">Delete</button>
                            </td>
                        </tr>
                       @endforeach
                            @endif
                    </tbody>                  
                    
                </table>
                @role('Admin')
                <br/>
                <input type="hidden" name="program_clear" id="program_clear" value="{{$prog}}">
                <a class="btn btn-danger clear" id="clear">Clear table</a>
                @endrole
                                </div>
                <div id="update-student" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="tuition-form-update">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="branch" id="update_id" readonly>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="cbrc_id">CBRC ID</label>
                                    <input type="text" class="form-control" name="cbrc_id" id="cbrc_id" required>
                                    
                                </div>

                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="program" id="program" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="middle_name">Middle Name</label>
                                    <input type="text" class="form-control" name="middle_name" id="middle_name" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="contact_no">Contact Number</label>
                                    <input type="text" class="form-control" name="contact_no" id="contact_no" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input type="text" class="form-control" name="address" id="address" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="birthdate">Birthdate</label>
                                    <input type="date" class="form-control" name="birthdate" id="birthdate" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="school">School</label>
                                    <input type="text" class="form-control" name="school" id="school" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="major">Major</label>
                                    <input type="text" class="form-control" name="major" id="major" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="take">Take</label>
                                    <input type="text" class="form-control" name="take" id="take" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="noa_no">Noa #</label>
                                    <input type="text" class="form-control" name="noa_no" id="noa_no" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="text" class="form-control" name="password" id="password" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="section">Section</label>
                                    <input type="text" class="form-control" name="section" id="section" required>
                                    
                                </div>


                                <div class="form-group">
                                    <label for="contact_person">Contact Person's Name</label>
                                    <input type="text" class="form-control" name="contact_person" id="contact_person" required>
                                    
                                </div>

                                <div class="form-group">
                                    <label for="contact_details">Contact Person's Number</label>
                                    <input type="text" class="form-control" name="contact_details" id="contact_details" required>
                                    
                                </div>

                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple update" data-dismiss="modal">Update</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

            
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>


@stop
@section('js')
<script src="{{ asset('/js/payment-fetch.js') }}"></script>
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/student-update.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('.record').addClass('active');
  $('.record').addClass('collapse in');
  $('.program').addClass('active');
  $('.program').addClass('collapse in');
});


    $('.update').on('click',function(){
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', $('#update_id').val());
                formData.append('cbrc_id', $('#cbrc_id').val());
                formData.append('last_name', $('#last_name').val());
                formData.append('first_name', $('#first_name').val());
                formData.append('middle_name', $('#middle_name').val());
                formData.append('username', $('#username').val());
                formData.append('password', $('#password').val());
                formData.append('program', $('#program').val());
                formData.append('major', $('#major').val());
                formData.append('school', $('#school').val());
                formData.append('noa_no', $('#noa_no').val());
                formData.append('take', $('#take').val());
                formData.append('birthdate', $('#birthdate').val());
                formData.append('contact_no', $('#contact_no').val());
                formData.append('email', $('#email').val());
                formData.append('address', $('#address').val());
                formData.append('contact_person', $('#contact_person').val());
                formData.append('contact_details', $('#contact_details').val());
                formData.append('section', $('#section').val());
              
                
                $.ajax({
                  url: "{{Auth::user()->position. 'update-enrollee'}}",
                  type: 'POST',         
                  data: formData,
                  processData: false,
                  contentType: false,
                  success:function(data){
                     
                      
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                                                
                            $('#add-tuition form')[0].reset();     
                        }
                    });
        // location.reload();
            });
    $('.delete').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('program', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'delete-enrollee'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });
    $('#clear').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var prog = $('#program_clear').val();
    if (prog == 'LET'){
        prog = 'lets';
    }
    if (prog == 'NLE'){
        prog = 'nles';
    }
    if (prog == 'Civil Service'){
        prog = 'civils';
    }
    if (prog == 'Criminology'){
        prog = 'crims';
    }
    if (prog == 'Psychometrician'){
        prog = 'psycs';
    }
    if (prog == 'NCLEX'){
        prog = 'nclexes';
    }
    if (prog == 'IELTS'){
        prog = 'ielts';
    }
    if (prog == 'Social Work'){
        prog = 'socials';
    }
    if (prog == 'Agriculture'){
        prog = 'agris';
    }
    if (prog == 'Midwifery'){
        prog = 'mids';
    }
    if (prog == 'Online Only'){
        prog = 'onlines';
    }
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('program', prog);
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'clear-enrollee'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

    $('#example-en2').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('program', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{Auth::user()->position.'delete-enrollee'}}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

</script>
@stop