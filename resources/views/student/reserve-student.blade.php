@extends('main')
@section('title')
Student Summary
@stop
@section('css')
<style type="text/css">
.form-control:disabled, .form-control[readonly] {
color: #000;
}
@media print {
.content-header, .left-side, .main-header, .main-sidebar, .no-print {
display: none!important;
}
.table {
border-collapse: collapse !important;
}
td{
color: #000;
}

}


</style>

<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
<!-- Page Title Area -->
<div class="container-fluid">
<div class="row page-title clearfix">
    
    <div class="page-title-left">
    <h6 class="page-title-heading mr-0 mr-r-5">Reserve Student</h6>
    </div>




</div>
<!-- /.page-title -->
</div>
<!-- /.container-fluid -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="container-fluid">
<div class="widget-list">
<div class="row">
<div class="col-md-12 widget-holder">
<div class="widget-bg p-5">

        @foreach($student as $data)
        <a href="{{'/student-summary/'.$data->id }}"  class="btn btn-info" >Return to Student Summary</a>
        @endforeach

        <br>
        <br>
        <br>
<div>
    @foreach($student as $data)
        
    <b>Name:    {{ $data->first_name . ' ' . $data->middle_name . ' ' . $data->last_name  }}</b><br>
    <b>Program: {{ $data->program_name }}</b><br>
    <b>Major:   {{ $data->major }}</b><br>

    @endforeach
</div>

<form method="POST" action="/proceed/reserve">

    <input type="hidden" name="student_id" value="{{$data->id}}">

    
    
    <div style=" margin:5% 0 0 40%;position:absolute;font-family:digital-clock-font; font-size:20px;">        
       
        
        <br>Amount Paid: <input id="amount_paid" name="amount_paid" style="text-align:center;" placeholder="0.00">
        <br><br><br>
            
        <div class="row  pull-right" style="padding-right:15px">
                <input type="submit"name="submit" value="Reserve Student" href="#" class="btn btn-info" />
        </div>
        @csrf
    </div>

<br><br>


<div class="col-lg-5">
        <label>Student Category</label>
        <div class="form-group">
            <select class="form-control" name="drop-down-category" type="text" id="drop-down-category" >
               
                @foreach($student_category as $category)
                 <option value="{{$category->id}}">{{ $category->category_name }}</option>
                @endforeach

            </select>
        </div>
</div>

<div class="col-lg-5">
        <label for="">Season</label>
        <div class="form-group">
            <select class="form-control" name="drop-down-season" type="text" id="drop-down-season" >
                <option value="" selected>--- Select Season ---</option>
                <option value="Season 1" >Season 1</option>
                <option value="Season 2" >Season 2</option>
            </select>
        </div>
    </div>
    
    <div class="col-lg-5">
        <label for="">Year</label>
        <div class="form-group">
            <select class="form-control" name="drop-down-year" type="text" id="drop-down-year" >
                <option value="0" selected>--- Year ---</option>    
                <option value="2019" >2019</option>
                <option value="2020" >2020</option>
                <option value="2021" >2021</option>
                <option value="2022" >2022</option>
                <option value="2023" >2023</option>
                <option value="2024" >2024</option>
                <option value="2025" >2025</option>            
            </select>
        </div>
    </div>
</form>






<!-- /.widget-bg -->
</div>
<!-- /.widget-holder -->
</div>
<!-- /.row -->
</div>
<!-- /.widget-list -->
</div>
<!-- /.container-fluid -->
</main>

@stop
@section('js')
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
$('table.display').DataTable();
});

$(document).ready(function() {
$('.record').addClass('active');
$('.record').addClass('collapse in');
});




$('#drop-down-category').on('change', function(e){

$.get('/data/fetch-tuition',function(data){

        $('#drop-down-tuition').append('<option value="">-- Select Tuition Fee --</option>')
    $.each(data, function(index, tuition){
        $('#drop-down-tuition').append('<option value="'+tuition.id+'">'+tuition.tuition_name+'</option>')
    });
});
});






</script>
@stop