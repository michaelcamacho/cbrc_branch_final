<?php

namespace App\Http\Controllers\HRIS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use App\Model\DataModel\Employee;
use App\Model\AdminSettings;
use Auth;
use Alert;
use DB;
use App\Branch;
use App\User;
use App\Role;

class UserController extends Controller
{
  
    private $year;
    private $Branches;
   
    public function __construct()
    {
            $this->middleware('auth');
            $settings = AdminSettings::findOrFail(1);
            $this->year = $settings->year;
            $this->settings = $settings;
            $main_id = Branch::select('main_branchid')->get();
          $this->Branches = Branch::whereNotIn('id', $main_id)->get();
    }
    
	

public function user_list(){
	$role = Role::all();
	$user = '';
	
	if($this->main_branch_id() != ''){
	
		$user = DB::connection('satelite_branch_db_connection')->table('users AS users')
		->leftJoin('roles AS roles', 'roles.name','=','users.role')
		->select('users.*','roles.display_name','roles.name AS role_name','roles.id AS role_id','users.username','users.email','users.password')
		->where('users.show','=','1')
		->get();
	}
	elseif($this->main_branch_id() == ''){
		
		$user = DB::connection('satelite_branch_db_connection')->table('users AS users')
		->leftJoin('roles AS roles', 'roles.name','=','users.role')
		->select('users.*','roles.display_name','roles.name AS role_name','roles.id AS role_id','users.username','users.email','users.password')
		->where('users.show','=','1')
		->where('users.branch_id','=', 	Auth::user()->branch_id)
		->get();
	}
	
	return view ('admin.users_list')
	->with('main_branch',$this->main_branch_id())
	->with('role',$role)
	->with('Branches',$this->list_of_branches())
	->with('user',$user)
	;


}

public function add_user(Request $request){

$input = $request->except(['_token']);


$branch_info =  Branch::findOrFail($input['branch']);

$show = 1;
$validatedData = $request->validate([
	'username' => 'unique:satelite_branch_db_connection.users,username',
	'email' => 'unique:satelite_branch_db_connection.users,email',
]);

		User::insert([
			'role' => $input['role'],
			'username' =>$input['username'],
			'email' => $input['email'],
			'password' => Hash::make($input['password']),
			'name' => $input['name'],
			'branch' => $branch_info->branch_name,
			'branch_id'	=> $branch_info->id,
			'show'		=> $show,
			'remember_token' => str_random(60),
		]);

		$user_id = User::max('id');
		$role_id = Role::where('name','=',$input['role'])->first();

		DB::connection('satelite_branch_db_connection')->table('role_user')->insert([
		'user_id' => $user_id,
		'role_id' => $role_id->id,
		]); 

		Alert::success('Success!', 'User Successfully added.');
		return redirect('/hris/users_list');
}



public function update_user(Request $request){
	$input = $request->except(['_token']);
	$branch_info =  Branch::findOrFail($input['branch']);
		
	$id = $input['id'];
	$branch = $input['branch'];
	$role = $input['role'];
	$role_id = Role::where('name' , '=' , $input['role'])->value('id');


		if($input['password'] == null){
		User::where('id','=',$id)->update([
			'role' => $input['role'],
			'username' =>$input['username'],
			'email' => $input['email'],
			'name' => $input['name'],
			'branch' => $branch_info->branch_name,
			'branch_id' => $branch_info->id
		]);
		}
		else{
		User::where('id','=',$id)->update([
			'role' => 'Admin',
			'username' =>$input['username'],
			'password' => Hash::make($input['password']),
			'email' => $input['email'],
			'name' => $input['name'],
			'branch'=> $branch_info->branch_name,
			'branch_id' => $branch_info->id
		]);
		}



		DB::connection('satelite_branch_db_connection')->table('role_user')->where('user_id','=',$id)->update([

            'user_id' => $id,
            'role_id'    => $role_id,
            ]); 

			Alert::success('Success!', 'User Successfully updated.');
			return redirect('/hris/users_list');
	
}


}
