@extends('main')
@section('title')
 Exam Monitoring
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link href="https://fonts.googleapis.com/css?family=Gochi+Hand|Space+Mono&display=swap" rel="stylesheet">
<style type="text/css">
  .tableRecords th{
    font-weight: normal; 
    font-family: 'Space Mono', monospace;
    font-weight: bold;
  }
</style>
@stop
@section('main-content')
<main class="main-wrapper clearfix">
    <div class="page-content-wrapper">
      <div class="container" style="margin-top: 10px;">
          <div class="row">
              <div class="col-12">
                <div class="card" >
                  <div class="card-header" style="background-color: #3CAEA3;color:#fff;">
                   EXAM MONITORING REPORTS
                  </div>
                <div class="card-body">
                  <form class="form-control" action="/exam-result" method="POST">
                    @csrf
                      <input type="hidden" value="{{$season}}" name="season" id="season"/>   
                      <input type="hidden" value="{{$year}}" name="year" id="year" />
                      <input type="hidden" value="{{$program}}" name="program" id="program"/>   
                      <input type="hidden" value="{{$major}}" name="major" id="major" />  
                      <input type="hidden" value="{{$branch}}" name="branch" id="branch" /> 
                    <div class="form-group">
                    <small style="color:red;font-weight: bold;font-size: 14px;">*Select exam category!!</small>
                      <select id="exam" name ="examcat" class="form-control" required>
                        <option id="option" value="" selected="true" disabled = "true"> --- Select Exam Category ----</option>

                      </select>                
                    </div>
                  <div class="table-responsive">
                      <table class="table table-striped tableRecords" id="myTable">
                        <thead>
                          <tr>
                            <th class="text-center">CBRC ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">School</th> 
                            <th class="text-center"># of Take</th>
                            <th class="text-center">RAW SCORE</th>
                            <th class="text-center">Percent</th>
                            <th class="text-center">Quartile</th>
                          </tr> 
                        </thead>
                        <tbody id="stdlist">
                    
                        </tbody>
                    </table>                  
                  </div>
                </form>  
                </div>
              </div>
              </div>
           </div>
        </div>
     </div>
</main>
        

@stop
@section('js')
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
$(window).on('load',function(){
 var auth = { "email":"admin@main.cbrc.solutions","password":"main@dmin"}
   $.ajax({
   url: 'https://cbrc.solutions/api/auth/login',
    method: 'POST',
    data: auth
  }).done(function(response){
     var apiToString = JSON.stringify(response);
     var apiTokenParse = JSON.parse(apiToString);
     var apiToken = apiTokenParse.access_token;
     console.log(apiToken);  



      $.get('https://cbrc.solutions/api/main/exam-category?token='+apiToken,function(data){
      $.each(data, function(index, examcat){
        $('#exam').append(' <option id="option" value="'+examcat.id+'">'+examcat.ExamCategory+'</option>')
      });
     });


       $('#exam').on('change', function() {
        var examcat = $( "#exam option:selected" ).val();
        var season = $('#season').val();
        var year = $('#year').val();
        var programs = $('#program').val();
        var major = $('#major').val();
        var branch = $('#branch').val();   

          var formData = {
              "Season":season,      
              "Year":year ,        
              "Program":programs,      
              "Major": major,          
              "branch": branch,
              "examcat": examcat, 
            };     
 

        $.ajax({
        url: "https://cbrc.solutions/api/main/exam-result?token="+apiToken,
        method: 'POST',
        data: formData,
            }).done(function(response){
                
                $.each(response, function(index, examMonitoring) {
                  $('#stdlist').empty();
                  $('#stdlist').append('<tr><td class="text-center">'+examMonitoring.id+'</td><td class="text-center">'+examMonitoring.Firstname+examMonitoring.Lastname+examMonitoring.Middlename+'</td><td class="text-center">'+examMonitoring.School+
                    '</td><td class="text-center">'+examMonitoring.Take+'</td><td class="text-center">'+examMonitoring.RawScore+
                    '</td><td class="text-center">'+examMonitoring.Percentage+'</td><td class="text-center">'+examMonitoring.Quartile)
              
                });         
            });
        });

   });
});

</script>
@stop