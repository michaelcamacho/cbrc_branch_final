@extends('main')
@section('title')
Student Summary
@stop
@section('css')
<style type="text/css">
.form-control:disabled, .form-control[readonly] {
color: #000;
}
@media print {
.content-header, .left-side, .main-header, .main-sidebar, .no-print {
display: none!important;
}
.table {
border-collapse: collapse !important;
}
td{
color: #000;
}

}


fieldset.info_separator {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    width:100%;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.info-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto;
    padding:0 10px;
    border-bottom:none;
}


</style>

<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
<!-- Page Title Area -->
<div class="container-fluid">
<div class="row page-title clearfix">    
    <div class="page-title-left">
    <h6 class="page-title-heading mr-0 mr-r-5">Student Summary</h6>
    </div>

</div>
<!-- /.page-title -->
</div>
<!-- /.container-fluid -->
<!-- =================================== -->
<!-- Different data widgets ============ -->
<!-- =================================== -->
<div class="container-fluid">
<div class="widget-list">
<div class="row">
<div class="col-md-12 widget-holder">
<div class="widget-bg p-5">

            {{-- BACK BUTTON --}}
            <a class="btn btn-info" href="/master_list/{{strtolower(Auth::user()->branch)}}"> Back to Master List</a>  
            {{-- BACK BUTTON --}}
                 
            <hr>     
            {{-- Student Information --}}         
                        
                        <div class="col-lg-12" >
                               
                        @foreach( $student as $data) 
                        
                        <fieldset class="info_separator">
                            <legend class="info-border">
                                Student Information
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#" class="pull-right"><span class="material-icons" >edit</span></a> 
                            </legend>
                                
                            <br>           
                            <span class="slide pull-right" >
                                    <a href="#" class="slide" >show more</a>
                            </span>    
                        
                            <b>CBRC ID:</b><span >{{ $data->cbrc_id }}</span><br>
                            <b>Name:</b> <span >{{ $data->first_name . ' ' . $data->middle_name . ' ' . $data->last_name}}</span><br>
                            <b>Program:</b><span >{{ $data->program_name }}</span><br>
                            <b>Major:</b><span >{{ $data->major }}</span><br>
                            
                         
                            <div id="one" class="content">
                            
                                <b>Birthdate:</b><span >{{ $data->birthdate }}</span><br>
                                <b>Address:</b><span >{{ $data->address }}</span><br>
                                <b>Email:</b><span >{{ $data->email }}</span><br>
                                <b>Contact No:</b><span >{{ $data->contact_no }}</span><br>
                                <b>NOA No:</b><span >{{ $data->noa_no }}</span><br>
                                
                                <fieldset class="info_separator">
                                    <legend class="info-border">in case of emergency</legend>
                                    <b>Contact Person:</b><span >{{ $data->contact_person }}</span><br>
                                    <b>Contact No:</b><span >{{ $data->contact_details }}</span><br>
                            
                            </div>
                            
                             
                            
                                </fieldset>
                            </fieldset> 
                            


                        <fieldset class="info_separator">
                            <legend class="info-border">Remaining Balance</legend>
                           
                            <b >Remaining Balance: </b><span>{{ isset($balance) ? $balance : '0.00'}}.00</span><br>
                        </fieldset>
                            
                                
                        @endforeach        
                        </div>
        
        
            {{-- /Student Information --}}

                    <div class="row justify-content-between pull-right" >
                     
                        <div class="row " style="margin-right:100px;">
                            
                            
                            {{-- @if(isset($latest_card))

                            @if($latest_card->status == 1 OR $latest_card->status == 3)
                             <a class="btn btn-info"href="/reserve_student/{{$student_id}}">Reserve</a> 
                            @endif

                            @if($latest_card->status == 2)
                             
                            @endif
                            
                            @else 
                            <a class="btn btn-info"href="/reserve_student/{{$student_id}}">Reserve</a> 
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="btn btn-info"href="/enroll_student/{{$student_id}}">Enroll</a> 
                            @endif 
                            --}}

                        </div>
                    </div>
                    
                    <br>             
                    <br>  
                    
                    <fieldset class="info_separator">
                            <legend class="info-border">
                                History of Enrollment
                            </legend>           
                    <div class="row">      
                      <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                          <thead>
                            <tr>
                                <th>Card No.</th>
                                <th>Season</th>
                                <th>Year</th>
                                <th>Balance</th>
                                <th>Status</th>
                                <th>option</th>
                            </tr>
                          </thead>
                          <tbody>
                       

                            @foreach( $enrollmentCard as $card)
                            <tr>
                                <td>{{$card->id}}</td>
                                <td>{{$card->season}}</td>
                                <td>{{$card->year}}</td>
                                <td>{{$card->balance}}</td>
                                <td>{{$card->status_name}}</td>
                                <td>    
                                        <a href="#" type="button" id="{{$card->id}}" class="btn btn-info card_info" data-toggle="modal" data-target="#exampleModalCenter">
                                                View Card
                                         </a>    
                                </td>
                            </tr>
                        @endforeach
                          </tbody>
                      </table>
                    </div>
                    <br>
                    <br>
                    <br>
                </fieldset>
        


                    
                    
            <!-- Enrollment Card Modal -->
                  <!-- Modal -->
                  <div class="modal fade"style="width: 100%" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"  aria-hidden="true" >
                    <div class="modal-dialog modal-lg modal-dialog-centered"    role="document">
                      <div class="modal-content" >
                        <div class="modal-header ">
                                    <a type="button" class="btn btn-danger" style="color:white;float:right" data-dismiss="modal">close</a>
                        </div>
                        <div class="modal-body">
                        <fieldset class="info_separator">
                            <legend class="info-border">Card Information</legend>
                         
                                <h6 class="pull-right">Status: <span id="card_status"></span></h6>
                                
                                
                                <b>Date Registered:<span id="card_date_registered"></span></b>
                                <br>
                                <b>Name:<span id="card_name"></span></b>
                                <br>
                                <b>Category:<span id="card_category"></span></b>
                                <br>
                                <b>Program:<span id="card_program"></span></b>
                                <br>
                                <b>Major:<span id="card_major"></span></b>
                                <br>
                                <b>Remaining Balance: <span id="card_balance"></span></b>
                            
                                {{-- <div class="row  pull-right justify-content-between" style="padding:0px 50px 0px 40px; ">
                                        <div id="add_payment_button"></div>
                                </div> --}}
    
                           
                        </fieldset>
                        <fieldset class="info_separator">
                            <legend class="info-border">Payment History</legend>
                        
                                <hr>
                                <table class="payment_history_table" style="width: 100% !important">
                                    <thead>
                                        <tr>
                                            <th>Payment no.</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Balance Before</th>
                                            <th>Balance After</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                                
                        </fieldset>                 
                                <div class="modal-footer">
                                </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
        <!-- Enrollment Card Modal -->
            


        
        <!-- Add Payment Modal -->
        <div class="modal fade  " id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="addPaymentModal" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                  <div class="modal-content">
                        <div class="modal-header">
                        
                                <h5 class="modal-title" id="addPaymentModal">Add Payment</h5>
                         
                        <a type="button" class="btn btn-danger" style="color:white" data-dismiss="modal">close</a>
                        </div>
                    <div class="modal-body">


                    <form action="/proceed/proceed_add_payment" method="POST">
                            <div style="font-family:digital-clock-font; font-size:20px;">
                                    <div class="row col-md-12">
                                            @csrf
                                    <input type="hidden" id="add_payment_card_id" name="add_payment_card_id"value="0">
                                    <input type="hidden" id="add_payment_student_id"name="add_payment_student_id" value="0">
                                            <div class="form-group col-md-6">
                                                <b>Current Balance: <br> <input id="add_payment_card_balance" name="add_payment_card_balance" style="border:0;"  readonly/></b>
                                            </div>  
                                            <div class="form-group col-md-6">
                                                <b>Enter Amount:<br><input type="number" name="amount_paid"/></b>
                                            </div>
                                            
                                            <br>
                                    </div>  
                                        {{-- <div class="row  pull-right" style="padding-right:15px">
                                                <input type="submit" class="btn btn-info" name="submit" value="Add Payment"/>
                                        </div> --}}
                            </div> 
                    </form>
                    
                    
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              </div>
<!-- Add Payment Modal -->




        
        
        <!-- payment information Modal -->
                  <div class="modal fade  " id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModal" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                          <div class="modal-content">
                                <div class="modal-header">
                                
                                        <h5 class="modal-title" id="paymentModal">Payment Information</h5>
                                 
                                <a type="button" class="btn btn-danger" style="color:white" data-dismiss="modal">close</a>
                                </div>
                            <div class="modal-body">
                            
                                    <div>
                                            <h6>Transaction no: 000<span id="payment_id"></span></h6>
                                            <b>Transaction Date:<span id="payment_transaction_date"></span></b>
                                            <br>
                                            <b>Amount Paid:<span id="payment_amount_paid"></span></b>
                                            <br>
                                            <b>Balance Before:<span id="payment_balance_before"></span></b>
                                            <br>
                                            <b>Balance After:<span id="payment_balance_after"></span></b>
                                            <br>
                                            <b>Transact By: <span id="payment_created_by"></span></b>
                                            <br>
                                            <b>Remarks <span id="payment_remarks"></span></b>
                                    
                                        </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                          </div>
                        </div>
                      </div>
        <!-- payment information Modal -->








</div>
<!-- /.widget-bg -->
</div>
<!-- /.widget-holder -->
</div>
<!-- /.row -->
</div>
<!-- /.widget-list -->
</div>
<!-- /.container-fluid -->
</main>

@stop
@section('js')
<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/student-summary.js') }}"></script>

<script type="text/javascript">

$(document).ready(function() {
$('table.display').DataTable();
});

$(document).ready(function() {
$('.record').addClass('active');
$('.record').addClass('collapse in');
});

//edit button




</script>
@stop