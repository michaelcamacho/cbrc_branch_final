@extends('main')
@section('title')
 Discounts
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('datatable/datatables.min.css') }}">

@stop
@section('main-content')
<main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="container-fluid">
                <div class="row page-title clearfix">
                    <div class="page-title-left">
                        <h6 class="page-title-heading mr-0 mr-r-5">Administrator</h6>
                        
                    </div>
                    <!-- /.page-title-left -->
                    <div class="page-title-right d-none d-sm-inline-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item">Records</li>
                            <li class="breadcrumb-item active">Discount Records</li>
                        </ol>
                    </div>
                    <!-- /.page-title-right -->
                </div>
                <!-- /.page-title -->
            </div>
            <!-- /.container-fluid -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container-fluid">
                <div class="widget-list">
                    <div class="row">
                        <div class="col-md-12 widget-holder">
                            <div class="widget-bg">
                                <div class="widget-body clearfix">
                                    <h5 class="box-title mr-b-0">Discount Records</h5>
                                   <br/>
                                   
                                </div>
                                <div class="widget-body clearfix">
                                <div id="discount-table">
                                    <table id="discount" class="table table-bordered table-hover display nowrap margin-top-10 w-p100" style="width: 100% !important">
                                        <div class="add">
                                        <a href="#" class="btn btn-primary ripple mr-3" data-toggle="modal" data-target="#add-discount">Add New Discount</a>
                                        </div>
                     <thead>
                       
                        <tr>
                            <th>Branch</th>
                            <th>Program</th>
                            <th>Category</th>
                            <th>Discount Category</th>
                            <th>Amount</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                         @if(isset($a_discount))
                            @foreach($a_discount as $a_discounts)
                        <tr id="row_{{$a_discounts->id}}">
                            <td id="a_branch_{{$a_discounts->id}}">{{$a_discounts->branch}}</td>
                            <td id="a_program_{{$a_discounts->id}}">{{$a_discounts->program}}</td>
                            <td id="a_category_{{$a_discounts->id}}">{{$a_discounts->category}}</td>
                            <td id="a_discount_category_{{$a_discounts->id}}">{{$a_discounts->discount_category}}</td>
                            <td id="a_amount_{{$a_discounts->id}}">{{$a_discounts->discount_amount}}</td>
                            <td><a href="#" class="btn btn-success a_edit" id="a_edit_{{$a_discounts->id}}" data-toggle="modal" data-target="#update-discount">Update</a></td>
                            <td><button class="btn btn-danger delete" user-id="{{$a_discounts->id}},{{$a_discounts->branch}}">Delete</button></td>
                        </tr>
                       @endforeach
                            @endif

                    </tbody>           
                    
                </table>
            </div>
        <div id="add-discount" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="discount-form">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <select class="form-control" name="branch" id="branch" required>
                                        <option value="0" disable="true" selected="true">--- Select Branch ---</option>
                                        @if(isset($branch))
                                        @foreach($branch as $branches)
                                        <option value="{{$branches->branch_name}}">{{$branches->branch_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="program">
                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" name="category" id="category">
                                        <option value="0" disable="true" selected="true">--- Select Category ---</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Scholar">Scholar</option>
                                        <option value="In House">In House</option>
                                        <option value="Final Coaching">Final Coaching</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Discount Category</label>
                                    <input class="form-control" type="text" name="discount_category" id="discount_category" required>
                                </div>
                                <div class="form-group">
                                    <label for="discount_amount">Amount</label>
                                    <input class="form-control" type="number" name="discount_amount" id="discount_amount" required>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple save" data-dismiss="modal">Save</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

            <div id="update-discount" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                    <div class="modal-content">
                        <a href="#" class="close" data-dismiss="modal" aria-hidden="true">×</a>
                        <div class="modal-body">
                            <div class="text-center my-3"><a href="#"><span><img src="assets/demo/logo-expand-dark.html" alt=""></span></a>
                            </div>
                            <form id="discount-form-update">
                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="branch" id="update_id" readonly>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <input type="text" class="form-control" name="branch" id="update_branch" readonly>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="program">Program</label>
                                    <select class="form-control" name="program" id="update_program">
                                        <option value="0" disable="true" selected="true">--- Select Program ---</option>
                                        @if(isset($program))
                                        @foreach($program as $programs)
                                        <option value="{{$programs->program_name}}">{{$programs->program_name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <select class="form-control" name="category" id="update_category">
                                        <option value="0" disable="true" selected="true">--- Select Category ---</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Scholar">Scholar</option>
                                        <option value="In House">In House</option>
                                        <option value="Final Coaching">Final Coaching</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Discount Category</label>
                                    <input class="form-control" type="text" name="discount_category" id="update_discount_category" required>
                                </div>
                                <div class="form-group">
                                    <label for="discount_amount">Amount</label>
                                    <input class="form-control" type="number" name="discount_amount" id="update_discount_amount" required>
                                </div>
                                                        
                                <div class="text-center mr-b-30">
                                    <a href="#" class="btn btn-rounded btn-success ripple update" data-dismiss="modal">Update</a>
                                </div>
                            </form>
                        </div>
                    </div>
                                            <!-- /.modal-content -->
                </div>
                                        <!-- /.modal-dialog -->
            </div>

                                </div>
                                
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container-fluid -->
        </main>
        

@stop
@section('js')

<script src="{{ asset('/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('/js/data-table.js') }}"></script>
<script src="{{ asset('/js/discount.js') }}"></script>
<script type="text/javascript">

    $('.save').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('branch', $('#branch').val());
                formData.append('program', $('#program').val());
                formData.append('category', $('#category').val());
                formData.append('discount_category', $('#discount_category').val());
                formData.append('discount_amount', $('#discount_amount').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/insert-discount') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-discount form')[0].reset();
                        location.reload();

                    }
                               
              });
            });

    $('.update').on('click',function(){
              
            
               var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', $('#update_id').val());
                formData.append('branch', $('#update_branch').val());
                formData.append('program', $('#update_program').val());
                formData.append('category', $('#update_category').val());
                formData.append('discount_category', $('#update_discount_category').val());
                formData.append('discount_amount', $('#update_discount_amount').val());

               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                  url: "{{ url('/update-discount') }}",
                  type: 'POST',
                  data: formData,
                  processData:false,
                  contentType: false,
                  success:function(data){
                        swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '1500'
                            })
                        $('#add-discount form')[0].reset();
                        location.reload();

                    }
                               
              });
            });
    $('#discount').on('click','.delete',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-discount') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });

    $('.delete').on('click',function(){
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var id = $(this).attr("user-id");
                var res = id.split(",");
          swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this.",
              type: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Yes, delete it!'
          }).then(function () {
                var formData = new FormData()
                formData.append('_token', '{{ csrf_token() }}');
                formData.append('id', res[0]);
                formData.append('branch', res[1]);
               $.ajax({
               type: 'POST',
               url: "{{ url('/delete-discount') }}",
               headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
               data: formData,
               processData:false,
                contentType: false,
                  success : function(data) {
                      swal({
                          title: 'Success!',
                          text: data.message,
                          type: 'success',
                          timer: '3000'
                      })
                      location.reload();
                  },
                  error : function () {
                      swal({
                          title: 'Oops...',
                          text: data.message,
                          type: 'error',
                          timer: '3000'
                      })
                  }
              });
          });
          });
</script>
@stop